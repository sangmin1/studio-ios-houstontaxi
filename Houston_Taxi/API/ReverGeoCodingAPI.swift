//
//  ReverGeoCodingURL.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/29.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

/**
 rever geo coding api
 */
class ReverGeoCodingAPI {
    
    func request_To_server(posx : Double, posy : Double, compeletion : @escaping (Bool,JSON) -> ()){

        let param : Parameters =  [:]



        let uRL = APIBase.getInstance().getinaviurl() + "addresses?posX=" + String(posx) + "&posY=" + String(posy)
        guard let url = URL(string: uRL) else {return}
        let headers = ["Content-Type":"application/x-www-form-urlencoded", "Accept":"application/json"]
        let header : HTTPHeaders = HTTPHeaders(headers)

        AF.request(url, method : .get,
                   parameters: param,
                   encoding: URLEncoding.default,
                   headers: header)
            .responseString { (data) in
                switch data.result {
                case .success(let value):
                    guard let dict = FunctionClass.shared.convertToDictionary(text: value) else {
                        compeletion(false, JSON(""))
                        return
                    }
                    compeletion(true, JSON(dict))
                case .failure(let error):
                    print("error",error.localizedDescription)
                }
        }
    }

}




//.responseJSON { (response) in
//    print("response result", response.result)
//    switch response.result {
//        case .failure(let error) :
//            print("reversegeocoding error :", error)
//            compeletion(false,JSON(""))
//        case .success(let value):
//
//            let data = JSON(value)
//            compeletion(true,data)
//        }
//}

//case .failure(let error):
//    print(error)
//    compeletion(false, JSON(""))
//}
