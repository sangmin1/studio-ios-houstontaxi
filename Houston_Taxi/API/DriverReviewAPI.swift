//
//  DriverReviewAPI.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON



class DriverReviewAPI {
    
    func requestToserver(currentDT : String , authcode : String, mobile : String,callId : String , callDT : String,data : String, completion : @escaping (Bool,_ data : JSON) -> ()){
        
        let param : Parameters =  ["currentDT":currentDT,
                                   "authCode":authcode,
                                   "mobile":mobile,
                                   "callId" : callId,
                                   "callDT" : callDT,
                                   "data" : data]
        
        
        
        
        let uRL = APIBase.getInstance().getBaseurl() + "/driver/review"
      
        //let delegate : Alamofire.SessionDelegate = APIBase.getInstance().session.delegate
        
        
        
        guard let url = URL(string: uRL) else {return}
        let headers = ["Content-Type":"application/json", "Accept":"application/x-www-form-urlencoded"]
        let header : HTTPHeaders = HTTPHeaders(headers)

        APIBase.getInstance().session.request(url, method : .post,
                            parameters: param,
                            encoding: JSONEncoding.default,
                            headers: header)
                            .validate(statusCode: 200..<299)
                            .responseJSON { (response) in
                                switch response.result {
                                    case .failure(let error) :
                                        print("driver review api error :" + error.localizedDescription)
                                        completion(false,JSON(""))
                                    case .success(let value):
                                        let data = JSON(value)
                                        completion(true,data)
                                    }
                            }
    }
    
    
}

