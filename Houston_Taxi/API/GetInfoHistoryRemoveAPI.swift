//
//  GetInfoHistoryRemoveAPI.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/17.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON



class GetInfoHistoryRemoveAPI {
    
    func requestRemove(currentDT : String , authcode : String, mobile : String, data : [Int], completion : @escaping (Bool,_ data : JSON) -> ()){
        
        let param : Parameters =  ["currentDT":currentDT,
                                   "authCode":authcode,
                                   "mobile":mobile,
                                   "data":self.IntarrToString(input: data)]
        
        
        
        
        let uRL = APIBase.getInstance().getBaseurl() + "/getinfo/history/remove"
      
        //let delegate : Alamofire.SessionDelegate = APIBase.getInstance().session.delegate
        
        
        
        guard let url = URL(string: uRL) else {return}
        let headers = ["Content-Type":"application/json", "Accept":"application/x-www-form-urlencoded"]
        let header : HTTPHeaders = HTTPHeaders(headers)

        APIBase.getInstance().session.request(url, method : .post,
                            parameters: param,
                            encoding: JSONEncoding.default,
                            headers: header)
                            .validate(statusCode: 200..<299)
                            .responseJSON { (response) in
                                switch response.result {
                                    case .failure(let error) :
                                        print("get info history remove error :" + error.localizedDescription)
                                        completion(false,JSON(""))
                                    case .success(let value):
                                        let data = JSON(value)
                                        completion(true,data)
                                    }
                            }
    }
    
    
    func IntarrToString(input : [Int]) -> String{
        var str = ""
        
        for value in input {
            str += String(value) + ","
        }
        
        
        let result = str.dropLast()
        
    
        return FunctionClass.shared.encrypt(msg:  String(result))
    }
    
    
    func requestRemoveAll(currentDT : String , authcode : String, mobile : String, completion : @escaping (Bool,_ data : JSON) -> ()){
        
        let param : Parameters =  ["currentDT":currentDT,
                                   "authCode":authcode,
                                   "mobile":mobile]
        
        
        
        
        let uRL = APIBase.getInstance().getBaseurl() + "/getinfo/history/removeall"
      
        //let delegate : Alamofire.SessionDelegate = APIBase.getInstance().session.delegate
        
        
        
        guard let url = URL(string: uRL) else {return}
        let headers = ["Content-Type":"application/json", "Accept":"application/x-www-form-urlencoded"]
        let header : HTTPHeaders = HTTPHeaders(headers)

        APIBase.getInstance().session.request(url, method : .post,
                            parameters: param,
                            encoding: JSONEncoding.default,
                            headers: header)
                            .validate(statusCode: 200..<299)
                            .responseJSON { (response) in
                                switch response.result {
                                    case .failure(let error) :
                                        print("get info history removeall error :" + error.localizedDescription)
                                        completion(false,JSON(""))
                                    case .success(let value):
                                        let data = JSON(value)
                                        completion(true,data)
                                    }
                            }
    }
}
