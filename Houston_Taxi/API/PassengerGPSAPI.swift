//
//  PassengerGPSAPI.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/09/03.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON



class PassengerGPSAPI {
    
    func requestToserver(currentDT : String , authcode : String, mobile : String, posName: String, posLon : String,posLat : String, completion : @escaping (Bool,_ data : JSON) -> ()){
        
        let param : Parameters =  ["currentDT":currentDT,
                                   "authCode":authcode,
                                   "mobile":mobile,
                                   "posName" : posName,
                                   "posLon": posLon,
                                   "posLat": posLat]
        
        
        
        
        let uRL = APIBase.getInstance().getBaseurl() + "/set/passenger/location"
      
        //let delegate : Alamofire.SessionDelegate = APIBase.getInstance().session.delegate
        
        
        
        guard let url = URL(string: uRL) else {return}
        let headers = ["Content-Type":"application/json", "Accept":"application/x-www-form-urlencoded"]
        let header : HTTPHeaders = HTTPHeaders(headers)

        APIBase.getInstance().session.request(url, method : .post,
                            parameters: param,
                            encoding: JSONEncoding.default,
                            headers: header)
                            .validate(statusCode: 200..<299)
                            .responseJSON { (response) in
                                switch response.result {
                                    case .failure(let error) :
                                        print("/set/passenger/location error :" + error.localizedDescription)
                                        completion(false,JSON(""))
                                    case .success(let value):
                                        let data = JSON(value)
                                        completion(true,data)
                                    }
                            }
    }
    
    
}
