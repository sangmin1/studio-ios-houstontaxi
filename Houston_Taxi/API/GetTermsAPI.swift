//
//  GetTerms.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/25.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


class GetTerms {
    
    func requestToserver( completion : @escaping (Bool,_ data : String?) -> ()){
        
        let param : Parameters =  [:]
        
        
        
        
        let uRL = APIBase.getInstance().getBaseurl() + "/getinfo/terms"
      
        //let delegate : Alamofire.SessionDelegate = APIBase.getInstance().session.delegate
        
        
        
        guard let url = URL(string: uRL) else {return}
        let headers = ["Content-Type":"application/json", "Accept":"application/x-www-form-urlencoded"]
        let header : HTTPHeaders = HTTPHeaders(headers)

        APIBase.getInstance().session.request(url, method : .post,
                            parameters: param,
                            encoding: JSONEncoding.default,
                            headers: header)
                            .validate(statusCode: 200..<299)
            .responseString(completionHandler: { (response) in
                switch response.result {
                case .failure(let error) :
                    print("alloc request error :" + error.localizedDescription)
                    completion(false,nil)
                case .success(let data) :
                     completion(true,data)
                }
            })
            
    }
    
    
}
//.responseData { (response) in
//        switch response.result {
//        case .failure(let error) :
//            print("alloc request error :" + error.localizedDescription)
//            completion(false,nil)
//        case .success(let data) :
//             completion(true,data)
//        }
//}
