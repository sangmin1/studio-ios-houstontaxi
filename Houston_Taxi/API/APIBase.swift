//
//  APIBase.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/29.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import Alamofire

class APIBase{
    private static var apiBase : APIBase?
    private static var sharedAPIBase : APIBase = {
        if apiBase == nil
        {
            apiBase = APIBase()
        }
        return apiBase!
    }()
    class func getInstance() -> APIBase {
        return sharedAPIBase
    }
    //
    private var url = "https://58.180.28.220:8000"
    private var inaviurl = "https://api-maps.cloud.toast.com/maps/v3.0/appkeys/GEa2XAYXee1B6cmd/"
    func getBaseurl()->String{
        return url
    }
    
    func getinaviurl() -> String {
        return inaviurl
    }
    
    
    let session : Session = {
        let manager = ServerTrustManager(evaluators: ["58.180.28.220" : DisabledTrustEvaluator()])
        
        let configuration = URLSessionConfiguration.af.default
        
        return Session(configuration: configuration, serverTrustManager: manager)
    }()
    
    
}
