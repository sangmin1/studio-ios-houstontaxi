//
//  InaviFindRouteAPI.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/07.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON


/**
 아이나비 제공 경로 탐색 api
 */

class InaviFindRouteAPI {
    
    func request_To_server(startx : Double,starty : Double,endx : Double, endy : Double, compeletion : @escaping (Bool,JSON) -> ()){

        let param : Parameters =  [:]

        let uRL = APIBase.getInstance().getinaviurl() + "route-normal?option=real_traffic&coordType=wgs84&startX=" + String(startx) + "&startY=" + String(starty) + "&endX=" + String(endx) + "&endY=" + String(endy)
        
        
       
        guard let target = uRL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            print("encoding error ocurred", uRL)
            return
        }

        guard let url = URL(string: target) else {return}
        let headers = ["Content-Type":"application/x-www-form-urlencoded", "Accept":"application/json"]
        let header : HTTPHeaders = HTTPHeaders(headers)

        AF.request(url, method : .get,
                            parameters: param,
                            encoding: URLEncoding.default,
                            headers: header)
        .responseString { (data) in
        switch data.result {
        case .success(let value):
            guard let dict = FunctionClass.shared.convertToDictionary(text: value) else {
                compeletion(false, JSON(""))
                return
            }
             compeletion(true, JSON(dict))
        case .failure(let error):
            print("error",error.localizedDescription)
            }
        }
    }
    
    
    
    
    
    
}
