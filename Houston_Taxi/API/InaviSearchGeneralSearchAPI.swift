//
//  InaviSearchGeneralSearchAPI.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/30.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import Alamofire
import SwiftyJSON

/**
  목적지 도착지 주소 검색 api
 */
class InaviSearchGeneralSearchAPI {
    
    func request_To_server(keyword : String, compeletion : @escaping (Bool,JSON) -> ()){

        let param : Parameters =  [:]
        let uRL = APIBase.getInstance().getinaviurl() + "searches?query=" + keyword
        guard let target = uRL.addingPercentEncoding(withAllowedCharacters: .urlQueryAllowed) else {
            print("encoding error ocurred", uRL)
            return
        }

        guard let url = URL(string: target) else {return}
        let headers = ["Content-Type":"application/x-www-form-urlencoded", "Accept":"application/json"]
        let header : HTTPHeaders = HTTPHeaders(headers)

        AF.request(url, method : .get,
                            parameters: param,
                            encoding: URLEncoding.default,
                            headers: header)
        .responseString { (data) in
        switch data.result {
        case .success(let value):
            guard let dict = FunctionClass.shared.convertToDictionary(text: value) else {
                compeletion(false, JSON(""))
                return
            }
             compeletion(true, JSON(dict))
        case .failure(let error):
            print("error",error.localizedDescription)
            }
        }
    }

}
