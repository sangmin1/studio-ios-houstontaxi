//
//  AllocationCompletePresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/10.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
import SwiftyJSON


class AllocationCompletePresenter: NSObject {
    
    weak var delegate : AllocationCompleteContract?
    weak var viewcontroller : AllocationCompleteViewController?
    //출발 위치
    private var departname : String = ""
    private var startx : Double = 0
    private var starty : Double = 0
    
    //목적지 좌표
    private var destname : String?
    private var endx : Double?
    private var endy : Double?
    
    //택시 위치
    private var carposx : Double = 0
    private var carposy : Double = 0
    
    
    
    //타이머
    private var timer = Timer()
    //타이머 블럭 flag
    private var blocktimer : Bool = false
    
    
    //콜체크 타이머
    private var callchecktimer = Timer()
    //콜체크 블럭 flag
    private var blockcallchecktimer : Bool = false
    
    //콜체크 상태
    private var currentcallcheck : callcheck = .onboard
    
    
    init(delegate : AllocationCompleteContract , viewcontroller : AllocationCompleteViewController){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
        
    }
    
    
    
    /**
    배차에 관한 정보들을 셋하는 함수
    */
    func setInfos(allocdata : [String : Any]){
        self.departname = allocdata["departname"] as! String
        self.startx = allocdata["startx"] as! Double
        self.starty = allocdata["starty"] as! Double
        
        if let destname = allocdata["destname"] as? String, let endx = allocdata["endx"] as? Double, let endy = allocdata["endy"] as? Double {
            self.destname = destname
            self.endx = endx
            self.endy = endy
            self.callCarpos()
        }
         
        
    }
    /**
      배차 정보를 반환하는 함수
     */
    func getInfos() -> [String : Any] {
        var allocdata : [String : Any] = [:]
        allocdata["departname"] = self.departname
        allocdata["startx"] = self.startx
        allocdata["starty"] = self.starty
        if let destname = self.destname, let endx = self.endx , let endy = self.endy {
            allocdata["destname"] = destname
            allocdata["endx"] = endx
            allocdata["endy"] = endy
        }
        
        return allocdata
        
        
    }
    
    /**
      출발 좌표 반환 함수
     */
    func getstartcoord() -> (startx : Double, starty : Double){
        return (self.startx,self.starty)
    }
    
    /**
     기사님 성함 반환 함수
     */
    func getdrivername() -> String {
        if let name = CallRepo.shared.getmodel().carNum {
            return name
        }
        else {
            return "홍길동"
        }
    }
    
    /**
     차량 정보 반환 함수 ( 이름  번호)
     */
    func getcarInfo() -> (String){
        var model = ""
        var num = ""
        if let m = CallRepo.shared.getmodel().carModel {
            model = m
        }
        if let n = CallRepo.shared.getmodel().carNum {
            num = n
        }
        print("get car info alloc complete",model + " | " + num)
        return model + " | " + num
    }
    
    /**
     기사님 에게 전화하기 
     */
    func callDriver(){
        if let n = CallRepo.shared.getmodel().driverTelNum {
            if let url = URL(string: "tel://\(n)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        
    }
    
    
    

    /**
     배차 취소 api 호출 함수
     */
    func callalloccancelapi(completion : @escaping (Bool) -> ()){
        
        let api = AllocCancelAPI()
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        
        let callid = FunctionClass.shared.encrypt(msg: CallRepo.callID)
        let calldt = FunctionClass.shared.encrypt(msg: CallRepo.callDt)
        
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile, callID: callid, callDT: calldt) { [unowned self] (done, data) in
            if done {
                self.blockcallchecktimer = true
                self.blocktimer = true
                print("alloc completionview view call check data",data)
                let issuccess = data["isSuccessful"].boolValue
                if issuccess == true {
                    CallRepo.callDt = ""
                    CallRepo.callID = ""
                    UserDefaults.standard.set("", forKey: userstandard.calldt.rawValue)
                    UserDefaults.standard.set("", forKey: userstandard.callid.rawValue)
                    UserDefaults.standard.set(0, forKey: userstandard.destlon.rawValue)
                    UserDefaults.standard.set(0, forKey: userstandard.destlat.rawValue)
                    UserDefaults.standard.set(0, forKey: userstandard.startlon.rawValue)
                    UserDefaults.standard.set(0, forKey: userstandard.startlat.rawValue)
                }
                completion(issuccess)
                
            }
        }
    }
    
}
extension AllocationCompletePresenter {
    /**
     택시 위치 조회 api 호출 함수
     */
    func callCarpos(){
        let api = GetCarPosInfo()
        
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        
        let callid = FunctionClass.shared.encrypt(msg: CallRepo.callID)
        let calldt = FunctionClass.shared.encrypt(msg: CallRepo.callDt)
        
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile, callID: callid, callDT: calldt) { [unowned self] (done, data) in
            if done {
                 print("car posx")
                 let carlon = data["carLon"].stringValue
                    let lon = FunctionClass.shared.decrypt(msg: carlon)
                    self.carposx = Double(lon)!
                
                 let carlat = data["carLat"].stringValue
                    let lat = FunctionClass.shared.decrypt(msg: carlat)
                    self.carposy = Double(lat)!
                print("car posx : \(lon) = posy : \(lat)")
                self.delegate?.setmarker(posx: self.carposx, posy: self.carposy)
            }
        }
    }
    
    
    /**
     10초마다 차량의 위치를 받아오는 함수
     */
    func runtimer(){
        timer = Timer.scheduledTimer(timeInterval: 10, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer(){
        print("10sec update timer")
        if blocktimer == true {
            timer.invalidate()
            return
        }
        else {
            if currentcallcheck == .allocsucess {// 배차 완료시 계속 택시 위치를 잡는다.
                self.callCarpos()
            }
            else if currentcallcheck == .onboard {//탑승 경우 탑승 화면으로 넘어감
                self.delegate?.showOnBoardingViewController()
                blocktimer = true
                blockcallchecktimer = true
            }
            else if currentcallcheck == .allocfailed { //기사님이 배차 취소 한경우
                //제플린 620번 팝업 배차완료 실패
                self.delegate?.showdriverdeclinedpopup()
                blocktimer = true
                blockcallchecktimer = true
            }
            else if currentcallcheck == .cancel { //배차 완료 후 자의로 취소 한경우
                //제플린 610 배차 완료 취소
                blocktimer = true
                blockcallchecktimer = true
            }
            else if currentcallcheck == .drop {
                blocktimer = true
                blockcallchecktimer = true
            }
            else if currentcallcheck == .connecterror {
                
            }
            else if currentcallcheck == .onboardfailed {
                blocktimer = true
                blockcallchecktimer = true
                guard let del = UIApplication.shared.connectedScenes.first else {
                     return
                }
                guard let scene = del.delegate as? SceneDelegate else {
                    return
                }
                
                ToastView.shared.short(scene.window!, txt_msg: "탑승 실패 하셨습니다.")
                self.viewcontroller?.navigationController?.popToViewController(ofClass: MainViewController.self, animated: true)
            }
        }
        
    }
}
extension AllocationCompletePresenter {
    /**
    콜체크 api 함수
    */
    func callCallCheck(){
        let api = GetCallCheckAPI()
        
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        
        let callid = FunctionClass.shared.encrypt(msg: CallRepo.callID)
        let calldt = FunctionClass.shared.encrypt(msg: CallRepo.callDt)
        
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile, callID: callid, callDT: calldt) { [unowned self] (done, data) in
            if done {
                print("alloc completionview view call check data",data)
                let issuccess = data["isSuccessful"].boolValue
                
                print("isSuccess,",issuccess)
                guard let msg = data["msg"].string else {
                    print("alloc completion view controller msg failed")
                    return
                }
                print("alloc complet view msg",FunctionClass.shared.decrypt(msg: msg))
                guard let callStatus = data["callStatus"].string else {
                    print("alloc completion view controller callstatus failed")
                    return
                }
                let callstat = FunctionClass.shared.decrypt(msg: callStatus)
                print("alloc completion callcheck",callstat)
                for status in callcheck.allCases {
                    if status.rawValue == callstat {
                        self.currentcallcheck = status
                    }
                }
            }
        }
    }
    
    
    /**
     2초마다 콜체크 상태를 확인하는 함수
     */
    func runcallchecktimer(){
        callchecktimer = Timer.scheduledTimer(timeInterval: 2, target: self,   selector: (#selector(updatecallchecktimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updatecallchecktimer(){
        print("2sec call check update timer")
        if blockcallchecktimer == true {
            callchecktimer.invalidate()
            return
        }
        else {
            self.callCallCheck()
        }
    }
    
    
}



    
//    func callDriverInfo() {
//        let api = GetDriverInfo()
//
//        let currentDt = FunctionClass.shared.getcurrentDt()
//        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
//        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
//
//        let callid = FunctionClass.shared.encrypt(msg: CallRepo.callID)
//        let calldt = FunctionClass.shared.encrypt(msg: CallRepo.callDt)
//
//
//        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile, callID: callid, callDT: calldt) { (done, data) in
//            if done {
//                CallRepo.shared.setmodel(input: CallModel(json: data))
//                let msg = data["msg"].stringValue
//                print("driver info msg",FunctionClass.shared.decrypt(msg: msg))
//                let issuccess = data["isSuccessful"].boolValue
//                print("success",issuccess)
//            }
//        }
//
//    }
