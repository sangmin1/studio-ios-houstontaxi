//
//  AllocationCompletionCallTaxiBtn.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/10.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
  배차완료 화면 전화 버튼 컴포넌트
 */
class AllocationCompletionCallTaxiBtn: UIButton {
    
    private var image = UIImageView()
    
    private var label = PaddingLabel()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = maincolor
        makeimage()
        makelabel()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.bounds.contains(point) {
            return self
        }
        else {
            return nil
        }
    }
    
    
}
extension AllocationCompletionCallTaxiBtn {
    private func makeimage(){
        self.addSubview(image)
        image.translatesAutoresizingMaskIntoConstraints = false
        image.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        image.trailingAnchor.constraint(equalTo: self.centerXAnchor, constant: -4).isActive = true
        image.widthAnchor.constraint(equalToConstant: 20).isActive = true
        image.heightAnchor.constraint(equalToConstant: 20).isActive = true
        image.image = UIImage(named: "mapBtnIcCallN")
    }
    private func makelabel(){
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        label.leadingAnchor.constraint(equalTo: self.centerXAnchor, constant: 4).isActive = true
        label.widthAnchor.constraint(equalToConstant: 40).isActive = true
        label.heightAnchor.constraint(equalToConstant: 26).isActive = true
        label.font = UIFont(name: fonts.bold.rawValue, size: 20)
        label.textColor = .white
        label.text = "전화"
    }
    
}
