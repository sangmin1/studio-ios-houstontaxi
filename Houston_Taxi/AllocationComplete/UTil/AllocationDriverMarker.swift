//
//  AllocationDriverMarker.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/10.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class AllocationDriverMarker: UIView {
    
    private var image = UIImageView()
    
    private var label = PaddingLabel()
    
    
    init(frame : CGRect, name : String){
        super.init(frame :frame)
        makeimage()
        makelabel()
        label.text = name // + "기사님"
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
}
extension AllocationDriverMarker {
    private func makeimage(){
        self.addSubview(image)
        image.translatesAutoresizingMaskIntoConstraints = false
        image.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        image.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        image.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        image.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        image.imagewithinset(leftcap: 0.5, image: UIImage(named: "mapPinDriver")!)
    }
    private func makelabel(){
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.topAnchor.constraint(equalTo: self.topAnchor, constant: 6).isActive = true
        label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 34).isActive = true
        label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -10).isActive = true
        label.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -13).isActive = true
        label.font = UIFont(name: fonts.bold.rawValue, size: 17)
        label.textColor = .rgb(red: 26, green: 134, blue: 164, alpha: 1)
        label.textAlignment = .center
        label.adjustsFontSizeToFitWidth = true
    }
    
}
