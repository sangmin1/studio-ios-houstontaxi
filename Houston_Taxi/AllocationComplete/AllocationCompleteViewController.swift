//
//  AllocationCompleteViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/10.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
import iNaviMaps


/**
 제플린 600 배차 완료 화면
 */
class AllocationCompleteViewController : UIViewController {
    
    lazy var mapView = InaviMapView(frame: .zero)
    
    
    private var whitebox = ShadowView()
    //택시가 배차 되었습니다
    private var infotitle = PaddingLabel()
    // 차량 모델 및 번호
    private var infolabel = PaddingLabel()
    //전화 버튼
    private var callTaxiBtn = AllocationCompletionCallTaxiBtn()
    //호출 취소
    private var cancelBtn = ShadowBtn()
    //현재 위치로 버튼
    private var currentlocationBtn = UIButton()
    
    
    lazy private var driverdeclinepopup = DriverDeclinedPopUP(frame : self.view.frame, delegate : self)
    
    lazy private var userdeclinepopup = UserDeclinePopUp(frame: self.view.frame, delegate: self)
    
    lazy private var drivermarkerview = AllocationDriverMarker(frame: CGRect(x: 0, y: 0, width: 128, height: 38), name: self.presenter.getdrivername())
    
    private var drivermarker = INVMarker()
    
    private var departmarker = INVMarker()
    
    
    //mapview camera 객체 초기 화면에서 경로 전체를 한 화면에 보이기위하여 필요함
    private var camera = INVCameraUpdate()
    
    
    
    
    lazy private var btnstack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [cancelBtn,callTaxiBtn])
        stack.spacing = 16.5
        stack.distribution = .fillEqually
        return stack
    }()
    
    
    lazy private var presenter = AllocationCompletePresenter(delegate : self , viewcontroller : self)
    /**
     전화면에서 출발지 좌표 및 도착지(없는 경우도 있음)좌표 정보를 받아온다
     */
    convenience init(allocdata : [String : Any]){
        self.init()
        presenter.setInfos(allocdata : allocdata)
        
        
        
        
        departmarker.iconImage = INVImage(image: UIImage(named: "mapPinDeparture")!)
        departmarker.position = INVLatLng(lat: presenter.getstartcoord().starty, lng: presenter.getstartcoord().startx)
        departmarker.mapView = self.mapView
         
        
        
        infolabel.text = presenter.getcarInfo()
       
           
        
//        self.mapView.moveCamera(camera)
//
//        mapView.moveCamera(INVCameraUpdate(targetTo: INVLatLng(lat: presenter.getstartcoord().starty, lng: presenter.getstartcoord().startx)))
//
        mapView.cameraPosition = INVCameraPosition(INVLatLng(lat: presenter.getstartcoord().starty, lng: presenter.getstartcoord().startx), zoom: 13.0)
        //10초마다 차량 위치를 호출하는 함수
        presenter.runtimer()
        //2초마다 배차 상태를 호출하는 함수
        presenter.runcallchecktimer()
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        makemapview()
        makewhitebox()
        makeinfotitle()
        makeinfolabel()
        makebtnstack()
        makecurrentlocationbtn()
        
        drivermarker.iconImage = INVImage(image: drivermarkerview.asImage())
        
        
        
        self.view.addSubview(driverdeclinepopup)
        driverdeclinepopup.alpha = 0
        
        
        
        
        self.view.addSubview(userdeclinepopup)
        userdeclinepopup.alpha = 0
        
        
    
        currentlocationBtn.addTarget(self, action: #selector(currentlocationbtnpressed(sender:)), for: .touchUpInside)
        cancelBtn.addTarget(self, action: #selector(cancelbtnpressed(sender:)), for: .touchUpInside)
        callTaxiBtn.addTarget(self, action: #selector(calltaxibtnpressed(sender:)), for: .touchUpInside)
        
        
        self.view.addSubview(drivermarkerview)
    }
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.view.sendSubviewToBack(drivermarkerview)
        whitebox.layer.cornerRadius = 8
        cancelBtn.layer.cornerRadius = 25
        callTaxiBtn.layer.cornerRadius = 25
    }
    
    /**
     현재 위치 버튼 이벤트 함수
     */
    @objc func currentlocationbtnpressed(sender : UIButton){
        let lat = UserDefaults.standard.double(forKey: userstandard.lat.rawValue)
        let lon = UserDefaults.standard.double(forKey: userstandard.lon.rawValue)
        mapView.moveCamera(INVCameraUpdate(targetTo: INVLatLng(lat: lat, lng: lon)))
    }
    /**
     호출 취소 버튼 이벤트 함수
     */
    @objc func cancelbtnpressed(sender : UIButton){
        print("pressed")
        self.view.bringSubviewToFront(userdeclinepopup)
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.userdeclinepopup.alpha = 1
        }
        
    }
    /**
     기사님 번호를 호출하는 함수
     */
    @objc func calltaxibtnpressed(sender : UIButton){
        presenter.callDriver()
    }
    
    
    
}
extension AllocationCompleteViewController : UserDeclinePopUpDelegate {
    /**
     유저가 택시 승차 거부를 눌렀을때 발생하는 이벤트 함수
     */
    func userdeclineconfirmedpressed() {
        presenter.callalloccancelapi { [unowned self] (done) in
            if done {
                self.navigationController?.popToViewController(ofClass: MainViewController.self, animated: true)
                FunctionClass.shared.showusercancelpopup()
            }
        }
        
    }
}
extension AllocationCompleteViewController : DriverDeclinedPopUPDelegate {
    /**
     재 호출 함수 -> 다시 택시 찾는 화면으로 넘어감
     */
    func reallocatebtnpressed() {
        self.navigationController?.popViewController(animated: true)
    }
    
    
}
extension AllocationCompleteViewController : AllocationCompleteContract{
    /**
     기사님 요청으로 거절 되었을때 팝업을 띄우는 함수
     */
    func showdriverdeclinedpopup(){
        self.view.bringSubviewToFront(driverdeclinepopup)
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.driverdeclinepopup.alpha = 1
            
        }
    }
    /**
     기사님의 마커를 셋하는 함수
     */
    func setmarker(posx : Double , posy : Double) {
        drivermarker.iconImage = INVImage(image: drivermarkerview.asImage())
        drivermarker.position = INVLatLng(lat:  posy , lng: posx )
        drivermarker.mapView = mapView
        
    }
    /**
     승차시 승차화면으로 넘어가는 함수
     */
    func showOnBoardingViewController(){
        let view = OnBoardingViewController(allocdata: presenter.getInfos())
        self.navigationController?.pushViewController(view, animated: true)
    }
}

extension AllocationCompleteViewController : INVMapViewDelegate {
    
}
extension AllocationCompleteViewController  {
    private func makemapview(){
        self.view.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        mapView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        mapView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        mapView.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        mapView.delegate = self
    }
    private func makewhitebox(){
        self.view.addSubview(whitebox)
        whitebox.translatesAutoresizingMaskIntoConstraints = false
        whitebox.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 30).isActive = true
        whitebox.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        whitebox.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        whitebox.heightAnchor.constraint(lessThanOrEqualToConstant: 300).isActive = true
        whitebox.backgroundColor = .white
        whitebox.shadowcolor = .black
        whitebox.shadowoffset = CGSize(width: 0, height: 3)
        whitebox.shadowradius = 6
        whitebox.shadowopacity = 0.1
    }
    private func makeinfotitle(){
        self.view.addSubview(infotitle)
        infotitle.translatesAutoresizingMaskIntoConstraints = false
        infotitle.topAnchor.constraint(equalTo: whitebox.topAnchor, constant: 15).isActive = true
        infotitle.leadingAnchor.constraint(equalTo: whitebox.leadingAnchor, constant: 0).isActive = true
        infotitle.trailingAnchor.constraint(equalTo: whitebox.trailingAnchor, constant: 0).isActive = true
        infotitle.heightAnchor.constraint(equalToConstant: 31).isActive = true
        infotitle.text = "택시가 배차되었습니다."
        infotitle.textColor = maincolor
        infotitle.font = UIFont(name: fonts.bold.rawValue, size: 24)
        infotitle.textAlignment = .center
        infotitle.adjustsFontSizeToFitWidth = true
    }
    private func makeinfolabel(){
        self.view.addSubview(infolabel)
        infolabel.translatesAutoresizingMaskIntoConstraints = false
        infolabel.topAnchor.constraint(equalTo: infotitle.bottomAnchor, constant: 5).isActive = true
        infolabel.leadingAnchor.constraint(equalTo: whitebox.leadingAnchor, constant: 0).isActive = true
        infolabel.trailingAnchor.constraint(equalTo: whitebox.trailingAnchor, constant: 0).isActive = true
        infolabel.heightAnchor.constraint(equalToConstant: 26).isActive = true
        infolabel.textAlignment = .center
        infolabel.font = UIFont(name: fonts.bold.rawValue, size: 20)
        //infolabel.text = "소나타 | 서울 12가 1234"
        infolabel.adjustsFontSizeToFitWidth = true
         
        whitebox.bottomAnchor.constraint(equalTo: infolabel.bottomAnchor, constant: 16).isActive = true
        
    }
    
    
    private func makebtnstack(){
        self.view.addSubview(btnstack)
        btnstack.translatesAutoresizingMaskIntoConstraints = false
        btnstack.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: -13).isActive = true
        btnstack.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        btnstack.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        btnstack.heightAnchor.constraint(equalToConstant: 50).isActive = true
        btnstack.isUserInteractionEnabled = true
        
        cancelBtn.shadowcolor = .black
        cancelBtn.shadowoffset = CGSize(width: 0, height: 3)
        cancelBtn.shadowradius = 6
        cancelBtn.shadowopacity = 0.16
        
        cancelBtn.setTitle("호출 취소", for: .normal)
        cancelBtn.setTitleColor(maincolor, for: .normal)
        cancelBtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20)
        cancelBtn.backgroundColor = .white
        
        
        
    }
    private func makecurrentlocationbtn(){
        self.view.addSubview(currentlocationBtn)
        currentlocationBtn.translatesAutoresizingMaskIntoConstraints = false
        currentlocationBtn.bottomAnchor.constraint(equalTo: btnstack.topAnchor, constant: 0).isActive = true
        currentlocationBtn.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -3).isActive = true
        currentlocationBtn.widthAnchor.constraint(equalToConstant: 54).isActive = true
        currentlocationBtn.heightAnchor.constraint(equalToConstant: 54).isActive = true
        currentlocationBtn.setImage(UIImage(named: "mapBtnMy"), for: .normal)
        
        
    }
    
    
    
    
    
}
