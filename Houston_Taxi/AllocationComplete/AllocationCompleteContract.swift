//
//  AllocationCompleteContract.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/10.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation


protocol AllocationCompleteContract : NSObjectProtocol {
    
    func showdriverdeclinedpopup()
    func showOnBoardingViewController()
    
    func setmarker(posx : Double , posy : Double)
    
}
