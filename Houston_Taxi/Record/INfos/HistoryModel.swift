//
//  HistoryModel.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/17.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import SwiftyJSON



class HistoryModel: NSObject {
    
    
    required init(json : JSON){
        
        isSuccessful = json["isSuccessful"].boolValue
        
        currentDT = json["currentDT"].stringValue
        let msgvalue = json["msg"].stringValue
        msg = FunctionClass.shared.decrypt(msg: msgvalue)
        let countvalue = json["count"].stringValue
        count = Int(FunctionClass.shared.decrypt(msg: countvalue))
        
        for datavalue in json["data"].arrayValue {
            data.append(Data(json: datavalue))
        }
        
        
    }
    
    var count : Int?
    var isSuccessful : Bool
    var msg : String?
    var currentDT : String?
    
    var data : [Data] = []
    
    
    
    
    
    internal class Data : NSObject {
        
        required init(json : JSON){
            
            let idvalue = json["id"].stringValue
            id = Int(FunctionClass.shared.decrypt(msg: idvalue))
            
            let carIdvalue = json["carID"].stringValue
            carId = Int(FunctionClass.shared.decrypt(msg: carIdvalue))
            
            let cusPosnamevalue = json["cusPosName"].stringValue
            
            cusPosName = FunctionClass.shared.decrypt(msg: cusPosnamevalue)
            
            let cusLonvalue = json["cusLon"].stringValue
            cusLon = Double(FunctionClass.shared.decrypt(msg: cusLonvalue))
            
            let cusLatvalue = json["cusLat"].stringValue
            cusLat = Double(FunctionClass.shared.decrypt(msg: cusLatvalue))
            
            let destinationNamevalue = json["destinationName"].stringValue
            destinationName = FunctionClass.shared.decrypt(msg: destinationNamevalue)
           
            
            let destinationLonvalue = json["destinationLon"].stringValue
            destinationLon = Double(FunctionClass.shared.decrypt(msg: destinationLonvalue))
            
            let destinationLatvalue = json["destinationLat"].stringValue
            destinationLat = Double(FunctionClass.shared.decrypt(msg: destinationLatvalue))
            
            let driverTelNumvalue = json["driverTelNum"].stringValue
            driverTelNum = FunctionClass.shared.decrypt(msg: driverTelNumvalue)
            
            let typeNamevalue = json["typeName"].stringValue
            typeName = FunctionClass.shared.decrypt(msg: typeNamevalue)
            
            let colorvalue = json["color"].stringValue
            color = FunctionClass.shared.decrypt(msg: colorvalue)
            
            let driverNamevalue = json["driverName"].stringValue
            driverName = FunctionClass.shared.decrypt(msg: driverNamevalue)
            
            let getInTimevalue = json["getInTime"].stringValue
            getInTime = FunctionClass.shared.decrypt(msg: getInTimevalue)
            
            let getOutTimevalue = json["getOutTime"].stringValue
            getOutTime = FunctionClass.shared.decrypt(msg: getOutTimevalue)
            
            
            
            
            
        }
        
        
        
        var id : Int?
        var carId : Int?
        
        var cusPosName : String?
        var cusLon : Double?
        var cusLat : Double?
        
        var destinationName : String?
        var destinationLon : Double?
        var destinationLat : Double?
        
        var driverTelNum : String?
        
        var typeName : String?
        
        var color : String?
        var driverName : String?
        //승차 일시
        var getInTime : String?
        //하차 일시
        var getOutTime : String?
    }
    
}
