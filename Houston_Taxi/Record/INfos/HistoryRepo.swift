//
//  HistoryRepo.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/17.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import SwiftyJSON


class HistoryRepo {
    static let shared = HistoryRepo()
    private init(){}
    
    var model : HistoryModel?
    
    
    func setmodel(input : HistoryModel){
        self.model = input
    }
    
    
    func getmodel() -> HistoryModel?{
        return self.model
    }
    
    
    func findbyId(id : Int) -> HistoryModel.Data? {
        
        if let result = model?.data.filter({ $0.id == id }) {
            return result[0]
        }
        else {
            return nil
        }
    }
    
    func getIdbyindexpath(index : Int) -> Int{
        guard let data = model?.data[index] else {
            return 0
        }
        
        guard let result = data.id else {
            return 0
        }
        
        return result
        
    }
    
}
