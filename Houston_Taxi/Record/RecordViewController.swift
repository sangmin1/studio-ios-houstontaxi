//
//  RecordViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 탑승 이력 뷰 컨트롤러
 */
class RecordViewController: UIViewController, UINavigationControllerDelegate {
    
    private var topbox = UIView()
    
    private var titlelabel = PaddingLabel()
    
    private var xbtn = WideTapBtn()
    
    
    lazy var cv : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.delegate = presenter
        cv.dataSource = presenter
        return cv
    }()
    
    private var deleterecordbtn = DeleteRecordBtn()
    

    lazy private var presenter = RecordPresenter(delegate : self, viewcontroller : self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        maketopbox()
        maketitlelabel()
        makexbtn()
        makecv()
        makedeleterecordbtn()
        presenter.registerCV(cv: cv)
        deleterecordbtn.addTarget(self, action: #selector(deleterecordbtnpressed(sender:)), for: .touchUpInside)
        xbtn.addTarget(self, action: #selector(xbtnpressed(sender:)), for: .touchUpInside)
        self.navigationController?.delegate = self
    }
    /**
     탑승 이력 삭제 버튼 이벤트 함수
     */
    @objc func deleterecordbtnpressed(sender : UIButton){
        presenter.callHistoryAPI { (done) in
            if done {
                let view = DeleteRecordViewController()
                self.navigationController?.pushViewController(view, animated: true)
            }
        }
    }
    /**
     xbtn 버튼 이벤트 함수
     */
    @objc func xbtnpressed(sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        self.cv.reloadData()
    }
}
extension RecordViewController : RecordContract{
    func showViewcontroller(view : UIViewController){
        self.navigationController?.pushViewController(view, animated: true)
    }
}

extension RecordViewController  {
    private func maketopbox(){
        self.view.addSubview(topbox)
        topbox.translatesAutoresizingMaskIntoConstraints = false
        topbox.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        topbox.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        topbox.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        topbox.heightAnchor.constraint(equalToConstant: 56).isActive = true
        topbox.backgroundColor = .clear
    }
    private func maketitlelabel(){
        self.view.addSubview(titlelabel)
        titlelabel.translatesAutoresizingMaskIntoConstraints = false
        titlelabel.centerYAnchor.constraint(equalTo: topbox.centerYAnchor, constant: 0).isActive = true
        titlelabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        titlelabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        titlelabel.heightAnchor.constraint(equalToConstant: 23).isActive = true
        titlelabel.textColor = .rgb(red: 75, green: 75, blue: 75, alpha: 1)
        titlelabel.font = UIFont(name: fonts.bold.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .bold)
        titlelabel.textAlignment = .center
        titlelabel.text = "탑승이력"
    }
    private func makexbtn(){
        self.view.addSubview(xbtn)
        xbtn.translatesAutoresizingMaskIntoConstraints = false
        xbtn.centerYAnchor.constraint(equalTo: titlelabel.centerYAnchor, constant: 0).isActive = true
        xbtn.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        xbtn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        xbtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        xbtn.setImage(UIImage(named: "titleBtnCloseN"), for: .normal)
    }
    private func makecv(){
        self.view.addSubview(cv)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.topAnchor.constraint(equalTo: topbox.bottomAnchor, constant: 0).isActive = true
        cv.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        cv.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        cv.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        cv.backgroundColor = .white
        cv.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 55, right: 0)
    }
    private func makedeleterecordbtn(){
        self.view.addSubview(deleterecordbtn)
        deleterecordbtn.translatesAutoresizingMaskIntoConstraints = false
        deleterecordbtn.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        deleterecordbtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        deleterecordbtn.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        deleterecordbtn.heightAnchor.constraint(equalToConstant: 55).isActive = true
        deleterecordbtn.setimage(image: UIImage(named: "menuIcTrashN")!)
        deleterecordbtn.setlabel(label : "탑승이력 삭제")
    }
}
