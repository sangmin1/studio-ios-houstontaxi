//
//  RecordContract.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
protocol RecordContract : NSObjectProtocol {
    func showViewcontroller(view : UIViewController)
}
