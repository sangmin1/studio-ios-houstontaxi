//
//  RecordPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

class RecordPresenter: NSObject {
    
    weak var delegate : RecordContract?
    weak var viewcontroller : RecordViewController?
    let cellid = "recordcellid"
    let emptycellid = "recordemptycellid"
    
    var recordisEmtpy : Bool = false
    init(delegate : RecordContract , viewcontroller : RecordViewController ){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
    }
    
    func registerCV(cv : UICollectionView){
        cv.register(RecordCell.self, forCellWithReuseIdentifier: cellid)
        cv.register(EmptyRecordCell.self, forCellWithReuseIdentifier: emptycellid)
    }
    
    
    /**
     탑승 이력 조회 api
     */
    func callHistoryAPI(completion : @escaping (Bool) -> ()){
        FunctionClass.shared.showdialog(show: true)
        let api = GetInfoHistory()
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile) { (done, data) in
            if done {
                HistoryRepo.shared.setmodel(input: HistoryModel(json: data))
                
                if let msg = data["msg"].string {
                    print("finding call driver info msg",FunctionClass.shared.decrypt(msg: msg))
                }
                let success = data["isSuccessful"].boolValue
                print("finding success",success)
                if success == true {
                   completion(true)
                }
                else {
                    completion(false)
                }
            }
            else {
                completion(false)
            }
            FunctionClass.shared.showdialog(show: false)
        }
        
        
    }
    
    
    /**
     경로 탐색 api
     
     */
    func callInaviFindRouteAPI(startx : Double, starty : Double, endx : Double ,endy : Double,completion : @escaping (Bool) -> ()){
        
        let api = InaviFindRouteAPI()
        
        api.request_To_server(startx: startx, starty: starty, endx: endx, endy: endy) { (done, data) in
            if done {
                print("CallWithDestPresenter findroute data",data["header"])
                let tempmodel = InaviRouteModel(json: data)
                InaviRouteRepo.shared.setmodel(input: tempmodel)
                if let header = InaviRouteRepo.shared.getmodel().header {
                    print("header.isSuccessful!",header.isSuccessful!)
                    completion(header.isSuccessful!)
                }
                else {
                    print("header failed")
                    completion(false)
                }
            }
            else {
                completion(false)
            }
        }
    }
    
    
    
    
    
    
}
extension RecordPresenter : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = HistoryRepo.shared.getmodel()?.data.count {
            if count == 0 {
                recordisEmtpy = true
                return 1
            }
            else {
                recordisEmtpy = false
                return count
            }
            
        }
        else {
            recordisEmtpy = true
            return 1
        }
        
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
       
        if recordisEmtpy == true {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: emptycellid, for: indexPath) as! EmptyRecordCell
            return cell
        }
        else {
            if let model = HistoryRepo.shared.getmodel()?.data[indexPath.row] {
                 let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellid, for: indexPath) as! RecordCell
                cell.delegate = self
                cell.setdate(date: model.getInTime ?? "")
                cell.setdestname(destname: model.destinationName ?? "목적지없음")
                if model.getOutTime ?? "" != "" {
                    cell.setdesttime(desttime: model.getOutTime ?? "")
                }
                
                
                cell.setdepartname(departname: model.cusPosName ?? "")
                if model.getInTime ?? "" != "" {
                    cell.setdeparttime(departime: model.getInTime ?? "" )
                }
                

                
                cell.setdrivername(drivername: model.driverName ?? "", carmodel: model.color ?? "", carnum: String(model.carId ?? 0))
                
                if model.getInTime ?? "" != "" && model.getOutTime ?? "" != "" {
                    cell.setspendtime(desttime : model.getOutTime ?? "", departtime : model.getInTime ?? "")
                }
    
                cell.setid(id : model.id ?? 0)
                return cell
            }
        }
        
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: emptycellid, for: indexPath) as! EmptyRecordCell
        return cell
    
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: mainboundwidth, height: 252)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 11
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 11
    }
    
    
    
    
    
}
extension RecordPresenter : RecordCellDelegate {
    func callTaxipressed(id : Int){
        guard let model = HistoryRepo.shared.findbyId(id : id) else {
            ToastView.shared.short(self.viewcontroller!.view, txt_msg: "정보를 찾을 수 없습니다.")
            return
        }
        
        guard let departname = model.cusPosName, let startx = model.cusLon, let starty = model.cusLat else {
            return
        }
         print("record cell start", departname, startx, starty)
        if let destname = model.destinationName , let endx = model.destinationLon, let endy = model.destinationLat {
            print("record cell dest", destname, endx, endy)
            self.callInaviFindRouteAPI(startx: startx, starty: starty, endx: endx, endy: endy) { [unowned self] (done) in
                if done {
                    let view = CallWithDestViewController(departname: departname, destname: destname, startx: startx, starty: starty, endx: endx, endy: endy)
                    self.delegate?.showViewcontroller(view : view)
                }
            }
            
        }
        else {
            let view = CallWithOutDestViewController(posx: startx, posy: starty, address: departname)
            self.delegate?.showViewcontroller(view : view)
        }
        
        
    }
    
}
