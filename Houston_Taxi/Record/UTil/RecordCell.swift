//
//  RecordCell.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


protocol RecordCellDelegate : NSObjectProtocol {
    func callTaxipressed(id : Int)
}
/**
 탑승 이력 셀
 */
class RecordCell: UICollectionViewCell {
    
    private var box = ShadowView()
    
    //탑승 시간
    private var datelabel = PaddingLabel()
    //소요시간
    private var spendtime = PaddingLabel()
    
    private var dotlineimage = UIImageView()
    //출발 주소
    private var departurelabel = PaddingLabel()
    //출발 시각
    private var departuretime = PaddingLabel()
    //도착 주소
    private var destlabel = PaddingLabel()
    //도착 시간
    private var desttime = PaddingLabel()
    //이 경로로 호출하기
    private var callbtn = UIButton()
    
    private var underline = UIButton()
    //기사 정보
    private var driverinfolabel = PaddingLabel()
    //탑승이력 아이디
    private var id : Int = 0
    
    weak var delegate : RecordCellDelegate?
    override init(frame: CGRect) {
        super.init(frame : frame)
        makebox()
        makedatelabel()
        makespendtime()
        makedotlineimage()
        makedeparturelabel()
        makedeparturetime()
        makedestlabel()
        makedesttime()
        
        makecallbtn()
        makeunderline()
        makedriverinfolabel()
        
        callbtn.addTarget(self, action: #selector(callbtnpressed(sender:)), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async { [unowned self] in
            self.box.layer.cornerRadius = 4
            self.callbtn.layer.cornerRadius = 4

        }
    }
    
    @objc func callbtnpressed(sender : UIButton){
        self.delegate?.callTaxipressed(id: self.id)
    }
    
    func setdate(date : String){
//        var str = date.replacingOccurrences(of: "T", with: " ")
//        str = str.replacingOccurrences(of: "Z", with: "")
//        str = String(str.dropLast(3))
        let seperatedresult = date.components(separatedBy: " ")
        self.datelabel.text = seperatedresult[0]
    }
    func setdepartname(departname : String){
        self.departurelabel.text = departname
        
    }
    func setdeparttime(departime : String){
//        var str = departime.replacingOccurrences(of: "T", with: " ")
//        str = str.replacingOccurrences(of: "Z", with: "")
//        str = String(str.dropLast(3))
        
        let seperatedresult = departime.components(separatedBy: " ")
        let finalresult = seperatedresult[1].components(separatedBy: ":")
        self.departuretime.text = finalresult[0] + ":" + finalresult[1]
    }
    
    func setdestname(destname : String){
        self.destlabel.text = destname
    }
    
    func setdesttime(desttime : String){
        
        
//        print("desttime",desttime)
//        var str = desttime.replacingOccurrences(of: "T", with: " ")
//        str = str.replacingOccurrences(of: "Z", with: "")
//        str = String(str.dropLast(3))
       
        let seperatedresult = desttime.components(separatedBy: " ")
        let finalresult = seperatedresult[1].components(separatedBy: ":")
        
        self.desttime.text = finalresult[0] + ":" + finalresult[1]
    }
    
    func setdrivername(drivername : String, carmodel : String, carnum : String){
        self.driverinfolabel.text = """
        \(drivername) 기사님
        \(carmodel) | \(carnum)
        """
    }
    
    func setspendtime(desttime : String, departtime : String){
//        var deststr = desttime.replacingOccurrences(of: "T", with: " ")
//        deststr = deststr.replacingOccurrences(of: "Z", with: "")
//        deststr = String(deststr.dropLast(3))
        let destseperatedresult = desttime.components(separatedBy: " ")
        let destfinalresult = destseperatedresult[1].components(separatedBy: ":")
        
        
//        var departstr = departtime.replacingOccurrences(of: "T", with: " ")
//        departstr = departstr.replacingOccurrences(of: "Z", with: "")
//        departstr = String(departstr.dropLast(3))
        
        let departseperatedresult = departtime.components(separatedBy: " ")
        let departfinalresult = departseperatedresult[1].components(separatedBy: ":")
        
        
        var mindiff = 0
        var hourdiff = Int(destfinalresult[0])! - Int(departfinalresult[0])!
        if Int(destfinalresult[1])! < Int(departfinalresult[1])! {
            mindiff = Int(destfinalresult[1])! - Int(departfinalresult[1])! + 60
            hourdiff -= 1
        }
        else {
             mindiff = Int(destfinalresult[1])! - Int(departfinalresult[1])!
        }
        
        if hourdiff <= 0 {
            spendtime.text = "\(mindiff)분 소요"
        }
        else {
            spendtime.text = "\(hourdiff)시간 \(mindiff)분 소요"
        }
        
        
        
    }
    
    func setid(id : Int){
        self.id = id
    }
    func getid() -> Int {
        return self.id
    }
    
    
    
}
extension RecordCell {
    private func makebox(){
        self.addSubview(box)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.topAnchor.constraint(equalTo: self.topAnchor, constant: 12).isActive = true
        box.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        box.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        box.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12).isActive = true
        box.backgroundColor = .white
        box.shadowcolor = .black
        box.shadowoffset = CGSize(width: 0, height: 5)
        box.shadowradius = 10
        box.shadowopacity = 0.1
    }
    private func makedatelabel(){
        self.addSubview(datelabel)
        datelabel.translatesAutoresizingMaskIntoConstraints = false
        datelabel.topAnchor.constraint(equalTo: box.topAnchor, constant: 12).isActive = true
        datelabel.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 16).isActive = true
        datelabel.widthAnchor.constraint(lessThanOrEqualToConstant: 140).isActive = true
        datelabel.heightAnchor.constraint(equalToConstant: 21).isActive = true
        datelabel.textColor = .rgb(red: 75, green: 75, blue: 75, alpha: 1)
        datelabel.font = UIFont(name: fonts.bold.rawValue, size: 16) //.systemFont(ofSize: 16, weight: .bold)
        datelabel.text = "2020-20-20"
    }
    private func makespendtime(){
        self.addSubview(spendtime)
        spendtime.translatesAutoresizingMaskIntoConstraints = false
        spendtime.centerYAnchor.constraint(equalTo: datelabel.centerYAnchor, constant: 1).isActive = true
        spendtime.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: -16).isActive = true
        spendtime.widthAnchor.constraint(equalToConstant: 150).isActive = true
        spendtime.heightAnchor.constraint(equalToConstant: 18).isActive = true
        spendtime.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        spendtime.font = UIFont(name: fonts.medium.rawValue, size: 14) //.systemFont(ofSize: 14, weight: .medium)
        spendtime.text = "1시간 28분 소요"
        spendtime.textAlignment = .right
    }
    private func makedotlineimage(){
        self.addSubview(dotlineimage)
        dotlineimage.translatesAutoresizingMaskIntoConstraints = false
        dotlineimage.topAnchor.constraint(equalTo: datelabel.bottomAnchor, constant: 17.5).isActive = true
        dotlineimage.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 16).isActive = true
        dotlineimage.widthAnchor.constraint(equalToConstant: 16).isActive = true
        dotlineimage.heightAnchor.constraint(equalToConstant: 50).isActive = true
        dotlineimage.image = UIImage(named: "menuIcDot")
    }
    private func makedeparturelabel(){
        self.addSubview(departurelabel)
        departurelabel.translatesAutoresizingMaskIntoConstraints = false
        //departurelabel.topAnchor.constraint(equalTo: datelabel.bottomAnchor, constant: 12.5).isActive = true
        departurelabel.bottomAnchor.constraint(equalTo: dotlineimage.centerYAnchor, constant: -3.5).isActive = true
        departurelabel.leadingAnchor.constraint(equalTo: dotlineimage.trailingAnchor, constant: 18).isActive = true
        //departurelabel.widthAnchor.constraint(lessThanOrEqualToConstant: 250).isActive = true
        departurelabel.heightAnchor.constraint(equalToConstant: 26).isActive = true
        departurelabel.textColor = .rgb(red: 75, green: 75, blue: 75, alpha: 1)
        departurelabel.font = UIFont(name: fonts.medium.rawValue, size: 20) //.systemFont(ofSize: 20, weight: .medium)
        departurelabel.text = "삼환하이펙스 A동"
        departurelabel.adjustsFontSizeToFitWidth = true
        
    }
    private func makedeparturetime(){
        self.addSubview(departuretime)
        departuretime.translatesAutoresizingMaskIntoConstraints = false
        departuretime.centerYAnchor.constraint(equalTo: departurelabel.centerYAnchor, constant: 0).isActive = true
        departuretime.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: -16).isActive = true
        departuretime.widthAnchor.constraint(equalToConstant: 50).isActive = true
        departuretime.heightAnchor.constraint(equalToConstant: 21).isActive = true
        departuretime.textColor = .rgb(red: 184, green: 184, blue: 184, alpha: 1)
        departuretime.font = UIFont(name: fonts.medium.rawValue, size: 16) //.systemFont(ofSize: 16, weight: .medium)
        departuretime.text = "15:30"
        departuretime.textAlignment = .right
        departurelabel.trailingAnchor.constraint(equalTo: departuretime.leadingAnchor, constant: -10).isActive = true
        
    }
    private func makedestlabel(){
        self.addSubview(destlabel)
        destlabel.translatesAutoresizingMaskIntoConstraints = false
        destlabel.topAnchor.constraint(equalTo: dotlineimage.centerYAnchor, constant: 3.5).isActive = true
        destlabel.leadingAnchor.constraint(equalTo: dotlineimage.trailingAnchor, constant: 18).isActive = true
        //destlabel.widthAnchor.constraint(lessThanOrEqualToConstant: 250).isActive = true
        destlabel.heightAnchor.constraint(equalToConstant: 26).isActive = true
        destlabel.textColor = .rgb(red: 75, green: 75, blue: 75, alpha: 1)
        destlabel.font = UIFont(name: fonts.medium.rawValue, size: 20) //.systemFont(ofSize: 20, weight: .medium)
        destlabel.text = "삼환하이펙스 A동"
        destlabel.adjustsFontSizeToFitWidth = true
    }
    private func makedesttime(){
        self.addSubview(desttime)
        desttime.translatesAutoresizingMaskIntoConstraints = false
        desttime.centerYAnchor.constraint(equalTo: destlabel.centerYAnchor, constant: 0).isActive = true
        desttime.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: -16).isActive = true
        desttime.widthAnchor.constraint(equalToConstant: 50).isActive = true
        desttime.heightAnchor.constraint(equalToConstant: 21).isActive = true
        desttime.textColor = .rgb(red: 184, green: 184, blue: 184, alpha: 1)
        desttime.font = UIFont(name: fonts.medium.rawValue, size: 16) //.systemFont(ofSize: 16, weight: .medium)
        desttime.text = "16:58"
        desttime.textAlignment = .right
        destlabel.trailingAnchor.constraint(equalTo: desttime.leadingAnchor, constant: -10).isActive = true
    }
    private func makecallbtn(){
        self.addSubview(callbtn)
        callbtn.translatesAutoresizingMaskIntoConstraints = false
        callbtn.topAnchor.constraint(equalTo: dotlineimage.bottomAnchor, constant: 14).isActive = true
        callbtn.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 20).isActive = true
        callbtn.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: -20).isActive = true
        callbtn.heightAnchor.constraint(equalToConstant: 33).isActive = true
        callbtn.setTitle("이 경로로 호출하기", for: .normal)
        callbtn.setTitleColor(maincolor, for: .normal)
        callbtn.titleLabel?.font = UIFont(name: fonts.medium.rawValue, size: 14) //.systemFont(ofSize: 14, weight: .medium)
        callbtn.layer.borderColor = maincolor.cgColor
        callbtn.layer.borderWidth = 1
        
    }
    private func makeunderline(){
        self.addSubview(underline)
        underline.translatesAutoresizingMaskIntoConstraints = false
        underline.topAnchor.constraint(equalTo: callbtn.bottomAnchor, constant: 12.5).isActive = true
        underline.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 0).isActive = true
        underline.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: 0).isActive = true
        underline.heightAnchor.constraint(equalToConstant: 1).isActive = true
        underline.backgroundColor = .rgb(red: 0, green: 0, blue: 0, alpha: 0.05)
    }
    private func makedriverinfolabel(){
        self.addSubview(driverinfolabel)
        driverinfolabel.translatesAutoresizingMaskIntoConstraints = false
        driverinfolabel.topAnchor.constraint(equalTo: underline.bottomAnchor, constant: 11.5).isActive = true
        driverinfolabel.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 16).isActive = true
        driverinfolabel.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: -16).isActive = true
        driverinfolabel.bottomAnchor.constraint(equalTo: box.bottomAnchor, constant: -11.5).isActive = true
        driverinfolabel.numberOfLines = 2
        driverinfolabel.lineBreakMode = .byWordWrapping
        driverinfolabel.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        driverinfolabel.font = UIFont(name: fonts.medium.rawValue, size: 16) //.systemFont(ofSize: 16, weight: .medium)
        driverinfolabel.text = """
        김팅크 기사님
        소나타 | 서울 12가 3456
"""
    }
}
