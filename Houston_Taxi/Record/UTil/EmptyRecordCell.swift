//
//  EmptyRecordCell.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 탑승 이력이 없을시 나타나는 셀
 */
class EmptyRecordCell: UICollectionViewCell {
    
    private var label = PaddingLabel()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        makelabel()
        self.backgroundColor = .white
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
}
extension EmptyRecordCell {
    private func makelabel(){
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        label.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        label.widthAnchor.constraint(equalToConstant: 200).isActive = true
        label.heightAnchor.constraint(equalToConstant: 23).isActive = true
        label.textColor = .rgb(red: 184, green: 184, blue: 184, alpha: 1)
        label.font = UIFont(name: fonts.medium.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .medium)
        label.textAlignment = .center
        label.text = "탑승이력이 없습니다."
        
        
    }
}
