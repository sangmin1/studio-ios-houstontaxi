//
//  Circle.swift
//  Commelier
//
//  Created by sangmin han on 2020/07/10.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
class Circle: UIView {
    
    var radius : Double = 0
    var startingdegree : Double = 270
    var paths : [UIBezierPath] = []
    var centers : [CGPoint] = []
    
    var btns : [WideTapBtn] = []
   
    let label = PaddingLabel()
    var colors : [UIColor] = [.red,.blue,.green,.yellow,.cyan,.black]
    
    
    override func draw(_ rect: CGRect) {
        radius = Double(rect.width) / 2 - 20.0
        let center = CGPoint(x: rect.width / 2, y: rect.height / 2)
        
        for seq in stride(from: 0, to: 4.1, by: 1) {
            let path = UIBezierPath()
            paths.append(path)
            print("seq",seq)
            let startingx = Double(center.x) + radius * cos((72.0 * (seq) * Double.pi + 20 ) / 180)
            let startingy = Double(center.y) + radius * sin((72.0 * (seq) * Double.pi + 20 ) / 180)
            path.move(to: CGPoint(x: startingx, y: startingy))
            
            let fancenter = CGPoint(x: Double(center.x) + radius * ( 3 / 4 ) * cos(((72.0 * (seq) - 36.0) * Double.pi / 180)), y: Double(center.y) + radius * ( 3 / 4 ) * sin(((72.0 * (seq) - 36.0) * Double.pi / 180)))
            centers.append( fancenter)
            for i in stride(from: 72.0 * seq, to: 72.0 * (seq + 1) + 1, by: 1) {
                let radian = i * Double.pi / 180
                let x = Double(center.x) + radius * cos(radian)
                let y = Double(center.y) + radius * sin(radian)
                path.addLine(to: CGPoint(x: x, y: y))
            }
            
            
            path.addLine(to: CGPoint(x: Double(center.x) + (radius / 2) * cos(72.0 * (seq + 1) * Double.pi / 180 ), y: Double(center.y) + (radius / 2) * sin(72.0 * (seq + 1) * Double.pi / 180)))
            
            for i in stride(from: 72.0 * (seq + 1) , to: 72.0 * seq, by: -1) {
                let radian = i * Double.pi / 180
                let x = Double(center.x) + (radius / 2) * cos(radian)
                let y = Double(center.y) + (radius / 2) * sin(radian)
                path.addLine(to: CGPoint(x: x, y: y))
            }
            let endingx = Double(center.x) + radius * cos((72.0 * (seq) * Double.pi ) / 180)
            let endingy = Double(center.y) + radius * sin((72.0 * (seq) * Double.pi ) / 180)
            path.addLine(to: CGPoint(x: endingx, y: endingy))
            UIColor.rgb(red: 169, green: 211, blue: 255, alpha: 1).setStroke()

            path.lineWidth = 2
            path.stroke()
            path.close()
        }
        UIColor.rgb(red: 169, green: 211, blue: 255, alpha: 1).setFill()
        paths[0].fill()
        
        for i in 0...4 {
            let btn = WideTapBtn(frame: CGRect(x: centers[i].x - 25, y: centers[i].y - 25, width: 50, height: 50))
            btn.setTitleColor(.black, for: .normal)
            btn.setTitle("\(i + 1)점", for: .normal)
            btn.tag = i
            btn.transform = CGAffineTransform(rotationAngle: CGFloat(90 * Double.pi / 180.0))
            btn.addTarget(self, action: #selector(btnpressed(sender:)), for: .touchUpInside)
            btns.append(btn)
            
            self.addSubview(btn)
            
        }
        
        label.frame = CGRect(x: center.x - 50, y: center.y - 10, width: 100, height: 20)
        label.textColor = .black
        label.text = "향미"
        label.textAlignment = .center
        label.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        label.transform = CGAffineTransform(rotationAngle: CGFloat(90 * Double.pi / 180.0))
        self.addSubview(label)
        
        
    }
    
    
    @objc func btnpressed(sender : UIButton){
        for (index,path) in paths.enumerated() {
            if index == sender.tag {
                UIColor.rgb(red: 169, green: 211, blue: 255, alpha: 1).setFill()
                path.fill()
            }
            else {
                UIColor.white.setFill()
                path.fill()
            }
        }
        self.setNeedsLayout()
        self.setNeedsDisplay()
    }
    
    
    
}
 
