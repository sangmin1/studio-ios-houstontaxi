//
//  CircleShadowBtn.swift
//  Commelier
//
//  Created by sangmin han on 2020/07/09.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit



class CircleShadowBtn: UIButton {
    
    override func didMoveToWindow() {
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowRadius = 4
        self.layer.shadowOpacity = 0.5
        self.layer.shadowOffset = CGSize(width: 0, height: 3)
        
    }
    
}

class ShadowBtn : UIButton {
    
    
    var shadowcolor = UIColor()
    var shadowradius : CGFloat = 0
   var shadowopacity : Float = 0
    var shadowoffset = CGSize()
    
    override func didMoveToWindow() {
        self.layer.shadowColor = shadowcolor.cgColor
        self.layer.shadowRadius = shadowradius
        self.layer.shadowOpacity = shadowopacity
        self.layer.shadowOffset = shadowoffset
        
    }
}
