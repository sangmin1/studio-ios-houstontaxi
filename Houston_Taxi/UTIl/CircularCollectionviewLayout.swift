//
//  CircularCollectionviewLayout.swift
//  Commelier
//
//  Created by sangmin han on 2020/07/10.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class CircularCollectionViewLayout: UICollectionViewLayout {
    
    let itemSize = CGSize(width: 60, height: 30)

    var radius: CGFloat = 50 {
      didSet {
        invalidateLayout()
      }
    }
    var anglePerItem: CGFloat {
      return atan(itemSize.width / radius)
    }
    
    var attributesList = [CircularCollectionViewLayoutAttributes]()
    
    
    func collectionViewContentSize() -> CGSize {
        return CGSize(width: CGFloat(collectionView!.numberOfItems(inSection: 0)) * itemSize.width,
                      height: collectionView!.bounds.height)
    }
    
   
    
    class func layoutAttributesClass() -> AnyClass {
      return CircularCollectionViewLayoutAttributes.self
    }
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
         return attributesList
    }
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return attributesList[indexPath.row]
    }

    override func prepare() {
        super.prepare()
      
        let centerX = collectionView!.contentOffset.x + (collectionView!.bounds.width / 2.0)
        let anchorPointY = ((itemSize.height / 2.0) + radius) / itemSize.height
        
        
        
        attributesList = (0..<collectionView!.numberOfItems(inSection: 0)).map { (i)
          -> CircularCollectionViewLayoutAttributes in
        // 1
            let attributes = CircularCollectionViewLayoutAttributes(forCellWith: NSIndexPath(item: i,
                                                                                             section: 0) as IndexPath)
            attributes.size = self.itemSize
            attributes.anchorPoint = CGPoint(x: 0.5, y: anchorPointY)
        // 2
            attributes.center = CGPoint(x: centerX, y: self.collectionView!.bounds.midY)
        // 3
        attributes.angle = 72 * CGFloat(i)
        return attributes
      }
    }
    
    
    
    
}
class CircularCollectionViewLayoutAttributes: UICollectionViewLayoutAttributes {
  // 1
  var anchorPoint = CGPoint(x: 0.5, y: 0.5)
  var angle: CGFloat = 0 {
    // 2
    didSet {
      zIndex = Int(angle * 1000000)
        transform = CGAffineTransform(rotationAngle: angle)
    }
  }
  // 3
    
    
    override func copy(with zone: NSZone? = nil) -> Any {
        let copiedAttributes: CircularCollectionViewLayoutAttributes =
            super.copy(with: zone) as! CircularCollectionViewLayoutAttributes
        copiedAttributes.anchorPoint = self.anchorPoint
        copiedAttributes.angle = self.angle
        return copiedAttributes
    }
}
