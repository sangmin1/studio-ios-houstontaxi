//
//  ShadowView.swift
//  Commelier
//
//  Created by sangmin han on 2020/07/09.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

class ShadowView: UIView {
    
    
    var shadowcolor = UIColor()
    var shadowradius : CGFloat = 0
    var shadowopacity : Float = 0
    var shadowoffset = CGSize()
    
    override func didMoveToWindow() {
        self.layer.shadowColor = shadowcolor.cgColor
        self.layer.shadowRadius = shadowradius
        self.layer.shadowOpacity = shadowopacity
        self.layer.shadowOffset = shadowoffset
        
    }
    
    
}
