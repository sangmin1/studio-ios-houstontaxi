//
//  InfoUserAgreementsViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class InfoUserAgreementsViewController: UIViewController {
    
    //네비게이션 바
    private var topbox = UIView()
    //뒤로가기 버튼
    private var backbtn = WideTapBtn()
    //이용 약관 동의
    private var titlelabel = PaddingLabel()
    //모두 동의 라벨
    private var totalagreelabel = PaddingLabel()
    //모두 동의 체크 박스
    private var totalagreecheckbox = WideTapBtn()
    
    private var underline = UIImageView()
    //개인정보 박스 컴포넌트
    lazy private var privacy = InfoUserAgreementsBtnBox(frame: .zero, type: .privacy, delegate: presenter)
    //위치정보 박스 컴포넌트
    lazy private var location = InfoUserAgreementsBtnBox(frame: .zero, type: .location, delegate: presenter)
    //서비스 이용 박스 컴포넌트
    lazy private var service = InfoUserAgreementsBtnBox(frame: .zero, type: .service, delegate: presenter)
    
    lazy private var stack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [privacy,location,service])
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.spacing = 0
        return stack
    }()
    //동의하기 버튼
    private var confirmbtn = UIButton()
    
    
    lazy private var presenter = InfoUserAgreementsPresenter(delegate : self, viewcontroller : self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        maketopbox()
        makebackbtn()
        maketitlelabel()
        maketotalagreelabel()
        maketotalagreecheckbox()
        makeunderline()
        makestack()
        makeconfirmbtn()
        
        backbtn.addTarget(self, action: #selector(backbtnpressed(sender:)), for: .touchUpInside)
        confirmbtn.addTarget(self, action: #selector(confirmbtnpressed(sender:)), for: .touchUpInside)
        totalagreecheckbox.addTarget(self, action: #selector(totalagreecheckboxpressed(sender:)), for: .touchUpInside)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        confirmbtn.layer.cornerRadius = 25
    }
    
    /**
     백버튼 이벤트 함수
     */
    @objc func backbtnpressed(sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    /**
     전부 동의 버튼 이벤트 함수
     */
    @objc func totalagreecheckboxpressed(sender : UIButton){
        if sender.isSelected == true {
            privacy.setbtnSelected(selected : false)
            location.setbtnSelected(selected : false)
            service.setbtnSelected(selected : false)
            
        }
        else {
            privacy.setbtnSelected(selected : true)
            location.setbtnSelected(selected : true)
            service.setbtnSelected(selected : true)
        }
        sender.isSelected = !sender.isSelected
    }
    /**
     확인 버튼 이벤트 함수
     */
    @objc func confirmbtnpressed(sender : UIButton){
        let view = MainViewController()
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    
}

extension InfoUserAgreementsViewController : InfoUseAgreementsContract {
    /**
     일괄 동의 해제 함수
     */
    func disableallSelected(){
        totalagreecheckbox.isSelected = false
    }
    /**
     각 동의서 상세페이지 로 가는 함수
     
     */
    func showTermsPage(type: infouseagreementstype){
        let view = InfoUseAgreementsSpecPage(type: type)
        self.navigationController?.pushViewController(view, animated: true)
    }
    
}
extension InfoUserAgreementsViewController {
    private func maketopbox(){
        self.view.addSubview(topbox)
        topbox.translatesAutoresizingMaskIntoConstraints = false
        topbox.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        topbox.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        topbox.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        topbox.heightAnchor.constraint(equalToConstant: 56).isActive = true
        topbox.backgroundColor = .white
    }
    private func makebackbtn(){
        self.view.addSubview(backbtn)
        backbtn.translatesAutoresizingMaskIntoConstraints = false
        backbtn.centerYAnchor.constraint(equalTo: topbox.centerYAnchor, constant: 0).isActive = true
        backbtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        backbtn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        backbtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        backbtn.setImage(UIImage(named: "titleBtnBackN"), for: .normal)
        
    }
    private func maketitlelabel(){
        self.view.addSubview(titlelabel)
        titlelabel.translatesAutoresizingMaskIntoConstraints = false
        titlelabel.centerYAnchor.constraint(equalTo: topbox.centerYAnchor, constant: 0).isActive = true
        titlelabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        titlelabel.widthAnchor.constraint(equalToConstant: 150).isActive = true
        titlelabel.heightAnchor.constraint(equalToConstant: 23).isActive = true
        titlelabel.textColor = .rgb(red: 75, green: 75, blue: 75, alpha: 1)
        titlelabel.font = UIFont(name: fonts.bold.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .bold)
        titlelabel.textAlignment = .center
        titlelabel.text = "이용 약관 동의"
    }
    
    private func maketotalagreelabel(){
        self.view.addSubview(totalagreelabel)
        totalagreelabel.translatesAutoresizingMaskIntoConstraints = false
        totalagreelabel.topAnchor.constraint(equalTo: topbox.bottomAnchor, constant: 29).isActive = true
        totalagreelabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 24).isActive = true
        totalagreelabel.widthAnchor.constraint(equalToConstant: 250).isActive = true
        totalagreelabel.heightAnchor.constraint(equalToConstant: 26).isActive = true
        totalagreelabel.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        totalagreelabel.font = UIFont(name: fonts.bold.rawValue, size: 20) //.systemFont(ofSize: 20, weight: .bold)
        totalagreelabel.text = "아래 내용에 모두 동의합니다."
    }
    private func maketotalagreecheckbox(){
        self.view.addSubview(totalagreecheckbox)
        totalagreecheckbox.translatesAutoresizingMaskIntoConstraints = false
        totalagreecheckbox.centerYAnchor.constraint(equalTo: totalagreelabel.centerYAnchor, constant: 0).isActive = true
        totalagreecheckbox.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -22).isActive = true
        totalagreecheckbox.widthAnchor.constraint(equalToConstant: 30).isActive = true
        totalagreecheckbox.heightAnchor.constraint(equalToConstant: 30).isActive = true
        totalagreecheckbox.setImage(UIImage(named: "dtListUnChecked"), for: .normal)
        totalagreecheckbox.setImage(UIImage(named: "dtListChecked"), for: .selected)
    }
    private func makeunderline(){
        self.view.addSubview(underline)
        underline.translatesAutoresizingMaskIntoConstraints = false
        underline.bottomAnchor.constraint(equalTo: totalagreelabel.bottomAnchor, constant: 29).isActive = true
        underline.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        underline.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        underline.heightAnchor.constraint(equalToConstant: 1).isActive = true
        underline.backgroundColor = .rgb(red: 0, green: 0, blue: 0, alpha: 0.1)
    }
    private func makestack(){
        self.view.addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.topAnchor.constraint(equalTo: underline.bottomAnchor, constant: 0).isActive = true
        stack.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        stack.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        stack.heightAnchor.constraint(equalToConstant: 85 * 3).isActive = true
        
        //개인 정보 정보 세팅
        privacy.setbtntitle(title: "개인 정보 수집 및 이용")
        //위치 정보 수집 및 이용 내용 세팅
        location.setbtntitle(title: "위치 정보 수집 및 이용")
        //서비스 이용 세팅
        service.setbtntitle(title: "서비스 이용")
    }
    private func makeconfirmbtn(){
        self.view.addSubview(confirmbtn)
        confirmbtn.translatesAutoresizingMaskIntoConstraints = false
        confirmbtn.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -12).isActive = true
        confirmbtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        confirmbtn.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        confirmbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        confirmbtn.backgroundColor = maincolor
        confirmbtn.setTitle("동의하기", for: .normal)
        confirmbtn.setTitleColor(.white, for: .normal)
        confirmbtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20)  //.systemFont(ofSize: 20, weight: .bold)
    }
}
