//
//  InfoUserAgreementsPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class InfoUserAgreementsPresenter: NSObject {
    
    
    weak var delegate : InfoUseAgreementsContract?
    weak var viewcontroller : InfoUserAgreementsViewController?
    
    init(delegate : InfoUseAgreementsContract , viewcontroller : InfoUserAgreementsViewController){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
    }
    
    /**
     각 동의서별 내용 api 호출 함수
     */
    func callGetTerms(type: infouseagreementstype){
        
        let api = GetTerms()
        api.requestToserver { [unowned self] (done, data) in
            if done {
                TermsRepo.shared.terms = data
                self.delegate?.showTermsPage(type: type)
            }
        }
        
    }
}
//개인정보, 위치정보, 서비스 이용 버튼 박스 컴포넌트 델리게이트 ( 비즈니스 로직 담당)
extension InfoUserAgreementsPresenter : InfoUserAgreementsBtnBoxDelegate{
    func checkboxpressed(type: infouseagreementstype, isSelected: Bool) {
        if isSelected == false {
            self.delegate?.disableallSelected()
        }
    }
    
    func titlebtnpressed(type: infouseagreementstype) {
        self.callGetTerms(type: type)
        
    }
    
    
}
