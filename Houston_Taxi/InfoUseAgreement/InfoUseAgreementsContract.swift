//
//  InfoUseAgreementsContract.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation

protocol InfoUseAgreementsContract : NSObjectProtocol {
    func disableallSelected()
    func showTermsPage(type: infouseagreementstype)
}
// 개인정보, 위치정보, 서비스 이용 버튼 박스 컴포넌트 델리게이트
protocol InfoUserAgreementsBtnBoxDelegate : NSObjectProtocol {
    func checkboxpressed(type : infouseagreementstype, isSelected : Bool)
    func titlebtnpressed(type : infouseagreementstype)
    
}
