//
//  InfoUseAgreementsSpecPage.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

class InfoUseAgreementsSpecPage: UIViewController {
    
    //네비게이션 바
    private var topbox = UIView()
    //뒤로가기 버튼
    private var backbtn = WideTapBtn()
    //내용 타입 ( 개인정보 , 위치정보 , 서비스)
    private var titlelabel = PaddingLabel()
    
    //상세 내용 텍스트 뷰
    private var textview = UITextView()
    //정보 타입 ( 개인정보 , 위치정보 , 서비스)
    private var type : infouseagreementstype = .none
    
    convenience init(type : infouseagreementstype){
        self.init()
        self.type = type
        //정보 탑입에 따라 타이틀을 세팅하는 것
        if type == .privacy {
            titlelabel.text = "개인 정보 수집 및 이용"
        }
        else if type == .location {
            titlelabel.text = "위치 정보 수집 및 이용"
        }
        else if type == .service{
            titlelabel.text = "서비스 이용"
        }
        else {
            //위에 아무것도 해당 안되면 다시 나감
            //에러 사항
            self.navigationController?.popViewController(animated: true)
        }


        
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        maketopbox()
        makebackbtn()
        maketitlelabel()
        maketextview()
        if let term = TermsRepo.shared.terms {
            let encodedData = term.data(using: String.Encoding.utf8, allowLossyConversion: true)
            print("terms",term)
            do {
                self.textview.attributedText = try NSAttributedString(data: encodedData!, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
            }
            catch let error as NSError {
                print(error.localizedDescription)
            }
            catch{
                print("failed")
            }
            
        }
        
        backbtn.addTarget(self, action: #selector(backbtnpressed(sender:)), for: .touchUpInside)
    }
    
    @objc func backbtnpressed(sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    
    
}
extension InfoUseAgreementsSpecPage {
    private func maketopbox(){
        self.view.addSubview(topbox)
        topbox.translatesAutoresizingMaskIntoConstraints = false
        topbox.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        topbox.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        topbox.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        topbox.heightAnchor.constraint(equalToConstant: 56).isActive = true
        topbox.backgroundColor = .white
    }
    private func makebackbtn(){
        self.view.addSubview(backbtn)
        backbtn.translatesAutoresizingMaskIntoConstraints = false
        backbtn.centerYAnchor.constraint(equalTo: topbox.centerYAnchor, constant: 0).isActive = true
        backbtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        backbtn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        backbtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        backbtn.setImage(UIImage(named: "titleBtnBackN"), for: .normal)
        
    }
    private func maketitlelabel(){
        self.view.addSubview(titlelabel)
        titlelabel.translatesAutoresizingMaskIntoConstraints = false
        titlelabel.centerYAnchor.constraint(equalTo: topbox.centerYAnchor, constant: 0).isActive = true
        titlelabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        titlelabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        titlelabel.heightAnchor.constraint(equalToConstant: 23).isActive = true
        titlelabel.textColor = .rgb(red: 75, green: 75, blue: 75, alpha: 1)
        titlelabel.font = UIFont.systemFont(ofSize: 18, weight: .bold)
        titlelabel.textAlignment = .center
        titlelabel.adjustsFontSizeToFitWidth = true
        
    }
    private func maketextview(){
        self.view.addSubview(textview)
        textview.translatesAutoresizingMaskIntoConstraints = false
        textview.topAnchor.constraint(equalTo: topbox.bottomAnchor, constant: 0).isActive = true
        textview.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        textview.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        textview.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -12).isActive = true
        
        
    }
}
