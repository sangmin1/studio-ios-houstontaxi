//
//  InfoUseAgreementsBtnBox.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 개인정보, 위치정보, 서비스 이용 버튼 박스 컴포넌트
 */
class InfoUserAgreementsBtnBox: UIView {
    //글자 버튼
    private var btn = UIButton()
    //체크 박스
    private var checkbox = WideTapBtn()
    //박스 하단의 선
    private var underline = UIImageView()
    // 개인정보, 위치정보, 서비스 인지 판별을 위한 변수
    private var type : infouseagreementstype = .none
    
    
    weak var delegate : InfoUserAgreementsBtnBoxDelegate?
    
    
    
    init(frame: CGRect,type : infouseagreementstype, delegate : InfoUserAgreementsBtnBoxDelegate) {
        super.init(frame: frame)
        self.type = type
        self.delegate = delegate
        makebtn()
        makecheckbox()
        makeunderline()
        btn.addTarget(self, action: #selector(btnpressed(sender:)), for: .touchUpInside)
        checkbox.addTarget(self, action: #selector(checkboxpressed(sender:)), for: .touchUpInside)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    //버튼의 내용을 셋하는 함수
    func setbtntitle(title : String){
        
        let attr = NSAttributedString(string: title, attributes: [
            NSAttributedString.Key.foregroundColor : UIColor.rgb(red: 100, green: 100, blue: 100, alpha: 1),
            NSAttributedString.Key.font : UIFont(name: fonts.medium.rawValue, size: 20)!,
            NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue,
            NSAttributedString.Key.underlineColor : UIColor.rgb(red: 100, green: 100, blue: 100, alpha: 1)
        ])
        btn.setAttributedTitle(attr, for: .normal)
    }
    //글자를 눌렀을때 실행되는 함수
    @objc func btnpressed(sender : UIButton){
        self.delegate?.titlebtnpressed(type : self.type)
    }
    //체크박스를 눌렀을때 실행되는 함수
    @objc func checkboxpressed(sender : UIButton){
        sender.isSelected = !sender.isSelected
        self.delegate?.checkboxpressed(type : self.type, isSelected : sender.isSelected)
    }
    
    
    func setbtnSelected(selected : Bool){
        checkbox.isSelected = selected
    }
    
    
    
    
}
extension InfoUserAgreementsBtnBox {
    private func makebtn(){
        self.addSubview(btn)
        btn.translatesAutoresizingMaskIntoConstraints = false
        btn.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        btn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 24).isActive = true
        btn.widthAnchor.constraint(equalToConstant: 200).isActive = true
        btn.heightAnchor.constraint(equalToConstant: 26).isActive = true
        btn.setTitleColor(.rgb(red: 100, green: 100, blue: 100, alpha: 1), for: .normal)
        btn.titleLabel?.font = UIFont(name: fonts.medium.rawValue, size: 20)//UIFont.systemFont(ofSize: 20, weight: .medium)
        btn.contentHorizontalAlignment = .left
        
        
    }
    private func makecheckbox(){
        self.addSubview(checkbox)
        checkbox.translatesAutoresizingMaskIntoConstraints = false
        checkbox.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        checkbox.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -22).isActive = true
        checkbox.widthAnchor.constraint(equalToConstant: 30).isActive = true
        checkbox.heightAnchor.constraint(equalToConstant: 30).isActive = true
        checkbox.setImage(UIImage(named: "dtListUnChecked"), for: .normal)
        checkbox.setImage(UIImage(named: "dtListChecked"), for: .selected)
    }
    private func makeunderline(){
        self.addSubview(underline)
        underline.translatesAutoresizingMaskIntoConstraints = false
        underline.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        underline.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        underline.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        underline.heightAnchor.constraint(equalToConstant: 1).isActive = true
        underline.backgroundColor = .rgb(red: 0, green: 0, blue: 0, alpha: 0.05)
    }
    
}
