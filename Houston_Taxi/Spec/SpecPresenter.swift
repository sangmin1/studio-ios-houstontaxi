//
//  SpecPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/16.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class SpecPresenter: NSObject {
    
    
    weak var delegate : SpecContract?
    weak var viewcontroller : SpecViewController?
    
    private var currentkeyword : String = ""
    let destinationcellid = "destinationcellid"
    let recentrecordcellid = "recentrecordcellid"
    
    
    //현재 목적지 설정 중인지 도착지 설중인지 판별을 위한 변수
    private var currentsearchtype : searchtype = .destination
    
    //최종 목적지 좌표
    private var destcoordinatex : Double = 0
    private var destcoordinatey : Double = 0
    //최종 출발지 좌표
    private var departcoordinatex : Double = 0
    private var departcoordinatey : Double = 0
    
    /**
     현재 도착지가 설정 되어 있는지 판별을 위한 변수
     */
    private var departureisSet : Bool = true
    
    /**
     현재 도착지가 설정 되어 있는지 판별을 위한 함수
     */
    private var destinationisSet : Bool = false
    
    init(delegate : SpecContract , viewcontroller : SpecViewController){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
    }
    
    func getcellid() -> [String]{
        return [destinationcellid,recentrecordcellid]
    }
    
    
    /**
     처음 화면 진입시 현재 좌표를 토대로 출발지를 셋하는 함수
     */
    func setIntialDepartureValue(){
        let currentlat = UserDefaults.standard.double(forKey: userstandard.lat.rawValue)
        let currentlon = UserDefaults.standard.double(forKey: userstandard.lon.rawValue)
        departcoordinatex = currentlon
        departcoordinatey = currentlat
        
        
        self.callreversegeocodeapi(posx: currentlon, posy: currentlat) { [unowned self] (addr, done) in
            if done {
                self.delegate?.setinitialDepartAddress(addr : addr)
            }
            else {
                
            }
        }
    }
    /**
     출발지 목적지가 동일한지 판별하는 함수
     */
    func isSameLocation() -> Bool {
        if destcoordinatex == departcoordinatex && destcoordinatey == departcoordinatey {
            return true
        }
        else {
            return false
            
        }
    }
    /**
    출발지가 설정 되어있는지를 셋하는 함수 맵에서 출발지 확인을 누르면 true로 바뀜
    */
    func setdepartureisSet(set : Bool){
        self.departureisSet = set
    }
    /**
    출발지가 설정 되어있는지 여뷰 확인 안되있으면 목적지 선택후 다시 출발지로 포커스가 감
    */
    func checkdepartureisSet() -> Bool {
        return self.departureisSet
    }
    
    /**
    목적지가 설정 되어있는지를 셋하는 함수 맵에서 출발지 확인을 누르면 true로 바뀜
    */
    func setdestisSet(set : Bool){
        self.destinationisSet = set
    }
    /**
    목적지가 설정 되어있는지 여뷰 확인 안되있으면 목적지 선택후 다시 출발지로 포커스가 감
    */
    func checkdestisSet() -> Bool {
        return self.destinationisSet
    }
    
    
    
    /**
     출발지 좌표를 받는 함수 (x,y)순
     */
    func getdepartcoordinate() -> [Double]{
        return [self.departcoordinatex,self.departcoordinatey]
    }
    /**
     목적지 좌표를 받는 함수 (x,y)순
     */
    func getdestcoordinate() -> [Double] {
        return [self.destcoordinatex,self.destcoordinatey]
    }
    
    
    /**
     주소 통합검색  api 함수
     
    - parameters :
      - keyword : 검색 키워드
      - type : 검색 타입(목적지, 도착지)
     */
    func callInaviGeneralSearchAPI(keyword : String , type : searchtype){
        FunctionClass.shared.showdialog(show: true)
        currentsearchtype = type
        let api = InaviSearchGeneralSearchAPI()
        currentkeyword = keyword
        api.request_To_server(keyword: keyword) { (done, data) in
            if done {
                print("data",data)
                let tempmodel = InaviGeneralSearchModel(json: data)
                InaviGeneralSearchRepo.shared.setmodel(input: tempmodel)
                let isempty = tempmodel.poi.count == 0 ? true : false
                self.delegate?.refreshcv(isempty: isempty, infolabelkeyword: keyword)
            }
            else {
                
            }
            FunctionClass.shared.showdialog(show: false)
        }
        
    }
    
    
    /**
     지도 이동시 리버스 지오코딩을 실행하는 하수
     
     - parameters :
      - posx : x좌표
      - posy : y좌표
      - addr : 콜백 결과 주소
      - done : 콜백 성공 여부
     */
    func callreversegeocodeapi(posx : Double, posy : Double, completion : @escaping(_ addr : String, _ done : Bool) -> ())  {
        let reversegeocodeapi = ReverGeoCodingAPI()
        
        if currentsearchtype == .departure {
            departcoordinatex = posx
            departcoordinatey = posy
        }
        else {
            destcoordinatex = posx
            destcoordinatey = posy
        }
        
        reversegeocodeapi.request_To_server(posx: posx, posy: posy) { (done, data) in
            if done {
                let tempmodel = ReverseGeoCodingModel(json: data)
                guard let adm = tempmodel.adm else {
                    print("spec presenter adm failed")
                    completion("",false)
                    return
                }
                
                guard let bldname = adm.bldname else {
                    print("spec presenter bldname failed")
                    completion("",true)
                    return
                }
                //빌딩이름이 없으면 도로명 주소 표출
                if bldname == "" {
                    guard let roadname = adm.roadname else {
                        print("spec presenter roadnamme failed")
                        completion("",true)
                        return
                    }
                    completion(roadname,true)
                    return
                }
                else {
                    //빌딩 이름이 있으면 빌딩명 표출
                    completion(bldname,true)
                }
            }
            else {
                completion("",false)
            }
        }
    }
    
    /**
     경로 탐색 api
     
     */
    func callInaviFindRouteAPI(completion : @escaping (Bool) -> ()){
        
        let api = InaviFindRouteAPI()
        
        api.request_To_server(startx: departcoordinatex, starty: departcoordinatey, endx: destcoordinatex, endy: destcoordinatey) { (done, data) in
            if done {
                print("CallWithDestPresenter findroute data",data["header"])
                let tempmodel = InaviRouteModel(json: data)
                InaviRouteRepo.shared.setmodel(input: tempmodel)
                if let header = InaviRouteRepo.shared.getmodel().header {
                    print("header.isSuccessful!",header.isSuccessful!)
                    completion(header.isSuccessful!)
                }
                else {
                    print("header failed")
                    completion(false)
                }
            }
            else {
                completion(false)
            }
        }
    }
    
    
    /**
     집 또는 회사가 등록 되어있는지 확인 하는 함수
     
     없으면 false,nil
     있으면 true, 모델
     */
    func checkModelExist(type : enlistmode) -> (Bool,HouseOfficeModel?){
        if type == .house {
            if let house = HouseOfficeRepo.shared.gethouse() {
                return (true,house)
            }
        }
        if type == .office {
            if let office = HouseOfficeRepo.shared.getoffice() {

                return (true,office)
            }
        }
        return (false , nil)
    }
    
    
    
    
    
    
    
}
extension SpecPresenter : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return InaviGeneralSearchRepo.shared.getpoi().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: destinationcellid, for: indexPath) as! AddressCell
        let model = InaviGeneralSearchRepo.shared.getpoi()[indexPath.row]
        if let title = model.name1, let lat = model.rpy, let lon = model.rpx, let poiid = model.poiid, let addr = model.address  {
            cell.settitle(title: title, keyword: currentkeyword)
            cell.setcoordinate(lat : lat , lon : lon)
            cell.setpoiid(poiid: poiid)
            cell.setaddresslabel(address: addr)
        }
        else {
            cell.settitle(title: "오류입니다", keyword: "오류입니다")
            cell.setcoordinate(lat : 0.0 , lon : 0.0)
            cell.setpoiid(poiid: 0)
            cell.setaddresslabel(address: "오류입니다")
        }
        return cell
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: mainboundwidth, height: 84)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    /**
     검색 리스트 셀 클릭시 맵으로 이동하기 위한 함수
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        let cell = collectionView.cellForItem(at: indexPath) as! AddressCell
        self.delegate?.showmapview(lat: cell.getcoordinate().lat, long: cell.getcoordinate().lon, type: currentsearchtype)
       
    }
    
    
    
}
