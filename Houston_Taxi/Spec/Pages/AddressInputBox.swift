//
//  AddressInputBox.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/16.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit



/**
목적지 도착지 인풋 박스
 */
class AddressInputBox: UIView {
    
    let focusedcolor = UIColor.white
    let defocusedcolor = UIColor.init(white: 1, alpha: 0.2)
    //배경 그라데이션 박스
    private var gradientbox = UIView()
    //
    private var dotlineimage = UIImageView()
    //출발지 인풋 필드
    private var departureinput = PaddingTextField()
    //출발지 클리어 버튼
    private var departurexbtn = WideTapBtn()
    
    //구분서
    private var divisionline = UIImageView()
    // 목적지 인풋 필드
    private var destinationinput = PaddingTextField()
    //목적지 클리어 버튼
    private var destinationxbtn = WideTapBtn()
    
    
    
    
    
    weak var delegate : AddressInputBoxDelegate?
    
    init(frame : CGRect, delegate : AddressInputBoxDelegate){
        super.init(frame: frame)
        self.delegate = delegate
        makegradientbox()
        makedotlineimage()
        makedivisionline()
        makedepartureinput()
        makedestinationinput()
        makedeparturexnbtn()
        makedestinationxbtn()
        
        departureinput.addTarget(self, action: #selector(textisEditing(textField:)), for: .allEditingEvents)
        destinationinput.addTarget(self, action: #selector(textisEditing(textField:)), for: .allEditingEvents)
        
        departureinput.clearsOnBeginEditing = true
        destinationinput.clearsOnBeginEditing = true
        
        departurexbtn.addTarget(self, action: #selector(inputfieldxbtnpressed(sender:)), for: .touchUpInside)
        destinationxbtn.addTarget(self, action: #selector(inputfieldxbtnpressed(sender:)), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        DispatchQueue.main.async { [weak self] in
            self?.gradientbox.layer.cornerRadius = 8
            self?.gradientbox.clipsToBounds = true
            self?.gradientbox.createGradientLayer(colors: .rgb(red: 40, green: 90, blue: 145, alpha: 1) , .rgb(red: 12, green: 161, blue: 155, alpha: 1), start: CGPoint(x: 0, y: 0.5), mid: CGPoint(x: 0.5, y: 0.5), end: CGPoint(x: 1, y: 0.5))
            self?.sendSubviewToBack(self!.gradientbox)
        }
    }

    
    /**
     텍스트 필드 인풋 에디팅 이벤트 감지 함수
     */
    @objc func textisEditing(textField : UITextField){
        if textField == departureinput {
            self.delegate?.setToMapPage(show : false)
            if textField.text?.count ?? 0 > 0 {
                departurexbtn.alpha = 1
            }
            else {
                departurexbtn.alpha = 0
            }
        }
        else {
            self.delegate?.setToMapPage(show: false)
            if textField.text?.count ?? 0 > 0 {
                destinationxbtn.alpha = 1

            }
            else {
                destinationxbtn.alpha = 0
            }
        }
    }
    
    /**
     돋보기 버튼 이벤트 함수
     */
    @objc func inputfieldxbtnpressed(sender :UIButton){

        if sender == destinationxbtn {
            destinationinput.resignFirstResponder()
            self.delegate?.searchDestination(keyword : destinationinput.text ?? "")
        }
        else{
            departureinput.resignFirstResponder()
            self.delegate?.searchDeparture(keyword : departureinput.text ?? "")
        }
    }
    
    /**
    출발지 인풋필드 에디팅 시 키보드의 done 버튼을 눌렀을때 호출되는 함수
    */
    @objc func departurekeyboarddonebtnpressed(){
        departureinput.resignFirstResponder()
        self.delegate?.searchDeparture(keyword : departureinput.text ?? "")
    }
    
    /**
     도착지 인풋필드 에디팅 시 키보드의 done 버튼을 눌렀을때 호출되는 함수
     */
    @objc func destinationdkeyboarddonebtnpressed(){
        destinationinput.resignFirstResponder()
        self.delegate?.searchDestination(keyword : destinationinput.text ?? "")
    }
    
    /**
    출발지 인풋필드의  placeholder를 셋하는 함수
    
    */
    func setdepartureplaceholder(text : String){
        let attplaceholder = NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor : UIColor.init(white: 1, alpha: 0.5)])
        departureinput.attributedPlaceholder = attplaceholder
    }
    
    /**
     도착지 인풋필드의  placeholder를 셋하는 함수
     
     */
    func setdestinationplaceholder(text : String){
        let attplaceholder = NSAttributedString(string: text, attributes: [NSAttributedString.Key.foregroundColor : UIColor.init(white: 1, alpha: 0.5)])
        destinationinput.attributedPlaceholder = attplaceholder
    }
    
    /**
     목적지 인풋 세팅 함수
     */
    func setdepartureinput(text : String){
        self.departureinput.text = text
    }
    /**
     출발지 인풋 세팅 함수
     */
    func setdestinationinput(text : String){
        self.destinationinput.text = text
    }
    /**
     텍스트 포커스 세팅 함수
     */
    func setfocus(type : searchtype){
        if type == .departure {
            departureinput.becomeFirstResponder()
        }
        else {
            destinationinput.becomeFirstResponder()
        }
    }
    
    /**
     맵으로 페이지가 변환 되었을때 인풋필드의 인터렉션을 막을지 안막을지 결정하는 함수
     맵 -> 인터렉션 불가
     리스트 -> 인터렉션 가능
     */
    func disablefirstresponder() {
        destinationinput.resignFirstResponder()
        departureinput.resignFirstResponder()
    }
    
    
    func gettexts() -> (depart : String , dest : String){
        return (self.departureinput.placeholder!, self.destinationinput.placeholder!)
    }
    
    
}
extension AddressInputBox : UITextFieldDelegate {
    func textFieldDidBeginEditing(_ textField: UITextField) {
        if textField == departureinput {
            destinationxbtn.alpha = 0
            destinationinput.text = ""
        }
        else {
            departurexbtn.alpha = 0
            departureinput.text = ""
        }
    }
    
    
    
    
    
}
extension AddressInputBox {
    private func makegradientbox(){
        self.addSubview(gradientbox)
        gradientbox.translatesAutoresizingMaskIntoConstraints = false
        gradientbox.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        gradientbox.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        gradientbox.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        gradientbox.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        
    }
    private func makedotlineimage(){
        self.addSubview(dotlineimage)
        dotlineimage.translatesAutoresizingMaskIntoConstraints = false
        dotlineimage.topAnchor.constraint(equalTo: self.topAnchor, constant: 20).isActive = true
        dotlineimage.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 17.5).isActive = true
        dotlineimage.widthAnchor.constraint(equalToConstant: 16).isActive = true
        dotlineimage.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -20).isActive = true
        dotlineimage.image = UIImage(named: "inputIcDot")
    }
    private func makedivisionline(){
        self.addSubview(divisionline)
        divisionline.translatesAutoresizingMaskIntoConstraints = false
        divisionline.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        divisionline.leadingAnchor.constraint(equalTo: dotlineimage.trailingAnchor, constant: 12.5).isActive = true
        divisionline.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -20).isActive = true
        divisionline.heightAnchor.constraint(equalToConstant: 1).isActive = true
        divisionline.backgroundColor = .init(white: 1, alpha: 0.2)
    }
    private func makedepartureinput(){
        self.addSubview(departureinput)
        departureinput.translatesAutoresizingMaskIntoConstraints = false
        departureinput.bottomAnchor.constraint(equalTo: divisionline.topAnchor, constant: -13).isActive = true
        departureinput.leadingAnchor.constraint(equalTo: divisionline.leadingAnchor, constant: 5).isActive = true
        departureinput.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -25).isActive = true
        departureinput.heightAnchor.constraint(equalToConstant: 23).isActive = true
        let attplaceholder = NSAttributedString(string: "출발지", attributes: [NSAttributedString.Key.foregroundColor : UIColor.init(white: 1, alpha: 0.5)])
        departureinput.attributedPlaceholder = attplaceholder
        departureinput.font = UIFont(name: fonts.medium.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .medium)
        departureinput.delegate = self
        departureinput.textColor = .white
        departureinput.addDoneOnKeyboardWithTarget(self, action: #selector(departurekeyboarddonebtnpressed), titleText: "검색")
        
    }
    private func makedeparturexnbtn(){
        self.addSubview(departurexbtn)
        departurexbtn.translatesAutoresizingMaskIntoConstraints = false
        departurexbtn.centerYAnchor.constraint(equalTo: departureinput.centerYAnchor, constant: 0).isActive = true
        departurexbtn.trailingAnchor.constraint(equalTo: gradientbox.trailingAnchor, constant: -21).isActive = true
        departurexbtn.widthAnchor.constraint(equalToConstant: 20).isActive = true
        departurexbtn.heightAnchor.constraint(equalToConstant: 20).isActive = true
        departurexbtn.setImage(UIImage(named: "input_btn_search_n"), for: .normal)
        departurexbtn.alpha = 1
        departurexbtn.imageView?.contentMode = .scaleAspectFit
    }
    private func makedestinationinput(){
        self.addSubview(destinationinput)
        destinationinput.translatesAutoresizingMaskIntoConstraints = false
        destinationinput.topAnchor.constraint(equalTo: divisionline.bottomAnchor, constant: 13).isActive = true
        destinationinput.leadingAnchor.constraint(equalTo: divisionline.leadingAnchor, constant: 5).isActive = true
        destinationinput.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -25).isActive = true
        destinationinput.heightAnchor.constraint(equalToConstant: 23).isActive = true
        let attplaceholder = NSAttributedString(string: "목적지", attributes: [NSAttributedString.Key.foregroundColor : UIColor.init(white: 1, alpha: 0.5)])
        destinationinput.attributedPlaceholder = attplaceholder
        destinationinput.font = UIFont(name: fonts.medium.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .medium)
        destinationinput.delegate = self
        destinationinput.textColor = .white
        destinationinput.addDoneOnKeyboardWithTarget(self, action: #selector(destinationdkeyboarddonebtnpressed), titleText: "검색")
    }
    private func makedestinationxbtn(){
        self.addSubview(destinationxbtn)
        destinationxbtn.translatesAutoresizingMaskIntoConstraints = false
        destinationxbtn.centerYAnchor.constraint(equalTo: destinationinput.centerYAnchor, constant: 0).isActive = true
        destinationxbtn.trailingAnchor.constraint(equalTo: gradientbox.trailingAnchor, constant: -21).isActive = true
        destinationxbtn.widthAnchor.constraint(equalToConstant: 20).isActive = true
        destinationxbtn.heightAnchor.constraint(equalToConstant: 20).isActive = true
        destinationxbtn.setImage(UIImage(named: "input_btn_search_n"), for: .normal)
        destinationxbtn.alpha = 1
        destinationxbtn.imageView?.contentMode = .scaleAspectFit
        
        
        
    }
    
    
    
}
