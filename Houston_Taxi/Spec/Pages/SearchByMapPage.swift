//
//  SearchByMapPage.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/17.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
import iNaviMaps


/**
지도로 검색하기 페이지
 */
class SearchByMapPage: UIView {
    
    private var xbtn = WideTapBtn()

    lazy var mapView = InaviMapView(frame: .zero)
    
    //현재 위치 버튼
    private var currentlocationBtn = UIButton()
    //출발지로 선태 버튼
    private var departurebtn = ShadowBtn()
    //도착지로 선택 버튼
    private var destinationbtn = ShadowBtn()
    //출발지 지정 아이콘
    private var departuremapicon = UIImageView()
    //도착지 지정 아이콘
    private var destinationmapicon = UIImageView()
    
    private var currentsearchtype : searchtype = .departure
    
    
    weak var delegate : SearchByMapPageDelegate?
    init(frame: CGRect, delegate : SearchByMapPageDelegate ) {
        super.init(frame: frame)
        self.delegate = delegate
        makemapview()
        makexbtn()
        makedeparturebtn()
        makedestinationbtn()
        makecurrentlocationbtn()
        
        makedeparturemapicon()
        makedestinationmapicon()
        xbtn.addTarget(self, action: #selector(xbtnpressed(sender:)), for: .touchUpInside)
        
        
        departurebtn.addTarget(self, action: #selector(departurebtnpressed(sender:)), for: .touchUpInside)
        destinationbtn.addTarget(self, action: #selector(destinationbtnpressed(sender:)), for: .touchUpInside)
        
        currentlocationBtn.addTarget(self, action: #selector(currentlocationbtnpressed(sender:)), for: .touchUpInside)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        DispatchQueue.main.async { [weak self] in
            self?.departurebtn.layer.cornerRadius = 29
            self?.destinationbtn.layer.cornerRadius = 29
        }
    }
    
    /**
      xbtn 이벤트 함수 -> 리스트 사라지고 맵이 표출됨
     */
    @objc func xbtnpressed(sender : UIButton){
        self.delegate?.setToMapPage(show : false)
    }
    
    /**
      출발지 버튼 이벤트 함수
     
     다시 리스트로 전환되며 목적지 인풋에 주소 입력
     */
    @objc func departurebtnpressed(sender : UIButton){
        let posx = mapView.cameraPosition.target.lng
        let posy = mapView.cameraPosition.target.lat
        
        self.delegate?.setdepartureFromMappage(posx: posx, posy: posy)
    }
    
    /**
     목적 버튼 이벤트 함수
     */
    @objc func destinationbtnpressed(sender : UIButton){
        let posx = mapView.cameraPosition.target.lng
        let posy = mapView.cameraPosition.target.lat
        self.delegate?.setdestfrommappage(posx: posx, posy: posy)
    }
    
    /**
     지도 처음 진입시 타입 및 위치 설정 함수
     
    - parameter :
    - lat : 위도
    - lon : 경도
    - type : 타입
     */
    func setinitialpos(lat : Double, lon : Double , type : searchtype){
        mapView.moveCamera(INVCameraUpdate(targetTo: INVLatLng(lat: lat, lng: lon)))
        currentsearchtype = type
        if type == .destination {
            departurebtn.isHidden = true
            destinationbtn.isHidden = false
        }
        else {
            departurebtn.isHidden = false
            destinationbtn.isHidden = true
        }
        
    }
    /**
     현재 위치 버튼 이벤트 함수
     */
    @objc func currentlocationbtnpressed(sender : UIButton){
        
        let lat = UserDefaults.standard.double(forKey: userstandard.lat.rawValue)
        let lon = UserDefaults.standard.double(forKey: userstandard.lon.rawValue)
        mapView.moveCamera(INVCameraUpdate(targetTo: INVLatLng(lat: lat, lng: lon)))
    }
    
}
extension SearchByMapPage : INVMapViewDelegate {
    
    func mapViewDidBecomeIdle(_ mapView: InaviMapView) {
        if self.alpha != 0 {
            self.delegate?.mapViewBecameIdle(posx : mapView.cameraPosition.target.lng , posy : mapView.cameraPosition.target.lat,type : currentsearchtype)
        }
    }
    
    
}
extension SearchByMapPage {
    private func makemapview(){
        self.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        mapView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        mapView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        mapView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        mapView.delegate = self
        mapView.userTrackingMode = .noTracking
    }
    private func makexbtn(){
        self.addSubview(xbtn)
        xbtn.translatesAutoresizingMaskIntoConstraints = false
        xbtn.topAnchor.constraint(equalTo: self.topAnchor, constant: SplashViewController.topinset + 13).isActive = true
        xbtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -13).isActive = true
        xbtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        xbtn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        xbtn.setImage(UIImage(named: "titleBtnCloseN"), for: .normal)
    }
    private func makedeparturebtn(){
        self.addSubview(departurebtn)
        departurebtn.translatesAutoresizingMaskIntoConstraints = false
        departurebtn.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -13 - SplashViewController.bottominset).isActive = true
        departurebtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        departurebtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        departurebtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        departurebtn.backgroundColor = .white
        departurebtn.shadowcolor = .black
        departurebtn.shadowoffset = CGSize(width: 0, height: 2)
        departurebtn.shadowradius = 4
        departurebtn.shadowopacity = 0.2
        
        departurebtn.setTitle("출발지로 선택", for: .normal)
        departurebtn.setTitleColor(.rgb(red: 60, green: 60, blue: 60, alpha: 1), for: .normal)
        departurebtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20) //.systemFont(ofSize: 20, weight: .bold)
        
    }
    private func makedestinationbtn(){
        self.addSubview(destinationbtn)
        destinationbtn.translatesAutoresizingMaskIntoConstraints = false
        destinationbtn.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -13 - SplashViewController.bottominset).isActive = true
        destinationbtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        destinationbtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        destinationbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        destinationbtn.backgroundColor = .white
        destinationbtn.shadowcolor = .black
        destinationbtn.shadowoffset = CGSize(width: 0, height: 2)
        destinationbtn.shadowradius = 4
        destinationbtn.shadowopacity = 0.2
        
        destinationbtn.setTitle("도착지로 선택", for: .normal)
        destinationbtn.setTitleColor(.rgb(red: 226, green: 83, blue: 33, alpha: 1), for: .normal)
        destinationbtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20) //.systemFont(ofSize: 20, weight: .bold)
    }
    private func makecurrentlocationbtn(){
        self.addSubview(currentlocationBtn)
        currentlocationBtn.translatesAutoresizingMaskIntoConstraints = false
        currentlocationBtn.bottomAnchor.constraint(equalTo: destinationbtn.topAnchor, constant: 0).isActive = true
        currentlocationBtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 3).isActive = true
        currentlocationBtn.widthAnchor.constraint(equalToConstant: 54).isActive = true
        currentlocationBtn.heightAnchor.constraint(equalToConstant: 54).isActive = true
        currentlocationBtn.setImage(UIImage(named: "mapBtnMy"), for: .normal)
    }
    private func makedeparturemapicon(){
        self.addSubview(departuremapicon)
        departuremapicon.translatesAutoresizingMaskIntoConstraints = false
        departuremapicon.bottomAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        departuremapicon.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        departuremapicon.widthAnchor.constraint(equalToConstant: 58).isActive = true
        departuremapicon.heightAnchor.constraint(equalToConstant: 38).isActive = true
        departuremapicon.image = UIImage(named: "mapPinDep")
    }
    private func makedestinationmapicon(){
        self.addSubview(destinationmapicon)
        destinationmapicon.translatesAutoresizingMaskIntoConstraints = false
        destinationmapicon.bottomAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        destinationmapicon.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        destinationmapicon.widthAnchor.constraint(equalToConstant: 58).isActive = true
        destinationmapicon.heightAnchor.constraint(equalToConstant: 38).isActive = true
        destinationmapicon.image = UIImage(named: "mapPinArr")
        destinationmapicon.alpha = 0
    }
}
