//
//  SearchByListPage.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/16.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit




class SearchByListPage : UIView {
    
    private var xbtn = WideTapBtn()
    //집 버튼
    private var homebtn = HomeOfficeBtn()
    //회사 버튼
    private var officebtn = HomeOfficeBtn()
    //검색 리스트
    lazy var cv : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        
        return cv
    }()
    
    //최근 기록 삭제 버튼
    private var deleterecentrecordbtn = DeleteRecordBtn()
    
    //검색 결과 없을시 라벨
    private var infolabel = PaddingLabel()
    //검색 버튼
    private var researchbtn = UIButton()
    
    
    weak var delegate : SearchByListPageDelegate?
    
    init(frame : CGRect, delegate : SearchByListPageDelegate, datasource : UICollectionViewDataSource, cvdelegate : UICollectionViewDelegate,cellid : [String]){
        super.init(frame: frame)
        self.delegate = delegate
        self.cv.dataSource = datasource
        self.cv.delegate = cvdelegate
        self.cv.register(AddressCell.self, forCellWithReuseIdentifier: cellid[0])
        makexbtn()
        makehomebtn()
        makeofficebtn()
        makecv()
        makeresearchbtn()
        makeinfolabel()
        makedeleterecentrecordbtn()
        
        deleterecentrecordbtn.addTarget(self, action: #selector(deleterecentrecordbtnpressed(sender:)), for: .touchUpInside)
        xbtn.addTarget(self, action: #selector(xbtnpressed(sender:)), for: .touchUpInside)
        
        homebtn.addTarget(self, action: #selector(homebtnpressed(sender:)), for: .touchUpInside)
        officebtn.addTarget(self, action: #selector(officebtnpressed(sender:)), for: .touchUpInside)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        DispatchQueue.main.async { [weak self] in
            self?.researchbtn.layer.cornerRadius = 8
        }
    }
    
    /**
     최근 기록 삭제 버튼 눌렀을때 페이지 이동하는 함수
     */
    @objc func deleterecentrecordbtnpressed(sender : UIButton){
        self.delegate?.showRemoveRecentRecordViewController()
    }
    
    /**
        백버튼 이벤트 함수 -> 리스트 사라지고 맵이 표출됨
     */
    @objc func xbtnpressed(sender : UIButton){
        self.delegate?.popviewController()
    }
    
    /**
     주조 리스트를 리프레시 하는 함수
     */
    func refreshcv(isempty : Bool, infolabelkeyword : String?){
        if isempty {
            self.cv.alpha = 0
            self.researchbtn.alpha = 0
            self.infolabel.alpha = 1
        }
        else {
            self.cv.alpha = 1
            self.researchbtn.alpha = 0
            self.infolabel.alpha = 0
            self.cv.reloadData()
        }
        
    }
    
    
    /**
     집 버튼 이벤트 함수
     */
    @objc func homebtnpressed(sender : UIButton){
        self.delegate?.homebtnpressed()
    }
    /**
     회사 버튼 이벤트 함수
     */
    @objc func officebtnpressed(sender : UIButton){
        self.delegate?.officebtnpressed()
    }
}
extension SearchByListPage {
    private func makexbtn(){
        self.addSubview(xbtn)
        xbtn.translatesAutoresizingMaskIntoConstraints = false
        xbtn.topAnchor.constraint(equalTo: self.safeAreaLayoutGuide.topAnchor, constant: 13).isActive = true
        xbtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        xbtn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        xbtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        xbtn.setImage(UIImage(named: "titleBtnBackN"), for: .normal)
    }
    private func makehomebtn(){
        self.addSubview(homebtn)
        homebtn.translatesAutoresizingMaskIntoConstraints = false
        homebtn.topAnchor.constraint(equalTo: xbtn.bottomAnchor, constant: 137).isActive = true
        homebtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        homebtn.trailingAnchor.constraint(equalTo: self.centerXAnchor, constant: -3.5).isActive = true
        homebtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        homebtn.setimage(image: UIImage(named: "dtIcHomeN")!)
        homebtn.setlabel(label: "집")
    }
    private func makeofficebtn(){
        self.addSubview(officebtn)
        officebtn.translatesAutoresizingMaskIntoConstraints = false
        officebtn.topAnchor.constraint(equalTo: xbtn.bottomAnchor, constant: 137).isActive = true
        officebtn.leadingAnchor.constraint(equalTo: self.centerXAnchor, constant: 3.5).isActive = true
        officebtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        officebtn.heightAnchor.constraint(equalToConstant: 40).isActive = true
        officebtn.setimage(image: UIImage(named: "dtIcOfficeN")!)
        officebtn.setlabel(label: "회사")
    }
    private func makecv(){
        self.addSubview(cv)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.topAnchor.constraint(equalTo: homebtn.bottomAnchor, constant: 4).isActive = true
        cv.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        cv.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        cv.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        cv.backgroundColor = .white
        cv.alpha = 1
        cv.keyboardDismissMode = .onDrag
    }
    private func makeresearchbtn(){
        self.addSubview(researchbtn)
        researchbtn.translatesAutoresizingMaskIntoConstraints = false
        researchbtn.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        researchbtn.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        researchbtn.widthAnchor.constraint(equalToConstant: 120).isActive = true
        researchbtn.heightAnchor.constraint(equalToConstant: 34).isActive = true
        researchbtn.backgroundColor = .white
        researchbtn.setTitle("검색", for: .normal)
        researchbtn.setTitleColor(.rgb(red: 100, green: 100, blue: 100, alpha: 1), for: .normal)
        researchbtn.titleLabel?.font = UIFont(name: fonts.medium.rawValue, size: 16) //.systemFont(ofSize: 16, weight: .medium)
        researchbtn.layer.borderColor = UIColor.rgb(red: 100, green: 100, blue: 100, alpha: 1).cgColor
        researchbtn.layer.borderWidth = 1
        researchbtn.alpha = 0
    }
    private func makeinfolabel(){
        self.addSubview(infolabel)
        infolabel.translatesAutoresizingMaskIntoConstraints = false
        infolabel.bottomAnchor.constraint(equalTo: researchbtn.topAnchor, constant: -14).isActive = true
        infolabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        infolabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        infolabel.heightAnchor.constraint(lessThanOrEqualToConstant: 50).isActive = true
        infolabel.numberOfLines = 0
        infolabel.lineBreakMode = .byWordWrapping
        infolabel.textColor = .rgb(red: 184, green: 184, blue: 184, alpha: 1)
        infolabel.font = UIFont(name: fonts.medium.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .medium)
        infolabel.textAlignment = .center
        infolabel.text = "검색 결과가 없습니다.\n강남역 (으)로 다시 검색할까요?"
        infolabel.alpha = 0
    }
    
    private func makedeleterecentrecordbtn(){
        self.addSubview(deleterecentrecordbtn)
        deleterecentrecordbtn.translatesAutoresizingMaskIntoConstraints = false
        deleterecentrecordbtn.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        deleterecentrecordbtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        deleterecentrecordbtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        deleterecentrecordbtn.heightAnchor.constraint(equalToConstant: 55).isActive = true
        deleterecentrecordbtn.isHidden = true
    }
}
