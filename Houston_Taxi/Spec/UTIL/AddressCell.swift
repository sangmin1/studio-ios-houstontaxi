//
//  AddressCell.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/16.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 주소 검색 결과 셀
 */
class AddressCell: UICollectionViewCell {
    
    //이름
    private var titlelabel = PaddingLabel()
    //상세주소
    private var addresslabel = PaddingLabel()
    //택시 아이콘
    private var icon = UIImageView()
    
    
    
    lazy private var labelstack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [titlelabel,addresslabel])
        stack.axis = .vertical
        stack.spacing = 4
        return stack
    }()
    
    
    private var underline = UIImageView()
    
    /**
     해당 주소 위도경도
     */
    private var lat : Double = 0
    private var lon : Double = 0
    
    /**
      poiid
     */
    private var poiid : Int = 0
    
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        makelabestack()
        makeicon()
        makeunderline()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func prepareForReuse() {
        super.prepareForReuse()
        self.addresslabel.isHidden = false
    }
    
    /**
     검색 결과  지명을 셋하는 함수
     
    - paramter :
    - title : 지명
    - keyword : 색칠될 키워드
     */
    func settitle(title : String,keyword : String){
        let attr = NSMutableAttributedString(string: title, attributes: [NSAttributedString.Key.font : UIFont.systemFont(ofSize: 20, weight: .medium), NSAttributedString.Key.foregroundColor : UIColor.rgb(red: 100, green: 100, blue: 100, alpha: 1)])
        let range = NSString(string: title).range(of: keyword)
        attr.addAttribute(NSAttributedString.Key.foregroundColor, value: maincolor, range: range)
        self.titlelabel.attributedText = attr
    }
    
    /**
     키워드 가 없는 지명 를 세하는 함수
     */
    func settitle(title : String){
        self.titlelabel.text = title
    }
     
    /**
      검색 결과 주소를 셋하는 함수
     */
    func setaddresslabel(address : String?){
        if let address = address {
            self.addresslabel.text = address
        }
        else {
            self.addresslabel.isHidden = true
        }
        
    }
    
    func gettitle() -> String {
        return self.titlelabel.text ?? ""
    }
    
    func getaddress() -> String? {
        return self.addresslabel.text
    }
    
    /**
     아이콘 이미지 셋하는 함수
     */
    func seticon(image : UIImage){
        self.icon.image = image
    }
    
    /**
     위도 경도 세팅 함수
     */
    func setcoordinate(lat : Double , lon : Double){
        self.lat = lat
        self.lon = lon
    }
    
    func getcoordinate() -> (lat : Double , lon : Double) {
        return (lat,lon)
    }
    
    func setpoiid(poiid : Int){
        self.poiid = poiid
    }
    func getpoiid() -> Int {
        return self.poiid
    }
    
    

}
extension AddressCell {
    private func makelabestack(){
        self.addSubview(labelstack)
        labelstack.translatesAutoresizingMaskIntoConstraints = false
        labelstack.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        labelstack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 24).isActive = true
        labelstack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 80).isActive = true
        labelstack.heightAnchor.constraint(lessThanOrEqualToConstant: 47).isActive = true
        
        NSLayoutConstraint(item: titlelabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 26).isActive = true
        titlelabel.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        titlelabel.font = UIFont(name: fonts.medium.rawValue, size: 20)//.systemFont(ofSize: 20, weight: .medium)
        titlelabel.text = "바비레드 강남본점"
        
        NSLayoutConstraint(item: addresslabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 21).isActive = true
        
        addresslabel.textColor = .rgb(red: 184, green: 184, blue: 184, alpha: 1)
        addresslabel.font = UIFont(name: fonts.medium.rawValue, size: 16)//.systemFont(ofSize: 16, weight: .medium)
        addresslabel.text = "서울 강남구 봉은사로6길 39"
        //addresslabel.isHidden = true
    }
    private func makeicon(){
        self.addSubview(icon)
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        icon.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -22).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 30).isActive = true
        icon.heightAnchor.constraint(equalToConstant: 30).isActive = true
        icon.image = UIImage(named: "6")
    }
    private func makeunderline(){
        self.addSubview(underline)
        underline.translatesAutoresizingMaskIntoConstraints = false
        underline.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        underline.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        underline.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        underline.heightAnchor.constraint(equalToConstant: 1).isActive = true
        underline.backgroundColor = .rgb(red: 0, green: 0, blue: 0, alpha: 0.05)
    }
}
