//
//  DeleteRecordBtn.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/17.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 최근 기록 삭제 버튼 컴포넌트
 */
class DeleteRecordBtn: UIButton {
    
    private var image = UIImageView()
    
    private var label = PaddingLabel()
    
    
    lazy private var stack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [image,label])
        stack.axis = .horizontal
        stack.spacing = 4
        return stack
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .rgb(red: 0, green: 0, blue: 0, alpha: 0.6)
        makestack()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.bounds.contains(point) {
            return self
        }
        else {
            return nil
        }
    }
    

    func setimage(image : UIImage){
        self.image.image = image
    }
    func setlabel(label : String){
        self.label.text = label
    }
    
    
    
}
extension DeleteRecordBtn {
    private func makestack(){
        self.addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        stack.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        stack.widthAnchor.constraint(lessThanOrEqualToConstant: 300).isActive = true
        stack.heightAnchor.constraint(equalToConstant: 22).isActive = true
        
        
        NSLayoutConstraint(item: image, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .width, multiplier: 1, constant: 22).isActive = true
        
        NSLayoutConstraint(item: image, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 22).isActive = true
        
        image.image = UIImage(named: "menuIcTrashN")
        
        label.textColor = .white
        label.font = UIFont(name: fonts.medium.rawValue, size: 20) //.systemFont(ofSize: 20, weight: .medium)
        label.text = "최근 기록 삭제"
    }
    
}
