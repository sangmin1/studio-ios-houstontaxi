//
//  HomeOfficebtn.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/16.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


/**
  집 회사 커스텀 버튼
*/
class HomeOfficeBtn: UIButton {
    
    private var image = UIImageView()
    
    private var label = PaddingLabel()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        makeimage()
        makelabel()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async { [unowned self] in
            self.layer.borderColor = UIColor.rgb(red: 0, green: 0, blue: 0, alpha: 0.1).cgColor
            self.layer.borderWidth = 1
            self.layer.cornerRadius = 20
        }
    }
    func setimage(image : UIImage){
        self.image.image = image
    }
    func setlabel(label : String){
        self.label.text = label
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.bounds.contains(point) {
            return self
        }
        else {
            return nil
        }
    }
    
}
extension HomeOfficeBtn {
    private func makeimage(){
        self.addSubview(image)
        image.translatesAutoresizingMaskIntoConstraints = false
        image.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        image.trailingAnchor.constraint(equalTo: self.centerXAnchor, constant: -4).isActive = true
        image.widthAnchor.constraint(equalToConstant: 20).isActive = true
        image.heightAnchor.constraint(equalToConstant: 20).isActive = true
    }
    private func makelabel(){
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        label.leadingAnchor.constraint(equalTo: self.centerXAnchor, constant: 4).isActive = true
        label.widthAnchor.constraint(equalToConstant: 50).isActive = true
        label.heightAnchor.constraint(equalToConstant: 23).isActive = true
        label.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        label.font = UIFont(name: fonts.medium.rawValue, size: 18)//.systemFont(ofSize: 18, weight: .medium)
        
    }
}
