//
//  InaviGeneralSearchRepo.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/30.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation

public class InaviGeneralSearchRepo {
        
    static let shared = InaviGeneralSearchRepo()
    private init(){}

    private var model : InaviGeneralSearchModel?

    func setmodel(input : InaviGeneralSearchModel){
        self.model = input
    }
    func getmodel() -> InaviGeneralSearchModel {
        return self.model!
    }
    
    func getpoi() -> [InaviGeneralSearchModel.Poi] {
        guard let model = self.model else {
            return []
            
        }
        return model.poi
    }
    
    func removeall(){
        self.model?.poi.removeAll()
    }
}
