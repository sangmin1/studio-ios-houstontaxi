//
//  InaviGeneralSearchModel.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/30.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import SwiftyJSON


class InaviGeneralSearchModel: NSObject {
    
    
    required init(json : JSON){
        
        search = Search(json: json)
        
        for model in json["search"]["poi"].arrayValue {
            poi.append(Poi(json: model))
        }
        
        
    }
    
    var search : Search?
    
    var poi : [Poi] = []
    
    
    
    
    internal class Search : NSObject {
        required init(json : JSON){
            poicount = json["poicount"].int

            
        }
        var poicount : Int?
    }
    
    internal class Poi : NSObject {
        
        required init(json : JSON){
            poiid = json["poiid"].int
            rpx = Double(json["rpx"].stringValue)
            rpy = Double(json["rpy"].stringValue)
            dpx = Double(json["dpx"].stringValue)
            dpy = Double(json["dpy"].stringValue)
            
            name1 = json["name1"].string
            name2 = json["name2"].string
            name3 = json["name3"].string
            name4 = json["name4"].string
            
            address = json["address"].string
            
            jibun = json["jibun"].string
            
        }
        
        var poiid : Int?
        var rpx : Double?
        var rpy : Double?
        var dpx : Double?
        var dpy : Double?
        
        var name1 : String?
        var name2 : String?
        var name3 : String?
        var name4 : String?
        
        var address : String?
        
        var jibun : String?
        
        
    }
    
    
    
}
