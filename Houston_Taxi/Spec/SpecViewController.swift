//
//  SpecViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/16.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 상세 페이지 - 목적지, 도착지 설정
 */
class SpecViewController: UIViewController {
    
    
    lazy private var addressinputbox = AddressInputBox(frame: .zero, delegate: self)
    
    
    lazy private var searchbylistpage = SearchByListPage(frame: .zero, delegate: self, datasource: presenter, cvdelegate: presenter, cellid: presenter.getcellid())
    
    
    lazy private var searchbymappage = SearchByMapPage(frame: .zero, delegate: self)
    
    lazy private var presenter = SpecPresenter(delegate: self, viewcontroller: self)
    
    lazy private var enlistpopup = EnlistHouseAndOfficePopUP(frame: self.view.frame, delegate: self)
    
    lazy private var samelocationpopup = SameLocationPopUP(frame: self.view.frame)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        self.hideKeyboardWhenTappedAround()
        makeaddresinputbox()
        makesearchbylistpage()
        makesearchbymappage()
        
        self.view.bringSubviewToFront(addressinputbox)
        presenter.setIntialDepartureValue()
        addressinputbox.setfocus(type: .destination)
        self.view.addSubview(enlistpopup)
        enlistpopup.alpha = 0
        self.view.addSubview(samelocationpopup)
        samelocationpopup.alpha = 0
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        InaviGeneralSearchRepo.shared.removeall()
        
    }
    
}
extension SpecViewController : SpecContract {
    func refreshcv(isempty : Bool, infolabelkeyword : String?) {
        searchbylistpage.refreshcv(isempty : isempty, infolabelkeyword : infolabelkeyword)
    }
    
    /**
     리스트 선택시 맵으로 뷰를 전환해주는 함수
     
    - parameter :
    - lat : 위도
    - lon : 경도
    - type : 타입
     */
    func showmapview(lat : Double, long : Double, type : searchtype) {
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.searchbymappage.alpha = 1
            self.searchbylistpage.alpha = 0
        }
        
        searchbymappage.setinitialpos(lat : lat, lon : long , type : type)
        //맵으로 이동 되어서 주소창의 인터렉션을 블락킹 한다.
        //addressinputbox.disableinputinteraction(disable : true)
    }
    func setinitialDepartAddress(addr : String){
        addressinputbox.setdepartureplaceholder(text: addr)
    }
}
extension SpecViewController : AddressInputBoxDelegate {
    /**
    addressinput box 컴포넌트에서
      사용자가 출발지 검색을 했을때
     */
    func searchDeparture(keyword: String) {
        presenter.callInaviGeneralSearchAPI(keyword: keyword, type: .departure)
    }
    /**
     addressinput box 컴포넌트에서
     사용자가 도착지 검색을 했을때
     */
    func searchDestination(keyword: String) {
        presenter.callInaviGeneralSearchAPI(keyword: keyword, type: .destination)
    }
    
    /**
     리스트 형태의 검색 페이지를 보여줄지
      맵 형태의 검색 페이지를 보여줄지 여부
     */
    func setToMapPage(show : Bool){
        if show {
            //맵으로 이동 되어서 주소창의 인터렉션을 블락킹 한다.
            //addressinputbox.disableinputinteraction(disable : f)
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.searchbymappage.alpha = 1
                self.searchbylistpage.alpha = 0
            }
        }
        else {
            //리스트로 이동되어서 주소창의 인터렉션을 허용 한다.
            //addressinputbox.disableinputinteraction(disable : false)
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.searchbymappage.alpha = 0
                self.searchbylistpage.alpha = 1
            }
        }
        
    }
}
extension SpecViewController : SearchByListPageDelegate{
    
    /**
      최근 기록 삭제 뷰로 넘어가는 함수 (현재 사용 안함)
     */
    func showRemoveRecentRecordViewController(){
        let view = RemoveRecentRecordViewController()
        self.navigationController?.pushViewController(view, animated: true)
    }
    /**
     리스트 검색 페이지에서 백버튼 눌렀을때 실행 되는 함수
     */
    func popviewController(){
        self.navigationController?.popViewController(animated: true)
    }
    /**
     집 버튼을 터치했을때 바로 택시 호출 화면으로 넘어가는 함수
     */
    func homebtnpressed(){
        let resultbool : Bool = presenter.checkModelExist(type : .house).0
        let resultmodel : HouseOfficeModel? = presenter.checkModelExist(type : .house).1
        addressinputbox.disablefirstresponder()
        if resultbool {
            if let model = resultmodel {
                let endx = model.lon
                let endy = model.lat
                //addressinputbox.disableinputinteraction(disable : false)
                addressinputbox.setdestinationinput(text: model.address)
                addressinputbox.setdestinationplaceholder(text: model.address)
                self.showmapview(lat: endy, long: endx, type: .destination)
            }
        }
        else {
            self.enlistpopup.settype(type: .house)
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.view.bringSubviewToFront(self.enlistpopup)
                self.enlistpopup.alpha = 1
            }
        }
    }
    /**
    회사 버튼을 터치했을때 바로 택시 호출 화면으로 넘어가는 함수
    */
    func officebtnpressed(){
        let resultbool : Bool = presenter.checkModelExist(type : .office).0
        let resultmodel : HouseOfficeModel? = presenter.checkModelExist(type : .office).1
        addressinputbox.disablefirstresponder()
        if resultbool {
            if let model = resultmodel {
                let endx = model.lon
                let endy = model.lat
                //addressinputbox.disableinputinteraction(disable : false)
                addressinputbox.setdestinationinput(text: model.address)
                addressinputbox.setdestinationplaceholder(text: model.address)
                self.showmapview(lat: endy, long: endx, type: .destination)
                
                

            }
        }
        else {
            
            self.enlistpopup.settype(type: .office)
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.view.bringSubviewToFront(self.enlistpopup)
                self.enlistpopup.alpha = 1
            }
        }
    }
    
}
extension SpecViewController : SearchByMapPageDelegate{
    /**
      맵 페이지에서 출발지로 선택버튼 눌렀을때 실행되는 함수
     
     - parameters :
     - posx : x좌표
     - posy : y좌표
     */
    func setdepartureFromMappage(posx : Double, posy : Double) {
        //리스트로 이동되어서 주소창의 인터렉션을 허용 한다.
        //addressinputbox.disableinputinteraction(disable : false)
        UIView.animate(withDuration: 0.2) { [unowned self]  in
            self.searchbymappage.alpha = 0
            self.searchbylistpage.alpha = 1
        }
       
        presenter.callreversegeocodeapi(posx: posx, posy: posy) { [unowned self] (addr, done) in
            if done {
                self.addressinputbox.setdepartureplaceholder(text : addr)
                self.addressinputbox.setfocus(type : .destination)
                self.presenter.setdepartureisSet(set : true)
                if self.presenter.checkdestisSet() == false {
                    InaviGeneralSearchRepo.shared.removeall()
                    self.searchbylistpage.refreshcv(isempty: false, infolabelkeyword: nil)
                    self.addressinputbox.setfocus(type : .destination)
                }
                else if self.presenter.isSameLocation() == true {
                    UIView.animate(withDuration: 0.2) { [unowned self] in
                        self.samelocationpopup.alpha = 1
                    }
                }
                else{
                    //화면을 넘어갈때 경로 탐색 api를 불러서 미리 경로를 받아온다
                    self.presenter.callInaviFindRouteAPI { (done) in
                        if done {
                            let view = CallWithDestViewController(departname: self.addressinputbox.gettexts().depart, destname: self.addressinputbox.gettexts().dest,startx: self.presenter.getdepartcoordinate()[0], starty: self.presenter.getdepartcoordinate()[1], endx: self.presenter.getdestcoordinate()[0], endy: self.presenter.getdestcoordinate()[1])
                            self.navigationController?.pushViewController(view, animated: true)
                        }
                    }
                }
            }
            else {
                self.presenter.setdepartureisSet(set : false)
                self.addressinputbox.setdepartureinput(text : "출발지를 다시 설정해주세요")
            }
        }
    }
    /**
     맵 페이지에서 목적지 선택 눌렀을때 실행되는 함수
     */
    func setdestfrommappage(posx : Double, posy : Double) {
        //리스트로 이동되어서 주소창의 인터렉션을 허용 한다.
        //addressinputbox.disableinputinteraction(disable : false)
        presenter.callreversegeocodeapi(posx: posx, posy: posy) { [unowned self] (addr, done) in
            if done {
                self.presenter.setdestisSet(set: true)
                self.addressinputbox.setdestinationplaceholder(text: addr)
                //출발지 가 설정 되어있는지 아닌지 여뷰 확인
                //안되있으면 다시 출발지로 포커스 이동
                //되어있으면 택시 호출 화면으로 이동
                if self.presenter.checkdepartureisSet() == false {
                    InaviGeneralSearchRepo.shared.removeall()
                    self.searchbylistpage.refreshcv(isempty: false, infolabelkeyword: nil)
                    self.addressinputbox.setfocus(type : .departure)
                }
                else if self.presenter.isSameLocation() == true {
                    UIView.animate(withDuration: 0.2) { [unowned self] in
                        self.samelocationpopup.alpha = 1
                    }
                }
                else{
                    //화면을 넘어갈때 경로 탐색 api를 불러서 미리 경로를 받아온다
                    self.presenter.callInaviFindRouteAPI { (done) in
                        if done {
                            let view = CallWithDestViewController(departname: self.addressinputbox.gettexts().depart, destname: self.addressinputbox.gettexts().dest,startx: self.presenter.getdepartcoordinate()[0], starty: self.presenter.getdepartcoordinate()[1], endx: self.presenter.getdestcoordinate()[0], endy: self.presenter.getdestcoordinate()[1])
                            self.navigationController?.pushViewController(view, animated: true)
                        }
                    }
                }
            }
            else {
                self.presenter.setdestisSet(set: false)
                self.addressinputbox.setdestinationinput(text : "목적지를 다시 설정해주세요")
            }
        }
        
    }
    
    func mapViewBecameIdle(posx : Double , posy : Double,type : searchtype){
        presenter.callreversegeocodeapi(posx: posx, posy: posy) { [unowned self] (addr, done) in
            if done {
                if type == .departure {
                    self.presenter.setdepartureisSet(set: true)
                    self.addressinputbox.setdepartureplaceholder(text : addr)
                    self.addressinputbox.setdepartureinput(text: addr)
                }
                else {
                    self.presenter.setdestisSet(set: true)
                    self.addressinputbox.setdestinationplaceholder(text : addr)
                    self.addressinputbox.setdestinationinput(text: addr)
                }
            }
            else {
            }
            
        }
    }

}
//집 회사 등록 팝업 델리게이트 익스테션
extension SpecViewController : EnlistHouseAndOfficePopUPDelegate {
    /**
     집, 회사 버튼 클릭시 등록되어 있지 않았을때 등록하라는 팝업을 띄우는 함수
     */
    func enlistbtnpresse(type: EnlistHouseAndOfficePopUP.popuptype) {
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.enlistpopup.alpha = 0
        }
        let enlistmode : enlistmode = type == .house ? .house : .office
        let view = EnlistViewController(enlistmode: enlistmode)
        self.navigationController?.pushViewController(view, animated: true)
        
    }
    
    
}
extension SpecViewController {
    private func makeaddresinputbox(){
        self.view.addSubview(addressinputbox)
        addressinputbox.translatesAutoresizingMaskIntoConstraints = false
        addressinputbox.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 56).isActive = true
        addressinputbox.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        addressinputbox.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        addressinputbox.heightAnchor.constraint(equalToConstant: 108).isActive = true
    }
    private func makesearchbylistpage(){
        self.view.addSubview(searchbylistpage)
        searchbylistpage.translatesAutoresizingMaskIntoConstraints = false
        searchbylistpage.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        searchbylistpage.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        searchbylistpage.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        searchbylistpage.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        
    }
    private func makesearchbymappage(){
        self.view.addSubview(searchbymappage)
        searchbymappage.translatesAutoresizingMaskIntoConstraints = false
        searchbymappage.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        searchbymappage.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        searchbymappage.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        searchbymappage.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        searchbymappage.alpha = 0
    }
}
