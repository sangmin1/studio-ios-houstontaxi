//
//  SpecContract.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/16.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation


protocol SpecContract : NSObjectProtocol {
    func refreshcv(isempty : Bool, infolabelkeyword : String?)
    func showmapview(lat : Double, long : Double, type : searchtype)
    
    
    func setinitialDepartAddress(addr : String)
    
}
protocol SearchByMapPageDelegate : NSObjectProtocol {
    func setToMapPage(show : Bool)
    
    
    func setdepartureFromMappage(posx : Double, posy : Double)
    func setdestfrommappage(posx : Double, posy : Double)
    func mapViewBecameIdle(posx : Double , posy : Double,type : searchtype)
}
protocol AddressInputBoxDelegate : NSObjectProtocol {
    func setToMapPage(show : Bool)
    
    
    
    func searchDeparture(keyword : String)
    func searchDestination(keyword : String)
    
    
    
}

protocol SearchByListPageDelegate  : NSObjectProtocol{
    func showRemoveRecentRecordViewController()
    func setToMapPage(show : Bool)
    func popviewController()
    
    
    func homebtnpressed()
    func officebtnpressed()
}
