//
//  CallCenterRepo.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/27.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation



class CallCenterRepo {
    static let shared = CallCenterRepo()
    private init(){}
    // = ["0317914114"]
    var number : [String] {
        get{
            let value = UserDefaults.standard.array(forKey: userstandard.callcenternums.rawValue) as! [String]
            return value
        }
       
    }
    
    var selected : [Bool] {
        get {
            let value = UserDefaults.standard.array(forKey: userstandard.callcenterSelected.rawValue) as! [Bool]
            return value
        }
        set (value) {
            UserDefaults.standard.set(value, forKey: userstandard.callcenterSelected.rawValue)
        }
    }
    
    
}
