//
//  CallCenterContract.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation

protocol CallCenterContract : NSObjectProtocol {
    
    func setcallcenterbtns(nums : [String],selected : [Bool])
}
