//
//  CallCenterBtn.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/21.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


/**
 콜센터 버튼 컴포넌트
 */
class CallCenterBtn: UIButton {
    
    private var label = PaddingLabel()
    
    private var checkbox = UIImageView()
    
    private var underline = UIImageView()
    
    override var isSelected: Bool {
        didSet {
            checkbox.image = isSelected ? UIImage(named: "dtListChecked") : UIImage(named: "dtListUnChecked")
        }
    }
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        makelabel()
        makecheckbox()
        makeunderline()
        
        
        self.widthAnchor.constraint(equalToConstant: mainboundwidth).isActive = true
        self.heightAnchor.constraint(equalToConstant: 84).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setlabel(label :String){
        self.label.text = label
    }
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.bounds.contains(point) {
            return self
        }
        else {
            return nil
        }
    }
    
}
extension CallCenterBtn {
    private func makelabel(){
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 24).isActive = true
        label.widthAnchor.constraint(equalToConstant: 100).isActive = true
        label.heightAnchor.constraint(equalToConstant: 26).isActive = true
        label.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        label.font = UIFont(name: fonts.medium.rawValue, size: 20)//.systemFont(ofSize: 20, weight: .medium)
        label.text = ""
    }
    private func makecheckbox(){
        self.addSubview(checkbox)
        checkbox.translatesAutoresizingMaskIntoConstraints = false
        checkbox.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        checkbox.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -22).isActive = true
        checkbox.widthAnchor.constraint(equalToConstant: 30).isActive = true
        checkbox.heightAnchor.constraint(equalToConstant: 30).isActive = true
        checkbox.image = UIImage(named: "dtListUnChecked")
    }
    private func makeunderline(){
        self.addSubview(underline)
        underline.translatesAutoresizingMaskIntoConstraints = false
        underline.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        underline.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        underline.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        underline.heightAnchor.constraint(equalToConstant: 1).isActive = true
        underline.backgroundColor = .rgb(red: 0, green: 0, blue: 0, alpha: 0.05)
    }
}
