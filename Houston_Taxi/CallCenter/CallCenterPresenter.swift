//
//  CallCenterPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class CallCenterPresenter: NSObject {
    
    weak var delegate : CallCenterContract?
    weak var viewcontroller : CallCenterViewController?
    
    init(delegate : CallCenterContract, viewcontroller : CallCenterViewController){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
        
    }
    
    
    func setcontent(){
        
        
        self.delegate?.setcallcenterbtns(nums: CallCenterRepo.shared.number, selected: CallCenterRepo.shared.selected)
    }
    
    
    func setCallCenterRepoData(selected : Bool , tag : Int){
        var data = CallCenterRepo.shared.selected
        data[tag] = selected
        CallCenterRepo.shared.selected = data
    }
    
}

