//
//  CallCenterViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 설정 -> 콜센터 페이지
 */
class CallCenterViewController: UIViewController {
    
    private var topbox = UIView()
    
    private var titlelabel = PaddingLabel()
    
    private var backbtn = WideTapBtn()
    
    /**
     모두이용 버튼
     */
    private var selectall = CallCenterBtn()
    /**
     콜센터1
     */
    private var center1 = CallCenterBtn()
    /**
     콜센터 2
     */
    private var center2 = CallCenterBtn()
    
    
    lazy private var centerbtnarr : [CallCenterBtn] = [center1,center2]
    lazy private var btnstack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [selectall,center1,center2])
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.spacing = 0
        return stack
    }()
    
    
    lazy private var presenter = CallCenterPresenter(delegate: self, viewcontroller: self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        maketopbox()
        maketitlelabel()
        makebackbtn()
        makebtnstack()
        backbtn.addTarget(self, action: #selector(backbtnpressed(sender:)), for: .touchUpInside)
        selectall.addTarget(self, action: #selector(allbtnpressed(sender:)), for: .touchUpInside)
        center2.addTarget(self, action: #selector(centerbtnpressed(sender:)), for: .touchUpInside)
        center1.addTarget(self, action: #selector(centerbtnpressed(sender:)), for: .touchUpInside)
        
        
        presenter.setcontent()
        
    }
    
    /**
     백버튼 이벤트 함수
     */
    @objc func backbtnpressed(sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func allbtnpressed(sender : UIButton){
        if sender.isSelected == true {
            center1.isSelected = false
            center2.isSelected = false
        }
        else {
            center1.isSelected = true
            center2.isSelected = true
        }
        sender.isSelected = !sender.isSelected
        
        
        for btn in centerbtnarr {
            if btn.isHidden == false {
                presenter.setCallCenterRepoData(selected: sender.isSelected, tag: btn.tag)
            }
        }
    }
    @objc func centerbtnpressed(sender : UIButton){
        if sender.isSelected == true {
            selectall.isSelected = false
        }
        sender.isSelected = !sender.isSelected
        presenter.setCallCenterRepoData(selected: sender.isSelected, tag: sender.tag)
    }
    
    
}
extension CallCenterViewController : CallCenterContract{
    func setcallcenterbtns(nums : [String],selected : [Bool]){
        centerbtnarr.forEach { (btn) in
            btn.isHidden = true
            
        }
        for (index, value) in nums.enumerated() {
            centerbtnarr[index].setlabel(label: value)
            centerbtnarr[index].isSelected = selected[index]
            centerbtnarr[index].isHidden = false
        }
        if selected.elementsEqual(Array(repeating: true, count: selected.count)) {
            selectall.isSelected = true
        }
        else {
            selectall.isSelected = false
        }
    }
}

extension CallCenterViewController {
    private func maketopbox(){
        self.view.addSubview(topbox)
        topbox.translatesAutoresizingMaskIntoConstraints = false
        topbox.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        topbox.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        topbox.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        topbox.heightAnchor.constraint(equalToConstant: 56).isActive = true
        topbox.backgroundColor = .white
    }
    private func makebackbtn(){
        self.view.addSubview(backbtn)
        backbtn.translatesAutoresizingMaskIntoConstraints = false
        backbtn.centerYAnchor.constraint(equalTo: topbox.centerYAnchor, constant: 0).isActive = true
        backbtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        backbtn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        backbtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        backbtn.setImage(UIImage(named: "titleBtnBackN"), for: .normal)
        
    }
    private func maketitlelabel(){
        self.view.addSubview(titlelabel)
        titlelabel.translatesAutoresizingMaskIntoConstraints = false
        titlelabel.centerYAnchor.constraint(equalTo: topbox.centerYAnchor, constant: 0).isActive = true
        titlelabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        titlelabel.widthAnchor.constraint(equalToConstant: 150).isActive = true
        titlelabel.heightAnchor.constraint(equalToConstant: 23).isActive = true
        titlelabel.textColor = .rgb(red: 75, green: 75, blue: 75, alpha: 1)
        titlelabel.font = UIFont(name: fonts.bold.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .bold)
        titlelabel.textAlignment = .center
        titlelabel.text = "콜센터"
        
    }
    private func makebtnstack(){
        self.view.addSubview(btnstack)
        btnstack.translatesAutoresizingMaskIntoConstraints = false
        btnstack.topAnchor.constraint(equalTo: topbox.bottomAnchor, constant: 0).isActive = true
        btnstack.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        btnstack.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        btnstack.heightAnchor.constraint(lessThanOrEqualToConstant: 1000).isActive = true
        selectall.setlabel(label: "모두 이용")
        center1.setlabel(label: "콜센터1")
        center1.tag = 0
        
        center2.setlabel(label: "콜센터2")
        center2.tag = 2
        
    }
}
