//
//  SplashLocationPopUP.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/16.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


/**
 접근 허용 팝업
 */
class SplashLocationPopUp: UIView {
    
    private var box = ShadowView()
    
    private var infolabel = PaddingLabel()
    
    private var appclosebtn = UIButton()
    
    private var setlocationbtn = UIButton()
    
    weak var delegate : SplashLocationPopUpDelegate?
    
    
    init(frame: CGRect, delegate : SplashLocationPopUpDelegate) {
        super.init(frame: frame)
        self.delegate = delegate
        self.backgroundColor = .init(white: 0.2, alpha: 0.4)
        makebox()
        makeinfolabel()
        makeappclosebtn()
        makesetlocationbtn()
        
        appclosebtn.addTarget(self, action: #selector(appclosebtnpressed(sender:)), for: .touchUpInside)
        setlocationbtn.addTarget(self, action: #selector(confirmbtnpressed(sender:)), for: .touchUpInside)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        DispatchQueue.main.async { [weak self] in
            self?.box.layer.cornerRadius = 8
            self?.appclosebtn.layer.addBorder(edge: [.top,.right], color: .rgb(red: 0, green: 0, blue: 0, alpha: 0.05), thickness: 1)
            self?.setlocationbtn.layer.addBorder(edge: [.top], color: .rgb(red: 0, green: 0, blue: 0, alpha: 0.05), thickness: 1)
        }
    }
    
    
    
    @objc func appclosebtnpressed(sender : UIButton){
        UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
    }
    
    @objc func confirmbtnpressed(sender : UIButton){
        self.delegate?.setlocationbtnpressed()
    }
    
    
}
extension SplashLocationPopUp {
    private func makebox(){
        self.addSubview(box)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        box.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        box.widthAnchor.constraint(equalToConstant: 315).isActive = true
        box.heightAnchor.constraint(equalToConstant: 166).isActive = true
        box.backgroundColor = .white
        box.shadowcolor = .black
        box.shadowoffset = CGSize(width: 0, height: 1)
        box.shadowradius = 5
        box.shadowopacity = 0.8
    }
    private func makeinfolabel(){
        self.addSubview(infolabel)
        infolabel.translatesAutoresizingMaskIntoConstraints = false
        infolabel.topAnchor.constraint(equalTo: box.topAnchor, constant: 32).isActive = true
        infolabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        infolabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        infolabel.heightAnchor.constraint(lessThanOrEqualToConstant: 60).isActive = true
        infolabel.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        infolabel.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        infolabel.textAlignment = .center
        infolabel.numberOfLines = 0
        infolabel.lineBreakMode = .byWordWrapping
        infolabel.text = "하남 콜 택시 이용을 위해\n위치정보를 켜주세요."
    }
    private func makeappclosebtn(){
        self.addSubview(appclosebtn)
        appclosebtn.translatesAutoresizingMaskIntoConstraints = false
        appclosebtn.bottomAnchor.constraint(equalTo: box.bottomAnchor, constant: 0).isActive = true
        appclosebtn.trailingAnchor.constraint(equalTo: box.centerXAnchor, constant: 0).isActive = true
        appclosebtn.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 0).isActive = true
        appclosebtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        appclosebtn.setTitle("앱 종료", for: .normal)
        appclosebtn.setTitleColor(.rgb(red: 100, green: 100, blue: 100, alpha: 1), for: .normal)
        appclosebtn.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        
    }
    private func makesetlocationbtn(){
        self.addSubview(setlocationbtn)
        setlocationbtn.translatesAutoresizingMaskIntoConstraints = false
        setlocationbtn.topAnchor.constraint(equalTo: appclosebtn.topAnchor, constant: 0).isActive = true
        setlocationbtn.leadingAnchor.constraint(equalTo: box.centerXAnchor, constant: 0).isActive = true
        setlocationbtn.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: 0).isActive = true
        setlocationbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        setlocationbtn.setTitle("위치정보 켜기", for: .normal)
        setlocationbtn.setTitleColor(maincolor, for: .normal)
        setlocationbtn.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        
        
    }
    
}

