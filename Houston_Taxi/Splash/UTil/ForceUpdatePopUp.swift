//
//  ForceUpdatePopUp.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/25.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
  강제 업데이트 팝업
 */
class ForceUpdatePopUp : UIView {
    
    
    
    
    private var box = ShadowView()
    
    private var title = PaddingLabel()
    
    
    private var label = PaddingLabel()
    
    
    
    //취소 버튼
    private var cancelbtn = UIButton()
    //지금 등록 버튼
    private var confirmbtn = UIButton()
    
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.init(white: 0.2, alpha: 0.6)
        makebox()
        maketitle()
        makelabel()
        makecanclebtn()
        makeconfirmbtn()
        
        cancelbtn.addTarget(self, action: #selector(canclebtnpressed(sender:)), for: .touchUpInside)
        confirmbtn.addTarget(self, action: #selector(confirmbtnpressed(sender:)), for: .touchUpInside)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async { [weak self] in
            self?.box.layer.cornerRadius = 8
            self?.cancelbtn.layer.addBorder(edge: [.top], color: .rgb(red: 0, green: 0, blue: 0, alpha: 0.05), thickness: 1)
            self?.confirmbtn.layer.addBorder(edge: [.top], color: .rgb(red: 0, green: 0, blue: 0, alpha: 0.05), thickness: 1)
            self?.confirmbtn.layer.addBorder(edge: .left, color: .rgb(red: 0, green: 0, blue: 0, alpha: 0.05), thickness: 1)
        }
    }
    
    
    
    /**
     취소 버튼 이벤트 함수
     */
    @objc func canclebtnpressed(sender : UIButton){
        UIControl().sendAction(#selector(URLSessionTask.suspend), to: UIApplication.shared, for: nil)
    }
    
    /**
     지금 등록 버튼 이벤트 함수
     */
    @objc func confirmbtnpressed(sender : UIButton){
        //to appstore
    }
    
    
}
extension ForceUpdatePopUp {
    private func makebox(){
        self.addSubview(box)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        box.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        box.widthAnchor.constraint(equalToConstant: 315).isActive = true
        box.heightAnchor.constraint(equalToConstant: 335).isActive = true
        box.backgroundColor = .white
        
        box.shadowcolor = UIColor.black
        box.shadowoffset = CGSize(width: 0, height: 3)
        box.shadowradius = 6
        box.shadowopacity = 0.1
    }
    private func maketitle(){
        self.addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        title.topAnchor.constraint(equalTo: box.topAnchor, constant: 32).isActive = true
        title.centerXAnchor.constraint(equalTo: box.centerXAnchor, constant: 0).isActive = true
        title.widthAnchor.constraint(equalToConstant: 300).isActive = true
        title.heightAnchor.constraint(equalToConstant: 26).isActive = true
        title.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        title.font = UIFont.systemFont(ofSize: 20, weight: .bold)
        title.text = "업데이트 안내"
        title.textAlignment = .center
        
        
    }
    private func makelabel(){
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.topAnchor.constraint(equalTo: title.bottomAnchor, constant: 14).isActive = true
        label.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 0).isActive = true
        label.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: 0).isActive = true
        label.heightAnchor.constraint(lessThanOrEqualToConstant: 191).isActive = true
        label.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        label.font = UIFont(name: fonts.medium.rawValue, size: 20)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.text = """
        TEXT TEXT TEXT TEXT TEXT TEXT
        TEXT TEXT TEXT TEXT TEXT TEXT
        TEXT TEXT TEXT TEXT TEXT TEXT

        TEXT TEXT TEXT TEXT TEXT TEXT
        TEXT TEXT TEXT TEXT TEXT TEXT
        TEXT TEXT TEXT TEXT TEXT TEXT

        TEXT TEXT TEXT TEXT TEXT TEXT
        TEXT TEXT TEXT TEXT TEXT TEXT
        """
    }
    private func makecanclebtn(){
        self.addSubview(cancelbtn)
        cancelbtn.translatesAutoresizingMaskIntoConstraints = false
        cancelbtn.bottomAnchor.constraint(equalTo: box.bottomAnchor, constant: 0).isActive = true
        cancelbtn.trailingAnchor.constraint(equalTo: box.centerXAnchor, constant: 0).isActive = true
        cancelbtn.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 0).isActive = true
        cancelbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        cancelbtn.setTitle("앱 종료", for: .normal)
        cancelbtn.setTitleColor(.rgb(red: 100, green: 100, blue: 100, alpha: 1), for: .normal)
        cancelbtn.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)
       
        
    }
    private func makeconfirmbtn(){
        self.addSubview(confirmbtn)
        confirmbtn.translatesAutoresizingMaskIntoConstraints = false
        confirmbtn.topAnchor.constraint(equalTo: cancelbtn.topAnchor, constant: 0).isActive = true
        confirmbtn.leadingAnchor.constraint(equalTo: box.centerXAnchor, constant: 0).isActive = true
        confirmbtn.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: 0).isActive = true
        confirmbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        confirmbtn.setTitle("업데이트", for: .normal)
        confirmbtn.setTitleColor(maincolor, for: .normal)
        confirmbtn.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)
    }
    
    
    
}
