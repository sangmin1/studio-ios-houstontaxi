//
//  SplashContract.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


protocol SplashContract: NSObjectProtocol {
    func showPhoneVerificationViewController()
    func showMainViewController()
    func showforceupdatepopup()
}
protocol SplashLocationPopUpDelegate : NSObjectProtocol {
    func setlocationbtnpressed()
}
