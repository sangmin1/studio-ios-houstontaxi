//
//  SplashPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class SplashPresenter: NSObject {
    
    weak var delegate : SplashContract?
    
    
    init(delegate : SplashContract){
        self.delegate = delegate
    }
    
    //타이머
    private var timer : Timer?
    //맥시멈 타임
    private var seconds = 2
    
    //타이머 변수에 트리거를 설정하는 함수
    
    func runtimer(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    //타이머를 1초식 감소시키는 함수
    @objc func updateTimer(){
        seconds -= 1
        if seconds == 0 {
            timer?.invalidate()
            timer = nil
            //인증을 했는지 안했는지 여부 판단
            //인증이 안됬으면 인증 화면으로
            //인증이 되었다면 버젼 확인 api 호출후 메인 화면으로 진입
            if UserDefaults.standard.bool(forKey: userstandard.isverified.rawValue) == false {
                self.delegate?.showPhoneVerificationViewController()
            }
            else {
                self.callVersion { [unowned self] (done) in
                    if done {
                        self.callfreqvisitAPI()
                    }
                }
            }
        }
        else if seconds < 0 {
            timer?.invalidate()
            timer = nil
        }
        else {
            
        }
        
        
    }
    
    /**
     타이머를 강제적으로 정지시키는 함수
     */
    func invalidatetimer(){
        timer?.invalidate()
        timer = nil
    }
    
    /**
     최근 목적지 빈도수 api 호출 함수
     */
    func callfreqvisitAPI(){
        let api = GetFreqVisitAPI()
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile) {[unowned self] (done,data) in
            if done {
                var tempmodel : [FregVisitModel] = []
                for model in data.arrayValue {
                    tempmodel.append(FregVisitModel(json: model))
                }

                FreqVisitRepo.shared.setmodel(input : tempmodel)
            }
            self.delegate?.showMainViewController()
        }
    }
    /**
     앱 버젼 확인 api  호출 함수
     */
    func callVersion(completion : @escaping(Bool) -> ()){
        let api = GetVersionAPI()
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile) { (done, data) in
            if done {
                print("version",data)
                let force = data["isForceful"].stringValue
                let result = Bool(FunctionClass.shared.decrypt(msg: force))
                print("result",result)
                if result == true {
                    self.delegate?.showforceupdatepopup()
                    completion(false)
                }
                else {
                    completion(true)
                }
                
                
            }
            else {
                completion(false)
            }
        }
    }
    
    
}
extension SplashContract {
    
}
