//
//  SplashViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
import CoreLocation



class SplashViewController: UIViewController,CLLocationManagerDelegate {
    
    //그라데이션 뷰
    private var gradientview = UIView()
    //택시 로고 이미지
    private var logoimage = UIImageView()
    // 성남 콜택시
    private var companylabel = PaddingLabel()
    // 승객용
    private var customer = PaddingLabel()
    //THINKWARE
    private var makerlogo = UIImageView()
    
    static var topinset : CGFloat = 0
    static var bottominset : CGFloat = 0
    
   
    
    let scene = UIApplication.shared.connectedScenes.first
       

    var locationManager = CLLocationManager()
    
    
    lazy private var presenter = SplashPresenter(delegate : self)
    
    
    //위치 접근 허용 팝업
    lazy private var  locationpopup = SplashLocationPopUp(frame: .zero,delegate : self)
    
    lazy private var forceupdatepopup = ForceUpdatePopUp(frame: self.view.frame)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .black
        self.navigationController?.navigationBar.isHidden = true
        
        makegradientview()
        makelogoimage()
        makecompanylabel()
        makecustomer()
        makemakerlogo()
        makelocationpopup()
        //위치 접근 허용 팝업 표출 함수
        locationManager.requestWhenInUseAuthorization()
        locationManager.delegate = self

        
        
       
        
        if CLLocationManager.locationServicesEnabled() {
            print("enabled?")
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
            switch CLLocationManager.authorizationStatus() {
            case .notDetermined:
                print("not determined")
            case .restricted, .denied:
                UIView.animate(withDuration: 0.2) { [unowned self] in
                    self.locationpopup.alpha = 1
                }
            case .authorizedAlways, .authorizedWhenInUse:
                presenter.runtimer()
            @unknown default:
                print("error")
            }

        }
        
        self.view.addSubview(forceupdatepopup)
        forceupdatepopup.alpha = 0
    }
   
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        guard let locValue: CLLocationCoordinate2D = manager.location?.coordinate else { return }
        UserDefaults.standard.set(locValue.latitude, forKey: userstandard.lat.rawValue)
        UserDefaults.standard.set(locValue.longitude, forKey: userstandard.lon.rawValue)
    }
    //사용자가 위치 접근을 허용 했는지 안했는지 여부 확인
    //접근 허용 했으면 타이머를 돌리며 현재 위치 갱신
    //접근 허용 거절이면 팝업 창을 띄움
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {

    if( status == .authorizedWhenInUse || status == .authorizedAlways) {
        UserDefaults.standard.set(locationManager.location?.coordinate.latitude, forKey: userstandard.lat.rawValue)
        UserDefaults.standard.set(locationManager.location?.coordinate.longitude, forKey: userstandard.lon.rawValue)
        presenter.runtimer()
    }
    else if status == .denied || status == .notDetermined {
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.locationpopup.alpha = 1
        }
    }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        gradientview.createGradientLayer(colors: UIColor.rgb(red: 12, green: 161, blue: 155, alpha: 1), UIColor.rgb(red: 40, green: 90, blue: 145, alpha: 1), start: CGPoint(x: 0.5, y: 0), mid: CGPoint(x: 0.5, y: 0.5), end: CGPoint(x: 0.5, y: 1))
        customer.layer.cornerRadius = 10.5
        customer.clipsToBounds = true
        SplashViewController.topinset = self.view.safeAreaInsets.top
        SplashViewController.bottominset = self.view.safeAreaInsets.bottom
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        //때때로 타이머가 안꺼질때가 있어서 강제적으로 뷰가 사라지면 정지시킴
        presenter.invalidatetimer()
    }
    
}
extension SplashViewController : SplashContract {
    //휴대폰 인증번호 화면으로 넘어가는 함수
    func showPhoneVerificationViewController(){
        let scene = UIApplication.shared.connectedScenes.first
        guard let del = scene?.delegate as? SceneDelegate else {
             return
        }
        //PhoneVerificationViewController
        del.window!.rootViewController = UINavigationController(rootViewController: PhoneVerificationViewController())
        
    }
    //번호가 있을시 메인 맵 뷰로 넘어가는 함수
    func showMainViewController() {
        let scene = UIApplication.shared.connectedScenes.first
        guard let del = scene?.delegate as? SceneDelegate else {
             return
        }
        del.window!.rootViewController = UINavigationController(rootViewController: MainViewController())
        
    }
    //강제 업데이트 팝업 표출
    func showforceupdatepopup(){
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.forceupdatepopup.alpha = 1
        }
    }
}
extension SplashViewController : SplashLocationPopUpDelegate {
    /**
     위치 접근 허용 팝업 "위치정보 켜기" 눌렀을때 실행 되는 함수
     */
    func setlocationbtnpressed() {
        //아이폰 기본 설정 화면으로 이동
        if let bundleId = Bundle.main.bundleIdentifier,
           let url = URL(string: "\(UIApplication.openSettingsURLString)&path=LOCATION/\(bundleId)") {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
        }
        //위치 정보 접근 권한 재확인 함수 실행
        self.locationManager(locationManager, didChangeAuthorization: CLLocationManager.authorizationStatus())
    }
}
extension SplashViewController {
    private func makegradientview(){
        self.view.addSubview(gradientview)
        gradientview.translatesAutoresizingMaskIntoConstraints = false
        gradientview.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        gradientview.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        gradientview.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        gradientview.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
    }
    private func makelogoimage(){
        self.view.addSubview(logoimage)
        logoimage.translatesAutoresizingMaskIntoConstraints = false
        logoimage.topAnchor.constraint(equalTo: self.view.topAnchor, constant: UIScreen.main.bounds.height  * 12 / 100).isActive = true
        logoimage.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        logoimage.widthAnchor.constraint(equalToConstant: 120).isActive = true
        logoimage.heightAnchor.constraint(equalToConstant: 118).isActive = true
        logoimage.image = UIImage(named: "icTaxi")
        
    }
    private func makecompanylabel(){
        self.view.addSubview(companylabel)
        companylabel.translatesAutoresizingMaskIntoConstraints = false
        companylabel.topAnchor.constraint(equalTo: logoimage.bottomAnchor, constant: 16).isActive = true
        companylabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        companylabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        companylabel.heightAnchor.constraint(equalToConstant: 42).isActive = true
        companylabel.textColor = .white
        companylabel.font = UIFont(name: fonts.bold.rawValue, size: 32)
        companylabel.text = "하남 콜 택시"
        companylabel.textAlignment = .center
    }
    private func makecustomer(){
        self.view.addSubview(customer)
        customer.translatesAutoresizingMaskIntoConstraints = false
        customer.topAnchor.constraint(equalTo: companylabel.bottomAnchor, constant: 32).isActive = true
        customer.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        customer.widthAnchor.constraint(equalToConstant: 80).isActive = true
        customer.heightAnchor.constraint(equalToConstant: 24).isActive = true
        customer.backgroundColor = .rgb(red: 255, green: 255, blue: 255, alpha: 0.2)
        customer.textColor = .white
        customer.font = UIFont(name: fonts.medium.rawValue, size: 16)
        customer.text = "승객용"
        customer.textAlignment = .center
    }
    private func makemakerlogo(){
        self.view.addSubview(makerlogo)
        makerlogo.translatesAutoresizingMaskIntoConstraints = false
        makerlogo.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -32).isActive = true
        makerlogo.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        makerlogo.widthAnchor.constraint(equalToConstant: 103).isActive = true
        makerlogo.heightAnchor.constraint(equalToConstant: 14).isActive = true
        makerlogo.image = UIImage(named: "biThinkware")
        
    }
    private func makelocationpopup(){
        self.view.addSubview(locationpopup)
        locationpopup.translatesAutoresizingMaskIntoConstraints = false
        locationpopup.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        locationpopup.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        locationpopup.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        locationpopup.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        locationpopup.alpha = 0
    }
    
    
    
    
}

