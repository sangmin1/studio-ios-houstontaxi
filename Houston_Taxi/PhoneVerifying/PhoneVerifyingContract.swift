//
//  PhoneVerifyingContract.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/06.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation


protocol PhoneVerifyingContract : NSObjectProtocol {
    func showInfoAccessAgreementsViewController()
    func showmismatchpopup()
    
}
//인증번호 입력 컨테이너 델리게이트
protocol VerifyingContainerContract : NSObjectProtocol {
    func verifybtnpressed(verifynumb : String)
    func retrybtnpresssed(completion : @escaping (Bool) -> ())
    func showmismatchpopup()
    
}
