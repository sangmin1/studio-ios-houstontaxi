//
//  PhoneVerifyingPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/06.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class PhoneVerifyingPresenter: NSObject {
    
    weak var delegate : PhoneVerifyingContract?
    weak var viewcontroller : PhoneVerifyingViewController?
    
    lazy private var verifyinputcontainer = VerifyingContainer(frame : .zero, delegate : self)
    
    
    init(delegate : PhoneVerifyingContract, viewcontroller : PhoneVerifyingViewController ){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
    }
    
    func getverifyinputcontainer() -> VerifyingContainer{
        return self.verifyinputcontainer
    }
    
    /**
     확인 버튼 눌럿 auth/set을 콜하는 함수
     */
    func callAuthSetAPI(verifynumb : String , completion : @escaping (Bool) -> ()){
        let api = AuthSetAPI()
        
        let currentDt = FunctionClass.shared.getcurrentDt()
        let auth = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        let data = FunctionClass.shared.encrypt(msg: verifynumb)
        api.requestToserver(currentDT: currentDt, authcode: auth, mobile: mobile, data: data) { (done, data) in
            if done {
                completion(data["isSuccessful"].boolValue)
            }
            else {
                completion(false)
            }
        }
        
        
    }
    
    /**
     인증번호 재 요청 api 호출 함수
     */
    func callAuthAPI(phonenumber : String,completion : @escaping (Bool) -> ()){
        
        let api = AuthAPI()
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: phonenumber)
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile) { (done, data) in
            if done {
                print("auth set data",data)
                completion(data["isSuccessful"].boolValue)
            }
            else {
                completion(false)
            }
        }
        
        
    }
    
    
    
}
//인증번호 입력 컨테이너 델리게이트 비즈니스 로직
extension PhoneVerifyingPresenter : VerifyingContainerContract {
    /**
     인증번호 미스매치 팝업창 표출 함수
     */
    func showmismatchpopup() {
        self.delegate?.showmismatchpopup()
    }
    
    /**
      확인 버튼 눌렀을때
     */
    func verifybtnpressed(verifynumb : String){
        self.callAuthSetAPI(verifynumb: verifynumb) { [unowned self] (done) in
            if done {
                UserDefaults.standard.set(true, forKey: userstandard.isverified.rawValue)
                self.delegate?.showInfoAccessAgreementsViewController()
            }
            else {
                self.showmismatchpopup()
                ToastView.shared.short(self.viewcontroller!.view, txt_msg: "잠시 후 시도해주세요")
            }
        }
        
    }
    /**
      인증번호 재요청
     */
    func retrybtnpresssed(completion : @escaping (Bool) -> () ){
        let numb = UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!
        FunctionClass.shared.showdialog(show: true)
        self.callAuthAPI(phonenumber: numb) { (done) in
            completion(done)
            FunctionClass.shared.showdialog(show: false)
        }
    }
    
    
}
