//
//  PhoneVerifyingViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/06.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 인증번호 입력 뷰 컨트롤러
 */
class PhoneVerifyingViewController: UIViewController{
    
    
    lazy private var presenter = PhoneVerifyingPresenter(delegate: self, viewcontroller: self)
    
    //컨테이너에서부터 컨테이너 객체를 가져온다
    lazy private var verifyinputcontainer = presenter.getverifyinputcontainer()
    
    lazy private var mismatchpopup = PhoneVerificationMisMatchPopUp(frame: self.view.frame)
    
    lazy private var stack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [verifyinputcontainer])
        stack.axis = .vertical
        stack.distribution = .fill
        return stack
    }()
    
   
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = .white
        makestack()
        
        self.view.addSubview(mismatchpopup)
        mismatchpopup.alpha = 0
        
        
    }
    
}
extension PhoneVerifyingViewController  {
    
    private func makestack(){
        self.view.addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        stack.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        stack.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        stack.heightAnchor.constraint(lessThanOrEqualToConstant: 1000).isActive = true
    }
    
    
}
extension PhoneVerifyingViewController : PhoneVerifyingContract {
    /**
     사용자 정보 동의 화면으로 넘어가는 함수
     */
    func showInfoAccessAgreementsViewController(){
        let view = InfoAccessAgreementsViewController()
        self.navigationController?.pushViewController(view, animated: true)
    }
    /**
     인증 번호 틀렸을때 팝업창 표출 함수
     */
    func showmismatchpopup(){
        mismatchpopup.fadeIn(0.2, delay: 0) { [unowned self] (done) in
            self.mismatchpopup.fadeOut(0.2, delay: 0.6) { (done) in
                
            }
        }
    }
}
