//
//  VerifyingContainer.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa

/**
 인증 번호 입력하고 인증을 받는 컨테이너
 */
class VerifyingContainer: UIView {
    
    let disposebag = DisposeBag()
    //휴대폰 번호 인증
    private var titlelabel = PaddingLabel()
    // 인증번호를 ~~
    private var infolabel = PaddingLabel()
    //인증번호 입력 필드
    private var verifynumbinputfield = PaddingTextField()
    //재요청 버튼
    private var regrantbtn = UIButton()
    //타이버 라벨
    private var timerlabel = PaddingLabel()
    //확인버튼
    private var confirmbtn = ShadowBtn()
    
    //해당 컨테이너 델리게이트
    weak var delegate : VerifyingContainerContract?
    
    var sourceObservable: Disposable?
    
    var maxsecond : Int = 300
    
    init(frame : CGRect,delegate : VerifyingContainerContract){
        super.init(frame: frame)
        self.delegate = delegate
        maketitlelabel()
        makeinfolabel()
        makeverifynumbinputfield()
        makeregrantbtn()
        maketimerlabel()
        makecofirmbtn()
        
        verifynumbinputfield.becomeFirstResponder()
        regrantbtn.addTarget(self, action: #selector(retrybtnpressed(sender:)), for: .touchUpInside)
        confirmbtn.addTarget(self, action: #selector(confirmbtnpressed(sender:)), for: .touchUpInside)
        self.bottomAnchor.constraint(equalTo: confirmbtn.bottomAnchor, constant: 50).isActive = true
        
        
        /**
         문자인증 필드의 숫자 개수가 6개 인지 아닌지 판별하여 버튼의 활성도를 셋하는 것
         */
        verifynumbinputfield.rx.text.orEmpty
            .map(validateconfirmbtn(src:))
            .subscribe(onNext : { [unowned self] result in
                self.confirmbtn.isUserInteractionEnabled = result
            })
            .disposed(by: disposebag)
        
        
        /**
         타이버 라벨 작동 함수
         */
        sourceObservable = Observable<Int>
            .interval(RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
            .map { [unowned self] _ in
                self.maxsecond = self.maxsecond - 1
            }
            .map { [unowned self] in
                "\(self.maxsecond / 60):\(self.maxsecond % 60)"
            }
            .bind(to: timerlabel.rx.text)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async { [unowned self] in
            self.verifynumbinputfield.layer.shadowColor = UIColor.rgb(red: 12, green: 161, blue: 155, alpha: 1).cgColor
            self.verifynumbinputfield.layer.shadowRadius = 6
            self.verifynumbinputfield.layer.shadowOpacity = 0.25
            self.verifynumbinputfield.layer.shadowOffset = CGSize(width: 0, height: 3)
            self.regrantbtn.roundCorners(corners: [.topRight,.bottomRight], radius: 8)
        }
        
    }
    override func didMoveToWindow() {
        super.didMoveToWindow()
        DispatchQueue.main.async { [unowned self] in
            self.verifynumbinputfield.layer.shadowColor = UIColor.rgb(red: 12, green: 161, blue: 155, alpha: 1).cgColor
            self.verifynumbinputfield.layer.shadowRadius = 6
            self.verifynumbinputfield.layer.shadowOpacity = 0.25
            self.verifynumbinputfield.layer.shadowOffset = CGSize(width: 0, height: 3)
            self.regrantbtn.roundCorners(corners: [.topRight,.bottomRight], radius: 8)
        }
        verifynumbinputfield.layer.cornerRadius = 8
        confirmbtn.layer.cornerRadius = 25
        
    }
    
    
    /**
     인증번호 텍스트 필드 의 유효성을 검사하는 함수
     */
    func validateconfirmbtn(src : String) -> Bool {
        if src.count == 5 {
            confirmbtn.backgroundColor = maincolor
            confirmbtn.setTitleColor(.white, for: .normal)
            return true
        }
        else {
            confirmbtn.backgroundColor = deactivecolor
            confirmbtn.setTitleColor(.rgb(red: 184, green: 184, blue: 184, alpha: 1), for: .normal)
            return false
        }
    }
    
    /**
      확인 버튼 이벤트 함수
     */
    @objc func confirmbtnpressed(sender : UIButton){
        self.delegate?.verifybtnpressed(verifynumb: verifynumbinputfield.text!)
    }
    /**
     재요청 버튼 이벤트 함수
     타이머를  invalidate 시키고 다시 작동 시킨다
     */
    @objc func retrybtnpressed(sender : UIButton){
        sourceObservable?.disposed(by: disposebag)
        self.maxsecond = 300
        self.delegate?.retrybtnpresssed{ [unowned self] (done) in
            if done {
                self.sourceObservable = Observable<Int>
                .interval(RxTimeInterval.seconds(1), scheduler: MainScheduler.instance)
                .map { [unowned self] _ in
                    self.maxsecond = self.maxsecond - 1
                    if self.maxsecond <= 0 {
                        self.sourceObservable?.disposed(by: self.disposebag)
                    }
                }
                .map { [unowned self] in
                    "\(self.maxsecond / 60):\(self.maxsecond % 60)"
                }
                .bind(to: self.timerlabel.rx.text)
            }
            else {
                ToastView.shared.short(self, txt_msg: "잠시 후 시도해 주세요")
            }
        }
    }
    
    
    
    
}
extension VerifyingContainer {
    private func maketitlelabel(){
        self.addSubview(titlelabel)
        titlelabel.translatesAutoresizingMaskIntoConstraints = false
        titlelabel.translatesAutoresizingMaskIntoConstraints = false
        titlelabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 56).isActive = true
        titlelabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        titlelabel.widthAnchor.constraint(equalToConstant: 150).isActive = true
        titlelabel.heightAnchor.constraint(equalToConstant: 27).isActive = true
        titlelabel.textColor = .rgb(red: 70, green: 70, blue: 70, alpha: 1)
        titlelabel.font = UIFont(name: fonts.bold.rawValue, size: 21)
        titlelabel.text = "휴대폰 번호 인증"
        titlelabel.textAlignment = .center
    }
    private func makeinfolabel(){
        self.addSubview(infolabel)
        infolabel.translatesAutoresizingMaskIntoConstraints = false
        infolabel.topAnchor.constraint(equalTo: titlelabel.bottomAnchor, constant: 24).isActive = true
        infolabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        infolabel.widthAnchor.constraint(equalToConstant: 320 * wscale).isActive = true
        infolabel.heightAnchor.constraint(equalToConstant: 23).isActive = true
        infolabel.textColor = .rgb(red: 184, green: 184, blue: 184, alpha: 1)
        infolabel.font = UIFont(name: fonts.medium.rawValue, size: 18)
        infolabel.text = "인증 번호를 입력해주세요."
        infolabel.textAlignment = .center
    }
    private func makeverifynumbinputfield(){
        self.addSubview(verifynumbinputfield)
        verifynumbinputfield.translatesAutoresizingMaskIntoConstraints = false
        verifynumbinputfield.topAnchor.constraint(equalTo: infolabel.bottomAnchor, constant: 40).isActive = true
        verifynumbinputfield.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        verifynumbinputfield.widthAnchor.constraint(equalToConstant: 351 * wscale).isActive = true
        verifynumbinputfield.heightAnchor.constraint(equalToConstant: 54).isActive = true
        verifynumbinputfield.placeholder = "인증 번호"
        verifynumbinputfield.font = UIFont(name: fonts.medium.rawValue, size: 18)
        verifynumbinputfield.textColor = .black
        verifynumbinputfield.layer.borderColor = maincolor.cgColor
        verifynumbinputfield.layer.borderWidth = 1
        verifynumbinputfield.left = 10
        verifynumbinputfield.right = 120
        verifynumbinputfield.backgroundColor = .white
        verifynumbinputfield.keyboardType = .decimalPad
        
    }
    private func makeregrantbtn(){
        self.addSubview(regrantbtn)
        regrantbtn.translatesAutoresizingMaskIntoConstraints = false
        regrantbtn.topAnchor.constraint(equalTo: verifynumbinputfield.topAnchor, constant: 0).isActive = true
        regrantbtn.trailingAnchor.constraint(equalTo: verifynumbinputfield.trailingAnchor, constant: 0).isActive = true
        regrantbtn.bottomAnchor.constraint(equalTo: verifynumbinputfield.bottomAnchor, constant: 0).isActive = true
        regrantbtn.widthAnchor.constraint(equalToConstant: 71).isActive = true
        regrantbtn.backgroundColor = maincolor
        regrantbtn.setTitle("재요청", for: .normal)
        regrantbtn.setTitleColor(.white, for: .normal)
        regrantbtn.titleLabel?.font = UIFont(name: fonts.medium.rawValue, size: 15)
    }
    private func maketimerlabel(){
        self.addSubview(timerlabel)
        timerlabel.translatesAutoresizingMaskIntoConstraints = false
        timerlabel.centerYAnchor.constraint(equalTo: verifynumbinputfield.centerYAnchor, constant: 0).isActive = true
        timerlabel.trailingAnchor.constraint(equalTo: regrantbtn.leadingAnchor, constant: -20).isActive = true
        timerlabel.widthAnchor.constraint(equalToConstant: 40).isActive = true
        timerlabel.heightAnchor.constraint(equalToConstant: 23).isActive = true
        timerlabel.textColor = maincolor
        timerlabel.font = UIFont(name: fonts.medium.rawValue, size: 18)
        timerlabel.textAlignment = .right
        timerlabel.text = "5:00"
    }
    private func makecofirmbtn(){
        self.addSubview(confirmbtn)
        confirmbtn.translatesAutoresizingMaskIntoConstraints = false
        confirmbtn.topAnchor.constraint(equalTo: verifynumbinputfield.bottomAnchor, constant: 20).isActive = true
        confirmbtn.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        confirmbtn.widthAnchor.constraint(equalToConstant: 351 * wscale).isActive = true
        confirmbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        confirmbtn.backgroundColor = deactivecolor
        confirmbtn.setTitleColor(.rgb(red: 184, green: 184, blue: 184, alpha: 1), for: .normal)
        confirmbtn.setTitle("확인", for: .normal)
        confirmbtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20)
        confirmbtn.shadowcolor = .black
        confirmbtn.shadowradius = 3
        confirmbtn.shadowopacity = 0.2
        confirmbtn.shadowoffset = CGSize(width: 0, height: 1)
        confirmbtn.isUserInteractionEnabled = false
    }
}
