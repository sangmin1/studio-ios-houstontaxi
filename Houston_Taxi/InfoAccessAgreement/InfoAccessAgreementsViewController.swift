//
//  InfoAccessAgreementsViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


/**
 제플린 140 정보 사용동의 화면
 */
class InfoAccessAgreementsViewController: UIViewController,UIScrollViewDelegate {
    
    
    lazy private var presenter = InfoAccessAgreementsPresenter(delegate: self, viewcontroller: self)
    //컨테이너
    lazy private var container = presenter.getcontainer()
    
    private var scrollview = UIScrollView()
    lazy private var stackview : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [container])
        stack.axis = .vertical
        stack.distribution = .fill
        stack.alignment = .fill
        return stack
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.navigationController?.navigationBar.isHidden = true
        makescrollview()
        makestackview()
    }
    
    
}
extension InfoAccessAgreementsViewController : InfoAccessAgreementsContract{
    //정보 이용 동의 페이지로 가는 함수
    func showInfoUserAgreementsViewController(){
        let view = InfoUserAgreementsViewController()
        self.navigationController?.pushViewController(view, animated: true)
    }
}
extension InfoAccessAgreementsViewController {
    func makescrollview(){
        self.view.addSubview(scrollview)
        scrollview.translatesAutoresizingMaskIntoConstraints = false
        scrollview.topAnchor.constraint(equalTo: self.view.topAnchor).isActive = true
        scrollview.leadingAnchor.constraint(equalTo: self.view.leadingAnchor).isActive = true
        scrollview.trailingAnchor.constraint(equalTo: self.view.trailingAnchor).isActive = true
        scrollview.bottomAnchor.constraint(equalTo: self.view.bottomAnchor,constant: 0).isActive = true
        scrollview.showsVerticalScrollIndicator = false
        scrollview.bounces = false
        scrollview.delegate = self
        scrollview.contentInset = UIEdgeInsets(top: 0, left: 0, bottom: 0, right: 0)
    }
    func makestackview(){
        scrollview.addSubview(stackview)
        stackview.translatesAutoresizingMaskIntoConstraints = false
        stackview.topAnchor.constraint(equalTo: scrollview.topAnchor).isActive = true
        stackview.leadingAnchor.constraint(equalTo: scrollview.leadingAnchor).isActive = true
        stackview.widthAnchor.constraint(equalTo: scrollview.widthAnchor).isActive = true
        stackview.bottomAnchor.constraint(equalTo: scrollview.bottomAnchor).isActive = true
    }
}
