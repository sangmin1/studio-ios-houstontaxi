//
//  InfoAccessAgreementsContainer.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class InfoAccessAgreementsContainer: UIView {
    
    private var titlelabel = PaddingLabel()
    
    //위치 정보 접근박스
    private var location = InfoAccessAgreementBox()
    //연락처 접근 박스
    private var contact = InfoAccessAgreementBox()
    //데이터 사용 접근 박스
    private var data = InfoAccessAgreementBox()
    
    
    lazy private var stack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [location, contact, data])
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.spacing  = 12
        return stack
    }()
    
    private var confirmbtn = ShadowBtn()
    
    
    
    weak var delegate : InfoAccessAgreementsContainerContract?
    
    
    init(frame : CGRect , delegate : InfoAccessAgreementsContainerContract){
        super.init(frame: frame)
        self.delegate = delegate
        maketitlelabel()
        makestack()
        makeconfirmbtn()
        
        confirmbtn.addTarget(self, action: #selector(confirmbtnpressed(sender:)), for: .touchUpInside)
//        self.bottomAnchor.constraint(equalTo: confirmbtn.bottomAnchor, constant: 12).isActive = true
        
        self.heightAnchor.constraint(greaterThanOrEqualToConstant: mainboundheight -  SplashViewController.topinset - SplashViewController.bottominset).isActive = true
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        confirmbtn.layer.cornerRadius = 25
        confirmbtn.clipsToBounds = true
    }
    
    //확인 버튼을 눌러서 정보 이용동의 페이지로 가는 함수
    @objc func confirmbtnpressed(sender : UIButton){
        self.delegate?.showInfoUserAgreementsViewController()
    }
}
extension InfoAccessAgreementsContainer {
    private func maketitlelabel(){
        self.addSubview(titlelabel)
        titlelabel.translatesAutoresizingMaskIntoConstraints = false
        titlelabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 56).isActive = true
        titlelabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        titlelabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        titlelabel.heightAnchor.constraint(lessThanOrEqualToConstant: 100).isActive = true
        titlelabel.textColor = .rgb(red: 70, green: 70, blue: 70, alpha: 1)
        titlelabel.font = UIFont(name: fonts.bold.rawValue, size: 21)
        titlelabel.numberOfLines = 0
        titlelabel.lineBreakMode = .byWordWrapping
        titlelabel.text = "하남 콜 택시 이용을 위해\n아래 기능을 사용합니다."
        titlelabel.textAlignment = .center
    }
    private func makestack(){
        self.addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.topAnchor.constraint(equalTo: titlelabel.bottomAnchor, constant: 40).isActive = true
        stack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        stack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        stack.heightAnchor.constraint(equalToConstant: 129 * 3 + 24).isActive = true
        
        //위치정보 접근 박스 내용 세팅
        location.seticon(icon: UIImage(named: "dtBtnLocateNor")!)
        location.settitle(title: "위치 정보 접근")
        location.setcontent(content: "내가 있는 곳으로 택시를 호출하고\n이동 중 내 위치를 실시간으로 확인할 수 있습니다.")
        //연락처 접근박스 내용 세팅
        contact.seticon(icon: UIImage(named: "dtBtnContactNor")!)
        contact.settitle(title: "연락처 접근")
        contact.setcontent(content: "매칭된 기사님과 통화를 하거나\n택시를 탄 후 지인들에게 안심메시지를\n보낼 수 있습니다.")
        //데이터 사용 박스 내용 세팅
        data.seticon(icon: UIImage(named: "dtBtnDataNor")!)
        data.settitle(title: "데이터 사용")
        data.setcontent(content: "원할한 이용을 위해 데이터를 사용할 수\n있으며, 가입한 요금제에 따라 데이터\n요금이 발생할 수 있습니다.")
    }
    private func makeconfirmbtn(){
        self.addSubview(confirmbtn)
        confirmbtn.translatesAutoresizingMaskIntoConstraints = false
        //confirmbtn.topAnchor.constraint(equalTo: stack.bottomAnchor, constant: 23).isActive = true
        confirmbtn.topAnchor.constraint(greaterThanOrEqualTo: stack.bottomAnchor, constant: 23).isActive = true
        confirmbtn.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12).isActive = true
        confirmbtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        confirmbtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        confirmbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        confirmbtn.backgroundColor = maincolor
        confirmbtn.setTitle("확인", for: .normal)
        confirmbtn.setTitleColor(.white, for: .normal)
        confirmbtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20)
        
        
        confirmbtn.shadowcolor = .black
        confirmbtn.shadowradius = 3
        confirmbtn.shadowopacity = 0.2
        confirmbtn.shadowoffset = CGSize(width: 0, height: 1)
        
        
    }
}
