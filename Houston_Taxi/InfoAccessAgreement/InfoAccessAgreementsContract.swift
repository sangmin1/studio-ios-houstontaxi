//
//  InfoAccessAgreementsContract.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

protocol InfoAccessAgreementsContract : NSObjectProtocol {
    func showInfoUserAgreementsViewController()
    
}

protocol InfoAccessAgreementsContainerContract : NSObjectProtocol {
    func showInfoUserAgreementsViewController()
    func getviewcontroller() -> UIViewController
}
