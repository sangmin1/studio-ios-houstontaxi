//
//  InfoAccessAgreementBox.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 위치 정보 접근 , 연락처 접근 , 데이터사용 중복 컴포넌트
 */
class InfoAccessAgreementBox: UIView {
    
    //박스 내 아이콘
    private var image = UIImageView()
    // 제목
    private var titlelabel = PaddingLabel()
    //내용
    private var contentlabel = PaddingLabel()
    
    
    lazy private var stack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [titlelabel,contentlabel])
        stack.axis = .vertical
        stack.distribution = .fill
        stack.spacing = 6
        return stack
    }()
    
    
    override init(frame : CGRect){
        super.init(frame: frame)
        self.backgroundColor = .rgb(red: 243, green: 243, blue: 243, alpha: 1)
        makeimage()
        makestack()
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
     // 아이콘을 셋하는 함수
    func seticon(icon : UIImage){
        self.image.image = icon
    }
    //제목을 셋하는 함수
    func settitle(title : String){
        self.titlelabel.text = title
    }
    //내용을 셋하는 함수
    func setcontent(content : String){
        self.contentlabel.text = content
    }
    
    
}
extension InfoAccessAgreementBox {
    private func makeimage(){
        self.addSubview(image)
        image.translatesAutoresizingMaskIntoConstraints = false
        image.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        image.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 26).isActive = true
        image.widthAnchor.constraint(equalToConstant: 36).isActive = true
        image.heightAnchor.constraint(equalToConstant: 36).isActive = true
    }
    private func makestack(){
        self.addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        stack.leadingAnchor.constraint(equalTo: image.trailingAnchor, constant: 26).isActive = true
        stack.widthAnchor.constraint(equalToConstant: 237).isActive = true
        stack.heightAnchor.constraint(lessThanOrEqualToConstant: 100).isActive = true
        
        titlelabel.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        titlelabel.font = UIFont(name: fonts.bold.rawValue, size: 18)
        
        contentlabel.numberOfLines = 0
        contentlabel.lineBreakMode = .byWordWrapping
        contentlabel.textColor = .rgb(red: 184, green: 184, blue: 184, alpha: 1)
        contentlabel.font = UIFont(name: fonts.medium.rawValue, size: 15)
    }
    
}
