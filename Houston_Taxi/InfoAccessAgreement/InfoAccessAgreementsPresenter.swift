//
//  InfoAccessAgreementsPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class InfoAccessAgreementsPresenter: NSObject {
    
    
    weak var delegate : InfoAccessAgreementsContract?
    weak var viewcontroller : InfoAccessAgreementsViewController?
    
    lazy private var container = InfoAccessAgreementsContainer(frame: .zero, delegate: self)
    
    init(delegate : InfoAccessAgreementsContract, viewcontroller : InfoAccessAgreementsViewController){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
    }
    //컨테이너 객체는 뷰컨트롤러에 전달하는 함수
    func getcontainer() -> InfoAccessAgreementsContainer{
        return self.container
    }
    
    
    
    
    
}
extension InfoAccessAgreementsPresenter : InfoAccessAgreementsContainerContract {
    func showInfoUserAgreementsViewController(){
        self.delegate?.showInfoUserAgreementsViewController()
    }
    func getviewcontroller() -> UIViewController {
        return self.viewcontroller ?? UIViewController()
    }
}
