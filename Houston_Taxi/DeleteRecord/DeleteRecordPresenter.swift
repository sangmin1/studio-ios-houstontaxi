//
//  DeleteRecordPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class DeleteRecordPresenter: NSObject {
    
    weak var delegate : DeleteRecordContract?
    
    weak var viewcontroller : DeleteRecordViewController?
    
    let cellid = "DeleteRecordCellid"
    let emptycellid = "recordemptycellid"
    
    var recordisEmtpy : Bool = false
    
    init(delegate : DeleteRecordContract , viewcontroller : DeleteRecordViewController){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
    }
    
    func registerCV(cv :UICollectionView){
        cv.register(DeleteRecordCell.self, forCellWithReuseIdentifier: cellid)
        //cv.register(EmptyRecordCell.self, forCellWithReuseIdentifier: emptycellid)
    }
    
    /**
     탑승이력 전체 삭제 API 호출 함수
     */
    func callDeleteAllAPI(){
        let api = GetInfoHistoryRemoveAPI()
        let currentdt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentdt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        api.requestRemoveAll(currentDT: currentdt, authcode: authcode, mobile: mobile) { (done, data) in
            if done {
                self.callHistoryAPI { [unowned self] (done) in
                    if done {
                        self.viewcontroller?.cv.reloadData()
                    }
                }
            }
            else {
                
            }
        }
    }
    /**
     탑승이력 삭제 api
     */
    func calldeleteapi(data : [Int]){
        let api = GetInfoHistoryRemoveAPI()
        let currentdt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentdt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        api.requestRemove(currentDT: currentdt, authcode: authcode, mobile: mobile, data: data) { [unowned self] (done, data) in
            if done {
                self.callHistoryAPI { [unowned self] (done) in
                    if done {
                        self.viewcontroller?.cv.reloadData()
                    }
                }
            }
            else {
                
            }
        }
    }
    
    /**
     탑승 이력 조회 api
     */
    func callHistoryAPI(completion : @escaping (Bool) -> ()){
        FunctionClass.shared.showdialog(show: true)
        let api = GetInfoHistory()
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile) { (done, data) in
            if done {
                HistoryRepo.shared.setmodel(input: HistoryModel(json: data))
                
                if let msg = data["msg"].string {
                    print(" msg",FunctionClass.shared.decrypt(msg: msg))
                }
                let success = data["isSuccessful"].boolValue
                print("success",success)
                if success == true {
                   completion(true)
                }
                else {
                    completion(false)
                }
                
            }
            else {
                completion(false)
            }
            FunctionClass.shared.showdialog(show: false)
        }
        
        
    }
    
    
    
}
extension DeleteRecordPresenter : UICollectionViewDelegate , UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        if let count = HistoryRepo.shared.getmodel()?.data.count {
            if count == 0 {
                recordisEmtpy = true
                return 1
            }
            else {
                recordisEmtpy = false
                return count
            }
            
        }
        else {
            recordisEmtpy = true
            return 1
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        if recordisEmtpy == true {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: emptycellid, for: indexPath) as! EmptyRecordCell
            
            return cell
        }
        else {
            guard let model = HistoryRepo.shared.getmodel()?.data[indexPath.row] else {
                return UICollectionViewCell()
            }
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellid, for: indexPath) as! DeleteRecordCell
            
            cell.setdate(date: model.getInTime ?? "")
            cell.setdestname(destname: model.destinationName ?? "목적지없음")
            if model.getOutTime ?? "" != "" {
                cell.setdesttime(desttime: model.getOutTime ?? "")
            }
            //cell.setdesttime(desttime: model.getOutTime ?? "")
            
            cell.setdepartname(departname: model.cusPosName ?? "")
            if model.getInTime ?? "" != "" {
                cell.setdeparttime(departime: model.getInTime ?? "" )
            }
            //cell.setdeparttime(departime: model.getInTime ?? "" )
            
            cell.setdrivername(drivername: model.driverName ?? "", carmodel: model.color ?? "", carnum: String(model.carId ?? 0))
            
            cell.setid(id : model.id ?? 0)
            
            return cell
            
        }
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: mainboundwidth, height: 252)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 11
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 11
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! DeleteRecordCell
        if cell.isSelected == true {
            cell.isSelected = false
            self.delegate?.disableallselectbtn()
        }
        else {
            collectionView.selectItem(at: indexPath, animated: false, scrollPosition: [])
            // cell.isSelected = true
        }

    }

    func processDelete(type : String){
        if type == "all" {
            self.callDeleteAllAPI()
        }
        else {
            if let indexpath = self.viewcontroller?.cv.indexPathsForSelectedItems {
                var data : [Int] = []
                for index in indexpath {
                    data.append(HistoryRepo.shared.getIdbyindexpath(index : index.row))
                }
                print("data",data)
                self.calldeleteapi(data: data)
            }
        }
    }
    
    
    
    
}
