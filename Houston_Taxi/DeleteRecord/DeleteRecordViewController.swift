
//  DeleteRecordViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
/**
  탑승이력 삭제 뷰 컨트롤러
 */
class DeleteRecordViewController: UIViewController {
    
    private var topbox = UIView()
    
    private var titlelabel = PaddingLabel()
    
    private var backbtn = WideTapBtn()
    
    lazy private var presenter = DeleteRecordPresenter(delegate: self, viewcontroller: self)
    
    lazy var cv : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.delegate = presenter
        cv.dataSource = presenter
        cv.allowsMultipleSelection = true
        return cv
    }()
    
    //전체선택 버튼
    private var selectallbtn = ShadowBtn()
    //삭제 버튼
    private var deletebtn = UIButton()
    
    lazy private var btnstack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [selectallbtn,deletebtn])
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.spacing = 15
        return stack
    }()
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        maketopbox()
        maketitlelabel()
        makebackbtn()
        makecv()
        makebtnstack()
        presenter.registerCV(cv: cv)
        backbtn.addTarget(self, action: #selector(backbtnpressed(seder:)), for: .touchUpInside)
        selectallbtn.addTarget(self, action: #selector(selectallbtnpresse(sender:)), for: .touchUpInside)
        deletebtn.addTarget(self, action: #selector(deletebtnpressed(sender:)), for: .touchUpInside)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        selectallbtn.layer.cornerRadius = 25
        deletebtn.layer.cornerRadius = 25
        
    }
    
    /**
     백버튼 이벤트 함수
     */
    @objc func backbtnpressed(seder : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     전체 선택 버튼 이벤트 함수
     */
    @objc func selectallbtnpresse(sender : UIButton){
        if selectallbtn.isSelected == false {
            selectallbtn.isSelected = true
            selectallbtn.setTitle("전체 선택 해제", for: .normal)
            for index in 0...cv.numberOfItems(inSection: 0) - 1 {
                let indexpath = IndexPath(item: index, section: 0)
                cv.selectItem(at: indexpath, animated: false, scrollPosition: [])
            }
        }
        else {
            selectallbtn.isSelected = false
            selectallbtn.setTitle("전체 선택", for: .normal)
            for index in 0...cv.numberOfItems(inSection: 0) - 1 {
                let indexpath = IndexPath(item: index, section: 0)
                cv.deselectItem(at: indexpath, animated: false)
            }
        }
        
    }
    
    /**
     삭제 버튼 이벤트 함수
     */
    @objc func deletebtnpressed(sender : UIButton){
        //all -> 전체 삭제, 그외 개별삭제
        if selectallbtn.isSelected == true {
            presenter.processDelete(type: "all")
        }
        else {
            presenter.processDelete(type: "byindividual")
        }
    }
    
}
extension DeleteRecordViewController : DeleteRecordContract {
    /**
     전체 선택 후 하나의 셀을 클릭했을때 전체 서택을 풀어버리는 함수
     */
    func disableallselectbtn(){
        selectallbtn.isSelected = false
        selectallbtn.setTitle("전체 선택", for: .normal)
    }
}
extension DeleteRecordViewController {
    private func maketopbox(){
        self.view.addSubview(topbox)
        topbox.translatesAutoresizingMaskIntoConstraints = false
        topbox.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        topbox.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        topbox.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        topbox.heightAnchor.constraint(equalToConstant: 56).isActive = true
        topbox.backgroundColor = .white
    }
    private func makebackbtn(){
        self.view.addSubview(backbtn)
        backbtn.translatesAutoresizingMaskIntoConstraints = false
        backbtn.centerYAnchor.constraint(equalTo: topbox.centerYAnchor, constant: 0).isActive = true
        backbtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        backbtn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        backbtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        backbtn.setImage(UIImage(named: "titleBtnBackN"), for: .normal)
        
    }
    private func maketitlelabel(){
        self.view.addSubview(titlelabel)
        titlelabel.translatesAutoresizingMaskIntoConstraints = false
        titlelabel.centerYAnchor.constraint(equalTo: topbox.centerYAnchor, constant: 0).isActive = true
        titlelabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        titlelabel.widthAnchor.constraint(equalToConstant: 150).isActive = true
        titlelabel.heightAnchor.constraint(equalToConstant: 23).isActive = true
        titlelabel.textColor = .rgb(red: 75, green: 75, blue: 75, alpha: 1)
        titlelabel.font = UIFont(name: fonts.bold.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .bold)
        titlelabel.textAlignment = .center
        titlelabel.text = "탑승이력 삭제"
    }
    private func makecv(){
        self.view.addSubview(cv)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.topAnchor.constraint(equalTo: topbox.bottomAnchor, constant: 0).isActive = true
        cv.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        cv.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        cv.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        cv.backgroundColor = .white
    }
    private func makebtnstack(){
        self.view.addSubview(btnstack)
        btnstack.translatesAutoresizingMaskIntoConstraints = false
        btnstack.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: 0).isActive = true
        btnstack.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        btnstack.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        btnstack.heightAnchor.constraint(equalToConstant: 50).isActive = true
        btnstack.clipsToBounds = false
        
        selectallbtn.backgroundColor = .white
        selectallbtn.setTitle("전체 선택", for: .normal)
        selectallbtn.setTitleColor(maincolor, for: .normal)
        selectallbtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20) //.systemFont(ofSize: 20, weight: .bold)
        selectallbtn.shadowcolor = .black
        selectallbtn.shadowoffset = CGSize(width: 0, height: 1)
        selectallbtn.shadowradius = 4
        selectallbtn.shadowopacity = 0.2
        selectallbtn.isSelected = false
        
        
        deletebtn.backgroundColor = maincolor
        deletebtn.setTitle("삭제", for: .normal)
        deletebtn.setTitleColor(.white, for: .normal)
        deletebtn.titleLabel?.font = UIFont (name: fonts.bold.rawValue, size: 20)//.systemFont(ofSize: 20, weight: .bold)
    }
}
