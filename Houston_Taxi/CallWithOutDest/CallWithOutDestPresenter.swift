//
//  CallWithOutDestPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/07.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class CallWithOutPresenter: NSObject {
    
    weak var delegate : CallWithOutDestContract?
    weak var viewcontroller : CallWithOutDestViewController?
    
    private var departname : String = ""
    private var startx : Double = 0
    private var starty : Double = 0
    
    
    
    init(delegate : CallWithOutDestContract , viewcontroller : CallWithOutDestViewController ){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
    }
    
    
    /**
     배차에 관한 정보들을 셋하는 함수
     */
    func setInfos(departname : String ,startx: Double, starty: Double){
        self.departname = departname
        self.startx = startx
        self.starty = starty
        
    }
    
    func getallocinfos() -> [String : Any]{
        return ["departname" : departname , "startx" : startx, "starty" : starty ]
    }
    
    
    
    
    
    
    
}
