//
//  CallWithoutDestViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/07.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
import iNaviMaps

/**
 제플린 400 택시 호출 도착지 없음 화면
 */
class CallWithOutDestViewController: UIViewController {
    
    lazy var mapView = InaviMapView(frame: .zero)
    
    //백버튼
    private var backbtn = WideTapBtn()
    //택시 호출 버튼
    private var calltaxi = UIButton()
    
    private var box = ShadowView()
    //점 이미지
    private var linedotImage = UIImageView()
    //출발지
    private var departurelabel = PaddingLabel()
    //기사님께 도착지를 ....
    private var destlabel = PaddingLabel()
    //현재 위치 버튼
    private var currentlocationbtn = UIButton()
    
    
    
    lazy private var presenter = CallWithOutPresenter(delegate: self, viewcontroller: self)
    
    
    /**
     이전 화면에서 바로호출한 좌표와 지명을 받아온다.
     */
    convenience init(posx : Double , posy : Double, address : String){
        self.init()
        presenter.setInfos(departname: address, startx: posx, starty: posy)
        let marker = INVMarker()
        marker.position = INVLatLng(lat: posy, lng: posx)
        marker.iconImage = INVImage(image: UIImage(named: "mapPinDeparture")!)
        marker.title = address
        marker.mapView = mapView
        mapView.moveCamera(INVCameraUpdate(targetTo: INVLatLng(lat: posy, lng: posx)))
        self.departurelabel.text = address
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        makebackbtn()
        makecalltaxibtn()
        makebox()
        makelinedotImage()
        makedeparturelabel()
        makedestlabel()
        makecurrentlocationbtn()
        makemapview()
        
        backbtn.addTarget(self, action: #selector(backbtnpressed(sender:)), for: .touchUpInside)
        calltaxi.addTarget(self, action: #selector(calltaxibtnpressed(sender:)), for: .touchUpInside)
        currentlocationbtn.addTarget(self, action: #selector(currentlocationbtnpressed(sender:)), for: .touchUpInside)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        box.roundCorners(corners: [.topRight,.topLeft], radius: 25)
    }
    /**
     백버튼 이벤트 함수
     */
    @objc func backbtnpressed(sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     택시 호출 이벤트 함수
     */
    @objc func calltaxibtnpressed(sender : UIButton){
        let view = FindingTaxiViewController(allocdata: presenter.getallocinfos())
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    /**
     현재 위치 버튼 이벤트 함수
     */
    @objc func currentlocationbtnpressed(sender : UIButton){
        let lat = UserDefaults.standard.double(forKey: userstandard.lat.rawValue)
        let lon = UserDefaults.standard.double(forKey: userstandard.lon.rawValue)
        mapView.moveCamera(INVCameraUpdate(targetTo: INVLatLng(lat: lat, lng: lon)))
    }
    
    
}
extension CallWithOutDestViewController : INVMapViewDelegate {
    
    
}
extension CallWithOutDestViewController : CallWithOutDestContract {
    private func makebackbtn(){
        self.view.addSubview(backbtn)
        backbtn.translatesAutoresizingMaskIntoConstraints = false
        backbtn.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 13).isActive = true
        backbtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        backbtn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        backbtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        backbtn.setImage(UIImage(named: "titleBtnBackN"), for: .normal)
    }
    private func makecalltaxibtn(){
        self.view.addSubview(calltaxi)
        calltaxi.translatesAutoresizingMaskIntoConstraints = false
        calltaxi.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        calltaxi.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        calltaxi.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        calltaxi.heightAnchor.constraint(equalToConstant: 54).isActive = true
        calltaxi.backgroundColor = maincolor
        calltaxi.setTitle("택시 호출", for: .normal)
        calltaxi.setTitleColor(.white, for: .normal)
        calltaxi.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20)
    }
    private func makebox(){
        self.view.addSubview(box)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.bottomAnchor.constraint(equalTo: calltaxi.topAnchor, constant: 0).isActive = true
        box.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        box.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        box.heightAnchor.constraint(equalToConstant: 110).isActive = true
        box.backgroundColor = .white
        box.shadowcolor = .black
        box.shadowoffset = CGSize(width: 0, height: 1)
        box.shadowradius = 6
        box.shadowopacity = 0.16
        self.view.sendSubviewToBack(box)
    }
    private func makelinedotImage(){
        self.view.addSubview(linedotImage)
        linedotImage.translatesAutoresizingMaskIntoConstraints = false
        linedotImage.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 32).isActive = true
        linedotImage.centerYAnchor.constraint(equalTo: box.centerYAnchor, constant: 0).isActive = true
        linedotImage.heightAnchor.constraint(equalToConstant: 60).isActive = true
        linedotImage.widthAnchor.constraint(equalToConstant: 14).isActive = true
        linedotImage.image = UIImage(named: "mainIcDot")
        
    }
    private func makedeparturelabel(){
        self.view.addSubview(departurelabel)
        departurelabel.translatesAutoresizingMaskIntoConstraints = false
        departurelabel.topAnchor.constraint(equalTo: box.topAnchor, constant: 20).isActive = true
        departurelabel.leadingAnchor.constraint(equalTo: linedotImage.trailingAnchor, constant: 20).isActive = true
        departurelabel.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: -20).isActive = true
        departurelabel.heightAnchor.constraint(equalToConstant: 26).isActive = true
        departurelabel.textColor = .rgb(red: 75, green: 75, blue: 75, alpha: 0.4)
        departurelabel.font = UIFont(name: fonts.medium.rawValue, size: 20)
        
    }
    private func makedestlabel(){
        self.view.addSubview(destlabel)
        destlabel.translatesAutoresizingMaskIntoConstraints = false
        destlabel.bottomAnchor.constraint(equalTo: box.bottomAnchor, constant: -20).isActive = true
        destlabel.leadingAnchor.constraint(equalTo: linedotImage.trailingAnchor, constant: 20).isActive = true
        destlabel.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: -20).isActive = true
        destlabel.heightAnchor.constraint(equalToConstant: 26).isActive = true
        destlabel.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        destlabel.font = UIFont(name: fonts.medium.rawValue, size: 20)
        destlabel.text = "기사님께 도착지를 말씀해주세요."
        destlabel.adjustsFontSizeToFitWidth = true
        
    }
    private func makecurrentlocationbtn(){
        self.view.addSubview(currentlocationbtn)
        currentlocationbtn.translatesAutoresizingMaskIntoConstraints = false
        currentlocationbtn.bottomAnchor.constraint(equalTo: box.topAnchor, constant: 0).isActive = true
        currentlocationbtn.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -3).isActive = true
        currentlocationbtn.widthAnchor.constraint(equalToConstant: 54).isActive = true
        currentlocationbtn.heightAnchor.constraint(equalToConstant: 54).isActive = true
        currentlocationbtn.setImage(UIImage(named: "mapBtnMy"), for: .normal)
        
    }
    private func makemapview(){
        self.view.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        mapView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        mapView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        mapView.bottomAnchor.constraint(equalTo: calltaxi.topAnchor, constant: 0).isActive = true
        mapView.delegate = self
        self.view.sendSubviewToBack(mapView)
    }
    
    
    
    
}
