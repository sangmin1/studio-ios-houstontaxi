//
//  SlideInMenu.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/17.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 슬라이드 인 메뉴
 */
class SlideInMenu : DraggableView , DraggableViewDelegate {
    
    private var box = UIView()
    //반가워요
    private var greetinglabel = PaddingLabel()
    //폰넘버
    private var phonenumberlabel = PaddingLabel()
    //님 라벨
    private var nimlabel = PaddingLabel()
    
    
    private var underline1 = UIImageView()
    
    //탑승이력
    private var recordsBtn = UIButton()
    //설정
    private var settingsBtn = UIButton()
    
    
    
    private var underline2 = UIImageView()
    //콜센터
    private var callcenterBtn = SlideInCallCenterBtn()
    
    weak var viewdelegate : SlideinMenuDelegate?
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        makebox()
        makegreetinglabel()
        makephonenumnerlabel()
        makenimlabel()
        makeunderline1()
        makerecordsbtn()
        makesettingbtn()
        makeunderline2()
        
        makecallcenterbtn()
        
        delegate = self
        recordsBtn.addTarget(self, action: #selector(recordbtnpressed(sender:)), for: .touchUpInside)
        settingsBtn.addTarget(self, action: #selector(settingpressed(sender:)), for: .touchUpInside)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        let arr = Array(UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        print("arr",arr)
        var str = ""
        for (index,char) in arr.enumerated() {
            if index == 3 || index == 7 {
                str += "-" + String(char)
            }
            else {
                str += String(char)
            }
        }
        phonenumberlabel.text = str
        phonenumberlabel.adjustsFontSizeToFitWidth = true
    }
    
  
    
    func setviewdelegate(delegate : SlideinMenuDelegate){
        self.viewdelegate = delegate
    }
    
    /**
     설정 버튼 이벤트 함수
     */
    @objc func settingpressed(sender : UIButton){
        let scene = UIApplication.shared.connectedScenes.first
        guard let del = scene?.delegate as? SceneDelegate else { return }
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.9, options: .curveEaseInOut, animations: {
                 del.slidemenu.frame = CGRect(x: -mainboundwidth, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            }) { (done) in
            }
        self.viewdelegate?.showSettingViewController()
    }
    
    /**
     탑승 이력 이벤트 함수
     */
    @objc func recordbtnpressed(sender : UIButton){
        //RecordViewController
        let scene = UIApplication.shared.connectedScenes.first
        guard let del = scene?.delegate as? SceneDelegate else { return }
        UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.9, options: .curveEaseInOut, animations: {
                 del.slidemenu.frame = CGRect(x: -mainboundwidth, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            }) { (done) in
            }
        
        
        self.viewdelegate?.showRecordViewController()
    }
    
    
    func panGestureDidEnd(_ panGesture: UIPanGestureRecognizer, originalCenter: CGPoint, translation: CGPoint, velocityInView: CGPoint) {
        let scene = UIApplication.shared.connectedScenes.first
        guard let del = scene?.delegate as? SceneDelegate else { return }
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.9, options: .curveEaseInOut, animations: {
                 
                 del.slidemenu.frame = CGRect(x: -mainboundwidth, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            }) { (done) in
            }
        }
    
    
    func backTofullscreen(){
        let scene = UIApplication.shared.connectedScenes.first
        
        guard let del = scene?.delegate as? SceneDelegate else { return }
            UIView.animate(withDuration: 0.5, delay: 0, usingSpringWithDamping: 0.8, initialSpringVelocity: 0.9, options: .curveEaseInOut, animations: {
                 
                 del.slidemenu.frame = CGRect(x: 0, y: 0, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height)
            }) { (done) in
            }
        
    }
     
}
extension SlideInMenu {
    private func makebox(){
        self.addSubview(box)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        box.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        box.widthAnchor.constraint(equalToConstant: 250).isActive = true
        box.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        box.backgroundColor = .white
    }
    private func makegreetinglabel(){
        self.addSubview(greetinglabel)
        greetinglabel.translatesAutoresizingMaskIntoConstraints = false
        greetinglabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 54).isActive = true
        greetinglabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 22).isActive = true
        greetinglabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        greetinglabel.heightAnchor.constraint(equalToConstant: 23).isActive = true
        greetinglabel.textColor = .rgb(red: 70, green: 70, blue: 70, alpha: 1)
        greetinglabel.font = UIFont(name: fonts.medium.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .medium)
        greetinglabel.text = "반가워요!"
        
    }
    private func makephonenumnerlabel(){
        self.addSubview(phonenumberlabel)
        phonenumberlabel.translatesAutoresizingMaskIntoConstraints = false
        phonenumberlabel.topAnchor.constraint(equalTo: greetinglabel.bottomAnchor, constant: 2).isActive = true
        phonenumberlabel.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 22).isActive = true
        phonenumberlabel.widthAnchor.constraint(lessThanOrEqualToConstant: 200).isActive = true
        phonenumberlabel.heightAnchor.constraint(equalToConstant: 31).isActive = true
        phonenumberlabel.textColor = .rgb(red: 70, green: 70, blue: 70, alpha: 1)
        phonenumberlabel.font = UIFont(name: fonts.bold.rawValue, size: 24) //.systemFont(ofSize: 24, weight: .bold)

        let arr = Array(UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        print("arr",arr)
        var str = ""
        for (index,char) in arr.enumerated() {
            if index == 3 || index == 7 {
                str += "-" + String(char)
            }
            else {
                str += String(char)
            }
        }
        phonenumberlabel.text = str
        phonenumberlabel.adjustsFontSizeToFitWidth = true
    }
    private func makenimlabel(){
        self.addSubview(nimlabel)
        nimlabel.translatesAutoresizingMaskIntoConstraints = false
        nimlabel.centerYAnchor.constraint(equalTo: phonenumberlabel.centerYAnchor, constant: 1).isActive = true
        nimlabel.leadingAnchor.constraint(equalTo: phonenumberlabel.trailingAnchor, constant: 2).isActive = true
        nimlabel.widthAnchor.constraint(equalToConstant: 20).isActive = true
        nimlabel.heightAnchor.constraint(equalToConstant: 23).isActive = true
        nimlabel.textColor = .rgb(red: 70, green: 70, blue: 70, alpha: 1)
        nimlabel.font = UIFont(name: fonts.medium.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .medium)
        nimlabel.text = "님"
    }
    
    private func makeunderline1(){
        self.addSubview(underline1)
        underline1.translatesAutoresizingMaskIntoConstraints = false
        underline1.topAnchor.constraint(equalTo: phonenumberlabel.bottomAnchor, constant: 24).isActive = true
        underline1.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        underline1.trailingAnchor.constraint(equalTo: self.box.trailingAnchor, constant: -12).isActive = true
        underline1.heightAnchor.constraint(equalToConstant: 1).isActive = true
        underline1.backgroundColor = .rgb(red: 0, green: 0, blue: 0, alpha: 0.05)
    }
    private func makerecordsbtn(){
        self.addSubview(recordsBtn)
        recordsBtn.translatesAutoresizingMaskIntoConstraints = false
        recordsBtn.topAnchor.constraint(equalTo: underline1.bottomAnchor, constant: 24).isActive = true
        recordsBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 22).isActive = true
        recordsBtn.widthAnchor.constraint(equalToConstant: 100).isActive = true
        recordsBtn.heightAnchor.constraint(equalToConstant: 23).isActive = true
        recordsBtn.setTitle("탑승이력", for: .normal)
        recordsBtn.setTitleColor(.rgb(red: 70, green: 70, blue: 70, alpha: 1), for: .normal)
        recordsBtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .bold)
        recordsBtn.contentHorizontalAlignment = .left
    }
    private func makesettingbtn(){
        self.addSubview(settingsBtn)
        settingsBtn.translatesAutoresizingMaskIntoConstraints = false
        settingsBtn.topAnchor.constraint(equalTo: recordsBtn.bottomAnchor, constant: 24).isActive = true
        settingsBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 22).isActive = true
        settingsBtn.widthAnchor.constraint(equalToConstant: 100).isActive = true
        settingsBtn.heightAnchor.constraint(equalToConstant: 23).isActive = true
        settingsBtn.setTitleColor(.rgb(red: 70, green: 70, blue: 70, alpha: 1), for: .normal)
        settingsBtn.setTitle("설정", for: .normal)
        settingsBtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .bold)
        settingsBtn.contentHorizontalAlignment = .left
        
    }
    private func makeunderline2(){
        self.addSubview(underline2)
        underline2.translatesAutoresizingMaskIntoConstraints = false
        underline2.topAnchor.constraint(equalTo: settingsBtn.bottomAnchor, constant: 24).isActive = true
        underline2.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        underline2.trailingAnchor.constraint(equalTo: self.box.trailingAnchor, constant: -12).isActive = true
        underline2.heightAnchor.constraint(equalToConstant: 1).isActive = true
        underline2.backgroundColor = .rgb(red: 0, green: 0, blue: 0, alpha: 0.05)
    }
    private func makecallcenterbtn(){
        self.addSubview(callcenterBtn)
        callcenterBtn.translatesAutoresizingMaskIntoConstraints = false
        callcenterBtn.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        callcenterBtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        callcenterBtn.trailingAnchor.constraint(equalTo: self.box.trailingAnchor, constant: 0).isActive = true
        callcenterBtn.heightAnchor.constraint(equalToConstant: 54).isActive = true
        
        
    }
    
    
    
}

protocol SlideinMenuDelegate : NSObjectProtocol {
    func showRecordViewController()
    func showSettingViewController()
}
