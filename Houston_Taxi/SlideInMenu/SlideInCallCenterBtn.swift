//
//  SlideInCallCenterBtn.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


/**
 슬라이드 인 메뉴 콜센터 버튼 컴포넌트
 */
class SlideInCallCenterBtn: UIButton {
    
    //콜센터 이미지
    private var image = UIImageView()
    //콜센터
    private var label = PaddingLabel()

    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .rgb(red: 243, green: 243, blue: 243, alpha: 1)
        makeimage()
        makelabel()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.bounds.contains(point) {
            return self
        }
        else {
            return nil
        }
    }
    
    
}
extension SlideInCallCenterBtn {
    private func makeimage(){
        self.addSubview(image)
        image.translatesAutoresizingMaskIntoConstraints = false
        image.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        image.trailingAnchor.constraint(equalTo: self.centerXAnchor, constant: -3).isActive = true
        image.widthAnchor.constraint(equalToConstant: 20).isActive = true
        image.heightAnchor.constraint(equalToConstant: 20).isActive = true
        image.image = UIImage(named: "menuIcCallctN")
    }
    private func makelabel(){
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        label.leadingAnchor.constraint(equalTo: self.centerXAnchor, constant: 3).isActive = true
        label.widthAnchor.constraint(equalToConstant: 100).isActive = true
        label.heightAnchor.constraint(equalToConstant: 23).isActive = true
        label.textColor = .rgb(red: 120, green: 120, blue: 120, alpha: 1)
        label.font = UIFont(name: fonts.medium.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .medium)
        label.text = "콜센터"
    }
}
