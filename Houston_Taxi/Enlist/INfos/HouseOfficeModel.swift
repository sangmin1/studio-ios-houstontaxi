//
//  HouseOfficeModel.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/14.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation

import RealmSwift

class HouseOfficeModel : Object {
    //이름
    @objc dynamic var name : String = ""
    //주소
    @objc dynamic var address : String = ""
    //집인 지 회사인지 house || office
    @objc dynamic var type : String = ""
    //좌표
    @objc dynamic var lat : Double = 0
    @objc dynamic var lon : Double = 0
    
    
    
    
}
