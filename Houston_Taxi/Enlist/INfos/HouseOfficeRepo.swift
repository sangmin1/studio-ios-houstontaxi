//
//  HouseOfficeRepo.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/14.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import RealmSwift



class HouseOfficeRepo {
    static let shared = HouseOfficeRepo()
    private init(){}
    
    
    
    func setmodel(model : HouseOfficeModel) -> Bool{
        
        let realm = try! Realm()
        
        if model.type == "house" {
            let model : Results<HouseOfficeModel> = realm.objects(HouseOfficeModel.self).filter("type == 'house'")
            
            if model.count != 0 {
                return false
            }
            else {
                try! realm.write {
                    realm.add(model)
                }
                return true
            }
        }
        else {
            let model : Results<HouseOfficeModel> = realm.objects(HouseOfficeModel.self).filter("type == 'office'")
            
            if model.count != 0 {
                return false
            }
            else {
                try! realm.write {
                    realm.add(model)
                }
                return true
            }
        }
    }
    
    func resetmodel(model : HouseOfficeModel){
        
        let realm = try! Realm()
        if model.type == "house" {
            let result : Results<HouseOfficeModel> = realm.objects(HouseOfficeModel.self).filter("type == 'house'")
            try! realm.write{
                realm.delete(result)
            }
            try! realm.write{
                realm.add(model)
            }
        }
        else {
            let result : Results<HouseOfficeModel> = realm.objects(HouseOfficeModel.self).filter("type == 'office'")
            try! realm.write{
                realm.delete(result)
            }
            try! realm.write{
                realm.add(model)
            }
        }
        
    }
    
    
    func deletemodel(type : String){
        let realm = try! Realm()
        if type == "house" {
            let result : Results<HouseOfficeModel> = realm.objects(HouseOfficeModel.self).filter("type == 'house'")
            try! realm.write{
                realm.delete(result)
            }
        }
        else {
            let result : Results<HouseOfficeModel> = realm.objects(HouseOfficeModel.self).filter("type == 'office'")
            try! realm.write{
                realm.delete(result)
            }
        }
        
    }
    
    
    func gethouse() -> HouseOfficeModel? {
        let realm = try! Realm()
        if let result : Results<HouseOfficeModel> = realm.objects(HouseOfficeModel.self).filter("type == 'house'") {
            return result.first
        }
        else {
            return nil
        }
        
        
    }
    func getoffice() -> HouseOfficeModel? {
        let realm = try! Realm()
        let result : Results<HouseOfficeModel> = realm.objects(HouseOfficeModel.self).filter("type == 'office'")
        
        return result.first
    }
}
