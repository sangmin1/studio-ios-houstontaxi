//
//  EnlistHouseViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
import iNaviMaps

/**
  집, 회사 등록 뷰 컨트롤러
  진입시 .house -> 집 등록으로 시작
  진입시 .offiec -> 회사 등록으로 시작
 */
class EnlistViewController: UIViewController {
    
    private var topbox = UIView()
    
    private var titlelabel = PaddingLabel()
    
    private var backbtn = WideTapBtn()
    
    private var inputfield = PaddingTextField()
    
    
    //검색 결과가 없습니다.
    private var noResultFound = PaddingLabel()
    
    lazy private var enlistconfirmpopup = EnlistConfirmPopUp(frame: self.view.frame)
    
    
    //텍스트 필드 안 맵 -> 리스트
    private var inputfielddeletebtn = WideTapBtn()
    
    lazy var cv : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .vertical
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.delegate = presenter
        cv.dataSource = presenter
        return cv
    }()
    
    
    private var mapview = InaviMapView()
    
    /**
     집으로 등록버튼
     */
    private var mapselectbtn = ShadowBtn()
    /**
     현재 위치 버튼
     */
    private var currentlocationbtn = UIButton()
    
    /**
     지도 중앙 등록 이미지
     */
    private var centermarker = UIImageView()
    
    //현재 지도를 보고 있는지 리스트를 보고 있는지 판별하는 변수
    private var didEnterMapView : Bool = false
    
    
    lazy private var presenter = EnlistPresenter(delegate: self, viewcontroller: self)
    
    /**
     화면을 호출 할때 어떤 형태(집,회사)인지 셋하는 함수
     
    - parameter :
    - enlistmode : .house, .office
     */
    convenience init(enlistmode : enlistmode){
        self.init()
        presenter.setenlisttype(type: enlistmode)
        if enlistmode == .house {
            titlelabel.text = "집 등록"
        }
        else {
            titlelabel.text = "회사 등록"
        }
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.hideKeyboardWhenTappedAround()
        
        maketopbox()
        maketitlelabel()
        makebackbtn()
        makeinputfield()
        makechangebtn()
        makecv()

        makemapview()
        makenoresultFound()
        makemapselectbtn()
        makecurrentlocationbtn()
        makecentermarker()
        
        presenter.registerCV(cv: cv)
        backbtn.addTarget(self, action: #selector(backbtnpressed(sender:)), for: .touchUpInside)
        mapselectbtn.addTarget(self, action: #selector(mapselectbtnpressed(sender:)), for: .touchUpInside)
        inputfielddeletebtn.addTarget(self, action: #selector(inputfieldDeletenbtnpressed(sender:)), for: .touchUpInside)
        self.view.addSubview(enlistconfirmpopup)
        enlistconfirmpopup.alpha = 0
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        inputfield.layer.shadowColor = UIColor.rgb(red: 12, green: 161, blue: 155, alpha: 1).cgColor
        inputfield.layer.shadowRadius = 6
        inputfield.layer.shadowOpacity = 0.25
        inputfield.layer.shadowOffset = CGSize(width: 0, height: 3)
        inputfield.layer.cornerRadius = 8
        mapselectbtn.layer.cornerRadius = 25
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        InaviGeneralSearchRepo.shared.removeall()
        
    }
    
    
    /**
     현재 위치 버튼 이벤트 함수
     */
    @objc func currentlocationbtnpressed(sender : UIButton){
        let lat = UserDefaults.standard.double(forKey: userstandard.lat.rawValue)
        let lon = UserDefaults.standard.double(forKey: userstandard.lon.rawValue)
        mapview.moveCamera(INVCameraUpdate(targetTo: INVLatLng(lat: lat, lng: lon)))
    }
    /**
     백버튼 이벤트 함수
     */
    @objc func backbtnpressed(sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    /**
     키보드의 done을 눌렀을때 실행 되는 함수
     */
    @objc func keyboarddonebtnpressed(){
        didEnterMapView = false
        inputfield.resignFirstResponder()
        inputfielddeletebtn.isUserInteractionEnabled = false
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.mapselectbtn.alpha = 0
            self.mapview.alpha = 0
            self.cv.alpha = 1
            self.noResultFound.alpha = 0
        }
        presenter.callInaviGeneralSearchAPI(keyword: inputfield.text ?? "")
    }
    
    /**
     집으로 동록 / 회사로 등록 이벤트 함수
     */
    @objc func mapselectbtnpressed(sender : UIButton){
        presenter.enlist()
    }
    
    /**
     텍스트 필드 안x 버튼 눌렀을때 -> 맵 에서 리스트로 변화
     */
    @objc func inputfieldDeletenbtnpressed(sender : UIButton){
        didEnterMapView = false
        inputfielddeletebtn.setImage(UIImage(named: "inputIcMyN"), for: .normal)
        inputfielddeletebtn.isUserInteractionEnabled = false
        inputfield.becomeFirstResponder()
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.mapselectbtn.alpha = 0
            self.mapview.alpha = 0
            self.cv.alpha = 1
            self.noResultFound.alpha = 0
        }
    }
    
}
extension EnlistViewController : EnlistContract {
    /**
     주소 셀클릭후 맵을 보여주는 함수
     
    - parameter :
    - lat : 위도
    - long : 경도
    - type : .house, .office
    - addr : 주소
     */
    func showmapview(lat: Double, long: Double, type: enlistmode,addr : String){
        inputfielddeletebtn.setImage(UIImage(named: "inputBtnDelBN"), for: .normal)
        inputfielddeletebtn.isUserInteractionEnabled = true
        didEnterMapView = true
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.mapselectbtn.alpha = 1
            self.mapview.alpha = 1
            self.cv.alpha = 0
            self.centermarker.alpha = 1
            self.noResultFound.alpha = 0
        }
        if type == .house {
            mapselectbtn.setTitle("집으로 등록", for: .normal)
        }
        else {
            mapselectbtn.setTitle("회사로 등록", for: .normal)
        }
        inputfield.text = addr
        mapview.moveCamera(INVCameraUpdate(targetTo: INVLatLng(lat: lat, lng: long)))
    }
    /**
     주소 검색 리스트 리프레시 함수
     */
    func refreshcv(isEmpty : Bool) {
        didEnterMapView = false
        if isEmpty {
            self.mapselectbtn.alpha = 0
            self.mapview.alpha = 0
            self.cv.alpha = 0
            self.centermarker.alpha = 0
            self.noResultFound.alpha = 1
        }
        else {
            self.mapselectbtn.alpha = 0
            self.mapview.alpha = 0
            self.cv.alpha = 1
            self.centermarker.alpha = 0
            self.noResultFound.alpha = 0
        }
        self.cv.reloadData()
    }
    /**
     집 혹은 회사 등록 확인 팝업 표출
     */
    func setenlistconfirmpopup(content : String){
        self.enlistconfirmpopup.setlabel(label: content)
        
        self.enlistconfirmpopup.fadeIn(0.5, delay: 0) { [unowned self] (done) in
            self.enlistconfirmpopup.fadeOut(0.5, delay: 1) { [unowned self] (done) in
                self.navigationController?.popViewController(animated: true)
            }
        }
    }
}
extension EnlistViewController : INVMapViewDelegate {
    func mapViewDidBecomeIdle(_ mapView: InaviMapView) {
        if didEnterMapView {
            presenter.callreversegeocodeapi(posx: mapView.cameraPosition.target.lng, posy: mapView.cameraPosition.target.lat) { [unowned self] (addr, done) in
                self.inputfield.text = addr
            }
        }
    }
    
}
extension EnlistViewController {
    private func maketopbox(){
        self.view.addSubview(topbox)
        topbox.translatesAutoresizingMaskIntoConstraints = false
        topbox.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        topbox.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        topbox.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        topbox.heightAnchor.constraint(equalToConstant: 56).isActive = true
        topbox.backgroundColor = .white
    }
    private func makebackbtn(){
        self.view.addSubview(backbtn)
        backbtn.translatesAutoresizingMaskIntoConstraints = false
        backbtn.centerYAnchor.constraint(equalTo: topbox.centerYAnchor, constant: 0).isActive = true
        backbtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        backbtn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        backbtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        backbtn.setImage(UIImage(named: "titleBtnBackN"), for: .normal)
        
    }
    private func maketitlelabel(){
        self.view.addSubview(titlelabel)
        titlelabel.translatesAutoresizingMaskIntoConstraints = false
        titlelabel.centerYAnchor.constraint(equalTo: topbox.centerYAnchor, constant: 0).isActive = true
        titlelabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        titlelabel.widthAnchor.constraint(equalToConstant: 150).isActive = true
        titlelabel.heightAnchor.constraint(equalToConstant: 23).isActive = true
        titlelabel.textColor = .rgb(red: 75, green: 75, blue: 75, alpha: 1)
        titlelabel.font = UIFont(name: fonts.bold.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .bold)
        titlelabel.textAlignment = .center
        
    }
    private func makeinputfield(){
        self.view.addSubview(inputfield)
        inputfield.translatesAutoresizingMaskIntoConstraints = false
        inputfield.topAnchor.constraint(equalTo: topbox.bottomAnchor, constant: 0).isActive = true
        inputfield.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        inputfield.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        inputfield.heightAnchor.constraint(equalToConstant: 54).isActive = true
        inputfield.textColor = .rgb(red: 70, green: 70, blue: 70, alpha: 1)
        inputfield.font = UIFont(name: fonts.medium.rawValue, size: 20) //.systemFont(ofSize: 20, weight: .medium)
        inputfield.left = 19
        inputfield.right = 40
        inputfield.placeholder = "장소명 또는 주소"
        inputfield.layer.borderColor = maincolor.cgColor
        inputfield.layer.borderWidth = 1
        inputfield.backgroundColor = .white
        inputfield.addDoneOnKeyboardWithTarget(self, action: #selector(keyboarddonebtnpressed), titleText: "검색")
    }
    private func makechangebtn(){
        self.view.addSubview(inputfielddeletebtn)
        inputfielddeletebtn.translatesAutoresizingMaskIntoConstraints = false
        inputfielddeletebtn.centerYAnchor.constraint(equalTo: inputfield.centerYAnchor, constant: 0).isActive = true
        inputfielddeletebtn.trailingAnchor.constraint(equalTo: inputfield.trailingAnchor, constant: -17).isActive = true
        inputfielddeletebtn.widthAnchor.constraint(equalToConstant: 20).isActive = true
        inputfielddeletebtn.heightAnchor.constraint(equalToConstant: 20).isActive = true
        inputfielddeletebtn.setImage(UIImage(named: "inputIcMyN"), for: .normal)
        inputfielddeletebtn.setImage(UIImage(named: "inputBtnDelBN"), for: .selected)
        inputfielddeletebtn.isSelected = false
        inputfielddeletebtn.isUserInteractionEnabled = false
    }
    
    private func makecv(){
        self.view.addSubview(cv)
        cv.translatesAutoresizingMaskIntoConstraints = false
        cv.topAnchor.constraint(equalTo: inputfield.bottomAnchor, constant: 13).isActive = true
        cv.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        cv.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        cv.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        cv.backgroundColor = .white
    }
    private func makemapview(){
        self.view.addSubview(mapview)
        mapview.translatesAutoresizingMaskIntoConstraints = false
        mapview.topAnchor.constraint(equalTo: inputfield.bottomAnchor, constant: 20).isActive = true
        mapview.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        mapview.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        mapview.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        mapview.alpha = 0
        mapview.delegate = self
    }
    private func makemapselectbtn(){
        self.view.addSubview(mapselectbtn)
        mapselectbtn.translatesAutoresizingMaskIntoConstraints = false
        mapselectbtn.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -12).isActive = true
        mapselectbtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        mapselectbtn.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        mapselectbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        mapselectbtn.backgroundColor = .white
        mapselectbtn.setTitle("집으로 등록", for: .normal)
        mapselectbtn.setTitleColor(.rgb(red: 60, green: 60, blue: 60, alpha: 1), for: .normal)
        mapselectbtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20) //.systemFont(ofSize: 20, weight: .bold)
        mapselectbtn.shadowcolor = .black
        mapselectbtn.shadowoffset = CGSize(width: 0, height: 3)
        mapselectbtn.shadowradius = 6
        mapselectbtn.shadowopacity = 0.16
        mapselectbtn.alpha = 0
    }
    private func makecurrentlocationbtn(){
        self.view.addSubview(currentlocationbtn)
        currentlocationbtn.translatesAutoresizingMaskIntoConstraints = false
        currentlocationbtn.bottomAnchor.constraint(equalTo: mapselectbtn.topAnchor, constant: 0).isActive = true
        currentlocationbtn.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -3).isActive = true
        currentlocationbtn.widthAnchor.constraint(equalToConstant: 54).isActive = true
        currentlocationbtn.heightAnchor.constraint(equalToConstant: 54).isActive = true
        currentlocationbtn.setImage(UIImage(named: "mapBtnMy"), for: .normal)
        currentlocationbtn.alpha = 0
    }
    private func makecentermarker(){
        self.view.addSubview(centermarker)
        centermarker.translatesAutoresizingMaskIntoConstraints = false
        centermarker.bottomAnchor.constraint(equalTo: mapview.centerYAnchor, constant: 0).isActive = true
        centermarker.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        centermarker.widthAnchor.constraint(equalToConstant: 58).isActive = true
        centermarker.heightAnchor.constraint(equalToConstant: 38).isActive = true
        centermarker.image = UIImage(named: "mapPinDep")
        centermarker.alpha = 0
    }
    private func makenoresultFound(){
        self.view.addSubview(noResultFound)
        noResultFound.translatesAutoresizingMaskIntoConstraints = false
        noResultFound.centerYAnchor.constraint(equalTo: mapview.centerYAnchor, constant: 0).isActive = true
        noResultFound.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        noResultFound.textColor = .rgb(red: 184, green: 184, blue: 184, alpha: 1)
        noResultFound.font = UIFont(name: fonts.medium.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .medium)
        noResultFound.textAlignment = .center
        noResultFound.text = "검색 결과가 없습니다.\n강남역 (으)로 다시 검색할까요?"
        noResultFound.alpha = 0
    }
}
