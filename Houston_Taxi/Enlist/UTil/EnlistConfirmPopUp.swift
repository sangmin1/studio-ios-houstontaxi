//
//  EnlistConfirmPopUp.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/14.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class EnlistConfirmPopUp: UIView {
    
    private var box = ShadowView()
    
    private var label = PaddingLabel()
    
    
    override init(frame : CGRect){
        super.init(frame: frame)
        self.backgroundColor = UIColor.init(white: 0, alpha: 0.2)
        makelabel()
        makebox()
        self.sendSubviewToBack(box)
        
    }
    
    
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async { [unowned self] in
            self.box.layer.cornerRadius = 8
        }
    }
    
    /**
     내용 셋업 하는 함수
     */
    func setlabel(label : String){
        self.label.text = label
        
    }
    
}
extension EnlistConfirmPopUp {
   
    private func makelabel(){
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        label.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        label.widthAnchor.constraint(equalToConstant: 200).isActive = true
        label.heightAnchor.constraint(lessThanOrEqualToConstant: 100).isActive = true
        
        label.textAlignment = .center
        label.font = UIFont(name: fonts.medium.rawValue, size: 20)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        
        
        
    }
    private func makebox(){
        self.addSubview(box)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        box.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        box.widthAnchor.constraint(equalToConstant: 315).isActive = true
        box.heightAnchor.constraint(equalTo: label.heightAnchor, multiplier: 1,constant: 60).isActive = true
        box.backgroundColor = .white
        
        box.shadowcolor = .black
        box.shadowoffset = CGSize(width: 0, height: 3)
        box.shadowradius = 6
        box.shadowopacity = 0.1
        
        
        
    }
    
    
    
    
}
