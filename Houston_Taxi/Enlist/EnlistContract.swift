//
//  EnlistHouseContract.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation


protocol EnlistContract : NSObjectProtocol {
    func showmapview(lat: Double, long: Double, type: enlistmode,addr : String)
    func refreshcv(isEmpty : Bool)
    func setenlistconfirmpopup(content : String)
}
