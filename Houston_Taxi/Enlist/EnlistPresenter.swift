//
//  EnlistHousePresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

class EnlistPresenter: NSObject {
    
    weak var delegate : EnlistContract?
    weak var viewcontroller : EnlistViewController?
    
    let housecellid = "enlisthousecellid"
    let officecellid = "enlistofficecellid"
    private var enlistmode : enlistmode = .house
    private var currentkeyword : String = ""
    
    private var currentaddress : String = ""
    
    private var currentposx : Double = 0
    private var currentposy : Double = 0
    
    
    init(delegate : EnlistContract, viewcontroller : EnlistViewController){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
    }
    /**
     현재 등록 모드를 셋하는 함수
     
    - parameters :
    - type : .house , .office
     */
    func setenlisttype(type : enlistmode){
        enlistmode = type
    }
    
    func registerCV(cv : UICollectionView){
        cv.register(AddressCell.self, forCellWithReuseIdentifier: housecellid)
        cv.register(AddressCell.self, forCellWithReuseIdentifier: officecellid)
    }
    
    /**
     주소 통합검색  api 함수
     
    - parameters :
    - keyword : 검색 키워드
     */
    func callInaviGeneralSearchAPI(keyword : String){
        FunctionClass.shared.showdialog(show: true)
        let api = InaviSearchGeneralSearchAPI()
        currentkeyword = keyword
        api.request_To_server(keyword: keyword) { (done, data) in
            if done {
                print("data",data)
                let tempmodel = InaviGeneralSearchModel(json: data)
                InaviGeneralSearchRepo.shared.setmodel(input: tempmodel)
                let isempty = tempmodel.poi.count == 0 ? true : false
                self.delegate?.refreshcv(isEmpty: isempty)
            }
            else {
                
            }
            FunctionClass.shared.showdialog(show: false)
        }
        
    }
    
    let reversegeocodeapi = ReverGeoCodingAPI()
    
    /**
     지도 이동시 리버스 지오코딩을 실행하는 하수
     
     - parameters :
      - posx : x좌표
      - posy : y좌표
      - addr : 콜백 결과 주소
      - done : 콜백 성공 여부
     */
    func callreversegeocodeapi(posx : Double, posy : Double, completion : @escaping(_ addr : String, _ done : Bool) -> ())  {
        currentposx = posx
        currentposy = posy
        reversegeocodeapi.request_To_server(posx: posx, posy: posy) { (done, data) in
            if done {
                let tempmodel = ReverseGeoCodingModel(json: data)
                
                guard let adm = tempmodel.adm else {
                    completion("",false)
                    return
                }
                
                guard let bldname = adm.bldname else {
                    completion("",true)
                    return
                }
                //빌딩이름이 없으면 도로명 주소 표출
                if bldname == "" {
                    guard let roadname = adm.roadname else {
                        completion("",true)
                        return
                    }
                    self.currentaddress = roadname
                    completion(roadname,true)
                    return
                }
                else {
                    //빌딩 이름이 있으면 빌딩명 표출
                    self.currentaddress = bldname
                    completion(bldname,true)
                }
            }
            else {
                completion("",false)
            }
        }
    }

    /**
     내부 디비에 집 또는 회사를 저장하는 함수
     */
    func enlist(){
        if enlistmode == .house {
            let str = """
            '\(currentaddress)'을
            집으로 등록하였습니다.
            """
            self.delegate?.setenlistconfirmpopup(content: str)
            
            let model = HouseOfficeModel()
            
            model.address = self.currentaddress
            model.lat = self.currentposy
            model.lon = self.currentposx
            model.name = "집"
            model.type = "house"
            HouseOfficeRepo.shared.resetmodel(model: model)
        }
        else {
            let str = """
            '\(currentaddress)'을
            회사로 등록하였습니다.
            """
            self.delegate?.setenlistconfirmpopup(content: str)
            let model = HouseOfficeModel()
            
            model.address = self.currentaddress
            model.lat = self.currentposy
            model.lon = self.currentposx
            model.name = "회사"
            model.type = "office"
            HouseOfficeRepo.shared.resetmodel(model: model)
        }
    }
    
    
}
extension EnlistPresenter : UICollectionViewDataSource, UICollectionViewDelegate, UICollectionViewDelegateFlowLayout {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return InaviGeneralSearchRepo.shared.getpoi().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let model = InaviGeneralSearchRepo.shared.getpoi()[indexPath.row]
        if enlistmode == .house {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: housecellid, for: indexPath) as! AddressCell
            if let title = model.name1, let lat = model.rpy, let lon = model.rpx, let poiid = model.poiid, let addr = model.address  {
                cell.settitle(title: title, keyword: currentkeyword)
                cell.setcoordinate(lat : lat , lon : lon)
                cell.setpoiid(poiid: poiid)
                cell.setaddresslabel(address: addr)
                cell.seticon(image: UIImage(named: "dtListBtnAddHomeN")!)
            }
            else {
                cell.settitle(title: "오류입니다", keyword: "오류입니다")
                cell.setcoordinate(lat : 0.0 , lon : 0.0)
                cell.setpoiid(poiid: 0)
                cell.setaddresslabel(address: "오류입니다")
            }
            return cell
        }
        else {
            let cell = collectionView.dequeueReusableCell(withReuseIdentifier: officecellid, for: indexPath) as! AddressCell
            if let title = model.name1, let lat = model.rpy, let lon = model.rpx, let poiid = model.poiid, let addr = model.address  {
                cell.settitle(title: title, keyword: currentkeyword)
                cell.setcoordinate(lat : lat , lon : lon)
                cell.setpoiid(poiid: poiid)
                cell.setaddresslabel(address: addr)
                cell.seticon(image: UIImage(named: "dtListBtnAddOfficeN")!)
            }
            else {
                cell.settitle(title: "오류입니다", keyword: "오류입니다")
                cell.setcoordinate(lat : 0.0 , lon : 0.0)
                cell.setpoiid(poiid: 0)
                cell.setaddresslabel(address: "오류입니다")
            }
            return cell
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: mainboundwidth, height: 84)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! AddressCell
        currentposx = cell.getcoordinate().lon
        currentposy = cell.getcoordinate().lat
        if let address = cell.getaddress() {
             currentaddress = address
        }
        self.delegate?.showmapview(lat: cell.getcoordinate().lat, long: cell.getcoordinate().lon, type: enlistmode,addr : cell.gettitle())
        
    }
    
    
    
    
    
    
    
    
    
    
    
}
