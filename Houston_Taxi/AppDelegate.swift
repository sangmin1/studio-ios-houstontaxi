//
//  AppDelegate.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import UIKit
import iNaviMaps
import IQKeyboardManagerSwift

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate,INVMapSdkDelegate {


    
    

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        // Override point for customization after application launch.
        INVMapSdk.sharedInstance().delegate = self
        INVMapSdk.sharedInstance().appKey = "GEa2XAYXee1B6cmd"
        IQKeyboardManager.shared.enable = true
        
        
        UserDefaults.standard.register(defaults: [userstandard.lat.rawValue : 0,
                                                  userstandard.lon.rawValue : 0,
                                                  userstandard.directcallfirsttime.rawValue : true,
                                                  userstandard.phonenumber.rawValue : "",
                                                  userstandard.isverified.rawValue : false,
                                                  userstandard.callid.rawValue : "",
                                                  userstandard.calldt.rawValue : "",
                                                  userstandard.destname.rawValue : "",
                                                  userstandard.destlon.rawValue : 0,
                                                  userstandard.destlat.rawValue : 0,
                                                  userstandard.startlon.rawValue : 0,
                                                  userstandard.startlat.rawValue : 0])
        
        UserDefaults.standard.set(["0317914114"], forKey: userstandard.callcenternums.rawValue)
        UserDefaults.standard.register(defaults: [userstandard.callcenterSelected.rawValue : [false]])
        return true
    }

   
    
    /**
     아이나비 맵 인증 실패시 호출되는 함수
     */
    func authFailure(_ errorCode: Int, message: String) {
        print("map failed errorcode\(errorCode) message : \(message)")
        // 인증 실패 처리
    }
     // MARK: UISceneSession Lifecycle
    func application(_ application: UIApplication, configurationForConnecting connectingSceneSession: UISceneSession, options: UIScene.ConnectionOptions) -> UISceneConfiguration {
        // Called when a new scene session is being created.
        // Use this method to select a configuration to create the new scene with.
        return UISceneConfiguration(name: "Default Configuration", sessionRole: connectingSceneSession.role)
    }

    func application(_ application: UIApplication, didDiscardSceneSessions sceneSessions: Set<UISceneSession>) {
        // Called when the user discards a scene session.
        // If any sessions were discarded while the application was not running, this will be called shortly after application:didFinishLaunchingWithOptions.
        // Use this method to release any resources that were specific to the discarded scenes, as they will not return.
    }


}

