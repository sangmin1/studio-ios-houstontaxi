//
//  MainFrequencyCell.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/15.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 빈도수 셀
 */
class MainFrequencyCell: UICollectionViewCell {
    
    private var label = PaddingLabel()
    private var id : String = ""
    private var lon : String = ""
    private var lat : String = ""
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .clear
        self.layer.borderColor = UIColor.init(white: 1, alpha: 0.3).cgColor
        self.layer.borderWidth = 1
        makelabel()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        DispatchQueue.main.async { [weak self] in
            self?.layer.cornerRadius = 20
            
        }
    }
    //셀의 주소를 셋하는 함수
    func setlabel(label : String){
        self.label.text = label
    }
    /**
     목적지의 좌표를 셋하는 함수
     
    - parameter :
    - lat : 위도
    - lon : 경도
     */
    func setlatlon(lat : String, lon : String){
        self.lat = lat
        self.lon = lon
    }
    /**
     목적지 아이디를 셋하는 함수
     */
    func setid(id :String){
        self.id = id
    }
    /**
     목적지 의 좌표를 반환 하는 함수
     */
    func getlatlon() ->(lat : Double, lon : Double){
        return (Double(lat)!,Double(lon)!)
    }
    /**
     목적지의 아이디를 반환하는 함수
     */
    func getid() -> String {
        return self.id
    }
    /**
     목적지의 이름을 반환하는 함수
     */
    func getname() -> String {
        return self.label.text ?? ""
    }
    
}
extension MainFrequencyCell {
    private func makelabel(){
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.topAnchor.constraint(equalTo: self.topAnchor, constant: 8).isActive = true
        label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 14).isActive = true
        label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -14).isActive = true
        label.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -8).isActive = true
        label.textColor = .white
        label.font = UIFont(name: fonts.medium.rawValue, size: 18)   //.systemFont(ofSize: 18, weight: .medium)
        label.text = "현대백화점 판교..."
        label.textAlignment = .center
    }
    
    
}

