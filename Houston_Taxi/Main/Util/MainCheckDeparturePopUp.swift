//
//  MainCheckDeparturePopUp.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/15.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit



/**
 출발지 확인
 */
class MainCheckDeparturePopUp: UIView {
    
    private var box = UIView()
    
    //계신 곳이 여기가 맞나요?
    private var titlelabel = PaddingLabel()
    //지동를 이동해 출발지를
    private var infolabel = PaddingLabel()
    //주소 라벨
    private var addresslabel = PaddingLabel()
    //확인 버튼
    private var confirmbtn = UIButton()
    
    //취소 버튼
    private var cancelbtn = UIButton()
    
    enum popuptype {
        case immediate
        case enlistedplace
    }
    
    private var type : popuptype = .immediate
    private var enlisttype : enlistmode = .house
    weak var delegate : MainCheckDeparturePopUpDelegate?
    
    init(frame: CGRect,delegate : MainCheckDeparturePopUpDelegate ) {
        super.init(frame: frame)
        self.delegate = delegate
        makebox()
        maketitlelabel()
        makeinfolabel()
        makeaddresslabel()
        makecancelbtn()
        makeconfirmbtn()
        confirmbtn.addTarget(self, action: #selector(confirmbtnpressed(sender:)), for: .touchUpInside)
        cancelbtn.addTarget(self, action: #selector(cancebtnpressed(sender:)), for: .touchUpInside)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func didMoveToWindow() {
        super.didMoveToWindow()
        DispatchQueue.main.async { [weak self] in
            self?.box.roundCorners(corners: [.topRight,.topLeft], radius: 20)
        }
    }
    
    /**
     현재 위치 텍스트 지정 함수
     */
    func setcurrentloc(addr : String){
        self.addresslabel.text = addr
    }
    /**
     확인 버튼 눌렀을대 실행 되는 함수
     
     type 변수 바로 호출(immediate) enlisted(집,회사)냐에 따라 실행 동작이 다름
     */
    @objc func confirmbtnpressed(sender : UIButton){
        if type == .immediate {
            self.delegate?.callTaxiDirectWithoutDest()
        }
        else {
            self.delegate?.callTaxiWithDest(type: enlisttype)
        }
        
    }
    
    /**
     취소 버튼 이벤트 함수
     */
    @objc func cancebtnpressed(sender : UIButton){
        self.delegate?.departurecheckpopupcanceld()
    }
    /**
     현재 집 인지 회사인지 선택여부를 셋하는 함수
     */
    func setenlisttype(type : enlistmode){
        self.enlisttype = type
    }
    /**
     바로 호출로 띄워졌는지 , 등록된 회사, 집, 으로 띄워졌는지 셋하는 함수
     */
    func setpopuptype(type : popuptype){
        self.type = type
    }
    
    
    
    
}
extension MainCheckDeparturePopUp {
    private func makebox(){
        self.addSubview(box)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        box.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        box.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        box.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        box.backgroundColor = .white
    }
    private func maketitlelabel(){
        self.addSubview(titlelabel)
        titlelabel.translatesAutoresizingMaskIntoConstraints = false
        titlelabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 20).isActive = true
        titlelabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        titlelabel.widthAnchor.constraint(equalToConstant: 250).isActive = true
        titlelabel.heightAnchor.constraint(equalToConstant: 27).isActive = true
        titlelabel.textColor = .rgb(red: 70, green: 70, blue: 70, alpha: 1)
        titlelabel.font = UIFont(name: fonts.bold.rawValue, size: 21) //.systemFont(ofSize: 21, weight: .bold)
        titlelabel.textAlignment = .center
        titlelabel.text = "계신 곳이 여기가 맞나요?"
    }
    private func makeinfolabel(){
        self.addSubview(infolabel)
        infolabel.translatesAutoresizingMaskIntoConstraints = false
        infolabel.topAnchor.constraint(equalTo: titlelabel.bottomAnchor, constant: 3).isActive = true
        infolabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        infolabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        infolabel.heightAnchor.constraint(equalToConstant: 20).isActive = true
        infolabel.textColor = .rgb(red: 184, green: 184, blue: 184, alpha: 1)
        infolabel.font = UIFont(name: fonts.medium.rawValue, size: 15) //.systemFont(ofSize: 15, weight: .medium)
        infolabel.textAlignment = .center
        infolabel.text = "지도를 이동해 출발지를 설정해 주세요."
    }
    private func makeaddresslabel(){
        self.addSubview(addresslabel)
        addresslabel.translatesAutoresizingMaskIntoConstraints = false
        addresslabel.topAnchor.constraint(equalTo: infolabel.bottomAnchor, constant: 17).isActive = true
        addresslabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        addresslabel.widthAnchor.constraint(equalToConstant: 300).isActive = true
        addresslabel.heightAnchor.constraint(equalToConstant: 23).isActive = true
        addresslabel.textColor = maincolor
        addresslabel.font = UIFont(name: fonts.medium.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .medium)
        addresslabel.textAlignment = .center
        addresslabel.text = "성남시 분당구 대왕판교로 644번길 21"
    }
    
    private func makecancelbtn(){
        self.addSubview(cancelbtn)
        cancelbtn.translatesAutoresizingMaskIntoConstraints = false
        cancelbtn.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        cancelbtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        cancelbtn.trailingAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        cancelbtn.heightAnchor.constraint(equalToConstant: 54).isActive = true
        cancelbtn.backgroundColor = maincolor
        cancelbtn.setTitle("취소", for: .normal)
        cancelbtn.setTitleColor(.white, for: .normal)
        cancelbtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20) //.systemFont(ofSize: 20, weight: .bold)
        
        
    }
    private func makeconfirmbtn(){
        self.addSubview(confirmbtn)
        confirmbtn.translatesAutoresizingMaskIntoConstraints = false
        confirmbtn.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        confirmbtn.leadingAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        confirmbtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        confirmbtn.heightAnchor.constraint(equalToConstant: 54).isActive = true
        confirmbtn.backgroundColor = maincolor
        confirmbtn.setTitle("확인", for: .normal)
        confirmbtn.setTitleColor(.white, for: .normal)
        confirmbtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20) //.systemFont(ofSize: 20, weight: .bold)
        
    }
    
}
