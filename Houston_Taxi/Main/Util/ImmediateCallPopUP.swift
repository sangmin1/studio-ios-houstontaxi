//
//  ImmediateCallPopUP.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/15.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
  바로 호출 팝업 ( 전체 화면)
 */
class ImmediateCallPopUP: UIView {
    //x 버튼
    private var closebtn = WideTapBtn()
    //바로 호출
    private var titlelabel = PaddingLabel()
    //도착지를 ~~~
    private var infolabel = PaddingLabel()
    //스마트폰 이미지
    private var image = UIImageView()
    //바로 호출 시작하기 버튼
    private var immediatecallbtn = UIButton()
    
    
    
    weak var delegate : ImmediateCallPopUPDelegate?
    
    init(frame: CGRect,delegate : ImmediateCallPopUPDelegate) {
        super.init(frame: frame)
        self.delegate = delegate
        self.backgroundColor = .white
        makeclosebtn()
        maketitlelabel()
        makeinfolabel()
        makeimage()
        makeimmediatecallBtn()
        
        closebtn.addTarget(self, action: #selector(closebtnpressed(sender:)), for: .touchUpInside)
        immediatecallbtn.addTarget(self, action: #selector(confirmbtnpressed(sender:)), for: .touchUpInside)
        immediatecallbtn.addTarget(self, action: #selector(confirmbtnpressed(sender:)), for: .touchUpInside)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    /**
     x 눌렀을때 실행되는 함수 -> 화면 fadeout
     */
    @objc func closebtnpressed(sender : UIButton){
        
        UIView.animate(withDuration: 0.2, animations: {[unowned self] in
            self.alpha = 0
            self.frame = CGRect(x: 0, y: mainboundheight, width: mainboundwidth, height: mainboundheight)
            
        }) { [unowned self] (done) in
            if done {
                self.delegate?.showdeparturecheckpopup()
            }
        }
    }
    
    
    /**
        바로 호출 버튼 이벤트 함수
     */
    @objc func confirmbtnpressed(sender : UIButton){
        self.delegate?.callTaxiDirectWithoutDest()
        
        
        
    }
    
    
    
    
    
    
    
}
extension ImmediateCallPopUP {
    private func makeclosebtn(){
        self.addSubview(closebtn)
        closebtn.translatesAutoresizingMaskIntoConstraints = false
        closebtn.topAnchor.constraint(equalTo: self.topAnchor, constant: SplashViewController.topinset + 13).isActive = true
        closebtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        closebtn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        closebtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        closebtn.setImage(UIImage(named: "titleBtnCloseN"), for: .normal)
    }
    private func maketitlelabel(){
        self.addSubview(titlelabel)
        titlelabel.translatesAutoresizingMaskIntoConstraints = false
        titlelabel.topAnchor.constraint(equalTo: self.topAnchor, constant: SplashViewController.topinset + 74 * hscale).isActive = true
        titlelabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        titlelabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        titlelabel.heightAnchor.constraint(equalToConstant: 31).isActive = true
        titlelabel.textColor = .rgb(red: 71, green: 71, blue: 71, alpha: 1)
        titlelabel.font = UIFont(name: fonts.bold.rawValue, size: 24) //.systemFont(ofSize: 24, weight: .bold)
        titlelabel.textAlignment = .center
        titlelabel.text = "바로 호출"
    }
    private func makeinfolabel(){
        self.addSubview(infolabel)
        infolabel.translatesAutoresizingMaskIntoConstraints = false
        infolabel.topAnchor.constraint(equalTo: titlelabel.bottomAnchor, constant: 12).isActive = true
        infolabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        infolabel.widthAnchor.constraint(equalToConstant: 250).isActive = true
        infolabel.heightAnchor.constraint(lessThanOrEqualToConstant: 80).isActive = true
        infolabel.numberOfLines = 0
        infolabel.lineBreakMode = .byWordWrapping
        infolabel.textColor = .rgb(red: 71, green: 71, blue: 71, alpha: 0.6)
        infolabel.font = UIFont(name: fonts.medium.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .medium)
        infolabel.text = "도착지를 설정하지 않고 간편하게\n택시를 부를 수 있는 기능이에요."
        infolabel.textAlignment = .center
    }
    private func makeimage(){
        self.addSubview(image)
        image.translatesAutoresizingMaskIntoConstraints = false
        image.topAnchor.constraint(equalTo: infolabel.bottomAnchor, constant: 4).isActive = true
        image.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        image.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        image.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -25).isActive = true
        image.contentMode = .scaleAspectFill
        image.image = UIImage(named: "imgTipDirectcall")
        
        
    }
    private func makeimmediatecallBtn(){
        self.addSubview(immediatecallbtn)
        immediatecallbtn.translatesAutoresizingMaskIntoConstraints = false
        immediatecallbtn.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        immediatecallbtn.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        immediatecallbtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        immediatecallbtn.heightAnchor.constraint(equalToConstant: 54).isActive = true
        immediatecallbtn.setTitle("바로 호출 시작하기", for: .normal)
        immediatecallbtn.setTitleColor(.white, for: .normal)
        immediatecallbtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20)  //.systemFont(ofSize: 20, weight: .bold)
        immediatecallbtn.backgroundColor = maincolor
    }
    
}
