//
//  MainWhereToPopUP.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/15.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit



/**
 메인 화면 어디로 가세요? 팝업
 */
class MainWhereToPopUp: UIView {
    
    private var box = UIView()
    //어디로 가세요?
    private var titlelabel = PaddingLabel()
    //집
    private var homebtn = WideTapBtn()
    private var homelabel = PaddingLabel()
    //회사
    private var companybtn = WideTapBtn()
    private var companylabel = PaddingLabel()
    //검색
    private var searchbtn = WideTapBtn()
    private var searchlabel = PaddingLabel()
    
    
    
    lazy private var btnstack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [homebtn,companybtn,searchbtn])
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.spacing = 24
        return stack
    }()
    
    weak var delegate : MainWhereToPopUpDelegate?
    
    init(frame : CGRect,delegate : MainWhereToPopUpDelegate){
        super.init(frame: frame)
        self.delegate = delegate
        self.backgroundColor = .clear
        makebox()
        maketitlelabel()
        makebtnstack()
        makelabels(label: homelabel, ascending: homebtn, title: "집")
        makelabels(label: companylabel, ascending: companybtn, title: "회사")
        makelabels(label: searchlabel, ascending: searchbtn, title: "검색")
        
        
        
        searchbtn.addTarget(self, action: #selector(searchbtnpressed(sender:)), for: .touchUpInside)
        homebtn.addTarget(self, action: #selector(housbtnpressed(sender:)), for: .touchUpInside)
        companybtn.addTarget(self, action: #selector(officebtnpressed(sender:)), for: .touchUpInside)
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        DispatchQueue.main.async { [weak self] in
            self?.box.roundCorners(corners: [.topRight,.topLeft], radius: 20)
            self?.box.clipsToBounds = true
        }
    }
    
    @objc func searchbtnpressed(sender : UIButton){
        self.delegate?.showSpecViewController()
    }
    /**
     집 버튼 눌렀을때 이벤트 함수
     등록 되어 있으면 택시 배차 화면으로
     아니면 집 등록 화면 으로 이동
     */
    @objc func housbtnpressed(sender : UIButton){
        self.delegate?.checkEnlistExist(type : .house)
    }
    /**
    회사 버튼 눌렀을때 이벤트 함수
    등록 되어 있으면 택시 배차 화면으로
    아니면 집 등록 화면 으로 이동
    */
    @objc func officebtnpressed(sender : UIButton){
        self.delegate?.checkEnlistExist(type : .office)
    }
    
    
}
extension MainWhereToPopUp {
    private func makebox(){
        self.addSubview(box)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        box.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        box.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        box.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        box.backgroundColor = .white
    }
    private func maketitlelabel(){
        self.addSubview(titlelabel)
        titlelabel.translatesAutoresizingMaskIntoConstraints = false
        titlelabel.topAnchor.constraint(equalTo: box.topAnchor, constant: 15).isActive = true
        titlelabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        titlelabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        titlelabel.heightAnchor.constraint(equalToConstant: 39).isActive = true
        titlelabel.textColor = .rgb(red: 70, green: 70, blue: 70, alpha: 1)
        titlelabel.font = UIFont(name: fonts.medium.rawValue, size: 30)    //.systemFont(ofSize: 30, weight: .medium)
        titlelabel.textAlignment = .center
        titlelabel.text = "어디로 가세요?"
    }
    private func makebtnstack(){
        self.addSubview(btnstack)
        btnstack.translatesAutoresizingMaskIntoConstraints = false
        btnstack.topAnchor.constraint(equalTo: titlelabel.bottomAnchor, constant: 20).isActive = true
        btnstack.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        btnstack.heightAnchor.constraint(equalToConstant: 76).isActive = true
        btnstack.widthAnchor.constraint(equalToConstant: 76 * 3 + 24 * 2).isActive = true
        homebtn.setImage(UIImage(named: "mainBtnHomeN"), for: .normal)
        companybtn.setImage(UIImage(named: "mainBtnOfficeN"), for: .normal)
        searchbtn.setImage(UIImage(named: "mainBtnSearchN"), for: .normal)
        
    }
    /**
     집 회사 검색 라벨을 배치하는 함수
     
    - parameter :
    - label : 배치시키려는 라벨
    - ascending : 참조를 맺어야햘 뷰
    - title : 라벨의 제목
     */
    private func makelabels(label : PaddingLabel, ascending : UIView, title : String){
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.topAnchor.constraint(equalTo: ascending.bottomAnchor, constant: 5).isActive = true
        label.centerXAnchor.constraint(equalTo: ascending.centerXAnchor, constant: 0).isActive = true
        label.widthAnchor.constraint(equalToConstant: 80).isActive = true
        label.heightAnchor.constraint(equalToConstant: 21).isActive = true
        label.textColor = .rgb(red: 70, green: 70, blue: 70, alpha: 1)
        label.font = UIFont(name: fonts.medium.rawValue, size: 16)  //  .systemFont(ofSize: 16, weight: .medium)
        label.textAlignment = .center
        label.text = title
    }
    
    
}
