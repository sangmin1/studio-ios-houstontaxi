//
//  FreqVisitRepo.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/25.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation



class FreqVisitRepo {
    static let shared = FreqVisitRepo()
    private init(){}
    
    
    
    private var model : [FregVisitModel] = []
    
    func setmodel(input : [FregVisitModel]){
        model = input
    }
    
    func getmodel() -> [FregVisitModel]{
        return self.model
    }
}
