//
//  FregVisitModel.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/25.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import SwiftyJSON

/**
 최빈 목적지 관련 정보 모델
 */
class FregVisitModel : NSObject {
    
    init(json : JSON){
        
        let idstr = json["id"].stringValue
            id = FunctionClass.shared.decrypt(msg: idstr)
        print("id",id)
        let num = json["hitNum"].stringValue
            hitNum = FunctionClass.shared.decrypt(msg: num)
        print("num",hitNum)
        let name = json["destinationName"].stringValue
            destinationName = FunctionClass.shared.decrypt(msg: name)
         print("name",name)
        let lon = json["destinationLon"].stringValue
            destinationLon = FunctionClass.shared.decrypt(msg: lon)
         print("destinationLon",destinationLon)
        
        let lat = json["destinationLat"].stringValue
        destinationLat = FunctionClass.shared.decrypt(msg: lat)
        
//        carNum = json["carNum"].string
//        driverName = json["driverName"].string
//        carColor = json["carColor"].string
//        carModel = json["carModel"].string
        
    }
    
    var id : String?
    var hitNum : String?
    var destinationName : String?
    var destinationLon : String?
    var destinationLat : String?
    
    
}
