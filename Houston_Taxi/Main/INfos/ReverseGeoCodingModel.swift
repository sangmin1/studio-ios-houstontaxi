//
//  ReverseGeoCodingModel.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/29.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import SwiftyJSON




// 참조 http://imapsapi.inavi.com/search.html#reference_addresses
class ReverseGeoCodingModel : NSObject {
    
    
    required init(json : JSON){
        header =  Header(json: json)
        location = Location(json: json)
        adm = Adm(json: json["location"]["adm"])
    }
    
    var header : Header?
    
    var location : Location?
    
    var adm : Adm?
    
    internal class Header : NSObject {
        
        required init(json : JSON){
            isSuccessful = json["isSuccessful"].bool
            resultCode = json["resultCode"].int
            resultMessage = json["resultMessage"].string
            
        }
        
        var isSuccessful : Bool?
        var resultCode : Int?
        var resultMessage : String?
        
        
    }
    
    
    internal class Location : NSObject {
        
        
        required init(json : JSON){
            address = json["address"].string
            distance = json["distance"].int
        }
        var hasAdmAddress : Bool?
        var address : String?
        var distance : Int? //미터 단위
        
    }
    
    internal class Adm : NSObject {
        
        required init(json : JSON){
            address = json["address"].string
            roadjibun = json["roadjibun"].string
            roadname = json["roadname"].string
            bldname = json["bldname"].string
            distance = json["distance"].int
        }
        
        var roadname : String?
        var roadjibun : String?
        var distance : Int?
        var address : String?
        var bldname : String?
        
    }
    
    
    
}







