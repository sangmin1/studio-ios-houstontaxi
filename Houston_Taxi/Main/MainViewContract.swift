//
//  MainViewContract.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/15.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation


protocol MainContract : NSObjectProtocol {
    func showCallWithDestViewController(destname : String , endx : Double, endy : Double)
    
    func showFindinTaxiViewController(allocdata : [String : Any])
    
}
protocol ImmediateCallPopUPDelegate : NSObjectProtocol {
    func showdeparturecheckpopup()
    func callTaxiDirectWithoutDest()
    
}
protocol MainWhereToPopUpDelegate : NSObjectProtocol {
    func showSpecViewController()
    func checkEnlistExist(type : enlistmode)
}
protocol MainCheckDeparturePopUpDelegate : NSObjectProtocol {
    func departurecheckpopupcanceld()
    func callTaxiDirectWithoutDest()
    func callTaxiWithDest(type : enlistmode)
}
protocol EnlistHouseAndOfficePopUPDelegate : NSObjectProtocol {
    func enlistbtnpresse(type : EnlistHouseAndOfficePopUP.popuptype)
    
    
}
