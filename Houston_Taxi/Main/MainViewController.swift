//
//  MainViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
import iNaviMaps

/**
 메인화면
 */
class MainViewController: UIViewController, UINavigationControllerDelegate {
    
    
    //메뉴 버튼
    private var menuBtn = WideTapBtn()
    
    lazy var mapView = InaviMapView(frame: view.bounds)
    
    //콜센터 버튼
    private var callcenterbtn = UIButton()
    //현재 위치 버튼
    private var currentlocationbtn = UIButton()
    
    //현재 위치버튼 어디로가세요 팝업과의 앵커
    private var currentlocationtowheretoanc : NSLayoutConstraint?
    //현재 위치 버튼 계신곳이 여기각 맞나요 팝업과의 앵커
    private var currentlocationtodeparturecheckanc : NSLayoutConstraint?
    
    
    
    //어디로 가세요 팝업
    lazy private var whereToPopUp = MainWhereToPopUp(frame: .zero,delegate : self)
    //바로 호출 팝업
    lazy private var directcallPopUP = ImmediateCallPopUP(frame: .zero, delegate: self)
    
    //출발지 확인 팝업
    lazy private var departurecheckpopup = MainCheckDeparturePopUp(frame: .zero, delegate: self)
    //출발지 확인 팝업 하단 앵커
    private var departurecheckbottomanc : NSLayoutConstraint?
    
    lazy private var enlistpopup = EnlistHouseAndOfficePopUP(frame: .zero, delegate: self)
    
    
    
    //바로 호출 버튼
    let directcallicon = UIButton()
    
    private var directcallpressed : Bool = false
    
    
    
    //지도 이동시 현재 주소 표시 라벨
    private var currentlocationlabel = PaddingLabel()
    
    
    
    var frequencygradientview = UIView()
    //빈도수 리스트 컬렉션뷰
    lazy var frequencycv : UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.scrollDirection = .horizontal
        layout.sectionInset = UIEdgeInsets(top: 7, left: 12, bottom: 7, right: 12)
        let cv = UICollectionView(frame: .zero, collectionViewLayout: layout)
        cv.delegate = presenter
        cv.dataSource = presenter
        cv.showsHorizontalScrollIndicator = false
        return cv
    }()
    
    
    //빈도수 및 초기 팝업 하단 앵커
    private var cvstackbottomanc : NSLayoutConstraint?
    
    lazy private var cvstack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [frequencycv])
        stack.axis = .horizontal
        stack.distribution = .fill
        return stack
    }()
    
    lazy private var presenter = MainPresenter(delegate: self, viewcontroller: self)
    
    //초기 팝업이 띄워져 있는지 여부
    private var iswhereToisUp : Bool = false
    //출발지 확인 팝업이 띄워져 있는지 여부
    private var isdepartureisUp : Bool = false
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .rgb(red: 150, green: 150, blue: 150, alpha: 1)
        self.navigationController?.navigationBar.isHidden = true
        self.navigationController?.delegate = self
        self.view.addSubview(mapView)
        
        mapView.delegate = self
        mapView.userTrackingMode = .noTracking
        mapView.locationIcon.circleRadius = 25
        mapView.locationIcon.circleColor = .rgb(red: 10, green: 185, blue: 180, alpha: 0.3)
        
        
        
        
        
        
        makecvstack()
        makewhereToPopUp()
        makecallcenterbtn()
        makecurrentlocationbtn()
        makemenubtn()
        makedirectcallPopUp()
        makedeparturecheckpopUp()
        makeenlistpopup()
        makedirectcallicon()
        makecurrentlocationlabel()
        
        
        
        
        presenter.registerFrequencyCell(cv: frequencycv)
        callcenterbtn.addTarget(self, action: #selector(callcenterbtnpressed(sender:)), for: .touchUpInside)
        currentlocationbtn.addTarget(self, action: #selector(currentlocationbtnpressed(sender:)), for: .touchUpInside)
        menuBtn.addTarget(self, action: #selector(menubtnpressed(sender:)), for: .touchUpInside)
        directcallicon.addTarget(self, action: #selector(directcallbtnpressed(sender:)), for: .touchUpInside)
        self.setinitiallocationpos()
        
        //최근 목적지가 있는지 없는지 에 따라 뷰를 숨겨줌
        frequencycv.isHidden = presenter.isFreqEmpty()
            
        
        self.presenter.checkTaxiState()
    
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        frequencygradientview.createGradientLayer(colors: .rgb(red: 40, green: 90, blue: 145, alpha: 1), .rgb(red: 12, green: 161, blue: 155, alpha: 1), start: CGPoint(x: 0, y: 0.5), mid: CGPoint(x: 0.5, y: 0.5), end: CGPoint(x: 1, y: 0.5))
        currentlocationlabel.layer.cornerRadius = 9
        currentlocationlabel.clipsToBounds = true
        
    }
    /**
     화면 진입시 현재 위치에 마커를 나타내는 함수
     */
    func setinitiallocationpos(){
        let lat = UserDefaults.standard.double(forKey: userstandard.lat.rawValue)
        let lon = UserDefaults.standard.double(forKey: userstandard.lon.rawValue)
        let locicon = mapView.locationIcon
        locicon.position = INVLatLng(lat: lat, lng: lon)
        locicon.isVisible = true
        mapView.moveCamera(INVCameraUpdate(targetTo: INVLatLng(lat: lat, lng: lon)))
        
    }
    /**
     콜센터 버튼 이벤트 함수
     */
    @objc func callcenterbtnpressed(sender : UIButton){
        if CallCenterRepo.shared.selected.elementsEqual(Array(repeating: false, count: CallCenterRepo.shared.selected.count)) {
            let view = CallCenterViewController()
            self.navigationController?.pushViewController(view, animated: true)
        }
        else {
            let num = CallCenterRepo.shared.number[0]
            guard let number = URL(string: "tel://" + num) else { return }
            UIApplication.shared.open(number)
            presenter.callPassengerGPSAPI()
            presenter.runrecentalloctimer()
        }
    }
    
    /**
     현재 위치로 가기 버튼 이벤트
     */
    @objc func currentlocationbtnpressed(sender : UIButton){
        let lat = UserDefaults.standard.double(forKey: userstandard.lat.rawValue)
        let lon = UserDefaults.standard.double(forKey: userstandard.lon.rawValue)
        mapView.moveCamera(INVCameraUpdate(targetTo: INVLatLng(lat: lat, lng: lon)))
    }
    /**
     슬라이드 메뉴 버튼 이벤트 함수
     */
    @objc func menubtnpressed(sender : UIButton){
        let scene = UIApplication.shared.connectedScenes.first
        guard let del = scene?.delegate as? SceneDelegate else { return }
        del.window?.bringSubviewToFront(del.slidemenu)
        del.slidemenu.setviewdelegate(delegate: self)
        UIView.animate(withDuration: 0.2) {
            del.slidemenu.frame = CGRect(x: 0, y: 0, width: mainboundwidth, height: mainboundheight)
        }
    }
    
    /**
     바로 호출하기 버튼 (맵에 있는것) 이벤트 함수
     */
    @objc func directcallbtnpressed(sender : UIButton){
        directcallpressed = true
        
        /**
         택시 호출 불가 지역일때 토스트를 띄우며 진행 하지 않음
         */
        if presenter.getcurrentplaceInfos().addr == "" {
            directcallpressed = false
            ToastView.shared.short(self.view, txt_msg: "택시를 호출 할 수 없는 지역입니다.")
            return
        }
        //최초 실행시 팝업창 표출
        if UserDefaults.standard.bool(forKey: userstandard.directcallfirsttime.rawValue) == true {
            self.departurecheckpopup.setpopuptype(type: .immediate)
            UserDefaults.standard.set(false, forKey: userstandard.directcallfirsttime.rawValue)
            self.view.bringSubviewToFront(directcallPopUP)
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.directcallPopUP.alpha = 1
                
            }
        }
        else {
            //팝업창 표출 x
            self.departurecheckpopup.setpopuptype(type: .immediate)
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.departurecheckpopup.alpha = 1
                self.departurecheckbottomanc?.constant = 0
                self.currentlocationtodeparturecheckanc?.isActive = true
                self.currentlocationtowheretoanc?.isActive = false
                self.frequencycv.alpha = 0
                self.whereToPopUp.alpha = 0
                self.cvstackbottomanc?.constant = 180
            }
        }
    }
    
    
    func navigationController(_ navigationController: UINavigationController, willShow viewController: UIViewController, animated: Bool) {
        UIView.animate(withDuration: 0.2, animations: { [unowned self] in
            self.frequencycv.alpha = 1
            self.whereToPopUp.alpha = 1
            self.cvstackbottomanc?.constant = 0
            self.callcenterbtn.alpha = 1
            self.departurecheckpopup.alpha = 0
            self.departurecheckbottomanc?.constant = 180
            self.directcallpressed = false
            self.currentlocationtodeparturecheckanc?.isActive = false
            self.currentlocationtowheretoanc?.isActive = true
            self.view.layoutIfNeeded()
        })
    }
    
    
    
}
extension MainViewController : MainContract {
    /**
     목적지가 있는 택시 호출 화면으로 이동 하는 함수
     */
    func showCallWithDestViewController(destname : String , endx : Double, endy : Double){
        guard let departname = currentlocationlabel.text else {
            return
        }
        let startx = mapView.cameraPosition.target.lng
        let starty = mapView.cameraPosition.target.lat
        
        let view = CallWithDestViewController(departname: departname, destname: destname, startx: startx, starty: starty, endx: endx, endy: endy)
        self.navigationController?.pushViewController(view, animated: true)
        
    }
    /**
     배차 요청 화면으로 넘어가는 함수
     */
    func showFindinTaxiViewController(allocdata : [String : Any]){
        let view = FindingTaxiViewController(allocdata: allocdata)
        self.navigationController?.pushViewController(view, animated: true)
    }
}
//바로 호출 팝업 델리게이트 익스텐션
extension MainViewController : ImmediateCallPopUPDelegate {
    /**
     바로 호출 안내팝업 닫힘과 동시에 여기가 계신곳 맞나요 팝업 표출 하는 함수
     */
    func showdeparturecheckpopup(){
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.whereToPopUp.alpha = 0
            self.departurecheckpopup.alpha = 1
            self.departurecheckbottomanc?.constant = 0
            self.currentlocationtodeparturecheckanc?.isActive = true
            self.currentlocationtowheretoanc?.isActive = false
        }
    }
    /**
     바로 호출 팝업(전체 화면)  및 계신곳이 여기가 맞나요?(팝업) 에서 바로호출 눌럿을때 발생하는 함수
     */
    func callTaxiDirectWithoutDest(){
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.whereToPopUp.alpha = 0
            self.departurecheckpopup.alpha = 1
            self.departurecheckbottomanc?.constant = 0
            self.currentlocationtodeparturecheckanc?.isActive = true
            self.currentlocationtowheretoanc?.isActive = false
        }
        
//        let view = CallWithOutDestViewController(posx: presenter.getcurrentplaceInfos().x, posy: presenter.getcurrentplaceInfos().y, address: presenter.getcurrentplaceInfos().addr)
//
        let view = FindingTaxiViewController(allocdata: presenter.getallocdata())
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    
    
}

 // 어디로 가세요 팝업 델리게이트 익스텐션
extension MainViewController : MainWhereToPopUpDelegate {
    /**
     집 회사 버튼 클릭시 등록 여부를 판단하여 택시 배차 혹은 집 회사 등록 팝업 표출 하는 함수
     */
    func checkEnlistExist(type: enlistmode) {
        
        let resultbool : Bool = presenter.checkModelExist(type : type).0
        
        
        
        if resultbool == true {
            self.departurecheckpopup.setenlisttype(type: type)
            self.departurecheckpopup.setpopuptype(type: .enlistedplace)
            self.showdeparturecheckpopup()
            
        }
        else {
            if type == .house {
                self.enlistpopup.settype(type: .house)
            }
            else {
                self.enlistpopup.settype(type: .office)
            }
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.view.bringSubviewToFront(self.enlistpopup)
                self.enlistpopup.alpha = 1
            }
        }
        
        
        
        
    }
    
    /**
    상세 주소 입력 화면으로 넘어가는 함수
     */
    func showSpecViewController(){
        UIView.animate(withDuration: 0.2) {
            self.frequencycv.alpha = 0
            self.whereToPopUp.alpha = 0
            self.cvstackbottomanc?.constant = 180
            self.view.layoutIfNeeded()
        }
        let view = SpecViewController()
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    
    
}
//아이나비 맵뷰 관련 함수 익스텐션
extension MainViewController : INVMapViewDelegate {
    /**
     맵이 인터렉션이 생겼을때 호출 되는 함수
     */
    func mapView(_ mapView: InaviMapView, regionIsChangingWithReason reason: Int) {
        
        if reason == -1 {
            //reason == -1  사용자의 제스쳐로 맵이 이동 되었을때
            UIView.animate(withDuration: 0.2, animations: { [unowned self] in
                self.frequencycv.alpha = 0
                self.whereToPopUp.alpha = 0
                self.cvstackbottomanc?.constant = 180
                self.callcenterbtn.alpha = 0
                self.view.layoutIfNeeded()
            }) { [unowned self] (done) in
                if done {
                    self.currentlocationlabel.alpha = 1
                }
            }
        }
        
    }
    
    /**
     맵이 인터렉션이 없을때 호출 되는 함수
     
     어디로 가세요? 팝업이 표출 됨
     */
    func mapViewDidBecomeIdle(_ mapView: InaviMapView) {
        presenter.callreversegeocodeapi(posx: mapView.cameraPosition.target.lng, posy: mapView.cameraPosition.target.lat) { [unowned self] (addr, done) in
            if done {
                self.directcallicon.setImage(UIImage(named: "mainBtnDirectcall"), for: .normal)
                self.directcallicon.isUserInteractionEnabled = true
                self.departurecheckpopup.setcurrentloc(addr : addr)
                self.currentlocationlabel.text = addr
            }
            else {
                self.directcallicon.setImage(UIImage(named: "mainBtnDirectcallDUnActive"), for: .normal)
                self.directcallicon.isUserInteractionEnabled = false
                self.currentlocationlabel.text = addr
            }
        }
        
        if directcallpressed == false {
            UIView.animate(withDuration: 0.2, animations: { [unowned self] in
                self.frequencycv.alpha = 1
                self.whereToPopUp.alpha = 1
                self.cvstackbottomanc?.constant = 0
                self.callcenterbtn.alpha = 1
                self.view.layoutIfNeeded()
            })
        }
        
        
    }
    
}
//계신곳이 여기가 맞나요 팝업 델리게이트 익스텐션
extension MainViewController : MainCheckDeparturePopUpDelegate {
    func departurecheckpopupcanceld(){
        UIView.animateKeyframes(withDuration: 0.4, delay: 0, options: .allowUserInteraction, animations: {
            UIView.addKeyframe(withRelativeStartTime: 0, relativeDuration: 0.5) {
                self.departurecheckpopup.alpha = 0
                self.departurecheckbottomanc?.constant = 180
                self.directcallpressed = false
                
            }
            
            UIView.addKeyframe(withRelativeStartTime: 0.5, relativeDuration: 0.5) {
                self.frequencycv.alpha = 1
                self.whereToPopUp.alpha = 1
                self.cvstackbottomanc?.constant = 0
                self.callcenterbtn.alpha = 1
                self.currentlocationtodeparturecheckanc?.isActive = false
                self.currentlocationtowheretoanc?.isActive = true
                self.view.layoutIfNeeded()
            }
            
        }, completion: nil)
    }
    
    /**
      집 또는 회사 누른 상태에서  출발지 확인 컨펌 받았을때 실행 되는 화면
     
     - parameter :
     - type : 집인지 회사인지
     */
    func callTaxiWithDest(type : enlistmode){
        let resultbool : Bool = presenter.checkModelExist(type : type).0
        let resultmodel : HouseOfficeModel? = presenter.checkModelExist(type : type).1
        
        
        
        
        if resultbool == true {

            
            if let model = resultmodel {
                
                guard let departname = currentlocationlabel.text else {
                    return
                }
                let destname = model.address
                let startx = mapView.cameraPosition.target.lng
                let starty = mapView.cameraPosition.target.lat
                let endx = model.lon
                let endy = model.lat
            
                presenter.callInaviFindRouteAPI(endx: endx, endy: endy) { [unowned self] (done) in
                    if done {
                        let view = CallWithDestViewController(departname: departname, destname: destname, startx: startx, starty: starty, endx: endx, endy: endy)
                        self.navigationController?.pushViewController(view, animated: true)
                    }
                }

            }
            
        }
        
        
    }
    
    
}
// 슬라이드 메뉴 익스텐션
extension MainViewController : SlideinMenuDelegate {
    func showRecordViewController() {
        presenter.callHistoryAPI { (done) in
            if done {
                let view = RecordViewController()
                self.navigationController?.pushViewController(view, animated: true)
            }
        }
       
    }
    func showSettingViewController(){
        let view = SettingViewController()
        self.navigationController?.pushViewController(view, animated: true)
    }
}
//집 회사 등록 팝업 델리게이트 익스테션
extension MainViewController : EnlistHouseAndOfficePopUPDelegate {
    func enlistbtnpresse(type: EnlistHouseAndOfficePopUP.popuptype) {
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.enlistpopup.alpha = 0
        }
        let enlistmode : enlistmode = type == .house ? .house : .office
        let view = EnlistViewController(enlistmode: enlistmode)
        self.navigationController?.pushViewController(view, animated: true)
        
    }
    
    
}
extension MainViewController {
    private func makecvstack(){
        self.view.addSubview(cvstack)
        cvstack.translatesAutoresizingMaskIntoConstraints = false
        cvstackbottomanc = cvstack.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
        cvstackbottomanc?.isActive = true
        cvstack.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        cvstack.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        cvstack.heightAnchor.constraint(lessThanOrEqualToConstant: 54).isActive = true
        
        NSLayoutConstraint(item: frequencycv, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: 54).isActive = true
        NSLayoutConstraint(item: frequencycv, attribute: .width, relatedBy: .equal, toItem: nil, attribute: .notAnAttribute, multiplier: 1, constant: UIScreen.main.bounds.width).isActive = true
        frequencycv.backgroundView = frequencygradientview
        
    }
    private func makewhereToPopUp(){
        self.view.addSubview(whereToPopUp)
        whereToPopUp.translatesAutoresizingMaskIntoConstraints = false
        whereToPopUp.bottomAnchor.constraint(equalTo: cvstack.topAnchor, constant: 0).isActive = true
        whereToPopUp.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        whereToPopUp.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        whereToPopUp.heightAnchor.constraint(equalToConstant: 190).isActive = true
    }
    private func makecallcenterbtn(){
        self.view.addSubview(callcenterbtn)
        callcenterbtn.translatesAutoresizingMaskIntoConstraints = false
        callcenterbtn.bottomAnchor.constraint(equalTo: whereToPopUp.topAnchor, constant: 0).isActive = true
        callcenterbtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 3).isActive = true
        callcenterbtn.widthAnchor.constraint(equalToConstant: 110).isActive = true
        callcenterbtn.heightAnchor.constraint(equalToConstant: 54).isActive = true
        callcenterbtn.setImage(UIImage(named: "mainBtnCallct"), for: .normal)
    }
    private func makecurrentlocationbtn(){
        self.view.addSubview(currentlocationbtn)
        currentlocationbtn.translatesAutoresizingMaskIntoConstraints = false
        currentlocationtowheretoanc = currentlocationbtn.bottomAnchor.constraint(equalTo: whereToPopUp.topAnchor, constant: 0)
        currentlocationtowheretoanc?.isActive = true
        currentlocationtodeparturecheckanc = currentlocationbtn.bottomAnchor.constraint(equalTo: departurecheckpopup.topAnchor, constant: 0)
        
        currentlocationbtn.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        currentlocationbtn.widthAnchor.constraint(equalToConstant: 54).isActive = true
        currentlocationbtn.heightAnchor.constraint(equalToConstant: 54).isActive = true
        currentlocationbtn.setImage(UIImage(named: "mapBtnMy"), for: .normal)
    }
    private func makemenubtn(){
        self.view.addSubview(menuBtn)
        menuBtn.translatesAutoresizingMaskIntoConstraints = false
        menuBtn.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 13).isActive = true
        menuBtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        menuBtn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        menuBtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        menuBtn.setImage(UIImage(named: "titleBtnMenu"), for: .normal)
    }
    
    private func makedirectcallPopUp(){
        self.view.addSubview(directcallPopUP)
        directcallPopUP.frame = CGRect(x: 0, y: 0, width: mainboundwidth, height: mainboundheight)
        directcallPopUP.alpha = 0
        
    }
    private func makedeparturecheckpopUp(){
        self.view.addSubview(departurecheckpopup)
        departurecheckpopup.translatesAutoresizingMaskIntoConstraints = false
        departurecheckbottomanc = departurecheckpopup.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0)
        departurecheckbottomanc?.isActive = true
        departurecheckpopup.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        departurecheckpopup.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        departurecheckpopup.heightAnchor.constraint(equalToConstant: 190).isActive = true
        departurecheckpopup.alpha = 0
    }
    private func makeenlistpopup(){
        self.view.addSubview(enlistpopup)
        enlistpopup.translatesAutoresizingMaskIntoConstraints = false
        enlistpopup.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        enlistpopup.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        enlistpopup.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        enlistpopup.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        enlistpopup.alpha = 0
    }
    
    
    private func makedirectcallicon(){
        self.view.addSubview(directcallicon)
        directcallicon.translatesAutoresizingMaskIntoConstraints = false
        directcallicon.bottomAnchor.constraint(equalTo: self.view.centerYAnchor, constant: 0).isActive = true
        directcallicon.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        directcallicon.widthAnchor.constraint(equalToConstant: 107).isActive = true
        directcallicon.heightAnchor.constraint(equalToConstant: 54).isActive = true
        directcallicon.setImage(UIImage(named: "mainBtnDirectcall"), for: .normal)
    }
    private func makecurrentlocationlabel(){
        self.view.addSubview(currentlocationlabel)
        currentlocationlabel.translatesAutoresizingMaskIntoConstraints = false
        currentlocationlabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        currentlocationlabel.topAnchor.constraint(equalTo: directcallicon.bottomAnchor, constant: 7).isActive = true
        currentlocationlabel.heightAnchor.constraint(equalToConstant: 19).isActive = true
        currentlocationlabel.backgroundColor = .rgb(red: 0, green: 0, blue: 0, alpha: 0.6)
        currentlocationlabel.textAlignment = .center
        currentlocationlabel.font = UIFont(name: fonts.medium.rawValue, size: 14) //.systemFont(ofSize: 14, weight: .medium)
        currentlocationlabel.alpha = 0
        currentlocationlabel.textColor = .white
        currentlocationlabel.leftInset = 8
        currentlocationlabel.rightInset = 8
    }
}
