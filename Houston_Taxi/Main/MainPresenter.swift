//
//  MainPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/15.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class MainPresenter: NSObject {
    
    weak var delegate : MainContract?
    weak var viewcontroller : MainViewController?
    let cellid = "mainfrequencycellid"
    //현재 바로 호출로 선정된 좌표와 이름
    private var currentposx : Double = 0
    private var currentposy : Double = 0
    private var currentaddress : String = ""
    
    
    
    weak var recentcallCheckTimer : Timer?
    private var recentcallcheckTimerSecond : Int = 60
    
    
    init(delegate : MainContract, viewcontroller : MainViewController){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
    }
    
    /**
     현재 좌표 데이터를 반환하는 함수
     */
    func getallocdata() -> [String : Any]{
        var allocdata : [String : Any] = [:]
        
         allocdata["departname"] = currentaddress
         allocdata["startx"] = currentposx
         allocdata["starty"] = currentposy
        
        return allocdata
    }
    /**
      빈도수 컬렉션 뷰의 셀을 레지스터 하는 함수
     */
    func registerFrequencyCell(cv : UICollectionView){
        cv.register(MainFrequencyCell.self, forCellWithReuseIdentifier: cellid)
    }
    
    /**
     최근 빈도수 목적지가 있는지 없는지 반환하는 함수
     
     */
    func isFreqEmpty() -> Bool{
        if FreqVisitRepo.shared.getmodel().count == 0 {
            return true
        }
        else {
            return false
            
        }
    }
    
    func getcurrentplaceInfos() -> (x : Double, y : Double , addr : String){
        return (self.currentposx, self.currentposy, self.currentaddress)
    }
    
    let reversegeocodeapi = ReverGeoCodingAPI()
    
    /**
     지도 이동시 리버스 지오코딩을 실행하는 하수
     
     - parameters :
      - posx : x좌표
      - posy : y좌표
      - addr : 콜백 결과 주소
      - done : 콜백 성공 여부
     */
    func callreversegeocodeapi(posx : Double, posy : Double, completion : @escaping(_ addr : String, _ done : Bool) -> ())  {
        currentposx = posx
        currentposy = posy
        reversegeocodeapi.request_To_server(posx: posx, posy: posy) { (done, data) in
            if done {
                let tempmodel = ReverseGeoCodingModel(json: data)
                
                guard let adm = tempmodel.adm else {
                    completion("택시를 호출 할 수 없는 지역입니다",false)
                    return
                }
                
                guard let bldname = adm.bldname else {
                    completion("택시를 호출 할 수 없는 지역입니다",false)
                    return
                }
                //빌딩이름이 없으면 도로명 주소 표출
                if bldname == "" {
                    guard let roadname = adm.roadname else {
                        completion("택시를 호출 할 수 없는 지역입니다",false)
                        return
                    }
                    //도로명 주소도 없으면
                    if roadname == "" {
                        completion("택시를 호출 할 수 없는 지역입니다",false)
                        return
                    }
                    self.currentaddress = roadname
                    completion(roadname,true)
                    return
                }
                else {
                    //빌딩 이름이 있으면 빌딩명 표출
                    self.currentaddress = bldname
                    completion(bldname,true)
                }
            }
            else {
                completion("택시를 호출 할 수 없는 지역입니다",false)
            }
        }
    }
    
    /**
     집 또는 회사가 등록 되어있는지 확인 하는 함수
     
     없으면 false,nil
     있으면 true, 모델
     */
    func checkModelExist(type : enlistmode) -> (Bool,HouseOfficeModel?){
        if type == .house {
            if let house = HouseOfficeRepo.shared.gethouse() {
                return (true,house)
            }
        }
        if type == .office {
            if let office = HouseOfficeRepo.shared.getoffice() {

                return (true,office)
            }
        }
        return (false , nil)
    }
    
    
    /**
     경로 탐색 api
     */
    func callInaviFindRouteAPI(endx : Double , endy : Double,completion : @escaping (Bool) -> ()){
        
        let api = InaviFindRouteAPI()
        FunctionClass.shared.showdialog(show: true)
        api.request_To_server(startx: currentposx, starty: currentposy, endx: endx, endy: endy) { (done, data) in
            if done {
                print("CallWithDestPresenter findroute data",data["header"])
                let tempmodel = InaviRouteModel(json: data)
                InaviRouteRepo.shared.setmodel(input: tempmodel)
                if let header = InaviRouteRepo.shared.getmodel().header {
                    print("header.isSuccessful!",header.isSuccessful!)
                    completion(header.isSuccessful!)
                }
                else {
                    print("header failed")
                    completion(false)
                }
            }
            else {
                completion(false)
            }
            FunctionClass.shared.showdialog(show: false)
        }
    }
    
    /**
     탑승 이력 조회 api
     */
    func callHistoryAPI(completion : @escaping (Bool) -> ()){
        FunctionClass.shared.showdialog(show: true)
        let api = GetInfoHistory()
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile) { (done, data) in
            if done {
                HistoryRepo.shared.setmodel(input: HistoryModel(json: data))
                
                if let msg = data["msg"].string {
                    print("finding call driver info msg",FunctionClass.shared.decrypt(msg: msg))
                }
                let success = data["isSuccessful"].boolValue
                print("finding success",success)
                if success == true {
                   completion(true)
                }
                else {
                    completion(false)
                }
            }
            else {
                completion(false)
            }
            FunctionClass.shared.showdialog(show: false)
        }
        
        
    }
    
    
}
extension MainPresenter : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout , UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return FreqVisitRepo.shared.getmodel().count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellid, for: indexPath) as! MainFrequencyCell
        let model = FreqVisitRepo.shared.getmodel()[indexPath.row]
        if let id = model.id {
            cell.setid(id: id)
        }
        if let lat = model.destinationLat , let lon = model.destinationLon {
            cell.setlatlon(lat: lat, lon: lon)
        }
        if let name = model.destinationName {
            cell.setlabel(label: name)
        }
        
        
        return cell

    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 10
    }
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        let width : CGFloat = 28
        let model = FreqVisitRepo.shared.getmodel()[indexPath.row]
        if let str = model.destinationName {
            let textwidth = self.textwidth(for: str, with: UIFont.systemFont(ofSize: 18, weight: .medium), height: 23, maxwidth: 158)
            return CGSize(width: width + textwidth, height: 40)
        }
        else {
            return CGSize(width: 0, height: 40)
        }
        
    }
    
    
    //셀의 가변적 넓이를 위해서 미리 텍스트의 크기를 계산 하는 함수
    func textwidth(for text : String, with font : UIFont, height : CGFloat , maxwidth : CGFloat) -> CGFloat {
        let nsstring = NSString(string: text)
        let maxwidth = maxwidth
        let textAttribute = [NSAttributedString.Key.font : font]
        let boundingRect = nsstring.boundingRect(with: CGSize(width: maxwidth, height: height ), options: .usesLineFragmentOrigin, attributes: textAttribute, context: nil)
        
        return ceil(boundingRect.width) < maxwidth ? ceil(boundingRect.width) : maxwidth
    }
    /**
     최근 목적지를 클릭했을때 호출 되는 함수(택시 호출 화면으로 이동)
     */
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let cell = collectionView.cellForItem(at: indexPath) as! MainFrequencyCell
        let lat = cell.getlatlon().lat
        let lon = cell.getlatlon().lon
        self.callInaviFindRouteAPI(endx: lon, endy: lat) { [unowned self] (done) in
            if done {
                self.delegate?.showCallWithDestViewController(destname : cell.getname() , endx : lon, endy : lat)
            }
        }
    }
    
    
}
//앱이 꺼졌다 켯을때 택시를 타고 있는지 배차 중인지 등등을 확인하고 그에 맞는 화면으로 보내주는 비즈니스 로직
extension MainPresenter {
    
    /**
     배차 상태 api 함수
     
     */
    func callCallCheck(completion : @escaping (callcheck,Bool) -> () ){
        let api = GetCallCheckAPI()
        //정보가 없다면 진행 하지 않는다.
        if UserDefaults.standard.string(forKey: userstandard.callid.rawValue)! == "" || UserDefaults.standard.string(forKey: userstandard.calldt.rawValue)! == "" {
            return
        }
        
        
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        
        let callid = FunctionClass.shared.encrypt(msg: CallRepo.callID)
        let calldt = FunctionClass.shared.encrypt(msg: CallRepo.callDt)
        
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile, callID: callid, callDT: calldt) { (done, data) in
            if done {
                
                let isSuccess = data["isSuccessful"].boolValue
                
                let msg = data["msg"].stringValue
               
                var callstat : callcheck = .fail
                guard let callStatus = data["callStatus"].string else {
                    completion(.fail,false)
                    return
                }
                if let callID = data["callID"].string , let callDT = data["callDT"].string {
                    print(" main check callid : ", FunctionClass.shared.decrypt(msg: callID))
                    print(" main check callid : ", FunctionClass.shared.decrypt(msg: callDT))
                }
                else {
                    print("calldt and callid not found in data")
                }
                
                let callstatstr = FunctionClass.shared.decrypt(msg: callStatus)
                print(" main callstatstr", callstatstr)
                for status in callcheck.allCases {
                    if status.rawValue == callstatstr {
                        callstat = status
                    }
                }
                completion(callstat,isSuccess)
            }
            else {
                completion(.fail,false)
            }
        }
    }
    
    /**
     배차 완료후 택시 정보를 호출 하는 함수
     */
    func callDriverInfo(completion : @escaping (Bool) -> ()) {
        //정보가 없다면 진행 하지 않는다.
        if UserDefaults.standard.string(forKey: userstandard.callid.rawValue)! == "" || UserDefaults.standard.string(forKey: userstandard.calldt.rawValue)! == "" {
            return
        }
        let api = GetDriverInfo()
        
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        
        
        CallRepo.callID = UserDefaults.standard.string(forKey: userstandard.callid.rawValue)!
        CallRepo.callDt = UserDefaults.standard.string(forKey: userstandard.calldt.rawValue)!
        let callid = FunctionClass.shared.encrypt(msg: CallRepo.callID)
        let calldt = FunctionClass.shared.encrypt(msg: CallRepo.callDt)
        
        
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile, callID: callid, callDT: calldt) { [unowned self] (done, data) in
            if done {
               
                CallRepo.shared.setmodel(input: CallModel(json: data))
                if let msg = data["msg"].string {
                    print("finding call driver info msg",FunctionClass.shared.decrypt(msg: msg))
                }
                let success = data["isSuccessful"].boolValue
                
                if success == true {
                    //self.delegate?.showAllocationCompleteViewController()
                }
                else {
                    ToastView.shared.short(self.viewcontroller!.view, txt_msg: "차량 조회를 실패 하셨습니다. 다시 배차를 해주세요")
                }
                completion(success)
            }
        }
    }
    
    
    /**
     현재 택시 배차 상태를 확인 하는 함수
     */
    func checkTaxiState(){
        
        self.callCallCheck { [unowned self]  (stat, done) in
            if done {
                switch stat {
                case .submitted:
                    print("do nothing")
                case .onboard:
                    self.showOnboardingview()
                case .allocsucess:
                    self.showAllocCompleteView()
                case .fail:
                    print("do nothin")
                case .allocfailed:
                    print("do nothin")
                case .connecterror:
                    print("do nothin")
                case .onboardfailed:
                    print("do nothin")
                case .drop:
                    print("do nothin")
                case .cancel:
                    print("do nothin")
                }
            }
            else {
                
            }
        }
    }
    
    /**
     탑승 화면으로 이동하는 함수
     */
    func showOnboardingview(){
        var allocdata : [String : Any] = [:]
        if UserDefaults.standard.double(forKey: userstandard.destlat.rawValue) != 0 || UserDefaults.standard.double(forKey: userstandard.destlat.rawValue) != 0 {
            allocdata["destname"] = UserDefaults.standard.string(forKey: userstandard.destname.rawValue)
            allocdata["endx"] = UserDefaults.standard.double(forKey: userstandard.destlon.rawValue)
            allocdata["endy"] = UserDefaults.standard.double(forKey: userstandard.destlat.rawValue)
        }
    
        let view = OnBoardingViewController(allocdata: allocdata)
        self.viewcontroller?.navigationController?.pushViewController(view, animated: true)
    }
    
    /**
     배차 완료 화면으로 이동 하는 함수
     */
    func showAllocCompleteView(){
        var allocdata : [String : Any] = [:]
        
        //만약에 저장되어있는 정보가 없다면 오류의 가능성이 있으므로 진행 하지 않는다
        if UserDefaults.standard.double(forKey: userstandard.startlon.rawValue) != 0 && UserDefaults.standard.double(forKey: userstandard.startlat.rawValue) != 0 {
            allocdata["departname"] = ""
            allocdata["startx"] = UserDefaults.standard.double(forKey: userstandard.startlon.rawValue)
            allocdata["starty"] = UserDefaults.standard.double(forKey: userstandard.startlat.rawValue)
        }
        else {
            return
        }
        
        
        
        
        if UserDefaults.standard.double(forKey: userstandard.destlat.rawValue) != 0 || UserDefaults.standard.double(forKey: userstandard.destlat.rawValue) != 0 {
            allocdata["destname"] = UserDefaults.standard.string(forKey: userstandard.destname.rawValue)
            allocdata["endx"] = UserDefaults.standard.double(forKey: userstandard.destlon.rawValue)
            allocdata["endy"] = UserDefaults.standard.double(forKey: userstandard.destlat.rawValue)
        }
        
        
        
        self.callDriverInfo { [unowned self] (done) in
            if done {
                let view = AllocationCompleteViewController(allocdata: allocdata)
                self.viewcontroller?.navigationController?.pushViewController(view, animated: true)
            }
        }
    }
    
    
}
//콜센터 버튼 클릭후 비즈니스 로직
extension MainPresenter {
    
    /**
     최근 배차 요청 api 호출 함수
     */
    func callRecentAllocAPI(completion : @escaping (callcheck,Bool) -> ()){
        let api = RecentAllocAPI()
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authCode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        
        
        api.requestToserver(currentDT: currentDt, authcode: authCode, mobile: mobile) { (done, data) in
            if done {
                let isSuccess = data["isSuccessful"].boolValue
                 
                 let msg = data["msg"].stringValue
                
                 var callstat : callcheck = .fail
                 guard let callStatus = data["callStatus"].string else {
                     completion(.fail,false)
                     return
                 }
                 if let callID = data["callID"].string , let callDT = data["callDT"].string {
                     print("main recent callid : ", FunctionClass.shared.decrypt(msg: callID))
                     print("main recent callid : ", FunctionClass.shared.decrypt(msg: callDT))
                    UserDefaults.standard.set(callID, forKey: userstandard.callid.rawValue)
                    UserDefaults.standard.set(callDT, forKey: userstandard.calldt.rawValue)
                    CallRepo.callID = callID
                    CallRepo.callDt = callDT
                 }
                 else {
                     print("calldt and callid not found in data")
                 }
                 
                 let callstatstr = FunctionClass.shared.decrypt(msg: callStatus)
                 print("main recent callstatstr", callstatstr)
                 for status in callcheck.allCases {
                     if status.rawValue == callstatstr {
                         callstat = status
                     }
                 }
                 completion(callstat,isSuccess)
                
            }
            else {
                completion(.fail,false)
            }
        }
        
    }
    /**
     배차 요청 확인 타이머
     */
    func runrecentalloctimer(){
        if recentcallCheckTimer == nil {
            recentcallCheckTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(checkrecentallocstate), userInfo: nil, repeats: true)
        }
        else {
            //이미 타이머가 돌아가고 있던 경우 원래 타이머를 파기시키기고 새로운 타이머를 할당
            recentcallCheckTimer?.invalidate()
            recentcallCheckTimer = nil
            recentcallcheckTimerSecond = 60
            recentcallCheckTimer = Timer.scheduledTimer(timeInterval: 2, target: self, selector: #selector(checkrecentallocstate), userInfo: nil, repeats: true)
        }
    }
    
    @objc func checkrecentallocstate(){
        print("checkrecentallocstate ")
        if recentcallcheckTimerSecond > 0 {
           recentcallcheckTimerSecond -= 2
        }
        else {
            self.recentcallCheckTimer?.invalidate()
        }
        self.callRecentAllocAPI { [weak self] (stat, done) in
            if done {
                switch stat {
                case .submitted://접수 되었으면 타이머를 파기시키고 택시 배차 요청 화면으로 이동
                    self?.recentcallCheckTimer?.invalidate()
                    self?.showTaxiFindingView()
                    //showtaxifindingview
                case .onboard:
                    self?.showOnboardingview()
                case .allocsucess:
                    self?.showAllocCompleteView()
                case .fail:
                    print("do nothin")
                case .allocfailed:
                    print("do nothin")
                case .connecterror:
                    print("do nothin")
                case .onboardfailed:
                    print("do nothin")
                case .drop:
                    print("do nothin")
                case .cancel:
                    print("do nothin")
                }
            }
        }
        
    }
    
    
    
    
    func callPassengerGPSAPI(){
        let api = PassengerGPSAPI()
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authCode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        
        let posName = FunctionClass.shared.encrypt(msg: self.currentaddress)
        
        let posLon = FunctionClass.shared.encrypt(msg: String(currentposx))
        let posLat = FunctionClass.shared.encrypt(msg: String(currentposy))
        
        api.requestToserver(currentDT: currentDt, authcode: authCode, mobile: mobile, posName: posName, posLon: posLon, posLat: posLat) { (done, data) in
            print("gps ")
            if done {
                
                
            }
            else {
                
            }
        }
    }
   
    
    func showTaxiFindingView(){
        var allocdata : [String : Any] = [:]
       
        allocdata["departname"] = currentaddress
        allocdata["startx"] = currentposx
        allocdata["starty"] = currentposy
        self.delegate?.showFindinTaxiViewController(allocdata : allocdata)
        
    }
    
    
    
}
