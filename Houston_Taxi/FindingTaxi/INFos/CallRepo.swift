//
//  CallRepo.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/10.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import SwiftyJSON


class CallRepo {
    static let shared = CallRepo()
    static var callID : String = ""
    static var callDt : String = ""
    
    static var carLon : Double = 0
    static var carLat : Double = 0
    private init(){}
    
    
    private var model : CallModel?
    
    func setmodel(input : CallModel){
        self.model = input
    }
    
    
    func getmodel() -> CallModel {
        guard let model = self.model else {
            print("model is empty??")
            return CallModel(json: JSON(""))
        }
        
        return model
    }
    
    
    
    
    
    
}
