//
//  CallModel.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/10.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import SwiftyJSON

/**
 배차 관련 정보 모델
 */
class CallModel : NSObject {
    
    init(json : JSON){
        
        let num = json["carNum"].stringValue
            carNum = FunctionClass.shared.decrypt(msg: num)
        print("carNum",carNum)
        let name = json["driverName"].stringValue
            driverName = FunctionClass.shared.decrypt(msg: name)
        print("driverName",driverName)
        let color = json["carColor"].stringValue
            carColor = FunctionClass.shared.decrypt(msg: color)
         print("carColor",carColor)
        let modelname = json["carModel"].stringValue
            carModel = FunctionClass.shared.decrypt(msg: modelname)
         print("carModel",carModel)
        
        let phonenumber = json["driverTelNum"].stringValue
        driverTelNum = FunctionClass.shared.decrypt(msg: phonenumber)
        
//        carNum = json["carNum"].string
//        driverName = json["driverName"].string
//        carColor = json["carColor"].string
//        carModel = json["carModel"].string
        
    }
    
    var carNum : String?
    var driverName : String?
    var carColor : String?
    var carModel : String?
    var driverTelNum : String?
    
    
}
