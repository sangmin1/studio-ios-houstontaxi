//
//  FindingTaxiPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/07.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class FindingTaxiPresenter: NSObject {
    
    weak var delegate : FindingTaxiContract?
    weak var viewcontroller : FindingTaxiViewController?
    
    private var departname : String = ""
    private var startx : Double = 0
    private var starty : Double = 0
    
    
    private var destname : String?
    private var endx : Double?
    private var endy : Double?
    
    //타이머
    private var timer : Timer?
    //타이머 블럭 flag
    private var blocktimer : Bool = false
    //현재 콜상태
    private var currentcallstat : callcheck = .submitted
    
    //현제 콜상태 반환 함수
    func getcurrentcallstat() -> callcheck {
        return self.currentcallstat
    }
    
    init(delegate : FindingTaxiContract , viewcontroller : FindingTaxiViewController ){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
        
    }
    
    /**
     배차에 관한 정보들을 셋하는 함수
     */
    func setInfos(allocdata : [String : Any]){
        self.departname = allocdata["departname"] as! String
        self.startx = allocdata["startx"] as! Double
        self.starty = allocdata["starty"] as! Double
        UserDefaults.standard.set(allocdata["startx"] as! Double, forKey: userstandard.startlon.rawValue)
        UserDefaults.standard.set(allocdata["starty"] as! Double, forKey: userstandard.startlat.rawValue)
        
        if let destname = allocdata["destname"] as? String, let endx = allocdata["endx"] as? Double, let endy = allocdata["endy"] as? Double {
            self.destname = destname
            self.endx = endx
            self.endy = endy
            UserDefaults.standard.set(endx, forKey: userstandard.destlon.rawValue)
            UserDefaults.standard.set(endy, forKey: userstandard.destlat.rawValue)
            UserDefaults.standard.set(destname, forKey: userstandard.destname.rawValue)
            
        }
        else {
            UserDefaults.standard.set(0, forKey: userstandard.destlon.rawValue)
            UserDefaults.standard.set(0, forKey: userstandard.destlat.rawValue)
            UserDefaults.standard.set("", forKey: userstandard.destname.rawValue)
        }
        
        
    }
    
    /**
     배차 정보를 반환하는 함수
     */
    func getInfos() -> [String : Any] {
        var allocdata : [String : Any] = [:]
        
        allocdata["departname"] = self.departname
        allocdata["startx"] = self.startx
        allocdata["starty"] = self.starty
        
        if let destname = self.destname, let endx = self.endx , let endy = self.endy {
            allocdata["destname"] = destname
            allocdata["endx"] = endx
            allocdata["endy"] = endy
        }
        
        return allocdata
        
        
    }
    
    /**
     배차 호출 함수
     */
    func callAllocRequestAPI(completion : @escaping (Bool) -> ()){
        let api = AllocRequestAPI()
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authCode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        
        
        let posName = FunctionClass.shared.encrypt(msg: departname)
        
        let posLon = FunctionClass.shared.encrypt(msg: String(startx))
        let posLat = FunctionClass.shared.encrypt(msg: String(starty))
        
        let postNameDetail = FunctionClass.shared.encrypt(msg: "")
        
        
        var destination = ""
        var destLat = ""
        var destLon = ""
        
        if let lat = endy ,let lon = endx {
            destLat = FunctionClass.shared.encrypt(msg: String(lat))
            destLon = FunctionClass.shared.encrypt(msg: String(lon))
        }
        if let dest = destname {
            destination = FunctionClass.shared.encrypt(msg: dest)
        }
        
        
        api.requestToserver(currentDT: currentDt, authcode: authCode, mobile: mobile, posName: posName, posLon: posLon, posLat: posLat, posNameDetail: postNameDetail, destLon: destLon, destLat: destLat, destination: destination) { (done, data) in
            if done {
                print("alloc request data",data)
                if let msg = data["msg"].string {
                    print("findg alloc request msg =", FunctionClass.shared.decrypt(msg: msg))
                }
                if let callID = data["callID"].string,let callDT = data["callDT"].string {
                    let callid = FunctionClass.shared.decrypt(msg: callID)
                    let calldt = FunctionClass.shared.decrypt(msg: callDT)
                    CallRepo.callDt = calldt
                    CallRepo.callID = callid
                    UserDefaults.standard.set(calldt, forKey: userstandard.calldt.rawValue)
                    UserDefaults.standard.set(callid, forKey: userstandard.callid.rawValue)
                    
                    
                    print("alloc request callId =",callid)
                    print("alloc request callDt =",calldt )
                }

                completion(data["isSuccessful"].boolValue)
            }
            else {
                
            }
        }
    }
    
    
    
    
    /**
     배차 완료후 택시 정보를 호출 하는 함수
     */
    func callDriverInfo() {
        let api = GetDriverInfo()
        
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        
        let callid = FunctionClass.shared.encrypt(msg: CallRepo.callID)
        let calldt = FunctionClass.shared.encrypt(msg: CallRepo.callDt)
        
        
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile, callID: callid, callDT: calldt) { [unowned self] (done, data) in
            if done {
                print("finding driver info data",data)
                CallRepo.shared.setmodel(input: CallModel(json: data))
                if let msg = data["msg"].string {
                    print("finding call driver info msg",FunctionClass.shared.decrypt(msg: msg))
                }
                let success = data["isSuccessful"].boolValue
                print("finding success",success)
                if success == true {
                    self.delegate?.showAllocationCompleteViewController()
                }
                else {
                    ToastView.shared.short(self.viewcontroller!.view, txt_msg: "차량 조회를 실패 하셨습니다. 다시 배차를 해주세요")
                }
            }
        }
        
    }
    
    
    
    /**
     배차 취소 api 호출 함수
     */
    func callalloccancelapi(completion : @escaping (Bool) -> ()){
        
        let api = AllocCancelAPI()
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        
        let callid = FunctionClass.shared.encrypt(msg: CallRepo.callID)
        let calldt = FunctionClass.shared.encrypt(msg: CallRepo.callDt)
        if callid == "" {
            completion(true)
            return
        }
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile, callID: callid, callDT: calldt) { [unowned self] (done, data) in
            if done {
                self.blocktimer = true
                print("finding alloc cancel data",data)
                let issuccess = data["isSuccessful"].boolValue
                if let msg = data["msg"].string {
                    print("finding alloc cancel msg",FunctionClass.shared.decrypt(msg: msg))
                }
                if issuccess == true {
                    CallRepo.callDt = ""
                    CallRepo.callID = ""
                    UserDefaults.standard.set("", forKey: userstandard.calldt.rawValue)
                    UserDefaults.standard.set("", forKey: userstandard.callid.rawValue)
                    UserDefaults.standard.set(0, forKey: userstandard.destlon.rawValue)
                    UserDefaults.standard.set(0, forKey: userstandard.destlat.rawValue)
                    UserDefaults.standard.set(0, forKey: userstandard.startlon.rawValue)
                    UserDefaults.standard.set(0, forKey: userstandard.startlat.rawValue)
                }
                completion(issuccess)
            }
        }
    }
    
    
    
    
    
    
}
extension FindingTaxiPresenter {
    /**
     배차 상태 api 함수
     
     */
    func callCallCheck(completion : @escaping (callcheck,Bool) -> () ){
        let api = GetCallCheckAPI()
        
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        
        let callid = FunctionClass.shared.encrypt(msg: CallRepo.callID)
        let calldt = FunctionClass.shared.encrypt(msg: CallRepo.callDt)
        
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile, callID: callid, callDT: calldt) { (done, data) in
            if done {
                print("finding callcheck api data", data)
                let isSuccess = data["isSuccessful"].boolValue
                print("finding callcheck isSuccessful ",isSuccess)
                let msg = data["msg"].stringValue
                print(" finding call callcheck msg",FunctionClass.shared.decrypt(msg: msg))
                var callstat : callcheck = .fail
                guard let callStatus = data["callStatus"].string else {
                    completion(.fail,false)
                    return
                }
                if let callID = data["callID"].string , let callDT = data["callDT"].string {
                    print("finding call check callid : ", FunctionClass.shared.decrypt(msg: callID))
                    print("finding call check callid : ", FunctionClass.shared.decrypt(msg: callDT))
                }
                else {
                    print("calldt and callid not found in data")
                }
                
                let callstatstr = FunctionClass.shared.decrypt(msg: callStatus)
                print("finding callstatstr", callstatstr)
                for status in callcheck.allCases {
                    if status.rawValue == callstatstr {
                        callstat = status
                    }
                }
                completion(callstat,isSuccess)
            }
            else {
                completion(.fail,false)
            }
        }
    }
    /**
     2초씩 callcheck api를 호출하여 상태 확인을 하는 타이머 함수
     */
    func runtimer(){
        self.blocktimer = false
        timer = Timer.scheduledTimer(timeInterval: 2, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer(){
        
        if blocktimer == true {
            
            timer?.invalidate()
            timer = nil
            
            return
        }
        else {
            print("finding call check fired")
            self.callCallCheck { [unowned self] (callcheck, done) in
                if done {
                    self.currentcallstat = callcheck
                    switch callcheck {
                    case .allocsucess ://배차 성공시
                        self.callDriverInfo()
                        self.blocktimer = true
                    case.submitted: // 접수상태일시 타이버를 계속 돌림
                        self.blocktimer = false
                    case .allocfailed : //배차 실패시 팝업을 띄워야 됨
                        self.blocktimer = true
                        self.delegate?.showAllocFailePopUp()
                    case .cancel : //배차 취소
                        self.blocktimer = true
                        self.delegate?.showAllocFailePopUp()
                        print("취소 되면 이전 화면으로")
                    case .fail,.onboardfailed : // 의외의 실패 경우는 타이머를 계쏙 돌림
                        self.blocktimer = false
                    case .drop :
                        self.blocktimer = true
                    case .onboard: // 탑승 처리시 탑승화면으로 이동
                        self.blocktimer = true
                        self.delegate?.showOnBoardingViewController(allocdata : self.getInfos())
    
                    case .connecterror :
                        print("통신 오류")
                    }
                }
                else {
                    
                }
            }
        }
        
    }
    
    
    
    
}
