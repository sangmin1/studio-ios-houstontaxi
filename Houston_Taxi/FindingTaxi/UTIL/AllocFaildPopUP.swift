//
//  AllocFaildPopUP.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/11.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

protocol AllocFailedPopUpDelegate : NSObjectProtocol {
    func reallocatebtnpressed()
    func allocfailedpopDeclinepressed()
}

/**
  배차 실패 팝업 함수
 */
class AllocFailedPopUp : UIView {
    
    private var box = ShadowView()
    
    private var label = PaddingLabel()
    
    private var cancelbtn = UIButton()
    
    private var confirmbtn = UIButton()
    
    weak var delegate : AllocFailedPopUpDelegate?
    
    init(frame : CGRect,delegate :AllocFailedPopUpDelegate){
        super.init(frame: frame)
        self.delegate = delegate
        makebox()
        makelabel()
        makecanclebtn()
        makeconfirmbtn()
        
        cancelbtn.addTarget(self, action: #selector(canclebtnpressed(sender:)), for: .touchUpInside)
        confirmbtn.addTarget(self, action: #selector(confirmbtnpressed(sender:)), for: .touchUpInside)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async { [weak self] in
            self?.box.layer.cornerRadius = 8
            self?.cancelbtn.layer.addBorder(edge: [.top], color: .rgb(red: 0, green: 0, blue: 0, alpha: 0.05), thickness: 1)
            self?.confirmbtn.layer.addBorder(edge: [.top], color: .rgb(red: 0, green: 0, blue: 0, alpha: 0.05), thickness: 1)
            self?.confirmbtn.layer.addBorder(edge: .left, color: .rgb(red: 0, green: 0, blue: 0, alpha: 0.05), thickness: 1)
        }
    }
    
    /**
     취소 버튼 이벤트 함수
     */
    @objc func canclebtnpressed(sender : UIButton){
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.alpha = 0
        }
        
        self.delegate?.allocfailedpopDeclinepressed()
    }
    
    /**
     다시 호출 버튼 이벤트 함수
     */
    @objc func confirmbtnpressed(sender : UIButton){
        self.delegate?.reallocatebtnpressed()
    }
    
    
    
}
extension AllocFailedPopUp {
    private func makebox(){
        self.addSubview(box)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        box.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        box.widthAnchor.constraint(equalToConstant: 315).isActive = true
        box.heightAnchor.constraint(equalToConstant: 202).isActive = true
        box.backgroundColor = .white
        
        box.shadowcolor = UIColor.black
        box.shadowoffset = CGSize(width: 0, height: 3)
        box.shadowradius = 6
        box.shadowopacity = 0.1
    }
    private func makelabel(){
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.topAnchor.constraint(equalTo: box.topAnchor, constant: 32).isActive = true
        label.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 0).isActive = true
        label.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: 0).isActive = true
        label.heightAnchor.constraint(lessThanOrEqualToConstant: 150).isActive = true
        label.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        label.font = UIFont(name: fonts.medium.rawValue, size: 20)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.text = """
        죄송합니다.
        이용 가능한 택시를 찾지 못했습니다.
        
        다시 호출할까요?
        """
    }
    private func makecanclebtn(){
        self.addSubview(cancelbtn)
        cancelbtn.translatesAutoresizingMaskIntoConstraints = false
        cancelbtn.bottomAnchor.constraint(equalTo: box.bottomAnchor, constant: 0).isActive = true
        cancelbtn.trailingAnchor.constraint(equalTo: box.centerXAnchor, constant: 0).isActive = true
        cancelbtn.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 0).isActive = true
        cancelbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        cancelbtn.setTitle("호출 취소", for: .normal)
        cancelbtn.setTitleColor(.rgb(red: 100, green: 100, blue: 100, alpha: 1), for: .normal)
        cancelbtn.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)
       
        
    }
    private func makeconfirmbtn(){
        self.addSubview(confirmbtn)
        confirmbtn.translatesAutoresizingMaskIntoConstraints = false
        confirmbtn.topAnchor.constraint(equalTo: cancelbtn.topAnchor, constant: 0).isActive = true
        confirmbtn.leadingAnchor.constraint(equalTo: box.centerXAnchor, constant: 0).isActive = true
        confirmbtn.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: 0).isActive = true
        confirmbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        confirmbtn.setTitle("다시 호출", for: .normal)
        confirmbtn.setTitleColor(maincolor, for: .normal)
        confirmbtn.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)
    
        
    }
}
