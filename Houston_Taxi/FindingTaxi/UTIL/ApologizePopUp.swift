//
//  ApologizePopUp.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/25.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

class ApologizePopUp: UIView {
    
    private var box = ShadowView()
    
    private var label = PaddingLabel()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        makebox()
        makelabel()
        let tap = UITapGestureRecognizer(target: self, action: #selector(tapped))
        self.addGestureRecognizer(tap)
        self.isUserInteractionEnabled = true
        self.backgroundColor = .rgb(red: 0, green: 0, blue: 0, alpha: 0.2)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    @objc func tapped(){
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.alpha = 0
        }
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async { [weak self] in
            self?.box.layer.cornerRadius = 8
        }
    }
    
}
extension ApologizePopUp {
    private func makebox(){
        self.addSubview(box)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        box.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        box.widthAnchor.constraint(equalToConstant: 315).isActive = true
        box.heightAnchor.constraint(equalToConstant: 116).isActive = true
        box.backgroundColor = .white
        
        box.shadowcolor = UIColor.black
        box.shadowoffset = CGSize(width: 0, height: 3)
        box.shadowradius = 6
        box.shadowopacity = 0.1
    }
    private func makelabel(){
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: box.centerYAnchor, constant: 0).isActive = true
        label.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 0).isActive = true
        label.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: 0).isActive = true
        label.heightAnchor.constraint(lessThanOrEqualToConstant: 150).isActive = true
        label.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        label.font = UIFont(name: fonts.medium.rawValue, size: 20)
        label.numberOfLines = 0
        label.lineBreakMode = .byWordWrapping
        label.textAlignment = .center
        label.text = """
이용에 불편을 드려 죄송합니다.
잠시 후 다시 시도해 주세요.
"""
    }
}
