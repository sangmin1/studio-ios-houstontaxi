//
//  FindingTaxiViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/07.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 제플린 500 배차 요청중 화면
 */
class FindingTaxiViewController: UIViewController {
    
    
    //그라데이션 백그라운드
    private var backgroundimage = UIImageView()
    //차량 사진들
    private var carimage = UIImageView()
    
    //근처에 있는 택시를 찾고 있습니다
    private var infolabel1 = PaddingLabel()
    //잠시만 기다려주세요
    private var infolabel2 = PaddingLabel()
    
    //호출 취소 버튼
    private var cancelbtn = UIButton()
    
    lazy private var allocfailedpopup = AllocFailedPopUp(frame: self.view.bounds, delegate: self)
    
    lazy private var presenter = FindingTaxiPresenter(delegate: self, viewcontroller: self)
    
    lazy private var cancelallocpopup = CancelAllocPopUp(frame: self.view.bounds, delegate: self)
    
    convenience init(allocdata : [String : Any]){
        self.init()
        presenter.setInfos(allocdata : allocdata)
        
        
    }
    
    
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = false
        makebackgroundimage()
        makeinfolabel1()
        makeinfolabel2()
        makecarimage()
        makecancelbtn()
        
        cancelbtn.addTarget(self, action: #selector(cancelbtnpressed(sender:)), for: .touchUpInside)
        
        self.view.addSubview(allocfailedpopup)
        allocfailedpopup.alpha = 0
        
        self.view.addSubview(cancelallocpopup)
        cancelallocpopup.alpha = 0
        
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //화면이 나오면 애니메이션 효과와
        //2초 주기로 callcheck 상태를 확인한다.
        runtimer()
        presenter.callAllocRequestAPI { [unowned self] (done) in
            if done {
                self.presenter.runtimer()
            }
            else {
                self.allocfailedpopup.alpha = 1
            }
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        cancelbtn.layer.cornerRadius = 25
    }
    
    //잠시만 기다려주세요 애니메이션 효과 타이머
    private var timer = Timer()
    
    func runtimer(){
        timer = Timer.scheduledTimer(timeInterval: 1, target: self, selector: #selector(updateTimer), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer(){
        infolabel2.fadeIn(0.5, delay: 0) { [unowned self] (done) in
            self.infolabel2.fadeOut(0.5, delay: 0.5) { (done) in
                
            }
        }
        
    }
    
    
    /**
     호출 취소 버튼 이벤트 함수 ( 팝업 표출)
     */
    @objc func cancelbtnpressed(sender : UIButton){
        self.view.bringSubviewToFront(cancelallocpopup)
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.cancelallocpopup.alpha = 1
        }
        //self.navigationController?.popViewController(animated: true)
    }
}
extension FindingTaxiViewController : CancelAllocPopUpDelegate {
    /**
     호출 취소 팝업 에서 호출 취소 버튼 이벤트 함수
     */
    func cancelallocatebtnpressed() {
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.cancelallocpopup.alpha = 0
        }
        presenter.callalloccancelapi { [unowned self] (done) in
            if done {
                self.navigationController?.popToViewController(ofClass: MainViewController.self, animated: true)
                if self.presenter.getcurrentcallstat() == .allocfailed {
                    FunctionClass.shared.showapologizepopup()
                }
                else {
                    FunctionClass.shared.showusercancelpopup()
                }
            }
        }
        
        
    }
}
extension FindingTaxiViewController : AllocFailedPopUpDelegate {
    /**
     재호출 이벤트 함수
     */
    func reallocatebtnpressed() {
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.allocfailedpopup.alpha = 0
        }
        presenter.callAllocRequestAPI { [unowned self] (done) in
            if done {
                self.presenter.runtimer()
            }
            else {
                self.allocfailedpopup.alpha = 1
            }
        }
    }
    /**
    택시를 찾지 못했습니다. 팝업 에서 호출 취소 버튼 이벤트 함수
    */
    func allocfailedpopDeclinepressed(){
        presenter.callalloccancelapi { [unowned self] (done) in
            if done {
                self.navigationController?.popToViewController(ofClass: MainViewController.self, animated: true)
                if self.presenter.getcurrentcallstat() == .allocfailed {
                    FunctionClass.shared.showapologizepopup()
                }
                else {
                    FunctionClass.shared.showusercancelpopup()
                }
            }
        }
    }
}
extension FindingTaxiViewController : FindingTaxiContract {
    /**
     배차 완료 화면으로 넘어가는 함수
     */
    func showAllocationCompleteViewController(){
        let view = AllocationCompleteViewController(allocdata: presenter.getInfos())
        self.navigationController?.pushViewController(view, animated: true)
    }
    /**
     배차 실패 팝업 화면 표출 함수
     */
    func showAllocFailePopUp(){
        self.view.bringSubviewToFront(self.allocfailedpopup)
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.allocfailedpopup.alpha = 1
        }
    }
    
    
    /**
     탑승 화면으로 넘어가는 함수
     */
    func showOnBoardingViewController(allocdata : [String : Any]){
        let view = OnBoardingViewController(allocdata: allocdata)
        self.navigationController?.pushViewController(view, animated: true)
    }
}

extension FindingTaxiViewController {
    private func makebackgroundimage(){
        self.view.addSubview(backgroundimage)
        backgroundimage.translatesAutoresizingMaskIntoConstraints = false
        backgroundimage.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        backgroundimage.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        backgroundimage.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        backgroundimage.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        backgroundimage.image = UIImage(named: "imgFindLoadingBg")?.withAlignmentRectInsets(UIEdgeInsets(top: 10, left: 10, bottom: 10, right: 10))
        
    }
    private func makeinfolabel1(){
        self.view.addSubview(infolabel1)
        infolabel1.translatesAutoresizingMaskIntoConstraints = false
        infolabel1.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 60).isActive = true
        infolabel1.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 35).isActive = true
        infolabel1.widthAnchor.constraint(equalToConstant: 300).isActive = true
        infolabel1.heightAnchor.constraint(lessThanOrEqualToConstant: 150).isActive = true
        infolabel1.numberOfLines = 0
        infolabel1.lineBreakMode = .byWordWrapping
        infolabel1.font = UIFont(name: fonts.bold.rawValue, size: 28)
        infolabel1.textColor = .white
        infolabel1.text = "근처에 있는 택시를\n찾고 있습니다."
    }
    private func makeinfolabel2(){
        self.view.addSubview(infolabel2)
        infolabel2.translatesAutoresizingMaskIntoConstraints = false
        infolabel2.topAnchor.constraint(equalTo: infolabel1.bottomAnchor, constant: 35).isActive = true
        infolabel2.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 35).isActive = true
        infolabel2.widthAnchor.constraint(equalToConstant: 300).isActive = true
        infolabel2.heightAnchor.constraint(equalToConstant: 30).isActive = true
        infolabel2.textColor = .rgb(red: 175, green: 221, blue: 219, alpha: 1)
        infolabel2.text = "잠시만 기다려 주세요."
        infolabel2.font = UIFont(name: fonts.bold.rawValue, size: 23)
        
        
    }
    private func makecarimage(){
        self.view.addSubview(carimage)
        carimage.translatesAutoresizingMaskIntoConstraints = false
        carimage.topAnchor.constraint(equalTo: infolabel1.bottomAnchor, constant: 30).isActive = true
        carimage.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        carimage.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        carimage.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        carimage.contentMode = .center
        carimage.contentMode = .scaleAspectFit
        carimage.image = UIImage(named: "imgTaxi32")
    }
    private func makecancelbtn(){
        self.view.addSubview(cancelbtn)
        cancelbtn.translatesAutoresizingMaskIntoConstraints = false
        cancelbtn.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -13).isActive = true
        cancelbtn.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        cancelbtn.widthAnchor.constraint(equalToConstant: 168).isActive = true
        cancelbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        cancelbtn.layer.borderColor = UIColor.white.cgColor
        cancelbtn.layer.borderWidth = 1
        cancelbtn.setTitle("호출 취소", for: .normal)
        cancelbtn.setTitleColor(.white, for: .normal)
        cancelbtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20)
    }
    
    
    
}

