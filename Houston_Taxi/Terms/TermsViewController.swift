//
//  TermsViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/21.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 이용약과 페이지
 */
class TermsViewController: UIViewController {
    
    private var topbox = UIView()
    
    private var titlelabel = PaddingLabel()
    
    private var backbtn = WideTapBtn()
    
    /**
     개인 정보 수집 및 이용
     */
    private var privacy = TermsBtn()
    /**
     위치정 보 수집 및 이용
     */
    private var location = TermsBtn()
    /**
     서비스 이용
     */
    private var service = TermsBtn()
    
    lazy private var btnstack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [privacy,location, service])
        stack.axis = .vertical
        stack.distribution = .fillEqually
        stack.spacing = 0
        return stack
    }()
    
    
    lazy private var presenter = TermsPresenter(delegate : self, viewcontroller : self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        maketopbox()
        maketitlelabel()
        makebackbtn()
        makebtnstack()
        backbtn.addTarget(self, action: #selector(backbtnpressed(sender:)), for: .touchUpInside)
        
        privacy.addTarget(self, action: #selector(termspressed(sender:)), for: .touchUpInside)
        location.addTarget(self, action: #selector(termspressed(sender:)), for: .touchUpInside)
        service.addTarget(self, action: #selector(termspressed(sender:)), for: .touchUpInside)
        
        
    }
    
    /**
     백버튼 이벤트 함수
     */
    @objc func backbtnpressed(sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    
    @objc func termspressed(sender : TermsBtn){
        presenter.callGetTerms(type: sender.type)
    }

    
}
extension TermsViewController : TermsContract{
    func showTermsPage(type: infouseagreementstype){
        let view = InfoUseAgreementsSpecPage(type: type)
        self.navigationController?.pushViewController(view, animated: true)
    }
}
extension TermsViewController  {
    private func maketopbox(){
        self.view.addSubview(topbox)
        topbox.translatesAutoresizingMaskIntoConstraints = false
        topbox.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        topbox.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        topbox.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        topbox.heightAnchor.constraint(equalToConstant: 56).isActive = true
        topbox.backgroundColor = .white
    }
    private func makebackbtn(){
        self.view.addSubview(backbtn)
        backbtn.translatesAutoresizingMaskIntoConstraints = false
        backbtn.centerYAnchor.constraint(equalTo: topbox.centerYAnchor, constant: 0).isActive = true
        backbtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        backbtn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        backbtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        backbtn.setImage(UIImage(named: "titleBtnBackN"), for: .normal)
        
    }
    private func maketitlelabel(){
        self.view.addSubview(titlelabel)
        titlelabel.translatesAutoresizingMaskIntoConstraints = false
        titlelabel.centerYAnchor.constraint(equalTo: topbox.centerYAnchor, constant: 0).isActive = true
        titlelabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        titlelabel.widthAnchor.constraint(equalToConstant: 150).isActive = true
        titlelabel.heightAnchor.constraint(equalToConstant: 23).isActive = true
        titlelabel.textColor = .rgb(red: 75, green: 75, blue: 75, alpha: 1)
        titlelabel.font = UIFont(name: fonts.bold.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .bold)
        titlelabel.textAlignment = .center
        titlelabel.text = "콜센터"
        
    }
    private func makebtnstack(){
        self.view.addSubview(btnstack)
        btnstack.translatesAutoresizingMaskIntoConstraints = false
        btnstack.topAnchor.constraint(equalTo: topbox.bottomAnchor, constant: 0).isActive = true
        btnstack.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        btnstack.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        btnstack.heightAnchor.constraint(equalToConstant: 84 * 3).isActive = true
        privacy.setlabel(label: "개인 정보 수집 및 이용")
        location.setlabel(label: "위치 정보 수집 및 이용")
        service.setlabel(label: "서비스 이용")
        
        privacy.type = .privacy
        location.type = .location
        service.type = .service
        
    }
    
    
}
