//
//  TermsContract.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/21.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation


protocol TermsContract : NSObjectProtocol {
    func showTermsPage(type: infouseagreementstype)
}
