//
//  TermsBtn.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/21.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


/**
약관 컴포넌트
 */
class TermsBtn: UIButton {
    
    private var label = PaddingLabel()
    
    
    
    private var underline = UIImageView()
    
    var type : infouseagreementstype = .none
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        makelabel()
        makeunderline()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    func setlabel(label :String){
        let attr = NSAttributedString(string: label, attributes: [NSAttributedString.Key.foregroundColor : UIColor.rgb(red: 100, green: 100, blue: 100, alpha: 1), NSAttributedString.Key.font : UIFont(name: fonts.medium.rawValue, size: 20)!, NSAttributedString.Key.underlineStyle : NSUnderlineStyle.single.rawValue, NSAttributedString.Key.underlineColor : UIColor.rgb(red: 100, green: 100, blue: 100, alpha: 1) ])
        self.label.attributedText = attr
    }
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.bounds.contains(point) {
            return self
        }
        else {
            return nil
        }
    }
    
}
extension TermsBtn {
    private func makelabel(){
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 24).isActive = true
        label.widthAnchor.constraint(equalToConstant: 200).isActive = true
        label.heightAnchor.constraint(equalToConstant: 26).isActive = true
        label.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        label.font = UIFont(name: fonts.medium.rawValue, size: 20) //.systemFont(ofSize: 20, weight: .medium)
        label.text = ""
    }
    private func makeunderline(){
        self.addSubview(underline)
        underline.translatesAutoresizingMaskIntoConstraints = false
        underline.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        underline.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        underline.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        underline.heightAnchor.constraint(equalToConstant: 1).isActive = true
        underline.backgroundColor = .rgb(red: 0, green: 0, blue: 0, alpha: 0.05)
    }
}

