//
//  TermsPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/21.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class TermsPresenter: NSObject {
    
    weak var delegate : TermsContract?
    weak var viewcontroller : TermsViewController?
    
    
    init(delegate : TermsContract, viewcontroller : TermsViewController){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
    }
    
    func callGetTerms(type: infouseagreementstype){
        
        let api = GetTerms()
        api.requestToserver { [unowned self] (done, data) in
            if done {
                TermsRepo.shared.terms = data
                self.delegate?.showTermsPage(type: type)
            }
        }
        
    }
    
    
    
    
}
