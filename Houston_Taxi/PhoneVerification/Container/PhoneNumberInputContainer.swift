//
//  PhoneNumberInputContainer.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
import RxSwift
import RxCocoa


/**
 인증 번호를 받을 핸드폰 번호를 입력받는 컨테이너
 */
class PhoneNumberInputContainer: UIView {
    let disposebag = DisposeBag()
    
    //휴대폰 번호 인증
    private var titlelabel = PaddingLabel()
    // 연락 받을 수 있는~~~
    private var infolabel = PaddingLabel()
    
    //휴대폰 번호 입력 필드
    private var phoneinputfield = PaddingTextField()
    //인증 번호 받기 버튼
    private var getVerifyNumberBtn = ShadowBtn()
    
    //해당 컨테이너 델리게이트
    weak var delegate : PhoneNumberInputContainerContract?
    
    init(frame : CGRect,delegate : PhoneNumberInputContainerContract){
        super.init(frame: frame)
        self.delegate = delegate
        maketitlelabel()
        makeinfolabel()
        makephoneinputfield()
        makeverifybtn()
        
        getVerifyNumberBtn.addTarget(self, action: #selector(getverifybtnpressed(sender:)), for: .touchUpInside)
        self.bottomAnchor.constraint(equalTo: getVerifyNumberBtn.bottomAnchor, constant: 50).isActive = true
        
        // 핸드폰 텍스트가 11자리인지 판별하여 인증번호 받기 버튼을 활성화 비활성화 함
        phoneinputfield.rx.text.orEmpty
            .map(countphonenumber)
            .subscribe(onNext : { [unowned self] result in
                self.getVerifyNumberBtn.isUserInteractionEnabled = result
            })
            .disposed(by: disposebag)
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async { [unowned self] in
            self.phoneinputfield.layer.shadowColor = UIColor.rgb(red: 12, green: 161, blue: 155, alpha: 1).cgColor
            self.phoneinputfield.layer.shadowRadius = 6
            self.phoneinputfield.layer.shadowOpacity = 0.25
            self.phoneinputfield.layer.shadowOffset = CGSize(width: 0, height: 3)
        }
    }
    override func didMoveToWindow() {
        super.didMoveToWindow()
        phoneinputfield.layer.cornerRadius = 8
        DispatchQueue.main.async { [unowned self] in
            self.phoneinputfield.layer.shadowColor = UIColor.rgb(red: 12, green: 161, blue: 155, alpha: 1).cgColor
            self.phoneinputfield.layer.shadowRadius = 6
            self.phoneinputfield.layer.shadowOpacity = 0.25
            self.phoneinputfield.layer.shadowOffset = CGSize(width: 0, height: 3)
        }
        getVerifyNumberBtn.layer.cornerRadius = 25
    }
    
    //인증번호 받기 버튼 눌렀을때
    @objc func getverifybtnpressed(sender : UIButton){
        //self.delegate?.showInfoAccessAgreementsViewController()
        self.delegate?.getVerifyNumberBtnpressed(phonenumber : phoneinputfield.text!)
    }
    
    
}
extension PhoneNumberInputContainer {
    private func maketitlelabel(){
        self.addSubview(titlelabel)
        titlelabel.translatesAutoresizingMaskIntoConstraints = false
        titlelabel.topAnchor.constraint(equalTo: self.topAnchor, constant: 56).isActive = true
        titlelabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        titlelabel.widthAnchor.constraint(equalToConstant: 150).isActive = true
        titlelabel.heightAnchor.constraint(equalToConstant: 27).isActive = true
        titlelabel.textColor = .rgb(red: 70, green: 70, blue: 70, alpha: 1)
        
        titlelabel.font = UIFont(name: fonts.bold.rawValue, size: 21)
        titlelabel.text = "휴대폰 번호 인증"
        titlelabel.textAlignment = .center
        
    }
    private func makeinfolabel(){
        self.addSubview(infolabel)
        infolabel.translatesAutoresizingMaskIntoConstraints = false
        infolabel.topAnchor.constraint(equalTo: titlelabel.bottomAnchor, constant: 15).isActive = true
        infolabel.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        infolabel.widthAnchor.constraint(equalTo: self.widthAnchor, multiplier: 1).isActive = true
        infolabel.heightAnchor.constraint(lessThanOrEqualToConstant: 100).isActive = true
        infolabel.numberOfLines = 0
        infolabel.lineBreakMode = .byWordWrapping
        infolabel.textColor = .rgb(red: 184, green: 184, blue: 184, alpha: 1)
        infolabel.font = UIFont(name: fonts.medium.rawValue, size: 18)
        infolabel.text = "연락 받을 수 있는 휴대폰 번호를 입력하세요.\n서비스 이용에 반드시 필요합니다."
        infolabel.textAlignment = .center
    }
    private func makephoneinputfield(){
        self.addSubview(phoneinputfield)
        phoneinputfield.translatesAutoresizingMaskIntoConstraints = false
        phoneinputfield.topAnchor.constraint(equalTo: infolabel.bottomAnchor, constant: 25).isActive = true
        phoneinputfield.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        phoneinputfield.widthAnchor.constraint(equalToConstant: 351 * wscale).isActive = true
        phoneinputfield.heightAnchor.constraint(equalToConstant: 54).isActive = true
        phoneinputfield.placeholder = "휴대폰 번호"
        phoneinputfield.font = UIFont(name: fonts.medium.rawValue, size: 20)
        phoneinputfield.textColor = .black
        phoneinputfield.layer.borderColor = maincolor.cgColor
        phoneinputfield.layer.borderWidth = 1
        phoneinputfield.left = 10
        phoneinputfield.delegate = self
        phoneinputfield.keyboardType = .decimalPad
        phoneinputfield.backgroundColor = .white
    }
    private func makeverifybtn(){
        self.addSubview(getVerifyNumberBtn)
        getVerifyNumberBtn.translatesAutoresizingMaskIntoConstraints = false
        getVerifyNumberBtn.topAnchor.constraint(equalTo: phoneinputfield.bottomAnchor, constant: 20).isActive = true
        getVerifyNumberBtn.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        getVerifyNumberBtn.widthAnchor.constraint(equalToConstant: 351  * wscale).isActive = true
        getVerifyNumberBtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        //
        getVerifyNumberBtn.backgroundColor = .rgb(red: 18, green: 94, blue: 115, alpha: 1)
        getVerifyNumberBtn.setTitle("인증번호 받기", for: .normal)
        getVerifyNumberBtn.setTitleColor(.white, for: .normal)
        getVerifyNumberBtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20)
        
        getVerifyNumberBtn.shadowcolor = .black
        getVerifyNumberBtn.shadowoffset = CGSize(width: 0, height: 3)
        getVerifyNumberBtn.shadowradius = 6
        getVerifyNumberBtn.shadowopacity = 0.16
        
    }
    
}
extension PhoneNumberInputContainer : UITextFieldDelegate {
    /**
     인풋창의 글자 개수를 읽어 버튼의 활성도 여부 판단
     */
    func countphonenumber(src : String) -> Bool{
        if src.count == 11 {
            getVerifyNumberBtn.backgroundColor = maincolor
            return true
        }
        else {
            getVerifyNumberBtn.backgroundColor = .rgb(red: 18, green: 94, blue: 115, alpha: 1)
            return false
        }
    }
    
    
    
}
