//
//  PhoneVerificationContract.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit



//휴대폰 인증 메인뷰 델리게이트
protocol PhoneVerificationContract : NSObjectProtocol {
    
    func showverifyingContainer()
    
}
//휴대폰 번호 입력 컨테이너 델리게이트
protocol PhoneNumberInputContainerContract : NSObjectProtocol {
    
    func getVerifyNumberBtnpressed(phonenumber : String)
}
