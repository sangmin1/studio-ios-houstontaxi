//
//  VerifyNumbMisMathPopUP.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
/**
 인증 번호 불일치 팝업
 */
class VerifyNumbMisMatchPopUp: UIView {
    
    
    private var label = PaddingLabel()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = .init(white: 0.2, alpha: 0.4)
        makelabel()
        
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        label.layer.cornerRadius = 8
        label.clipsToBounds = true
        
    }
    
    
}
extension VerifyNumbMisMatchPopUp {
    private func makelabel(){
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        label.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        label.widthAnchor.constraint(equalToConstant: 315 * wscale).isActive = true
        label.heightAnchor.constraint(equalToConstant: 90).isActive = true
        label.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        label.font = UIFont.systemFont(ofSize: 20, weight: .medium)
        label.textAlignment = .center
        label.text = "인증 번호가 일치하지 않습니다."
        label.backgroundColor = .white
        
    }
    
}
