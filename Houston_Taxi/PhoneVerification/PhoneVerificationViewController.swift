//
//  PhoneVerificationViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 전화 번호 입력 뷰 컨트롤러
 */
class PhoneVerificationViewController: UIViewController {
    
    
    lazy private var presenter = PhoneVerificationPresenter(delegate: self, viewcontroller: self)
    
    //컨테이너에서부터 컨테이너 객체를 가져온다
    lazy private var phoneinputcontainer = presenter.getphoneinputcontainer()
    
    
    
    lazy private var stack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [phoneinputcontainer])
        stack.axis = .vertical
        stack.distribution = .fill
        return stack
    }()
    
        
        
    
    let mismatchpopup = VerifyNumbMisMatchPopUp()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = .white
        makestack()
        makemismatchpopup()
        
    }
    
    
    
    
    
    
    
}

extension PhoneVerificationViewController : PhoneVerificationContract {
    /**
     인증번호 확인 화면으로 넘어가는 함수
     */
    func showverifyingContainer() {
        let view = PhoneVerifyingViewController()
        self.navigationController?.pushViewController(view, animated: true)
    }
    
}
extension PhoneVerificationViewController {
    private func makestack(){
        self.view.addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        stack.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        stack.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        stack.heightAnchor.constraint(lessThanOrEqualToConstant: 1000).isActive = true
    }
    private func makemismatchpopup(){
        self.view.addSubview(mismatchpopup)
        mismatchpopup.frame = self.view.frame
        mismatchpopup.alpha = 0
        
        
        
        
    }
    
    
}
