//
//  PhoneVerificationPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/13.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


/**
 휴대폰 인증 화면 프레젠터
 이 곳에서 모든 비즈니스 로직이 처리됨
 */
class PhoneVerificationPresenter: NSObject {
    
    weak var delegate : PhoneVerificationContract?
    weak var viewcontroller : PhoneVerificationViewController?
    
    lazy private var phoneinputcontainer = PhoneNumberInputContainer(frame : .zero, delegate : self)
    
    
    init(delegate : PhoneVerificationContract,viewcontroller : PhoneVerificationViewController){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
    }
    
    //뷰 컨트롤러에 컨테이너 객체를 넘겨주는 함수들
    func getphoneinputcontainer() -> PhoneNumberInputContainer{
        return self.phoneinputcontainer
    }
    
    
    
    /**
     인증 번호 api 호출 함수
     
     */
    func callAuthAPI(phonenumber : String,completion : @escaping (Bool) -> ()){
        
        let api = AuthAPI()
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: phonenumber)
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile) { (done, data) in
            if done {
                print("data",data)
                print("msg",FunctionClass.shared.decrypt(msg: data["msg"].stringValue))
                completion(data["isSuccessful"].boolValue)
                
            }
            else {
                completion(false)
            }
        }
        
        
    }
    
    
    
}
//휴대폰번호 입력 컨테이너 델리게이트 비즈니스 로직
extension PhoneVerificationPresenter : PhoneNumberInputContainerContract{
    /**
     인증 번호 받기 버튼 이벤트 함수
     */
    func getVerifyNumberBtnpressed(phonenumber : String){
        self.callAuthAPI(phonenumber: phonenumber){ [unowned self] (done) in
            if done {
                //문자 수신을 성공하면 핸드폰 번호를 내부 디비에 저장한다
                UserDefaults.standard.set(phonenumber, forKey: userstandard.phonenumber.rawValue)
                print("phonenumb",phonenumber)
                self.delegate?.showverifyingContainer()
            }
            else {
                ToastView.shared.short(self.viewcontroller!.view, txt_msg: "잠시 후 시도해주세요")
            }
        }
        
    }
}
