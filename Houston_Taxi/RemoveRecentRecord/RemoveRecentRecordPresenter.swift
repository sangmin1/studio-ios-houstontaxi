//
//  RemoveRecentRecordPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/17.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class RemoveRecentRecordPresenter: NSObject {
    
    weak var delegate : RemoveRecentRecordContract?
    weak var viewcontroller : RemoveRecentRecordViewController?
    
    let cellid = "removerecentrecordcellid"
    
    init(delegate : RemoveRecentRecordContract, viewcontroller : RemoveRecentRecordViewController ){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
    }
    
    func registerCv(cv : UICollectionView){
        cv.register(RemoveRecentRecordCell.self, forCellWithReuseIdentifier: cellid)
    }
    
}
extension RemoveRecentRecordPresenter : UICollectionViewDelegate, UICollectionViewDelegateFlowLayout, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 5
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: cellid, for: indexPath) as! RemoveRecentRecordCell
        
        
        return cell
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize(width: mainboundwidth, height: 84)
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumLineSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, minimumInteritemSpacingForSectionAt section: Int) -> CGFloat {
        return 0
    }
    
    
}
