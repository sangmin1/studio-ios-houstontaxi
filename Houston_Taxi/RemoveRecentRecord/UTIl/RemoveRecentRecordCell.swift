//
//  RemoveRecentRecordCell.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/17.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


/**
  최근 기록 삭제 리스트 셀
 */
class RemoveRecentRecordCell: UICollectionViewCell {
    
    
    private var titlelabel = PaddingLabel()
    private var addresslabel = PaddingLabel()
    
    lazy private var labelstack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [titlelabel,addresslabel])
        stack.axis = .vertical
        stack.spacing = 4
        return stack
    }()
    
    private var checkbox = WideTapBtn()
    
    private var underline = UIImageView()
    
    weak var delegate : RemoveRecentRecordCellDelegate?
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        makelabestack()
        makecheckbox()
        makeunderline()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setdelegate(delegate : RemoveRecentRecordCellDelegate){
        self.delegate = delegate
    }
    
    
    
    
    
    
    
    
}
extension RemoveRecentRecordCell {
    private func makelabestack(){
           self.addSubview(labelstack)
           labelstack.translatesAutoresizingMaskIntoConstraints = false
           labelstack.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
           labelstack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 24).isActive = true
           labelstack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 80).isActive = true
           labelstack.heightAnchor.constraint(lessThanOrEqualToConstant: 47).isActive = true
           
           NSLayoutConstraint(item: titlelabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 26).isActive = true
           titlelabel.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        titlelabel.font = UIFont(name: fonts.medium.rawValue, size: 20) //.systemFont(ofSize: 20, weight: .medium)
           titlelabel.text = "바비레드 강남본점"
           
           NSLayoutConstraint(item: addresslabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 21).isActive = true
           
           addresslabel.textColor = .rgb(red: 184, green: 184, blue: 184, alpha: 1)
        addresslabel.font = UIFont(name: fonts.medium.rawValue, size: 16) //.systemFont(ofSize: 16, weight: .medium)
           addresslabel.text = "서울 강남구 봉은사로6길 39"
           //addresslabel.isHidden = true
       }
    private func makecheckbox(){
        self.addSubview(checkbox)
        checkbox.translatesAutoresizingMaskIntoConstraints = false
        checkbox.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        checkbox.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -22).isActive = true
        checkbox.widthAnchor.constraint(equalToConstant: 30).isActive = true
        checkbox.heightAnchor.constraint(equalToConstant: 30).isActive = true
        checkbox.setImage(UIImage(named: "dtListUnChecked"), for: .normal)
    }
    private func makeunderline(){
        self.addSubview(underline)
        underline.translatesAutoresizingMaskIntoConstraints = false
        underline.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        underline.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        underline.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        underline.heightAnchor.constraint(equalToConstant: 1).isActive = true
        underline.backgroundColor = .rgb(red: 0, green: 0, blue: 0, alpha: 0.05)
    }
    
    
}
