//
//  OnBoardingPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/11.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
import iNaviMaps

class OnBoardingPresenter: NSObject {
    
    weak var delegate : OnBoardingContract?
    weak var viewcontroller : OnBoardingViewController?
    
    
    
    
    //택시 위치
    private var carposx : Double = 0
    private var carposy : Double = 0
    
    //목적지 좌표
    private var destlat : Double?
    private var destlon : Double?
    //목적지 이름
    private var destname : String?
    
    //현재 위치
    private var currentaddress : String = ""
    
    //지오 코딩 타이머
    private var geocodetimer = Timer()
    //지오 코딩 타이머 블럭 flag
    private var blockgeocodetimer : Bool = false
    
    
    
    
    
    //콜체크 타이머
    private var callchecktimer = Timer()
    //콜체크 블럭 flag
    private var blockcallchecktimer : Bool = false
    
    //차량 위치 추적 타이머
    private var timer = Timer()
    //차량 위치 타이머
    private var blocktimer : Bool = false
    
    //콜체크 상태
    private var currentcallcheck : callcheck = .onboard
    
    
    
    
    
    
    
    init(delegate : OnBoardingContract, viewcontroller : OnBoardingViewController){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
    }
    /**
     배차에 관한 정보들을 셋하는 함수
     */
    func setInfos(allocdata : [String : Any]){
        
        if let destname = allocdata["destname"] as? String, let endx = allocdata["endx"] as? Double, let endy = allocdata["endy"] as? Double {
            self.destname = destname
            self.destlon = endx
            self.destlat = endy
            
        }
        
    }
    
    
    /**
     도착지 좌표 반환 함수
     */
    func getdestcoord() -> (lat : Double?, lon : Double?){
        return (destlat,destlon)
        //return (testlat,testlon)
    }
    
    /**
     도착지 이름 반환 함수
     */
    func getdestname() -> String? {
        return self.destname
    }
    
    
    func getcarpos() -> (lon : Double, lat : Double){
        return (self.carposx, self.carposy)
    }
    
    
    
    
    func callreversegeocodeapi(posx : Double, posy : Double , completion : @escaping (String,Bool) -> () )  {
        let reversegeocodeapi = ReverGeoCodingAPI()
        reversegeocodeapi.request_To_server(posx: posx, posy: posy) { (done, data) in
            if done {
                let tempmodel = ReverseGeoCodingModel(json: data)
                
                guard let adm = tempmodel.adm else {
                    completion("현재 위치를 알 수 없습니다. 기사님에게 물어봐 주세요",true)
                    return
                }
                
                guard let bldname = adm.bldname else {
                    completion("현재 위치를 알 수 없습니다. 기사님에게 물어봐 주세요",true)
                    return
                }
                //빌딩이름이 없으면 도로명 주소 표출
                if bldname == "" {
                    guard let roadname = adm.roadname else {
                        completion("현재 위치를 알 수 없습니다. 기사님에게 물어봐 주세요",true)
                        return
                    }
                    completion(roadname,true)
                    return
                }
                else {
                    //빌딩 이름이 있으면 빌딩명 표출
                    completion(bldname,true)
                }
            }
            else {
                completion("",false)
            }
        }
    }
    
    
    
    
    
    
    
    
    
    
    
    
}
extension OnBoardingPresenter {
    /**
     콜체크 api 함수
     */
    func callCallCheck(){
        let api = GetCallCheckAPI()
        
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        
        let callid = FunctionClass.shared.encrypt(msg: CallRepo.callID)
        let calldt = FunctionClass.shared.encrypt(msg: FunctionClass.shared.getCallDt())
        
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile, callID: callid, callDT: calldt) { [unowned self] (done, data) in
            if done {
                let issuccess = data["isSuccessful"].boolValue
                print("on boarding call check issuccess",issuccess)
                guard let msg = data["msg"].string else {
                    print("on boarding view controller msg failed")
                    return
                }
                print("alloc complet view msg",FunctionClass.shared.decrypt(msg: msg))
                guard let callStatus = data["callStatus"].string else {
                    print("on boarding view controller callstatus failed")
                    return
                }
                let callstat = FunctionClass.shared.decrypt(msg: callStatus)
                print("onboarding callcheck",callstat)
                for status in callcheck.allCases {
                    if status.rawValue == callstat {
                        self.currentcallcheck = status
                    }
                }
                
            }
        }
    }
    
    /**
     2초마다 콜체크 상태를 확인하는 함수
     */
    func runcallchecktimer(){
        callchecktimer = Timer.scheduledTimer(timeInterval: 2, target: self,   selector: (#selector(updatecallchecktimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updatecallchecktimer(){
        print("2sec call check update timer")
        if blockcallchecktimer == true {
            callchecktimer.invalidate()
            return
        }
        else {
            
            if self.currentcallcheck == .onboard {//탑승중
                self.callCallCheck()
                blockcallchecktimer = false
                blockgeocodetimer = false
                blocktimer = false
            }
            else if self.currentcallcheck == .drop { //하차 -> 도착 화면으로 이동
                blockcallchecktimer = true
                blockgeocodetimer = true
                blocktimer = true
                self.delegate?.showTravelDoneViewController()
                return
                
            }
            else if self.currentcallcheck == .fail {//실패
                blockgeocodetimer = true
                blockcallchecktimer = true
                blocktimer = true
                return
            }
            else if self.currentcallcheck == .cancel { //배차 완료 후 자의로 취소 한경우
                //제플린 610 배차 완료 취소
                blockgeocodetimer = true
                blockcallchecktimer = true
                blocktimer = true
                return
            }
            else if self.currentcallcheck == .connecterror {
//                blockgeocodetimer = true
//                blockcallchecktimer = true
//                blocktimer = true
//                return
            }
            else if self.currentcallcheck == .onboardfailed {
                blockgeocodetimer = true
                blockcallchecktimer = true
                blocktimer = true
                self.viewcontroller?.navigationController?.popToViewController(ofClass: MainViewController.self, animated: true)
                return
            }
            
        }
    }
}
extension OnBoardingPresenter {
    /**
     지도 이동시 리버스 지오코딩을 실행하는 함수 (타이머 용)
     
     - parameters :
     - posx : x좌표
     - posy : y좌표
     - addr : 콜백 결과 주소
     - done : 콜백 성공 여부
     */
    func callreversegeocodeapi(posx : Double, posy : Double )  {
        let reversegeocodeapi = ReverGeoCodingAPI()
        reversegeocodeapi.request_To_server(posx: posx, posy: posy) { (done, data) in
            if done {
                let tempmodel = ReverseGeoCodingModel(json: data)
                
                guard let adm = tempmodel.adm else {
                    //completion("",false)
                    return
                }
                
                guard let bldname = adm.bldname else {
                    //completion("",true)
                    return
                }
                //빌딩이름이 없으면 도로명 주소 표출
                if bldname == "" {
                    guard let roadname = adm.roadname else {
                        //completion("",true)
                        return
                    }
                    self.currentaddress = roadname
                    self.delegate?.setaddress(address: bldname)
                    //completion(roadname,true)
                    return
                }
                else {
                    //빌딩 이름이 있으면 빌딩명 표출
                    self.currentaddress = bldname
                    self.delegate?.setaddress(address: bldname)
                    //completion(bldname,true)
                }
            }
            else {
                //completion("",false)
            }
        }
    }
    
    
    
    /**
     지오코딩 타이머 함수
     */
    func rungeocodetimer(){
        geocodetimer = Timer.scheduledTimer(timeInterval: 10, target: self,   selector: (#selector(updategeocodetimer)), userInfo: nil, repeats: true)
        
    }
    
    @objc func updategeocodetimer(){
        if blockgeocodetimer == true {
            callchecktimer.invalidate()
            return
        }
        else {
            self.callreversegeocodeapi(posx: carposx, posy: carposy)
        }
    }
}
extension OnBoardingPresenter {
    func callCarpos(){
//        testlon2 = testlon2 + 0.00001
//        testlat2 = testlat2 + 0.00001
//        self.delegate?.setmarker(posx: testlon2 , posy: testlat2)
        
        let api = GetCarPosInfo()

        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)

        let callid = FunctionClass.shared.encrypt(msg: CallRepo.callID)
        let calldt = FunctionClass.shared.encrypt(msg: CallRepo.callDt)

        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile, callID: callid, callDT: calldt) { [unowned self] (done, data) in
            if done {
                let carlon = data["carLon"].stringValue
                let lon = FunctionClass.shared.decrypt(msg: carlon)
                self.carposx = Double(lon)!

                let carlat = data["carLat"].stringValue
                let lat = FunctionClass.shared.decrypt(msg: carlat)
                self.carposy = Double(lat)!
                print("car posx : \(lon) = posy : \(lat)")
                
                
                if let destlon = self.destlon , let destlat = self.destlat {
                    self.callInaviFindRouteAPI(startx: self.carposx, starty: self.carposy, endx: destlon, endy: destlat) { (done) in
                        if done {
                            self.delegate?.makerout()
                            self.delegate?.setmarker(posx:self.carposx, posy: self.carposy)
                        }
                    }
                }
                else {
                    self.delegate?.setmarker(posx:self.carposx, posy: self.carposy)
                }
                
                
            }
            
            
            
//            self.testlon2 = self.testlon2 + 0.00001
//            self.testlat2 = self.testlat2 + 0.00001
//            self.delegate?.setmarker(posx: self.testlon2 , posy: self.testlat2)
            
            
        }
    }
    
    /**
     10초마다 차량의 위치를 받아오는 함수
     */
    func runtimer(){
        timer = Timer.scheduledTimer(timeInterval: 10, target: self,   selector: (#selector(updateTimer)), userInfo: nil, repeats: true)
    }
    
    @objc func updateTimer(){
        print("on boarding 10sec update timer")
        if blocktimer == true {
            timer.invalidate()
            return
        }
        else {
            if currentcallcheck == .allocsucess {// 배차 완료시 계속 택시 위치를 잡는다.
                
            }
            else if currentcallcheck == .onboard {//탑승 경우 탑승 화면으로 넘어감
                //self.delegate?.showOnBoardingViewController()
                self.callCarpos()
            }
            else if currentcallcheck == .allocfailed { //기사님이 배차 취소 한경우
                //제플린 620번 팝업 배차완료 실패
                //self.delegate?.showdriverdeclinedpopup()
                blocktimer = true
                blockcallchecktimer = true
                blockgeocodetimer = true
            }
            else if currentcallcheck == .cancel { //배차 완료 후 자의로 취소 한경우
                //제플린 610 배차 완료 취소
                blocktimer = true
                blockcallchecktimer = true
                blockgeocodetimer = true
            }
            else if currentcallcheck == .drop {
                
                blocktimer = true
                blockcallchecktimer = true
                blockgeocodetimer = true
            }
            else if currentcallcheck == .connecterror {
                
            }
            else if currentcallcheck == .onboardfailed {
                blocktimer = true
                blockcallchecktimer = true
                blockgeocodetimer = true
                self.viewcontroller?.navigationController?.popToViewController(ofClass: MainViewController.self, animated: true)
            }
        }
        
    }
    
    /**
     경로 탐색 api
     
     */
    func callInaviFindRouteAPI(startx : Double,  starty : Double , endx : Double , endy : Double,completion : @escaping (Bool) -> ()){
        
        let api = InaviFindRouteAPI()
        
        api.request_To_server(startx: startx, starty: starty, endx: endx, endy: endy) { (done, data) in
            if done {
                print("CallWithDestPresenter findroute data",data["header"])
                let tempmodel = InaviRouteModel(json: data)
                InaviRouteRepo.shared.setonboardingModel(input: tempmodel)
                if let header = InaviRouteRepo.shared.getonboardingModel().header {
                    print("header.isSuccessful!",header.isSuccessful!)
                    completion(header.isSuccessful!)
                }
                else {
                    print("header failed")
                    completion(false)
                }
                
            }
            else {
                completion(false)
            }
        }
    }
    
    
    /**
     레포에서 부터 좌표를 가져와 INVRouteLink 객체 배열을 만들어서 뷰 컨트롤러에 넘겨주는 함수
     경로를 그리는데 필수인 함수
     */
    func setrouteOnMap() -> [INVRouteLink] {
    
        var links : [INVRouteLink] = []
        for coord in InaviRouteRepo.shared.getonboardingcoords() {
            let link = INVRouteLink(coords: coord)
            link.lineColor = .rgb(red: 10, green: 185, blue: 180, alpha: 1)
            link.strokeColor = .rgb(red: 10, green: 185, blue: 180, alpha: 1)
            links.append(link)
        }
        return links
    }
    
    
    
    
    
}
