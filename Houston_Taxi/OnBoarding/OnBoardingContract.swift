//
//  OnBoardingContract.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/11.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation


protocol OnBoardingContract : NSObjectProtocol {
    func setaddress(address : String)
    func showTravelDoneViewController()
    func setmarker(posx : Double , posy : Double)
    
    
    func makerout()
}
