//
//  OnBoardingMessageBtn.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/19.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


/**
 안심 메시지 버튼 컴포넌트
 */
class OnBoardingMessageBtn: UIButton {
    
    private var icon = UIImageView()
    
    
    private var label = PaddingLabel()
    
    
    lazy private var stack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [icon,label])
        stack.distribution = .fillProportionally
        stack.spacing = 8
        stack.axis = .horizontal
        stack.alignment = .center
        return stack
    }()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = maincolor
        makestack()
        
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async { [unowned self] in
            self.layer.cornerRadius = self.frame.height / 2
        }
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
           if self.bounds.contains(point) {
               return self
           }
           else {
               return nil
           }
       }
       
    
    
}
extension OnBoardingMessageBtn {
    private func makestack(){
        self.addSubview(stack)
        stack.translatesAutoresizingMaskIntoConstraints = false
        stack.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        stack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 26).isActive = true
        stack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -26).isActive = true
        stack.heightAnchor.constraint(equalToConstant: 26).isActive = true
        
        icon.image = UIImage(named: "mapBtnIcMsgN")
        
        label.textColor = .white
        label.font = UIFont(name: fonts.bold.rawValue, size: 20)
        label.textAlignment = .center
        label.text = "안심메세지"
        
        
    }
    
    
}
