//
//  OnBoardingViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/11.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
import iNaviMaps
import MessageUI



/**
 제플린 700 - 이동 중 화면
 */
class OnBoardingViewController: UIViewController {
    
    
    lazy private var mapView = InaviMapView(frame: self.view.frame)
    
    //현재 위치 버튼
    private var currentlocationbtn = UIButton()
    
    private var box = ShadowView()
    //주소 라벨
    private var infolabel = PaddingLabel()
    
    //도착지 마커
    private var destmarker = INVMarker()
    //현재 위치
    private var mymarker = INVMarker()
    let myImage = INVImage(image: UIImage(named: "mapPinMyloca")!)
    
    //안심메시지
    private var messagebtn = OnBoardingMessageBtn()
    
    private var polyline = INVPolyline()
    

    
    lazy private var presenter = OnBoardingPresenter(delegate: self, viewcontroller: self)
    
    
    
    var route = INVRoute()
    convenience init(allocdata : [String : Any]){
        self.init()
        
        presenter.setInfos(allocdata: allocdata)
        
    }

    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = .white
        self.view.addSubview(mapView)
        
        //mapView.locationIcon.isVisible = false
        
        mapView.delegate = self
        
        makebox()
        makeinfolabel()
        makemessagebtn()
        makecurrentlocationbtn()
        
        
        
        
        
        
        
        currentlocationbtn.addTarget(self, action: #selector(currentlocationbtnpressed(sender:)), for: .touchUpInside)
        messagebtn.addTarget(self, action: #selector(messagebtnpressed(sender:)), for: .touchUpInside)
        
        
        
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        //콜체크 타이머를 돌리는 함수
        presenter.runcallchecktimer()
        //10초마다 차량 위치를 받아오는 함수
        
        presenter.callCarpos()
        presenter.runtimer()
        
        
        
        
        //목적지 존재시 목적지 마커 세팅 함수
        if let lat = presenter.getdestcoord().lat , let lon = presenter.getdestcoord().lon {
            print("목적지 잇음")
            print("dest code" ,lat, lon )
            destmarker.iconImage = INVImage(image: UIImage(named: "mapPinArr")!)
            destmarker.position = INVLatLng(lat: lat, lng: lon)
            destmarker.mapView = mapView
            mapView.moveCamera(INVCameraUpdate(targetTo: INVLatLng(lat: lat, lng: lon)))
            //self.mapView.cameraPosition = INVCameraPosition(INVLatLng(lat: lat, lng: lon), zoom: 14.0)
            if let destname = presenter.getdestname() {
                let str = "\(destname)으로\n가고 있습니다."
                let attributedString = NSMutableAttributedString(string: str, attributes: [
                  .font: UIFont(name: fonts.medium.rawValue, size: 24)!,
                  .foregroundColor: UIColor(white: 70.0 / 255.0, alpha: 1.0)
                ])
                let range = NSString(string: str).range(of: destname)
                attributedString.addAttribute(.font, value: UIFont(name: fonts.bold.rawValue, size: 24)!, range: range)
                infolabel.attributedText = attributedString
            }
        }
        else {
            print("목적지 없음")
            //목적지가 존재하지 않을때 현재 지나가는 위치를 지오코딩하여 보여주는 함수(10초 기반)
            presenter.rungeocodetimer()
        }
         
    }
    
    
    
    
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        self.box.layer.cornerRadius = 8
        self.messagebtn.layer.cornerRadius = 25
        
    }
    
    
    /**
     현재 위치로 가기 버튼 이벤트
     */
    @objc func currentlocationbtnpressed(sender : UIButton){
        let lat = UserDefaults.standard.double(forKey: userstandard.lat.rawValue)
        let lon = UserDefaults.standard.double(forKey: userstandard.lon.rawValue)
        mapView.moveCamera(INVCameraUpdate(targetTo: INVLatLng(lat: lat, lng: lon)))
    }
    
    /**
     안심 메시지 버튼 이벤트 함수
     */
    @objc func messagebtnpressed(sender : UIButton){
        guard MFMessageComposeViewController.canSendText() else {
            print("SMS services are not available")
            return
        }
        var content : String = ""
        var destPos : String = ""
        var spendtime : String = ""
        var carNum : String = "차량 정보를 조회 할 수 없습니다. 기사님에게 물어봐 주세요"
        if let car = CallRepo.shared.getmodel().carNum {
            carNum = car
        }
        
        
        //목적지 존재시 목적지 마커 세팅 함수
        if let _ = presenter.getdestcoord().lat , let _ = presenter.getdestcoord().lon {
            
            if let pos = presenter.getdestname() {
                destPos = pos
            }
            guard let time = InaviRouteRepo.shared.getonboardingModel().spend_time else {
                spendtime = "0분"
                return
            }
            spendtime = String(Int(time / 60)) + "분"
            
            
        }
        let composeViewController = MFMessageComposeViewController()
        composeViewController.messageComposeDelegate = self
        
        presenter.callreversegeocodeapi(posx: presenter.getcarpos().lon, posy: presenter.getcarpos().lat) { [weak self] (addr,done) in
            content = "\(carNum)을 타고 \(addr)을 지나고 있습니다."
            if destPos != "" {
                if destPos == addr {
                    content = "\(addr)에 도착하셨습니다."
                }
                else {
                    content += "\(destPos)까지 \(spendtime)남았습니다"
                }
                
            }
            composeViewController.body = content
            self?.present(composeViewController, animated: true, completion: nil)
        }
    }
    
    
}
extension OnBoardingViewController: MFMessageComposeViewControllerDelegate {
    func messageComposeViewController(_ controller: MFMessageComposeViewController, didFinishWith result: MessageComposeResult) {
        switch result {
        case .cancelled:
            print("cancelled")
            dismiss(animated: true, completion: nil)
        case .sent:
            print("sent message:", controller.body ?? "")
            dismiss(animated: true, completion: nil)
        case .failed:
            print("failed")
            dismiss(animated: true, completion: nil)
        @unknown default:
            print("unkown Error")
            dismiss(animated: true, completion: nil)
        }
    }
}
extension OnBoardingViewController : INVMapViewDelegate {
}
extension OnBoardingViewController : OnBoardingContract {
    /**
     이동중에 마커의 위치를 갱신 시키는 함수
     */
    func setmarker(posx: Double, posy: Double) {
        DispatchQueue.main.async { [weak self] in
            print("posy \(posy) posx \(posx)")
            self?.mymarker.iconImage = self!.myImage
            self?.mymarker.position = INVLatLng(lat: posy, lng: posx)
            self?.mymarker.mapView = self?.mapView
            
            
//            //목적지가 있을 경우 폴리라인 그리는 작업
//            if let lat = self?.presenter.getdestcoord().lat , let lon = self?.presenter.getdestcoord().lon {
//                 print("lat \(lat) lon \(lon) dest")
//
//
//                self?.polyline.coords = [INVLatLng(lat: lat, lng: lon),INVLatLng(lat: posy, lng: posx)]
//                self?.polyline.width = 2
//                self?.polyline.capType = .round
//                self?.polyline.color = .rgb(red: 226, green: 83, blue: 33, alpha: 1)
//                self?.polyline.mapView = self?.mapView
//
//
//            }
        }
    }
    
    /**
     경로를 그리는 함수
     */
    func makerout(){
        route = INVRoute(links: self.presenter.setrouteOnMap())
        route.mapView = self.mapView
        
         
    }
    
    
    
    
    /**
     실시간 지오코딩으로 현재 지나고 있는 지역을 셋하는 함수
     */
    func setaddress(address: String) {
        let str = "\(address) 을\n지나고 있습니다."
        let attributedString = NSMutableAttributedString(string: str, attributes: [
            .font: UIFont(name: fonts.medium.rawValue, size: 24)!,
            .foregroundColor: UIColor(white: 70.0 / 255.0, alpha: 1.0)
        ])
        let range = NSString(string: str).range(of: address)
        attributedString.addAttribute(.font, value: UIFont(name: fonts.bold.rawValue, size: 24)!, range: range)
        infolabel.attributedText = attributedString
    }
    
    /**
     이동 완료시 승차
     */
    func showTravelDoneViewController(){
        let view = TravelDoneViewController()
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    
}
extension OnBoardingViewController {
    private func makebox(){
        self.view.addSubview(box)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 16).isActive = true
        box.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        box.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        box.backgroundColor = .white
        
        
        box.shadowcolor = .black
        box.shadowoffset = CGSize(width: 0, height: 5)
        box.shadowradius = 10
        box.shadowopacity = 0.1
        
        
    }
    private func makeinfolabel(){
        self.view.addSubview(infolabel)
        infolabel.translatesAutoresizingMaskIntoConstraints = false
        infolabel.topAnchor.constraint(equalTo: box.topAnchor, constant: 14).isActive = true
        infolabel.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 10).isActive = true
        infolabel.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: -10).isActive = true
        infolabel.heightAnchor.constraint(lessThanOrEqualToConstant: 100).isActive = true
        
        infolabel.textAlignment = .center
        infolabel.lineBreakMode = .byWordWrapping
        infolabel.numberOfLines = 0
        
        
        
        
        box.bottomAnchor.constraint(equalTo: infolabel.bottomAnchor, constant: 15).isActive = true
        
        
    }
    private func makemessagebtn(){
        self.view.addSubview(messagebtn)
        messagebtn.translatesAutoresizingMaskIntoConstraints = false
        messagebtn.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -13).isActive = true
        messagebtn.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        messagebtn.widthAnchor.constraint(equalToConstant: 180).isActive = true
        messagebtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
    }
    private func makecurrentlocationbtn(){
        self.view.addSubview(currentlocationbtn)
        currentlocationbtn.translatesAutoresizingMaskIntoConstraints = false
        currentlocationbtn.bottomAnchor.constraint(equalTo: messagebtn.topAnchor, constant: 0).isActive = true
        currentlocationbtn.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        currentlocationbtn.widthAnchor.constraint(equalToConstant: 54).isActive = true
        currentlocationbtn.heightAnchor.constraint(equalToConstant: 54).isActive = true
        currentlocationbtn.setImage(UIImage(named: "mapBtnMy"), for: .normal)
        
        
    }
    
    
}
