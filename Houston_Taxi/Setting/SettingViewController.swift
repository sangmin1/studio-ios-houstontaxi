//
//  SettingViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 설정 페이지
 */
class SettingViewController: UIViewController {
    
    private var topbox = UIView()
    
    private var titlelabel = PaddingLabel()
    
    private var xbtn = WideTapBtn()
     //자주 가는 곳
    private var section1 = PaddingLabel()
    //집
    private var house = SettingFirstSectionBtn()
    //회사
    private var office = SettingFirstSectionBtn()
    // 환경 설정
    private var section2 = PaddingLabel()
    //콜센터
    private var callcenter = SettingSecondSectionBtn()
    //앱 버전
    private var appversion = SettingSecondSectionBtn()
    //이용 약관
    private var terms = SettingSecondSectionBtn()
    
    
    lazy private var removeenlistpopup = RemoveEnlistedPlacePopUp(frame: .zero, delegate: self)
    
    lazy private var presenter = SettingPresenter(delegate : self, viewcontroller : self)
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        maketopbox()
        maketitlelabel()
        makexbtn()
        makesection1()
        makehouse()
        makeoffice()
        makesection2()
        makecallcenter()
        makeappversion()
        maketerms()
        makeremoveenlistpopup()
        
        
        
        house.addTarget(self, action: #selector(housepressed(sender:)), for: .touchUpInside)
        office.addTarget(self, action: #selector(officepressed(sender:)), for: .touchUpInside)
        xbtn.addTarget(self, action: #selector(xbtnpressed(sender:)), for: .touchUpInside)
        callcenter.addTarget(self, action: #selector(callcenterbtnpressed(sender:)), for: .touchUpInside)
        terms.addTarget(self, action: #selector(termsbtnpressed(sender:)), for: .touchUpInside)
        presenter.setcontent()
        presenter.callGetVersionAPI()
        
        
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        presenter.setcontent()
        
    }
    /**
     집 버튼 이벤트 함수
     */
    @objc func housepressed(sender : UIButton){
        //isSelected 면 집이등록 되어 있으므로 집 등록 삭제 팝업을 띄워 줘야됨
        if sender.isSelected == true {
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.removeenlistpopup.alpha = 1
                self.removeenlistpopup.settype(type: .house)
            }
        }
        else {
            let view = EnlistViewController(enlistmode : .house)
            self.navigationController?.pushViewController(view, animated: true)
        }
        
    }
    /**
     회사 버튼 이벤트 함수
     */
    @objc func officepressed(sender : UIButton){
        if sender.isSelected == true {
            UIView.animate(withDuration: 0.2) { [unowned self] in
                self.removeenlistpopup.alpha = 1
                self.removeenlistpopup.settype(type: .office)
            }
        }
        else {
            let view = EnlistViewController(enlistmode : .office)
            self.navigationController?.pushViewController(view, animated: true)
        }
        
    }
    /**
     xbtn 이벤트 함수
     */
    @objc func xbtnpressed(sender : UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    /**
     콜센터 버튼 이벤트 함수
     */
    @objc func callcenterbtnpressed(sender : UIButton){
        if CallCenterRepo.shared.selected.elementsEqual(Array(repeating: false, count: CallCenterRepo.shared.selected.count)) {
            let view = CallCenterViewController()
            self.navigationController?.pushViewController(view, animated: true)
        }
        else {
            let num = CallCenterRepo.shared.number[0]
            guard let number = URL(string: "tel://" + num) else { return }
            UIApplication.shared.open(number)
        }
    }
    /**
     이용 약관 버튼 이벤트함수
     */
    @objc func termsbtnpressed(sender : UIButton){
        let view = TermsViewController()
        self.navigationController?.pushViewController(view, animated: true)
    }

    
    
}
extension SettingViewController : RemoveEnlistedPlacePopUpDelegate {
    func removebtnpressed(type: enlistmode) {
        presenter.removeHouseOfficeModel(type : type)
        presenter.setcontent()
        ToastView.shared.short(self.view, txt_msg: "삭제 되었습니다.")
        
        
    }
    
    
    
}
extension SettingViewController : SettingContract {
    /**
     집 버튼 내용을 셋하는 함수
     
    - parameter :
    - isSelected : Bool 참이면 -> 집등록 존재 , 거짓이면 -> 집등록 x
    - address : 주소
     */
    func sethousebtn(isSelected : Bool, address : String?){
        house.isSelected = isSelected
        house.showdeletebtn(show: isSelected)
        house.setaddresslabel(address: address)
        
        
    }
    /**
     회사 버튼 내용을 셋하는 함수
     
    - parameter :
    - isSelected : Bool 참이면 -> 집등록 존재 , 거짓이면 -> 집등록 x
    - address : 주소
     */
    func setofficebtn(isSelected : Bool, address : String?){
        office.isSelected = isSelected
        office.showdeletebtn(show: isSelected)
        office.setaddresslabel(address: address)
    }
    /**
     버젼 셋하는 함수
     
     */
    func setversion(version : String){
        self.appversion.setinfolabel(info: version)
    }
    /**
     콜센터 내용을 셋하는 함수
     */
    func setCallcenter(content : String){
        callcenter.setinfolabel(info: content)
    }
}
extension SettingViewController {
    private func maketopbox(){
        self.view.addSubview(topbox)
        topbox.translatesAutoresizingMaskIntoConstraints = false
        topbox.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 0).isActive = true
        topbox.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        topbox.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        topbox.heightAnchor.constraint(equalToConstant: 56).isActive = true
        topbox.backgroundColor = .clear
    }
    private func maketitlelabel(){
        self.view.addSubview(titlelabel)
        titlelabel.translatesAutoresizingMaskIntoConstraints = false
        titlelabel.centerYAnchor.constraint(equalTo: topbox.centerYAnchor, constant: 0).isActive = true
        titlelabel.centerXAnchor.constraint(equalTo: self.view.centerXAnchor, constant: 0).isActive = true
        titlelabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        titlelabel.heightAnchor.constraint(equalToConstant: 23).isActive = true
        titlelabel.textColor = .rgb(red: 75, green: 75, blue: 75, alpha: 1)
        titlelabel.font = UIFont(name: fonts.bold.rawValue, size: 18) //.systemFont(ofSize: 18, weight: .bold)
        titlelabel.textAlignment = .center
        titlelabel.text = "설정"
    }
    private func makexbtn(){
        self.view.addSubview(xbtn)
        xbtn.translatesAutoresizingMaskIntoConstraints = false
        xbtn.centerYAnchor.constraint(equalTo: titlelabel.centerYAnchor, constant: 0).isActive = true
        xbtn.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        xbtn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        xbtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        xbtn.setImage(UIImage(named: "titleBtnCloseN"), for: .normal)
    }
    private func makesection1(){
        self.view.addSubview(section1)
        section1.translatesAutoresizingMaskIntoConstraints = false
        section1.topAnchor.constraint(equalTo: topbox.bottomAnchor, constant: 0).isActive = true
        section1.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        section1.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        section1.heightAnchor.constraint(equalToConstant: 30).isActive = true
        section1.backgroundColor = .rgb(red: 243, green: 243, blue: 243, alpha: 1)
        section1.leftInset = 28
        section1.textColor = .rgb(red: 75, green: 75, blue: 75, alpha: 1)
        section1.font = UIFont(name: fonts.bold.rawValue, size: 16) //.systemFont(ofSize: 16, weight: .bold)
        section1.text = "자주 가는 곳"
    }
    private func makehouse(){
        self.view.addSubview(house)
        house.translatesAutoresizingMaskIntoConstraints = false
        house.topAnchor.constraint(equalTo: section1.bottomAnchor, constant: 0).isActive = true
        house.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        house.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        house.heightAnchor.constraint(equalToConstant: 84).isActive = true
        house.settitle(title: "집")
        house.setaddresslabel(address: nil)
        house.setimage(image: UIImage(named: "dtListBtnAddHomeN")!)
    }
    private func makeoffice(){
        self.view.addSubview(office)
        office.translatesAutoresizingMaskIntoConstraints = false
        office.topAnchor.constraint(equalTo: house.bottomAnchor, constant: 0).isActive = true
        office.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        office.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        office.heightAnchor.constraint(equalToConstant: 84).isActive = true
        office.settitle(title: "회사")
        office.setaddresslabel(address: "")
        office.setimage(image: UIImage(named: "dtListBtnAddOfficeN")!)
    }
    private func makesection2(){
        self.view.addSubview(section2)
        section2.translatesAutoresizingMaskIntoConstraints = false
        section2.topAnchor.constraint(equalTo: office.bottomAnchor, constant: 0).isActive = true
        section2.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        section2.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        section2.heightAnchor.constraint(equalToConstant: 30).isActive = true
        section2.backgroundColor = .rgb(red: 243, green: 243, blue: 243, alpha: 1)
        section2.leftInset = 28
        section2.textColor = .rgb(red: 75, green: 75, blue: 75, alpha: 1)
        section2.font = UIFont(name: fonts.bold.rawValue, size: 16) //.systemFont(ofSize: 16, weight: .bold)
        section2.text = "환경설정"
    }
    private func makecallcenter(){
        self.view.addSubview(callcenter)
        callcenter.translatesAutoresizingMaskIntoConstraints = false
        callcenter.topAnchor.constraint(equalTo: section2.bottomAnchor, constant: 0).isActive = true
        callcenter.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        callcenter.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        callcenter.heightAnchor.constraint(equalToConstant: 84).isActive = true
        callcenter.settitle(title: "콜센터")
        callcenter.setinfolabel(info: "모두 이용")

    }
    private func makeappversion(){
        self.view.addSubview(appversion)
        appversion.translatesAutoresizingMaskIntoConstraints = false
        appversion.topAnchor.constraint(equalTo: callcenter.bottomAnchor, constant: 0).isActive = true
        appversion.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        appversion.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        appversion.heightAnchor.constraint(equalToConstant: 84).isActive = true
        appversion.settitle(title: "앱 버전")
        appversion.setinfolabel(info: "v19.04")
    }
    private func maketerms(){
        self.view.addSubview(terms)
        terms.translatesAutoresizingMaskIntoConstraints = false
        terms.topAnchor.constraint(equalTo: appversion.bottomAnchor, constant: 0).isActive = true
        terms.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        terms.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        terms.heightAnchor.constraint(equalToConstant: 84).isActive = true
        terms.settitle(title: "이용약관")
        terms.setinfolabel(info: nil)
    }
    
    
    private func makeremoveenlistpopup(){
        self.view.addSubview(removeenlistpopup)
        removeenlistpopup.frame = self.view.frame
        removeenlistpopup.alpha = 0
    }
    
}
