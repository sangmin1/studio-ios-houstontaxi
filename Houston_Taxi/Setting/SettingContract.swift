//
//  SettingContract.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation


protocol SettingContract : NSObjectProtocol {
    func sethousebtn(isSelected : Bool, address : String?)
    func setofficebtn(isSelected : Bool, address : String?)
    func setversion(version : String)
    func setCallcenter(content : String)
}

protocol RemoveEnlistedPlacePopUpDelegate : NSObjectProtocol {
    func removebtnpressed(type : enlistmode)
}
