//
//  SettingPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class SettingPresenter: NSObject {
    
    weak var delegate : SettingContract?
    weak var viewcontroller : SettingViewController?
    
    
    init(delegate : SettingContract, viewcontroller : SettingViewController){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
    }
    
    
    /**
     화면의 내용을 셋하는 함수
     */
    func setcontent(){
        
        let housemodel = HouseOfficeRepo.shared.gethouse()
        let officemodel = HouseOfficeRepo.shared.getoffice()
        
        // 회사가 등록 되어있는지 여부 판단
        if let model = officemodel {
            print("office model",model.address)
            self.delegate?.setofficebtn(isSelected: true, address: model.address)
        }
        else {
            self.delegate?.setofficebtn(isSelected: false, address: nil)
        }
        
        // 집이 등록 되어있는지 여부 판단
        if let model = housemodel {
            print("house model",model.address)
            self.delegate?.sethousebtn(isSelected: true, address: model.address)
        }
        else {
            self.delegate?.sethousebtn(isSelected: false, address: nil)
        }
        
        
        //콜센터 등록 여부 판단
        var selectcount = 0
        for selected in CallCenterRepo.shared.selected {
            if selected == true {
                selectcount += 1
            }
        }
        if selectcount == CallCenterRepo.shared.number.count {
            self.delegate?.setCallcenter(content: "모두이용")
        }
        else if selectcount == 0 {
            self.delegate?.setCallcenter(content: "등록필요")
        }
        else {
            self.delegate?.setCallcenter(content: "일부이용")
        }
        
        
    }
    
    /**
     집이나 회사 등록된 곳을 삭제하는 함수
     */
    func removeHouseOfficeModel(type : enlistmode){
        if type == .house {
            HouseOfficeRepo.shared.deletemodel(type: "house")
        }
        else {
            HouseOfficeRepo.shared.deletemodel(type: "office")
        }
    }
    /**
     버젼 확인 api 호출 함수
     */
    func callGetVersionAPI(){
        let api = GetVersionAPI()
        let currentDt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentDt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        api.requestToserver(currentDT: currentDt, authcode: authcode, mobile: mobile) { (done, data) in
            if done{
                print("version" ,data)
                let version = data["version"].stringValue
                self.delegate?.setversion(version : FunctionClass.shared.decrypt(msg: version))
            }
        }
    }
    
    
    
    
}
