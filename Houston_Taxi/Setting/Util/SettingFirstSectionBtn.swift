//
//  SettingFirstSectionBtn.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 자주 가는 곳 버튼 컴포넌트
 */
class SettingFirstSectionBtn: UIButton {
    //이름
    private var titlelabel = PaddingLabel()
    //상세주소
    private var addresslabel = PaddingLabel()
    //택시 아이콘
    private var icon = UIImageView()
    //삭제 버튼
    private var deletebtn = UIImageView()
    
    
    
    lazy private var labelstack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [titlelabel,addresslabel])
        stack.axis = .vertical
        stack.spacing = 4
        return stack
    }()
    
    
    private var underline = UIImageView()
    
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        makelabestack()
        makeicon()
        makedeletebtn()
        makeunderline()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /**
     검색 결과  지명을 셋하는 함수
     
    - paramter :
    - title : 지명
     */
    func settitle(title : String){
        self.titlelabel.text = title
    }
     
    /**
      검색 결과 주소를 셋하는 함수
     
     - paramter :
     - address : 주소(nil 처리시 isHidden = true 처리)
     */
    func setaddresslabel(address : String?){
        if let address = address {
            self.addresslabel.text = address
            self.addresslabel.isHidden = false
        }
        else {
            self.addresslabel.isHidden = true
        }
        
    }
    /**
      이미지 셋하는 함수
     
     - parameter :
     - image : 이미지
     */
    func setimage(image : UIImage){
        self.icon.image = image
    }
    
    /**
     등록되어 있을경우 쓰레기토이미지를 보여주고 없으면 등록 이미지를 보여줌
     */
    func showdeletebtn(show : Bool){
        if show == true {
            icon.isHidden = true
            deletebtn.isHidden = false
            
        }
        else {
            icon.isHidden = false
            deletebtn.isHidden = true
        }
        
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.bounds.contains(point) {
            return self
        }
        else {
            return nil
        }
    }
    
    
    
}
extension SettingFirstSectionBtn {
    private func makelabestack(){
        self.addSubview(labelstack)
        labelstack.translatesAutoresizingMaskIntoConstraints = false
        labelstack.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        labelstack.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 24).isActive = true
        labelstack.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 80).isActive = true
        labelstack.heightAnchor.constraint(lessThanOrEqualToConstant: 47).isActive = true
        
        NSLayoutConstraint(item: titlelabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 26).isActive = true
        titlelabel.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        titlelabel.font = UIFont(name: fonts.medium.rawValue, size: 20)//.systemFont(ofSize: 20, weight: .medium)
        titlelabel.text = "바비레드 강남본점"
        
        NSLayoutConstraint(item: addresslabel, attribute: .height, relatedBy: .equal, toItem: nil, attribute: .height, multiplier: 1, constant: 21).isActive = true
        
        addresslabel.textColor = .rgb(red: 184, green: 184, blue: 184, alpha: 1)
        addresslabel.font = UIFont(name: fonts.medium.rawValue, size: 16)//.systemFont(ofSize: 16, weight: .medium)
        addresslabel.text = "서울 강남구 봉은사로6길 39"
        //addresslabel.isHidden = true
    }
    private func makeicon(){
        self.addSubview(icon)
        icon.translatesAutoresizingMaskIntoConstraints = false
        icon.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        icon.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -22).isActive = true
        icon.widthAnchor.constraint(equalToConstant: 30).isActive = true
        icon.heightAnchor.constraint(equalToConstant: 30).isActive = true
        icon.image = UIImage(named: "6")
    }
    
    private func makedeletebtn(){
        self.addSubview(deletebtn)
        deletebtn.translatesAutoresizingMaskIntoConstraints = false
        deletebtn.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        deletebtn.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -22).isActive = true
        deletebtn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        deletebtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        deletebtn.image = UIImage(named: "trashcant")
        deletebtn.isHidden = true
    }
    private func makeunderline(){
        self.addSubview(underline)
        underline.translatesAutoresizingMaskIntoConstraints = false
        underline.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        underline.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        underline.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        underline.heightAnchor.constraint(equalToConstant: 1).isActive = true
        underline.backgroundColor = .rgb(red: 0, green: 0, blue: 0, alpha: 0.05)
    }
}
