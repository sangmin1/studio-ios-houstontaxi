//
//  SettingSecondSectionBtn.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/07/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

/**
 환경 설정 버튼 컴포너트
 */
class SettingSecondSectionBtn: UIButton {
    
    private var title = PaddingLabel()
    
    private var infolabel = PaddingLabel()
    
    private var underline = UIImageView()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        maketitle()
        makeinfolabel()
        makeunderline()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    /**
     컴포넌트 제목 세팅 함수
     
    - paramter :
    - title : 제목
     */
    func settitle(title : String){
        self.title.text = title
    }
    /**
     정보 라벨 세팅 함수
     
    - paramter :
    - info : 정보
     */
    func setinfolabel(info : String?){
        if let info = info {
            self.infolabel.text = info
        }
        else {
            self.infolabel.isHidden = true
        }
        
    }
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if self.bounds.contains(point) {
            return self
        }
        else {
            return nil
        }
    }
    
    
    
}
extension SettingSecondSectionBtn {
    private func maketitle(){
        self.addSubview(title)
        title.translatesAutoresizingMaskIntoConstraints = false
        title.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        title.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 24).isActive = true
        title.widthAnchor.constraint(equalToConstant: 150).isActive = true
        title.heightAnchor.constraint(equalToConstant: 26).isActive = true
        title.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        title.font = UIFont(name: fonts.medium.rawValue, size: 20)//.systemFont(ofSize: 20, weight: .medium)
        title.text = "test"
    }
    private func makeinfolabel(){
        self.addSubview(infolabel)
        infolabel.translatesAutoresizingMaskIntoConstraints = false
        infolabel.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        infolabel.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -22).isActive = true
        infolabel.widthAnchor.constraint(equalToConstant: 100).isActive = true
        infolabel.heightAnchor.constraint(equalToConstant: 21).isActive = true
        infolabel.textColor = .rgb(red: 184, green: 184, blue: 184, alpha: 1)
        infolabel.font = UIFont(name: fonts.medium.rawValue, size: 16) //.systemFont(ofSize: 16, weight: .medium)
        infolabel.textAlignment = .right
    }
    private func makeunderline(){
        self.addSubview(underline)
        underline.translatesAutoresizingMaskIntoConstraints = false
        underline.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        underline.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 12).isActive = true
        underline.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -12).isActive = true
        underline.heightAnchor.constraint(equalToConstant: 1).isActive = true
        underline.backgroundColor = .rgb(red: 0, green: 0, blue: 0, alpha: 0.05)
    }
}
