//
//  RemoveEnlistedPlacePopUp.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/14.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class RemoveEnlistedPlacePopUp: UIView {
    
    private var box = ShadowView()
    
    private var contentlabel = PaddingLabel()
    
    
    //현재 팝업 타입 ( 집 , 회사)
    private var type : enlistmode = .house
    
    //취소 버튼
    private var cancelbtn = UIButton()
    //지금 등록 버튼
    private var confirmbtn = UIButton()
    
    
    weak var delegate : RemoveEnlistedPlacePopUpDelegate?
    
    
    init(frame: CGRect,delegate : RemoveEnlistedPlacePopUpDelegate) {
        super.init(frame: frame)
        self.delegate = delegate
        self.backgroundColor = UIColor.init(white: 0.2, alpha: 0.6)
        makebox()
        makelabel()
        makecanclebtn()
        makeconfirmbtn()
        
        cancelbtn.addTarget(self, action: #selector(canclebtnpressed(sender:)), for: .touchUpInside)
        confirmbtn.addTarget(self, action: #selector(confirmbtnpressed(sender:)), for: .touchUpInside)
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        DispatchQueue.main.async { [weak self] in
            self?.box.layer.cornerRadius = 8
            self?.cancelbtn.layer.addBorder(edge: [.top], color: .rgb(red: 0, green: 0, blue: 0, alpha: 0.05), thickness: 1)
            self?.confirmbtn.layer.addBorder(edge: [.top], color: .rgb(red: 0, green: 0, blue: 0, alpha: 0.05), thickness: 1)
            self?.confirmbtn.layer.addBorder(edge: .left, color: .rgb(red: 0, green: 0, blue: 0, alpha: 0.05), thickness: 1)
        }
    }
    
    /**
     팝업 표출시 팝업 타입을 지정하는 함수
     */
    func settype(type : enlistmode){
        self.type = type
        if type == .house {
            contentlabel.text = "집으로 등록된 곳을 삭제할까요?"
        }
        else {
            contentlabel.text = "회사로 등록된 곳을 삭제할까요?"
        }
    }
    /**
     취소 버튼 이벤트 함수
     */
    @objc func canclebtnpressed(sender : UIButton){
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.alpha = 0
        }
        
    }
    
    /**
     지금 등록 버튼 이벤트 함수
     */
    @objc func confirmbtnpressed(sender : UIButton){
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.alpha = 0
        }
        self.delegate?.removebtnpressed(type: self.type)
    }
    
    
    
    
}
extension RemoveEnlistedPlacePopUp {
    private func makebox(){
        self.addSubview(box)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        box.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        box.widthAnchor.constraint(equalToConstant: 315).isActive = true
        box.heightAnchor.constraint(equalToConstant: 140).isActive = true
        box.backgroundColor = .white
        
        box.shadowcolor = UIColor.black
        box.shadowoffset = CGSize(width: 0, height: 3)
        box.shadowradius = 6
        box.shadowopacity = 0.1
    }
    private func makelabel(){
        self.addSubview(contentlabel)
        contentlabel.translatesAutoresizingMaskIntoConstraints = false
        contentlabel.topAnchor.constraint(equalTo: box.topAnchor, constant: 32).isActive = true
        contentlabel.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 0).isActive = true
        contentlabel.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: 0).isActive = true
        contentlabel.heightAnchor.constraint(lessThanOrEqualToConstant: 80).isActive = true
        contentlabel.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        contentlabel.font = UIFont(name: fonts.medium.rawValue, size: 20)
        
        contentlabel.textAlignment = .center
        contentlabel.text = "집으로 등록된 곳을 삭제할까요?"
    }
    private func makecanclebtn(){
        self.addSubview(cancelbtn)
        cancelbtn.translatesAutoresizingMaskIntoConstraints = false
        cancelbtn.bottomAnchor.constraint(equalTo: box.bottomAnchor, constant: 0).isActive = true
        cancelbtn.trailingAnchor.constraint(equalTo: box.centerXAnchor, constant: 0).isActive = true
        cancelbtn.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 0).isActive = true
        cancelbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        cancelbtn.setTitle("취소", for: .normal)
        cancelbtn.setTitleColor(.rgb(red: 100, green: 100, blue: 100, alpha: 1), for: .normal)
        cancelbtn.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)
       
        
    }
    private func makeconfirmbtn(){
        self.addSubview(confirmbtn)
        confirmbtn.translatesAutoresizingMaskIntoConstraints = false
        confirmbtn.topAnchor.constraint(equalTo: cancelbtn.topAnchor, constant: 0).isActive = true
        confirmbtn.leadingAnchor.constraint(equalTo: box.centerXAnchor, constant: 0).isActive = true
        confirmbtn.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: 0).isActive = true
        confirmbtn.heightAnchor.constraint(equalToConstant: 50).isActive = true
        confirmbtn.setTitle("삭제", for: .normal)
        confirmbtn.setTitleColor(maincolor, for: .normal)
        confirmbtn.titleLabel?.font = UIFont.systemFont(ofSize: 20, weight: .bold)
    
        
    }
    
    
    
    
    
    
}
