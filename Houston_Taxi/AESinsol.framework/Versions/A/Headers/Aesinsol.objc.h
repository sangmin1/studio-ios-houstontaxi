#ifndef __Aesinsol_H__
#define __Aesinsol_H__

@import Foundation;
#include "ref.h"
#include "Universe.objc.h"

FOUNDATION_EXPORT NSString* _Nonnull AesinsolDecrypt(NSString* _Nullable skey, NSString* _Nullable securemess);

FOUNDATION_EXPORT NSString* _Nonnull AesinsolEncrypt(NSString* _Nullable skey, NSString* _Nullable message);

#endif
