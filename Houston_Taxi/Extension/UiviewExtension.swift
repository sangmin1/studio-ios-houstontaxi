//
//  UiviewExtension.swift
//  CarpumUser
//
//  Created by 한상민 on 14/07/2019.
//  Copyright © 2019 shot. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher


extension UILabel {
    func addCharacterSpacing(kernValue: Double) {
        if let labelText = text, labelText.count > 0 {
            let attributedString = NSMutableAttributedString(string: labelText)
            attributedString.addAttribute(NSAttributedString.Key.kern, value: kernValue, range: NSRange(location: 0, length: attributedString.length - 1))
            self.text = ""
            self.attributedText = attributedString
            
        }
    }
    func setLineHeight(lineHeight: CGFloat,kerval : Double) {
        let text = self.text
        if let text = text {
            let attributeString = NSMutableAttributedString(string: text)
            let style = NSMutableParagraphStyle()
            
            if attributeString.length != 0 {
                 attributeString.addAttribute(NSAttributedString.Key.kern, value: kerval, range: NSRange(location: 0, length: attributeString.length - 1))
            }
           
            //style.lineSpacing = linespace
            style.minimumLineHeight = lineHeight
            style.maximumLineHeight = lineHeight
            attributeString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSMakeRange(0, text.count))
            self.attributedText = attributeString
        }
    }
    func setattributedlineheigh(lineHeight: CGFloat,kerval : Double) {
        let text = self.attributedText as? NSMutableAttributedString
        self.attributedText = nil
        if let text = text {
            let attributeString = text
            let style = NSMutableParagraphStyle()
            
            attributeString.addAttribute(NSAttributedString.Key.kern, value: kerval, range: NSRange(location: 0, length: attributeString.length - 1))
            //style.lineSpacing = linespace
            
            style.maximumLineHeight = lineHeight
            
            attributeString.addAttribute(NSAttributedString.Key.paragraphStyle, value: style, range: NSMakeRange(0, text.length))
            self.attributedText = attributeString
        }
    }
}



extension UIColor {
   static func rgb(red : CGFloat, green : CGFloat, blue : CGFloat, alpha : CGFloat ) -> UIColor {
       return UIColor(red: red / 255, green: green / 255, blue: blue / 255, alpha: alpha)
    }
}

extension UIViewController {
    func hideKeyboardWhenTappedAround() {
        let tap: UITapGestureRecognizer = UITapGestureRecognizer(target: self, action: #selector(UIViewController.dismissKeyboard))
        tap.cancelsTouchesInView = false
        view.addGestureRecognizer(tap)
    }
    
    @objc func dismissKeyboard() {
        view.endEditing(true)
    }
}

extension UIImageView {
    func imagewithinset(leftcap : CGFloat, image : UIImage) {
        let image = image
        let imagwidth = image.size.width
        let imageheight = image.size.height
        
        let leftcapinset = leftcap * imagwidth
        let rightcapinset = imagwidth - leftcapinset
        let topinset = 0.5 * imageheight
        let bottominset = 0.5 * imageheight
        
        let inset = UIEdgeInsets(top: topinset, left: leftcapinset, bottom: bottominset, right: rightcapinset)
        let imagewithinset = image.resizableImage(withCapInsets: inset, resizingMode: .stretch)
        
        
        self.image = imagewithinset
        
        
    }
    func imagewithverticalinset(topcap : CGFloat , image : UIImage){
        let image = image
        let imagwidth = image.size.width
        let imageheight = image.size.height
        
        let leftcapinset = 0.5 * imagwidth
        let rightcapinset = imagwidth - leftcapinset
        let topinset = topcap * imageheight
        let bottominset = imageheight - topinset
        
        let inset = UIEdgeInsets(top: topinset, left: leftcapinset, bottom: bottominset, right: rightcapinset)
        let imagewithinset = image.resizableImage(withCapInsets: inset, resizingMode: .stretch)
        
        
        self.image = imagewithinset
    }
    func setimage(input: String) {
        let url = URL(string: input)
        
        let processor = DownsamplingImageProcessor(size: CGSize(width: 350, height: 350))
        self.kf.indicatorType = .none
        self.kf.setImage(
            with: url,
            options: [
                .processor(processor),
                .scaleFactor(UIScreen.main.scale),
                .transition(.fade(1)),
                .cacheOriginalImage
            ])
        {
            result in
            switch result {
            case .success: break
                
            case .failure(let error):
                print("image Job failed: \(error.localizedDescription)")
            }
        }
    }
    func setImage(with urlString: String) {
        let cache = ImageCache.default
        cache.retrieveImage(forKey: urlString, options: nil) { (image, _) in // 캐시에서 키를 통해 이미지를 가져온다.
            if let image = image { // 만약 캐시에 이미지가 존재한다면
                self.image = image // 바로 이미지를 셋한다.
            } else {
                let url = URL(string: urlString) // 캐시가 없다면
                let resource = ImageResource(downloadURL: url!, cacheKey: urlString) // URL로부터 이미지를 다운받고 String 타입의 URL을 캐시키로 지정하고
                self.kf.setImage(with: resource) // 이미지를 셋한다.
            }
        }
    }
    
    
}
extension UIButton {
    func imagewithinset(leftcap : CGFloat, image : UIImage) {
        let image = image
        let imagwidth = image.size.width
        let imageheight = image.size.height
        
        let leftcapinset = leftcap * imagwidth
        let rightcapinset = imagwidth - leftcapinset
        let topinset = 0.5 * imageheight
        let bottominset = 0.5 * imageheight
        
        let inset = UIEdgeInsets(top: topinset, left: leftcapinset, bottom: bottominset, right: rightcapinset)
        let imagewithinset = image.resizableImage(withCapInsets: inset, resizingMode: .stretch)
        
        
        self.setImage( imagewithinset, for: .normal)
        
        
    }
    func setpulse(){
        
        let colorAnimation = CABasicAnimation(keyPath: "backgroundColor")
        colorAnimation.fromValue = UIColor.red.cgColor
        colorAnimation.duration = 1  // animation duration
        
        self.layer.add(colorAnimation, forKey: "ColorPulse")
        
    }
    
    
    func  addRippleEffectonBtn() {
           /*! Creates a circular path around the view*/
          let path = UIBezierPath(roundedRect:  CGRect(x: 0, y: 0, width: self.bounds.size.width * ( 4 / 5), height: self.bounds.size.height * ( 4 / 5 )), cornerRadius: 10)
           //let path = UIBezierPath(ovalIn: CGRect(x: 0, y: 0, width: self.bounds.size.width, height: self.bounds.size.height ))
           /*! Position where the shape layer should be */
           let shapePosition = CGPoint(x: self.bounds.size.width / 2.0, y: self.bounds.size.height / 2.0)
           let rippleShape = CAShapeLayer()
           rippleShape.bounds = CGRect(x: 0, y: 0, width: self.bounds.size.width * ( 4 / 5), height: self.bounds.size.height * ( 4 / 5))
           rippleShape.path = path.cgPath
           rippleShape.fillColor = UIColor.rgb(red: 220, green: 220, blue: 220, alpha: 1).cgColor
           rippleShape.strokeColor = UIColor.rgb(red: 220, green: 220, blue: 220, alpha: 1).cgColor
           rippleShape.lineWidth = 1
           rippleShape.position = shapePosition
           rippleShape.opacity = 0
           
           /*! Add the ripple layer as the sublayer of the reference view */
           self.layer.addSublayer(rippleShape)
           /*! Create scale animation of the ripples */
           let scaleAnim = CABasicAnimation(keyPath: "transform.scale")
           scaleAnim.fromValue = NSValue(caTransform3D: CATransform3DIdentity)
           scaleAnim.toValue = NSValue(caTransform3D: CATransform3DMakeScale(2, 2, 1))
           /*! Create animation for opacity of the ripples */
           let opacityAnim = CABasicAnimation(keyPath: "opacity")
           opacityAnim.fromValue = 1
           opacityAnim.toValue = nil
           /*! Group the opacity and scale animations */
           let animation = CAAnimationGroup()
           animation.animations = [scaleAnim, opacityAnim]
           animation.timingFunction = CAMediaTimingFunction(name: CAMediaTimingFunctionName.easeOut)
           animation.duration = CFTimeInterval(0.7)
           animation.repeatCount = 1
           animation.isRemovedOnCompletion = true
           rippleShape.add(animation, forKey: "rippleEffect")
       }
    
}





public let kShapeDashed : String = "kShapeDashed"


extension CALayer {

  func addBorder(edge: UIRectEdge, color: UIColor, thickness: CGFloat) {
    let border = CALayer()
    switch edge {
    case .top:
        border.frame = CGRect(x: 0, y: 0, width:  self.frame.width, height: thickness)
        border.name = "top"
        break
    case .bottom:
        border.frame = CGRect(x: 0, y: self.frame.height - thickness, width:  self.frame.width, height: thickness)
        border.name = "bottom"
        break
    case .left:
        border.frame =  CGRect(x: 0, y: 0 , width: thickness, height: self.frame.height)
        border.name = "left"
        break
    case .right:
        border.frame = CGRect(x: self.frame.width - thickness, y: 0 , width: thickness, height: self.frame.height)
        border.name = "right"
        break
    default:
        break
    }

    border.backgroundColor = color.cgColor

    addSublayer(border)
  }
   
}

extension CALayer {
    //.rgb(red: 34, green: 34, blue: 34, alpha: 1), alpha: 1, x: 0, y: 0, blur: 4, spread: 4
    func makeshadow(color : UIColor = .rgb(red: 71, green: 71, blue: 71, alpha: 1) , alpha : Float = 0.8, x : CGFloat = 0 , y : CGFloat = 2 , blur : CGFloat = 4 , spread : CGFloat = 4){
        
        self.masksToBounds = false
        self.shadowColor = UIColor(red: 0.84, green: 0.86, blue: 0.89, alpha: 1).cgColor
        self.shadowOpacity = alpha
        self.shadowOffset = CGSize(width: x, height: y )
        self.shadowRadius = blur / 2.0
        
        
    }
   
}


extension UIStackView {
    
    func removeAllArrangedSubviews() {
        
        let removedSubviews = arrangedSubviews.reduce([]) { (allSubviews, subview) -> [UIView] in
            self.removeArrangedSubview(subview)
            return allSubviews + [subview]
        }
        NSLayoutConstraint.deactivate(removedSubviews.flatMap({ $0.constraints }))
        removedSubviews.forEach({ $0.removeFromSuperview() })
    }
}
extension UIImage {
    func cropImageByAlpha() -> UIImage {
        let cgImage = self.cgImage
        let context = createARGBBitmapContextFromImage(inImage: cgImage!)
        let height = cgImage!.height
        let width = cgImage!.width
        
        var rect: CGRect = CGRect(x: 0, y: 0, width: CGFloat(width), height: CGFloat(height))
        context?.draw(cgImage!, in: rect)
        
        let pixelData = self.cgImage!.dataProvider!.data
        let data: UnsafePointer<UInt8> = CFDataGetBytePtr(pixelData)
        
        var minX = width
        var minY = height
        var maxX: Int = 0
        var maxY: Int = 0
        
        //Filter through data and look for non-transparent pixels.
        for y in 0..<height {
            for x in 0..<width {
                let pixelIndex = (width * y + x) * 4 /* 4 for A, R, G, B */
                
                if data[Int(pixelIndex)] != 0 { //Alpha value is not zero pixel is not transparent.
                    if (x < minX) {
                        minX = x
                    }
                    if (x > maxX) {
                        maxX = x
                    }
                    if (y < minY) {
                        minY = y
                    }
                    if (y > maxY) {
                        maxY = y
                    }
                }
            }
        }
        
        rect = CGRect( x: CGFloat(minX), y: CGFloat(minY), width: CGFloat(maxX-minX), height: CGFloat(maxY-minY))
        let imageScale:CGFloat = self.scale
        let cgiImage = self.cgImage?.cropping(to: rect)
        return UIImage(cgImage: cgiImage!, scale: imageScale, orientation: self.imageOrientation)
    }
    
    private func createARGBBitmapContextFromImage(inImage: CGImage) -> CGContext? {
        
        let width = cgImage!.width
        let height = cgImage!.height
        
        let bitmapBytesPerRow = width * 4
        let bitmapByteCount = bitmapBytesPerRow * height
        
        let colorSpace = CGColorSpaceCreateDeviceRGB()
        if colorSpace == nil {
            return nil
        }
        
        let bitmapData = malloc(bitmapByteCount)
        if bitmapData == nil {
            return nil
        }
        
        let context = CGContext (data: bitmapData, width: width, height: height, bitsPerComponent: 8, bytesPerRow: bitmapBytesPerRow, space: colorSpace, bitmapInfo: CGImageAlphaInfo.premultipliedFirst.rawValue)
        
        return context
    }
    
    func maskWithColor(color: UIColor) -> UIImage? {

        let maskImage = self.cgImage
       let width = self.size.width
       let height = self.size.height
        let bounds = CGRect(x: 0, y: 0, width: width, height: height)

       let colorSpace = CGColorSpaceCreateDeviceRGB()
        let bitmapInfo = CGBitmapInfo(rawValue: CGImageAlphaInfo.premultipliedLast.rawValue)
        let bitmapContext = CGContext(data: nil, width: Int(width), height: Int(height), bitsPerComponent: 8, bytesPerRow: 0, space: colorSpace, bitmapInfo: bitmapInfo.rawValue) //needs rawValue of bitmapInfo

        bitmapContext!.clip(to: bounds, mask: maskImage!)
        bitmapContext!.setFillColor(color.cgColor)
        bitmapContext!.fill(bounds)

       //is it nil?
        if let cImage = bitmapContext!.makeImage() {
            let coloredImage = UIImage(cgImage: cImage)

           return coloredImage

       } else {
           return nil
       }
    }
    
}


extension UIView {
    func createGradientLayer(color1 : UIColor , color2 : UIColor ,  start :CGPoint , end : CGPoint) {
        var gradientLayer: CAGradientLayer!
        gradientLayer = CAGradientLayer()
        gradientLayer.name = "gardient"
        gradientLayer.frame = self.bounds
     
        gradientLayer.colors = [color1.cgColor, color2.cgColor]
        gradientLayer.startPoint = start
        gradientLayer.endPoint = end
     
        self.layer.addSublayer(gradientLayer)
    }
    func createGradientLayer(colors : UIColor... ,  start :CGPoint, mid : CGPoint, end : CGPoint) {
        var gradientLayer: CAGradientLayer!
        gradientLayer = CAGradientLayer()
         self.layer.sublayers?.forEach({ (layer) in
            if layer.name == "gardient" {
                return
            }
        })
        gradientLayer.name = "gardient"
        gradientLayer.frame = self.bounds
     
        gradientLayer.colors = colors.map{$0.cgColor}
        gradientLayer.startPoint = start
        gradientLayer.anchorPoint = mid
        gradientLayer.endPoint = end
     
        self.layer.addSublayer(gradientLayer)
    }
    func deletegradientlayer() {
        self.layer.sublayers?.removeAll(where: { (view) -> Bool in
            if view.name == "gradient" {
                return true
            }
            else {
                return false
            }
               
           })
       }
   
    
}
extension Float {
    
    var clean : String {
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 1
        return formatter.string(from: number) ?? ""
    }
    var clean2 : String {
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 1
        return formatter.string(from: number) ?? ""
    }
    
//    var clean : String {
//        return String(self)
//    }

}
extension Double {
   var clean : String {
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumFractionDigits = 0
       
        formatter.maximumFractionDigits = 1
    
        //maximum digits in Double after dot (maximum precision)
        return formatter.string(from: number) ?? ""
    }
    var clean2 : String {
        let formatter = NumberFormatter()
        let number = NSNumber(value: self)
        formatter.minimumIntegerDigits = 1
        formatter.minimumFractionDigits = 0
        formatter.maximumFractionDigits = 1
        return formatter.string(from: number) ?? ""
    }

}
extension UIImage {
  static func imageWithLayer(layer: CALayer, cornerRadius: CGFloat = 4) -> UIImage {
       layer.cornerRadius = cornerRadius
       UIGraphicsBeginImageContextWithOptions(layer.bounds.size, false, 0)
       let imagwidth = layer.bounds.size.width
       let imageheight = layer.bounds.size.height
       
    let leftcapinset = 0.1 * imagwidth
       let rightcapinset = imagwidth - leftcapinset
       let topinset = 0.5 * imageheight
       let bottominset = 0.5 * imageheight
       
       //let inset = UIEdgeInsets(top: topinset, left: leftcapinset, bottom: bottominset, right: rightcapinset)
       let inset = UIEdgeInsets(top: topinset, left: 4, bottom: bottominset, right: 4)
          layer.render(in: UIGraphicsGetCurrentContext()!)
    let img = UIGraphicsGetImageFromCurrentImageContext()?.resizableImage(withCapInsets: inset, resizingMode: .stretch)
       UIGraphicsEndImageContext()
       return img!
   }
}


extension Array {
    func toJSONString(options: JSONSerialization.WritingOptions = .prettyPrinted) -> String {
        if let arr = self as? [[String:AnyObject]],
            let dat = try? JSONSerialization.data(withJSONObject: arr, options: options),
            let str = String(data: dat, encoding: String.Encoding.utf8) {
            return str
        }
        return "[]"
    }
}


extension UIStackView {
    func addBackground(color: UIColor) {
        let subView = UIView(frame: bounds)
        subView.backgroundColor = color
        subView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        insertSubview(subView, at: 0)
    }
}
extension UIApplication {
        func topViewController(base: UIViewController? = UIApplication.shared.keyWindow?.rootViewController) -> UIViewController? {
        if let nav = base as? CustomNavigationController {
            return topViewController(base: nav.visibleViewController)
        }
        if let tab = base as? UITabBarController {
            if let selected = tab.selectedViewController {
                return topViewController(base: selected)
            }
        }
        if let presented = base?.presentedViewController {
            return topViewController(base: presented)
        }
        return base
    }
}
extension String {
    func utf8DecodedString()-> String {
         let data = self.data(using: .utf8)
         if let message = String(data: data!, encoding: .nonLossyASCII){
                return message
          }
          return ""
    }
}

extension NSObject {
    var className: String {
        get {
            return NSStringFromClass(type(of: self))
        }
    }
}


extension String{
    func toDictionary() -> NSDictionary {
        let blankDict : NSDictionary = [:]
        if let data = self.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as! NSDictionary
            } catch {
                print(error.localizedDescription)
            }
        }
        return blankDict
    }
    func convertCurrency( money : NSNumber, style : NumberFormatter.Style ) -> String {
        
        let numberFormatter = NumberFormatter()
        numberFormatter.numberStyle = style
        
        return numberFormatter.string( from: money )!
    }
}


extension UIImageView {
    func makeblureffect(){//블러를 넣을 이미지뷰를먼저 만들고 그위에 원하는 이미지 덧 씌움
        var blurEffect : UIBlurEffect!
        var blurView : UIVisualEffectView!
        //let vibrancyEffect = UIVibrancyEffect(blurEffect: blurEffect)
        blurEffect = UIBlurEffect(style: .regular )
       
        blurView = UIVisualEffectView(effect: blurEffect)
       
        blurView.alpha = 0.9
        blurView.clipsToBounds = true
        blurView.autoresizingMask = [.flexibleWidth, .flexibleHeight]
        blurView.frame = CGRect(x: 0, y: self.frame.maxY - 171 , width: UIScreen.main.bounds.width, height: 171)
        blurView.roundCorners(corners: [.topLeft,.topRight], radius: 25)
       // blur.addSubview(blurView)
        self.insertSubview(blurView, at: 0)
        
    }

}
extension UIView {
    func tocircle(){
        self.layer.cornerRadius = self.frame.width / 2
        
    }
    func addShadow(shadowColor: UIColor, offSet: CGSize, opacity: Float, shadowRadius: CGFloat, cornerRadius: CGFloat, corners: UIRectCorner, fillColor: UIColor = .clear) {
        
        let shadowLayer = CAShapeLayer()
        let size = CGSize(width: cornerRadius, height: cornerRadius)
        let cgPath = UIBezierPath(roundedRect: self.bounds, byRoundingCorners: corners, cornerRadii: size).cgPath //1
        shadowLayer.path = cgPath //2
        shadowLayer.fillColor = fillColor.cgColor //3
        shadowLayer.shadowColor = shadowColor.cgColor //4
        shadowLayer.shadowPath = cgPath
        shadowLayer.shadowOffset = offSet //5
        shadowLayer.shadowOpacity = opacity
        shadowLayer.shadowRadius = shadowRadius
        self.layer.addSublayer(shadowLayer)
    }
    func roundCorners(corners: UIRectCorner, radius: CGFloat) {
        let path = UIBezierPath(roundedRect: bounds, byRoundingCorners: corners, cornerRadii: CGSize(width: radius, height: radius))
        let mask = CAShapeLayer()
        mask.path = path.cgPath
        layer.mask = mask
    }
    func fadeIn(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping ((Bool) -> Void) = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 1.0
            }, completion: completion)  }
    
    func fadeOut(_ duration: TimeInterval = 0.5, delay: TimeInterval = 0.0, completion: @escaping (Bool) -> Void = {(finished: Bool) -> Void in}) {
        UIView.animate(withDuration: duration, delay: delay, options: UIView.AnimationOptions.curveEaseIn, animations: {
            self.alpha = 0.0
        }, completion: completion)
    }
    
    
    // Using a function since `var image` might conflict with an existing variable
    // (like on `UIImageView`)
    func asImage() -> UIImage {
        let renderer = UIGraphicsImageRenderer(bounds: bounds)
        return renderer.image { rendererContext in
            layer.render(in: rendererContext.cgContext)
        }
    }
    
}
extension Date {
    
    func toString( dateFormat format: String ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone.autoupdatingCurrent
        dateFormatter.locale = Locale.current
        return dateFormatter.string(from: self)
    }
    
    func toStringKST( dateFormat format: String ) -> String {
        return self.toString(dateFormat: format)
    }
    
    func toStringUTC( dateFormat format: String ) -> String {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = format
        dateFormatter.timeZone = TimeZone(abbreviation: "UTC")
        return dateFormatter.string(from: self)
    }
}
extension UINavigationController {

  func popToViewController(ofClass: AnyClass, animated: Bool = true) {
    if let vc = viewControllers.filter({$0.isKind(of: ofClass)}).last {
      popToViewController(vc, animated: animated) 
    }
  }

  func popViewControllers(viewsToPop: Int, animated: Bool = true) {
    if viewControllers.count > viewsToPop {
      let vc = viewControllers[viewControllers.count - viewsToPop - 1]
      popToViewController(vc, animated: animated)
    }
  }

}
extension String {
    var htmlToAttributedString: NSAttributedString? {
        guard let data = data(using: .utf8) else { return nil }
        do {
            return try NSAttributedString(data: data, options: [.documentType: NSAttributedString.DocumentType.html, .characterEncoding:String.Encoding.utf8.rawValue], documentAttributes: nil)
        } catch {
            return nil
        }
    }
    var htmlToString: String {
        return htmlToAttributedString?.string ?? ""
    }
}
