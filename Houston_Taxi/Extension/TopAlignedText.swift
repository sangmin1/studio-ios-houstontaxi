//
//  TopAlignedText.swift
//  TeslaPlus
//
//  Created by sangmin han on 2020/06/12.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit



class VerticalTopAlignLabel: UILabel {

    var maxheight : CGFloat = 0
    var upInset: CGFloat = 0
    var downInset: CGFloat = 0
    var leftInset: CGFloat = 0
    var rightInset: CGFloat = 0
    override func drawText(in rect:CGRect) {
        guard let labelText = text else {  return super.drawText(in: rect) }

        let attributedText = NSAttributedString(string: labelText, attributes: [NSAttributedString.Key.font: font])
        var newRect = rect
        newRect.size.height = attributedText.boundingRect(with: rect.size, options: .usesLineFragmentOrigin, context: nil).size.height

        
        if numberOfLines != 0 {
            newRect.size.height = min(newRect.size.height, CGFloat(numberOfLines) * font.lineHeight)
        }
        let insets = UIEdgeInsets.init(top: upInset, left: leftInset, bottom: downInset, right: rightInset)
        newRect.size.height = min(maxheight,newRect.size.height)

        super.drawText(in: newRect.inset(by: insets))
    }

}
