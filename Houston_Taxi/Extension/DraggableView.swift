//
//  DraggableView.swift
//  MyWealthGuide
//
//  Created by 한상민 on 2019/11/21.
//  Copyright © 2019 shot. All rights reserved.
//

import Foundation
import UIKit

class DraggableView : UIView {
    
    weak var delegate : DraggableViewDelegate?
    
    var panGestureRecognizer : UIPanGestureRecognizer?
    var originalPosition : CGPoint?
    
    override init(frame: CGRect) {
            super.init(frame: frame)
            setUp()
        }
        
    required init?(coder aDecoder: NSCoder) {
           super.init(coder: aDecoder)
           setUp()
        }
    
    
    private var firstoriginx : CGFloat = 0
    func setUp(){
        isUserInteractionEnabled = true
        panGestureRecognizer = UIPanGestureRecognizer(target: self, action: #selector(panGestureAction(_:)))

        self.addGestureRecognizer(panGestureRecognizer!)
    }

    override func gestureRecognizerShouldBegin(_ gestureRecognizer: UIGestureRecognizer) -> Bool {
        let velocity = panGestureRecognizer?.velocity(in: self)
        return abs(velocity!.x) > abs(velocity!.y)
    }
    //+ translation.y
    @objc func panGestureAction(_ panGesture: UIPanGestureRecognizer) {
        let translation = panGesture.translation(in: superview)
        let velocityInView = panGesture.velocity(in: superview)

        switch panGesture.state {
        case .began:
            originalPosition = self.center
            firstoriginx = self.frame.origin.x
            delegate?.panGestureDidBegin?(panGesture, originalCenter: originalPosition!)
            break
        case .changed:
            print("translationx",translation.x)
            if translation.x > firstoriginx {
                return
            }
            
            delegate?.panGestureDidChange?(panGesture, originalCenter: originalPosition!, translation: translation, velocityInView: velocityInView)
            self.frame.origin = CGPoint(
                x: originalPosition!.x - self.bounds.midX + translation.x,
                y: originalPosition!.y  - self.bounds.midY
            )

            
            
            

            break
        case .ended:
            
            if translation.x > -100 {
                delegate?.backTofullscreen()
                return
            }
            else {
                delegate?.panGestureDidEnd?(panGesture, originalCenter: originalPosition!, translation: translation, velocityInView: velocityInView)
            }
            break
        default:

            delegate?.panGestureStateToOriginal?(panGesture, originalCenter: originalPosition!, translation: translation, velocityInView: velocityInView)

            break
        }
    }
   
    
}


@objc protocol DraggableViewDelegate: class {
    @objc optional func panGestureDidBegin(_ panGesture:UIPanGestureRecognizer, originalCenter:CGPoint)
    @objc optional func panGestureDidChange(_ panGesture:UIPanGestureRecognizer,originalCenter:CGPoint, translation:CGPoint, velocityInView:CGPoint)
    @objc optional func panGestureDidEnd(_ panGesture:UIPanGestureRecognizer, originalCenter:CGPoint, translation:CGPoint, velocityInView:CGPoint)
    @objc optional func panGestureStateToOriginal(_ panGesture:UIPanGestureRecognizer,originalCenter:CGPoint,  translation:CGPoint, velocityInView:CGPoint)
    func backTofullscreen()
}
