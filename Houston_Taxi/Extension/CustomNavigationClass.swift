//
//  CustomNavigationClass.swift
//  MyWealthGuide
//
//  Created by 한상민 on 2020/02/18.
//  Copyright © 2020 shot. All rights reserved.
//

import Foundation
import UIKit

class CustomNavigationController: UINavigationController {

    var isNewViewControllerBeingAdded: Bool = false

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
        
    override var prefersStatusBarHidden: Bool {
        return false
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.delegate = self
    }

    func contains(viewController: UIViewController) -> Bool {
        if self.topViewController?.className == viewController.className {
            return false
        }
        else {
            return true
        }
       
            //self.viewControllers.map{ $0.className }.contains(viewController.className)
    }

    override func pushViewController(_ viewController: UIViewController, animated: Bool) {
        if(!self.isNewViewControllerBeingAdded && !(self.topViewController?.className == viewController.className)) {
            self.isNewViewControllerBeingAdded = true
            super.pushViewController(viewController, animated: animated)
        }
    }
}


extension CustomNavigationController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, didShow viewController: UIViewController, animated: Bool) {
        self.isNewViewControllerBeingAdded = false
    }
}
