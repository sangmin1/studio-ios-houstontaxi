//
//  CenterAlignLabel.swift
//  Beeing
//
//  Created by sangmin han on 2020/04/03.
//  Copyright © 2020 shot. All rights reserved.
//

import Foundation
import UIKit

class CenterAlignedLabel: UILabel {

    override init(frame: CGRect) {
        super.init(frame: frame)
    }

    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }

    override func drawText(in rect: CGRect) {
        // offset by font
        // Logic of vertical center align in sketch is
        // capHeight to be the vertical center of the label.
        // Graphical explanation
        // |---|-------------|-----------|
        // |-----------------|             : ascender
        //                   |-----------| : descender
        //     |-------------|             : capHeight
        let topCapPad = font.ascender - font.capHeight
        let bottomCapPad = -font.descender
        let fontOffset = (bottomCapPad - topCapPad) / 2

        let offsetY = fontOffset
        super.drawText(in: rect.offsetBy(dx: 0, dy: offsetY))
    }
}
