//
//  FunctionClass.swift
//  CarpumUser
//
//  Created by 한상민 on 05/08/2019.
//  Copyright © 2019 shot. All rights reserved.
//

import Foundation
import UIKit
import Kingfisher
import CoreLocation
import AESinsol


class FunctionClass {
    
    static let shared = FunctionClass()
    private init(){}
    func showdialog(show : Bool){
        let scene = UIApplication.shared.connectedScenes.first
        if let del = (scene?.delegate as? SceneDelegate)  {
        
        //let window = del.window
        DispatchQueue.main.async {
            if show {
                del.spinner.alpha = 1
                del.window?.bringSubviewToFront(del.spinner)
                del.spinner.setvisibilitySpinner(status: true)
            }
            else {
                del.spinner.alpha = 0
                del.spinner.setvisibilitySpinner(status: false)
            }
        }
        
        }
    }
    
    func showapologizepopup(){
        let scene = UIApplication.shared.connectedScenes.first
        if let del = (scene?.delegate as? SceneDelegate)  {
            
            //let window = del.window
            DispatchQueue.main.async {
                del.window?.bringSubviewToFront(del.apologizepopup)
                del.apologizepopup.fadeIn(0.5, delay: 0) { (done) in
                    del.apologizepopup.fadeOut(0.5, delay: 2) { (done) in
                        
                    }
                }
//                del.apologizepopup.alpha = 1
//                del.window?.bringSubviewToFront(del.apologizepopup)
                
                
            }
            
        }
    }
    
    func showusercancelpopup(){
        let scene = UIApplication.shared.connectedScenes.first
        if let del = (scene?.delegate as? SceneDelegate)  {
            
            //let window = del.window
            DispatchQueue.main.async {
                del.window?.bringSubviewToFront(del.usercancelpopup)
                del.usercancelpopup.fadeIn(0.5, delay: 0) { (done) in
                    del.usercancelpopup.fadeOut(0.5, delay: 2) { (done) in
                        
                    }
                }
    
                
            }
            
        }
    }
    
    func parsetoimageuploadformFromUIImage(arr : [String]) -> String{
        let arr2 = arr
        /*let tempmodel = TempImageRepo.tempimagerepo.geturlmodels()
        for i in 0 ..< tempmodel.count {
            arr2.insert(tempmodel[i].getImageurl(), at: 0)
         }*/
        var imageinput2 = "["
        for i in 0 ..< arr2.count{
            imageinput2.append("\"")
            imageinput2.append(arr2[i])
            imageinput2.append("\"")
            if i < arr2.count - 1{
                imageinput2.append(",")
            }
        }
        imageinput2.append("]")
        return imageinput2
    }
    
    
    func parsehashtagtoString(tags : [String]) -> String {
        var str = ""
        for word in tags {
            str += word.replacingOccurrences(of: "#", with: "") + ","
        }
        
        
        return str
        
        
    }
    
    
    func fuckdm(src : String) -> [String]{
        let edited = src
        let editedarray = edited.split(separator: ",")
        var final : [String] = []
        
        for modifying in editedarray {
            var t = String(modifying)
            t = t.replacingOccurrences(of: "\"", with: "")
            t = t.replacingOccurrences(of: "[", with: "")
            t = t.replacingOccurrences(of: "]", with: "")
            t = t.replacingOccurrences(of: "\\", with: "")
            
            final.append(t)
        }
        
        return final
    }
    
    func fuckdm2(src : String) -> String{
            var t = String(src)
            t = t.replacingOccurrences(of: "\"", with: "")
            return t
    }
    
    
    func calculatetimedifference(time :String) -> String{
        
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateformatter.timeZone = TimeZone(abbreviation: "UTC")
        let time1 = dateformatter.date(from: time)
       
        
        let nowUTC = Date()
        let timeZoneOffset = Double(TimeZone.current.secondsFromGMT(for: nowUTC))
        
        let calendar = Calendar.current
        
       
        let modified = calendar.date(byAdding: .hour, value: Int(timeZoneOffset / 3600), to: time1!)
        
       // print("modified and hour", modified,hour)
        return dateformatter.string(from: modified!)
        
        
        
    }
    
    
    func calculatedistance(lat : Double, lon : Double) -> Double{
//        let currentlat = UserDefaults.standard.double(forKey: userstandard.lat.rawValue)
//        let currentlon = UserDefaults.standard.double(forKey: userstandard.lon.rawValue)
//        let mycoordinate = CLLocation(latitude: lat, longitude: lon)
//        var distanceInMeters : Double = 0
//        let coordinate = CLLocation(latitude: currentlat, longitude: currentlon)
//        distanceInMeters = mycoordinate.distance(from: coordinate)
//
        return 0
        
    }
    
    func convertToDictionary(text: String) -> [String: Any]? {
        if let data = text.data(using: .utf8) {
            do {
                return try JSONSerialization.jsonObject(with: data, options: []) as? [String: Any]
            } catch {
                print(error.localizedDescription)
            }
        }
        return nil
    }
    
    
    
    /**
      aes256 암호화 함수
     */
    func encrypt(msg : String) -> String {
        
        let enc = AesinsolEncrypt("com.insoline.hanam", msg)
        
        return enc
    }
    /**
     aes256 복호화 함수
     */
    func decrypt(msg : String) -> String {
        let dec = AesinsolDecrypt("com.insoline.hanam", msg)
        
        return dec
    }
    
    
    func getlocaltime(time : String) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateformatter.timeZone = TimeZone(abbreviation: "UTC")
        let time1 = dateformatter.date(from: time)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter2.locale = Locale.current // Change "en" to change the default locale if you want
        let stringDate = dateFormatter2.string(from: time1!)
        
        
        return stringDate
        
    }
    
    func getLocalTimeWithFormat(format : String, time : String) -> String {
        let dateformatter = DateFormatter()
        dateformatter.dateFormat = format
        dateformatter.timeZone = TimeZone(abbreviation: "UTC")
        let time1 = dateformatter.date(from: time)
        
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter2.locale = Locale.current // Change "en" to change the default locale if you want
        let stringDate = dateFormatter2.string(from: time1!)
        
        
        return stringDate
        
        
    }
    
   
    
    func getcurrentDt() -> String {
        
        let now = Date()
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd HH:mm:ss"
        dateFormatter2.locale = Locale.current
        var str = dateFormatter2.string(from: now)
        
        str = str.replacingOccurrences(of: "-", with: "")
        str = str.replacingOccurrences(of: ":", with: "")
        str = str.replacingOccurrences(of: " ", with: "")
        str = String(str.dropFirst(2))
        
        return str
    }
    
    func getCallDt() -> String {
        let now = Date()
        let dateFormatter2 = DateFormatter()
        dateFormatter2.dateFormat = "yyyy-MM-dd"
        dateFormatter2.locale = Locale.current
        var str = dateFormatter2.string(from: now)
        
        str = str.replacingOccurrences(of: "-", with: "")
        str = str.replacingOccurrences(of: ":", with: "")
        str = str.replacingOccurrences(of: " ", with: "")
        str = String(str.dropFirst(2))
        
        return str
    }
    
    
}

class IndicatorView : UIView {
    
    private var spinner = UIActivityIndicatorView()
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.backgroundColor = UIColor.init(white: 0.3, alpha: 0.6)
        makespinner()
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    func setvisibilitySpinner(status: Bool) {
        
        if status == true
        {
            spinner.startAnimating()
        }
        else{
            spinner.stopAnimating()
        }
    }
    
    func makespinner(){
        self.addSubview(spinner)
        spinner.translatesAutoresizingMaskIntoConstraints = false
        spinner.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        spinner.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        spinner.widthAnchor.constraint(equalToConstant: 100).isActive = true
        spinner.heightAnchor.constraint(equalToConstant: 100).isActive = true
        spinner.tintColor = maincolor
        spinner.contentMode = .scaleToFill
        spinner.style = .large
        spinner.color = maincolor
      
    }
}
