//
//  EnumsAndStatic.swift
//  Beeing
//
//  Created by 한상민 on 2020/03/27.
//  Copyright © 2020 shot. All rights reserved.
//

import Foundation
import UIKit


let hscale = UIScreen.main.bounds.height / 640
let wscale = UIScreen.main.bounds.width / 375
let mainboundwidth = UIScreen.main.bounds.width
let mainboundheight = UIScreen.main.bounds.height
let maincolor = UIColor.rgb(red: 26, green: 134, blue: 164, alpha: 1)
let deactivecolor = UIColor.rgb(red: 237, green: 237, blue: 237, alpha: 1)


var appwaslaunched : Bool = false




enum userstandard : String {
    case phonenumber = "phonenumber"
    case lat = "lat"
    case lon = "lon"
    case directcallfirsttime = "directcallfirsttime"
    case isverified = "isverified"
    case callcenterSelected = "callcenterSelected"
    case callcenternums = "callcenternums"
    case callid = "callid"
    case calldt = "calldt"
    case destlat = "destlat"
    case destlon = "destlon"
    case startlat = "startlat"
    case startlon = "startlon"
    case destname = "destname"
    
}
enum infouseagreementstype {
    case privacy
    case location
    case service
    case none
}
enum enlistmode {
    case house
    case office
}
enum searchtype {
    case departure
    case destination
}
enum fonts : String {
    case medium = "InaviRixGoM"
    case light = "InaviRixGoL"
    case ebold = "InaviRixGoEB"
    case bold = "InaviRixGoB"
}
enum callcheck : String , CaseIterable {
    
    case submitted = "접수"
    case onboard = "승차"
    case allocsucess = "배차완료"
    
    case fail = "실패"
    case allocfailed = "배차실패"
    case connecterror = "통신오류"
    case onboardfailed = "탑승실패"
   
    case drop = "하차"
    case cancel = "취소"
    
    
    
    
}
