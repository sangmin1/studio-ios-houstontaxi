//
//  PaddingTextField.swift
//  Beeing
//
//  Created by 한상민 on 2020/03/27.
//  Copyright © 2020 shot. All rights reserved.
//

import Foundation
import UIKit

class PaddingTextField: UITextField {

    var up : CGFloat = 0
    var left : CGFloat = 0
    var down : CGFloat = 0
    var right : CGFloat = 0
    

    override open func textRect(forBounds bounds: CGRect) -> CGRect {
        let padding = UIEdgeInsets(top: up, left: left, bottom: down, right: right)
        return bounds.inset(by: padding)
    }

    override open func placeholderRect(forBounds bounds: CGRect) -> CGRect {
        let padding = UIEdgeInsets(top: up, left: left, bottom: down, right: right)
        return bounds.inset(by: padding)
    }

    override open func editingRect(forBounds bounds: CGRect) -> CGRect {
        let padding = UIEdgeInsets(top: up, left: left, bottom: down, right: right)
        return bounds.inset(by: padding)
    }
}



class PaddingImageView : UIImageView {
    
    var paddingInset = UIEdgeInsets()
    
    override var  alignmentRectInsets: UIEdgeInsets {
        return paddingInset
    }
    
    
    
}
