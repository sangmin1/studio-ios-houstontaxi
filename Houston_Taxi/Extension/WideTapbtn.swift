//
//  WideTapbtn.swift
//  Beeing
//
//  Created by 한상민 on 2020/03/27.
//  Copyright © 2020 shot. All rights reserved.
//

import Foundation
import UIKit

class WideTapBtn: UIButton {
    
    override func hitTest(_ point: CGPoint, with event: UIEvent?) -> UIView? {
        if !isUserInteractionEnabled || isHidden || alpha <= 0.01 {
            return nil
        }
     
        let touchRect = bounds.insetBy(dx: -10, dy: -10)
        if touchRect.contains(point) {
            for subview in subviews.reversed() {
                let convertedPoint = subview.convert(point, from: self)
                if let hitTestView = subview.hitTest(convertedPoint, with: event) {
                    return hitTestView
                }
            }
     
            return self
        }
     
        return nil
    }
}
