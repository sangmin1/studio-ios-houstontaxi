//
//  DynamicCollectionView.swift
//  MyWealthGuide
//
//  Created by 한상민 on 2019/11/22.
//  Copyright © 2019 shot. All rights reserved.
//

import Foundation
import UIKit


class DynamicHeightCollectionView: UICollectionView {
    override func layoutSubviews() {
        super.layoutSubviews()
        if !__CGSizeEqualToSize(bounds.size, self.intrinsicContentSize) {
            self.invalidateIntrinsicContentSize()
        }
    }
    
    override var intrinsicContentSize: CGSize {
        return contentSize
    }
}
