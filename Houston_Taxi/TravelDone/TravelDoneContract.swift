
//
//  TravelContract.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/19.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation

protocol TravelDoneContract : NSObjectProtocol {
    
}
protocol EvaluatePopUpDelegate : NSObjectProtocol {
    
    func popupevaluatepressed(rank : Int , str : String)
}
