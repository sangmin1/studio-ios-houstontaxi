//
//  TravelDoneViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/19.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
import iNaviMaps


/**
   제플린 800 - 도착 화면
 */
class TravelDoneViewController : UIViewController {
    
    lazy var mapView = InaviMapView(frame: view.bounds)
    
    private var box = ShadowView()
    
    private var infolabel1 = PaddingLabel()
    
    private var infolabel2 = PaddingLabel()
    
     
    private var marker = INVMarker()
    
    //평가 버튼
    private var evaluatebtn = ShadowBtn()
    //닫기 버튼
    private var closebtn = ShadowBtn()
    
    
    lazy private var btnstack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [evaluatebtn, closebtn])
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.spacing = 15
        return stack
    }()
    
    
    
    lazy private var evaluatepopup = EvaluatePopUp(frame: self.view.bounds, delegate: self)
    
    lazy private var presenter = TravelDonePresenter(delegate : self, viewcontroller : self)
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationController?.navigationBar.isHidden = true
        self.view.backgroundColor = .white
        self.view.addSubview(mapView)
        mapView.userTrackingMode = .tracking
        mapView.locationIcon.isVisible = false
        
        marker.iconImage = INVImage(image: UIImage(named: "mapPinMyloca")!)
        marker.mapView = mapView
        mapView.delegate = self
        makebox()
        makeinfolabel1()
        makeinfolabel2()
        makebtnstack()
        
        
        self.view.addSubview(evaluatepopup)
        evaluatepopup.alpha = 0
        
        
        evaluatebtn.addTarget(self, action: #selector(evaluatebtnpressed(sender:)), for: .touchUpInside)
        closebtn.addTarget(self, action: #selector(closebtnpressed(sender:)), for: .touchUpInside)
        
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        box.layer.cornerRadius = 8
        
        evaluatebtn.layer.cornerRadius = 25
        closebtn.layer.cornerRadius = 25
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        
         // 뷰가 사라지면 저장 되어있던 calldt 와 callId를 없앤다
        CallRepo.callDt = ""
        CallRepo.callID = ""
        UserDefaults.standard.set("", forKey: userstandard.calldt.rawValue)
        UserDefaults.standard.set("", forKey: userstandard.callid.rawValue)
    }
    
    
    /**
     평가 버튼 이벤트 함수
     */
    @objc func evaluatebtnpressed(sender : UIButton){
        self.view.bringSubviewToFront(evaluatepopup)
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.evaluatepopup.alpha = 1
        }
    }
    
    /**
     닫기 버튼 이벤트 함수
     */
    @objc func closebtnpressed(sender : UIButton){
        self.navigationController?.popToViewController(ofClass: MainViewController.self, animated: true)
    }
    
    
    
}
extension TravelDoneViewController : EvaluatePopUpDelegate {
    func popupevaluatepressed(rank: Int, str: String) {
        presenter.callReviewAPI(rank: rank) { [unowned self] (done) in
            if done {
                self.navigationController?.popToViewController(ofClass: MainViewController.self, animated: true)
            }
            else {
                ToastView.shared.short(self.view, txt_msg: "잠시 후 시도해주세요")
            }
        }
    }
    
    
}
extension TravelDoneViewController : TravelDoneContract {
    
    
    
}
extension TravelDoneViewController : INVMapViewDelegate {
    func mapView(_ mapView: InaviMapView, didUpdateUserLocation userLocation: CLLocation?) {
        let lat = userLocation?.coordinate.latitude
        let lon = userLocation?.coordinate.longitude
        marker.position = INVLatLng(lat: lat!, lng: lon!)
    }
    
    
}

extension TravelDoneViewController {
    private func makebox(){
        self.view.addSubview(box)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 16).isActive = true
        box.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        box.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        box.backgroundColor = .white
    
        box.shadowcolor = .black
        box.shadowoffset = CGSize(width: 0, height: 5)
        box.shadowradius = 10
        box.shadowopacity = 0.1
        
    }
    private func makeinfolabel1(){
        self.view.addSubview(infolabel1)
        infolabel1.translatesAutoresizingMaskIntoConstraints = false
        infolabel1.topAnchor.constraint(equalTo: box.topAnchor, constant: 15).isActive = true
        infolabel1.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 20).isActive = true
        infolabel1.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: -20).isActive = true
        infolabel1.heightAnchor.constraint(lessThanOrEqualToConstant: 100).isActive = true
        infolabel1.numberOfLines = 0
        infolabel1.lineBreakMode = .byWordWrapping
        infolabel1.textAlignment = .center
        infolabel1.textColor = .rgb(red: 70, green: 70, blue: 70, alpha: 1)
        infolabel1.font = UIFont(name: fonts.medium.rawValue, size: 24)
        infolabel1.text = """
도착했습니다.
이용해 주셔서 감사합니다.
"""
        
        
    }
    private func makeinfolabel2(){
        self.view.addSubview(infolabel2)
        infolabel2.translatesAutoresizingMaskIntoConstraints = false
        infolabel2.topAnchor.constraint(equalTo: infolabel1.bottomAnchor, constant: 5).isActive = true
        infolabel2.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 20).isActive = true
        infolabel2.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: -20).isActive = true
        infolabel2.heightAnchor.constraint(lessThanOrEqualToConstant: 100).isActive = true
        infolabel2.numberOfLines = 0
        infolabel2.lineBreakMode = .byWordWrapping
        infolabel2.textAlignment = .center
        infolabel2.textColor = .rgb(red: 184, green: 184, blue: 184, alpha: 1)
        infolabel2.font = UIFont(name: fonts.medium.rawValue, size: 18)
        infolabel2.text = """
평가를 남겨주시면 더 좋은 서비스를
만드는 데 도움이 됩니다.
"""
        
        
        box.bottomAnchor.constraint(equalTo: infolabel2.bottomAnchor, constant: 14).isActive = true
        
    }
    
    private func makebtnstack(){
        self.view.addSubview(btnstack)
        btnstack.translatesAutoresizingMaskIntoConstraints = false
        btnstack.bottomAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.bottomAnchor, constant: -13).isActive = true
        btnstack.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        btnstack.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -12).isActive = true
        btnstack.heightAnchor.constraint(equalToConstant: 50).isActive = true
        
        
        
        
        evaluatebtn.setTitle("평가하기", for: .normal)
        evaluatebtn.setTitleColor(maincolor, for: .normal)
        evaluatebtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20)
        evaluatebtn.backgroundColor = .white
        
        evaluatebtn.shadowcolor = .black
        evaluatebtn.shadowoffset = CGSize(width: 0, height: 3)
        evaluatebtn.shadowradius = 6
        evaluatebtn.shadowopacity = 0.16
        
        closebtn.setTitle("닫기", for: .normal)
        closebtn.setTitleColor(maincolor, for: .normal)
        closebtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20)
        closebtn.backgroundColor = .white
        
        closebtn.shadowcolor = .black
        closebtn.shadowoffset = CGSize(width: 0, height: 3)
        closebtn.shadowradius = 6
        closebtn.shadowopacity = 0.16
        
    }
    
}
