//
//  EvaluatePopUp.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/20.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
import GrowingTextView



class EvaluatePopUp: UIView {
    
    private var box = ShadowView()
    
    private var infolabel = PaddingLabel()
    
    
    private var star1 = UIButton()
    private var star2 = UIButton()
    private var star3 = UIButton()
    private var star4 = UIButton()
    private var star5 = UIButton()
    
    lazy private var ratestack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [star1,star2,star3,star4,star5])
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.spacing = 6
        return stack
    }()
    
    private var rank : Int = 0
    
    private var placeholder = PaddingLabel()
    
    private var inputfield = GrowingTextView()
    
    private var underline = UIImageView()
    
    private var closebtn = UIButton()
    
    private var evaluatebtn = UIButton()
    
    lazy private var btnstack : UIStackView = {
        let stack = UIStackView(arrangedSubviews: [closebtn, evaluatebtn])
        stack.axis = .horizontal
        stack.distribution = .fillEqually
        stack.spacing = 1
        return stack
    }()
    
    weak var delegate : EvaluatePopUpDelegate?
    
    
    init(frame: CGRect, delegate : EvaluatePopUpDelegate) {
        super.init(frame: frame)
        self.delegate = delegate
        self.backgroundColor = UIColor.init(white: 0.1, alpha: 0.5)
       
        makebox()
        makeinfolabel()
        makeratestack()
        makeplaceholder()
        makeinputfield()
        makeunderline()
        makebtnstack()
        
    
        
        closebtn.addTarget(self, action: #selector(closebtnpressed(sender:)), for: .touchUpInside)
        evaluatebtn.addTarget(self, action: #selector(evaluatebtnpressed(sender:)), for: .touchUpInside)
        
        
        (ratestack.subviews as! [UIButton]).forEach { (btn) in
            btn.addTarget(self, action: #selector(rankbtnpressed(sender:)), for: .touchUpInside)
            btn.isUserInteractionEnabled = true
            
        }
        inputfield.isHidden = true
        
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    override func didMoveToWindow() {
        super.didMoveToWindow()
        DispatchQueue.main.async { [weak self] in
            self?.box.layer.cornerRadius = 8
            self?.closebtn.layer.addBorder(edge: [.top], color: .rgb(red: 0, green: 0, blue: 0, alpha: 0.05), thickness: 1)
            self?.closebtn.layer.addBorder(edge: [.right], color: .rgb(red: 0, green: 0, blue: 0, alpha: 0.05), thickness: 1)
            self?.evaluatebtn.layer.addBorder(edge: [.top], color: .rgb(red: 0, green: 0, blue: 0, alpha: 0.05), thickness: 1)
            self?.layoutIfNeeded()
            self?.inputfield.text = ""
        }
    }
    
    @objc func closebtnpressed(sender : UIButton){
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.alpha = 0
        }
    }
    
    @objc func evaluatebtnpressed(sender : UIButton){
        if let str = inputfield.text {
            self.delegate?.popupevaluatepressed(rank: rank, str: str)
        }
        else {
            self.delegate?.popupevaluatepressed(rank: rank, str: "")
        }
        
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.alpha = 0
        }
    }
    
    @objc func rankbtnpressed(sender : UIButton){
       
        rank = sender.tag + 1
        for (index , btn ) in (ratestack.subviews as! [UIButton]).enumerated() {
            if index <= sender.tag {
                btn.isSelected = true
            }
            else {
                btn.isSelected = false
            }
        }
    }
    
    
}
extension EvaluatePopUp : GrowingTextViewDelegate {
    

    
    func textViewDidChangeHeight(_ textView: GrowingTextView, height: CGFloat) {
        UIView.animate(withDuration: 0.2) { [unowned self] in
            self.layoutIfNeeded()
        }
    }
}
extension EvaluatePopUp {
    private func makebox(){
        self.addSubview(box)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.centerYAnchor.constraint(equalTo: self.centerYAnchor, constant: 0).isActive = true
        box.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        box.widthAnchor.constraint(equalToConstant: 314).isActive = true
        
        
        box.backgroundColor = .white
        
        box.shadowcolor = .black
        box.shadowoffset = CGSize(width: 0, height: 3)
        box.shadowradius = 6
        box.shadowopacity = 0.1
        
    }
    private func makeinfolabel(){
        self.addSubview(infolabel)
        infolabel.translatesAutoresizingMaskIntoConstraints = false
        infolabel.topAnchor.constraint(equalTo: box.topAnchor, constant: 30).isActive = true
        infolabel.centerXAnchor.constraint(equalTo: box.centerXAnchor, constant: 0).isActive = true
        infolabel.widthAnchor.constraint(equalToConstant: 200).isActive = true
        infolabel.heightAnchor.constraint(equalToConstant: 26).isActive = true
        infolabel.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        infolabel.font = UIFont(name: fonts.bold.rawValue, size: 20)
        infolabel.text = "운행이 어떠셨나요?"
        infolabel.textAlignment = .center
        
        
        
    }
    private func makeratestack(){
        self.addSubview(ratestack)
        ratestack.translatesAutoresizingMaskIntoConstraints = false
        ratestack.topAnchor.constraint(equalTo: infolabel.bottomAnchor, constant: 30).isActive = true
        ratestack.centerXAnchor.constraint(equalTo: self.centerXAnchor, constant: 0).isActive = true
        ratestack.widthAnchor.constraint(equalToConstant: 36 * 5 + 6 * 4).isActive = true
        ratestack.heightAnchor.constraint(equalToConstant: 34).isActive = true
        
        star1.setImage(UIImage(named: "popupIcoStarBg"), for: .normal)
        star1.setImage(UIImage(named: "popupIcoStar"), for: .selected)
        star1.isSelected = false
        star1.tag = 0
        
        star2.setImage(UIImage(named: "popupIcoStarBg"), for: .normal)
        star2.setImage(UIImage(named: "popupIcoStar"), for: .selected)
        star2.isSelected = false
        star2.tag = 1
        
        star3.setImage(UIImage(named: "popupIcoStarBg"), for: .normal)
        star3.setImage(UIImage(named: "popupIcoStar"), for: .selected)
        star3.isSelected = false
        star3.tag = 2
        
        star4.setImage(UIImage(named: "popupIcoStarBg"), for: .normal)
        star4.setImage(UIImage(named: "popupIcoStar"), for: .selected)
        star4.isSelected = false
        star4.tag = 3
        
        star5.setImage(UIImage(named: "popupIcoStarBg"), for: .normal)
        star5.setImage(UIImage(named: "popupIcoStar"), for: .selected)
        star5.isSelected = false
        star5.tag = 4
        
       
        
    }
    private func makeplaceholder(){
        self.addSubview(placeholder)
        placeholder.translatesAutoresizingMaskIntoConstraints = false
        placeholder.topAnchor.constraint(equalTo: ratestack.bottomAnchor, constant: 30).isActive = true
        placeholder.centerXAnchor.constraint(equalTo: box.centerXAnchor, constant: 0).isActive = true
        placeholder.widthAnchor.constraint(equalToConstant: 200).isActive = true
        placeholder.heightAnchor.constraint(equalToConstant: 30).isActive = true
        placeholder.font = UIFont(name: fonts.medium.rawValue, size: 16)
        placeholder.textColor = .rgb(red: 179, green: 179, blue: 179, alpha: 1)
        placeholder.text = "이용 후기를 남겨주세요."
        placeholder.alpha = 0
    }
    
    private func makeinputfield(){
        self.addSubview(inputfield)
        inputfield.translatesAutoresizingMaskIntoConstraints = false
        inputfield.topAnchor.constraint(equalTo: ratestack.bottomAnchor, constant: 30).isActive = true
        inputfield.centerXAnchor.constraint(equalTo: box.centerXAnchor, constant: 0).isActive = true
        inputfield.widthAnchor.constraint(equalToConstant: 200).isActive = true
        //inputfield.heightAnchor.constraint(greaterThanOrEqualToConstant: 30).isActive = true
        inputfield.backgroundColor = .clear
        inputfield.minHeight = 30
        inputfield.maxHeight = 100
        
        inputfield.text = "      "
        
        inputfield.placeholder = "이용 후기를 남겨주세요."
        inputfield.font = UIFont(name: fonts.medium.rawValue, size: 16)
        inputfield.textColor = .rgb(red: 100, green: 100, blue: 100, alpha: 1)
        inputfield.placeholderColor = .rgb(red: 179, green: 179, blue: 179, alpha: 1)
        
        inputfield.delegate = self
        
        
        
    }
    private func makeunderline(){
        self.addSubview(underline)
        underline.translatesAutoresizingMaskIntoConstraints = false
        underline.topAnchor.constraint(equalTo: inputfield.bottomAnchor, constant: 2).isActive = true
        underline.centerXAnchor.constraint(equalTo: box.centerXAnchor, constant: 0).isActive = true
        underline.widthAnchor.constraint(equalToConstant: 200).isActive = true
        underline.heightAnchor.constraint(equalToConstant: 1).isActive = true
        underline.backgroundColor = .rgb(red: 184, green: 184, blue: 184, alpha: 1)
    }
    private func makebtnstack(){
        self.addSubview(btnstack)
        btnstack.translatesAutoresizingMaskIntoConstraints = false
        btnstack.topAnchor.constraint(equalTo: underline.bottomAnchor, constant: 20).isActive = true
        btnstack.leadingAnchor.constraint(equalTo: box.leadingAnchor, constant: 0).isActive = true
        btnstack.trailingAnchor.constraint(equalTo: box.trailingAnchor, constant: 0).isActive = true
        btnstack.heightAnchor.constraint(equalToConstant: 50).isActive = true
        btnstack.isUserInteractionEnabled = true
        
        
        
        closebtn.setTitle("취소", for: .normal)
        closebtn.setTitleColor(.rgb(red: 100, green: 100, blue: 100, alpha: 1), for: .normal)
        closebtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20)
        
        
        evaluatebtn.setTitle("평가하기", for: .normal)
        evaluatebtn.setTitleColor(maincolor, for: .normal)
        evaluatebtn.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20)
        
        box.bottomAnchor.constraint(equalTo: btnstack.bottomAnchor, constant: 0).isActive = true
        
    }
    
    
}
