//
//  TravelDonePresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/19.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit


class TravelDonePresenter : NSObject {
    
    weak var delegate : TravelDoneContract?
    weak var viewcontroller : TravelDoneViewController?
    
    
    
    init(delegate : TravelDoneContract, viewcontroller : TravelDoneViewController){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
        
    }
    
    
    /**
     리뷰 api함수
     
    - paramter :
    - rank : 점수
     */
    func callReviewAPI(rank : Int,completion : @escaping (Bool) -> ()){
        let api = DriverReviewAPI()
        
        let currentdt = FunctionClass.shared.getcurrentDt()
        let authcode = FunctionClass.shared.encrypt(msg: currentdt)
        let mobile = FunctionClass.shared.encrypt(msg: UserDefaults.standard.string(forKey: userstandard.phonenumber.rawValue)!)
        let callid =  FunctionClass.shared.encrypt(msg: CallRepo.callID)
        let calldt = FunctionClass.shared.encrypt(msg: CallRepo.callDt)
        let data = FunctionClass.shared.encrypt(msg: String(rank))
        
        
        FunctionClass.shared.showdialog(show: true)
        api.requestToserver(currentDT: currentdt, authcode: authcode, mobile: mobile, callId: callid, callDT: calldt, data: data) { (done, data) in
            if done {
                let issuccess = data["isSuccessful"].boolValue
                guard let msg = data["msg"].string else {
                    print("on boarding view controller msg failed")
                    return
                }
                print("msg",msg)
                
                completion(true)
            }
            else {
                completion(false)
            }
            FunctionClass.shared.showdialog(show: false)
        }
        
        
        
    }
    
    
}
