//
//  CallWithDestViewController.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/05.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
import iNaviMaps

/**
  제플린 400 번대 택시 호출 - 도착지 있음 화면
 */
class CallWithDestViewController: UIViewController {
    //백버튼
    private var backbtn = WideTapBtn()
    
    lazy var mapView = InaviMapView(frame: .zero)
    
    //택시 호출
    private var calltaxi = UIButton()
    
    
    private var box = ShadowView()
    
    //예상 시간
    private var timelabel = PaddingLabel()
    //시간
    private var timevalue = PaddingLabel()
    //예상 거리
    private var distancelabel = PaddingLabel()
    //거리
    private var distancevalue = PaddingLabel()
    
    //현재 위치 버튼
    private var currentlocationbtn = UIButton()
    
    
    
    
    
    lazy private var presenter = CallWithDestPresenter(delegate : self, viewcontroller : self)
    

    lazy private var markerview = CustomDestMarker(frame: CGRect(x: 0, y: 0, width: 128, height: 38),min : self.presenter.getspendtime())
    
    //mapview camera 객체 초기 화면에서 경로 전체를 한 화면에 보이기위하여 필요함
    private var camera = INVCameraUpdate()
    
    //도착지 마커
    let destmarker = INVMarker()
    //출발지 마커
    let departmarker = INVMarker()
    
    /**
     뷰 컨트롤러 생성할때 인자를 넘겨 받는다
     
    - paramter :
    - departname : 출발지 지명
    - destname : 도착지 지명
     */
    convenience init(departname : String , destname : String ,startx: Double, starty: Double, endx: Double, endy: Double ){
        self.init()
        
        //전의 화면에서 목적지와 출발지를 받아와 전역으로 세팅 되어 있는 마커에 삽입
        destmarker.title = destname
        departmarker.title = departname
        
        presenter.setInfos(departname: departname, destname: destname, startx: startx, starty: starty, endx: endx, endy: endy)
        
        
        
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.backgroundColor = .white
        self.view.addSubview(markerview)
        self.navigationController?.interactivePopGestureRecognizer?.isEnabled = true
        makebackbtn()
        makecalltaxibtn()
        makebox()
        makeinfolabel()
        makemoneylabel()
        makedistancelabel()
        makedistancevalue()
        makecurrentlocationbtn()
        makemapview()
        self.view.backgroundColor = .white
        let route = INVRoute(links: self.presenter.setrouteOnMap())
         route.mapView = self.mapView
         
         
         
         var coordrect : [INVLatLng] = []
         route.links.forEach { (link) in
             link.coords.forEach { (coord) in
                 coordrect.append(coord)
             }
         }
         
         //경로가 맵 전체에 보이게끔 카메라를 이동 하는 함수
        if coordrect.count >= 2 {
            camera = INVCameraUpdate(fit: INVLatLngBounds(coords: coordrect), paddingInsets: UIEdgeInsets(top: 80, left: 80, bottom: 80, right: 80))
        }
        
        
        //도착 마커 세팅
        destmarker.iconImage = INVImage(image: self.markerview.asImage())
        destmarker.position = coordrect.last!
        destmarker.mapView = self.mapView
        //출발 마커 세팅
        departmarker.iconImage = INVImage(image: UIImage(named: "mapPinDeparture")!)
        departmarker.position = coordrect[0]
        departmarker.mapView = self.mapView
         
        
        backbtn.addTarget(self, action: #selector(backbtnpressed(sender:)), for: .touchUpInside)
        calltaxi.addTarget(self, action: #selector(calltaxibtnpressed(sender:)), for: .touchUpInside)
        currentlocationbtn.addTarget(self, action: #selector(currentlocationbtnpressed(sender:)), for: .touchUpInside)
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        box.roundCorners(corners: [.topRight,.topLeft], radius: 25)
        
        self.view.sendSubviewToBack(markerview)
        
    }
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        self.mapView.moveCamera(camera)
        
       
    }
    
    
    /**
     백버튼 이벤트 함수
     */
    @objc func backbtnpressed(sender: UIButton){
        self.navigationController?.popViewController(animated: true)
    }
    
    /**
     택시 호출 이벤트 함수
     */
    @objc func calltaxibtnpressed(sender : UIButton){
        let view = FindingTaxiViewController(allocdata: presenter.getallocinfos())
        self.navigationController?.pushViewController(view, animated: true)
    }
    
    /**
     현재 위치 버튼 이벤트 함수
     */
    @objc func currentlocationbtnpressed(sender : UIButton){
//        let lat = UserDefaults.standard.double(forKey: userstandard.lat.rawValue)
//        let lon = UserDefaults.standard.double(forKey: userstandard.lon.rawValue)
//        mapView.moveCamera(INVCameraUpdate(targetTo: INVLatLng(lat: lat, lng: lon)))
        
        let route = INVRoute(links: self.presenter.setrouteOnMap())
         route.mapView = self.mapView
         
         
         
         var coordrect : [INVLatLng] = []
         route.links.forEach { (link) in
             link.coords.forEach { (coord) in
                 coordrect.append(coord)
             }
         }
         
         //경로가 맵 전체에 보이게끔 카메라를 이동 하는 함수
        if coordrect.count >= 2 {
            camera = INVCameraUpdate(fit: INVLatLngBounds(coords: coordrect), paddingInsets: UIEdgeInsets(top: 80, left: 80, bottom: 80, right: 80))
        }
        
        self.mapView.moveCamera(camera)
    }
    
    
    
}

extension CallWithDestViewController : INVMapViewDelegate {
    
    
  
    

   

    
    

    
}

extension CallWithDestViewController : CallWithDestContract {
    private func makebackbtn(){
        self.view.addSubview(backbtn)
        backbtn.translatesAutoresizingMaskIntoConstraints = false
        backbtn.topAnchor.constraint(equalTo: self.view.safeAreaLayoutGuide.topAnchor, constant: 13).isActive = true
        backbtn.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 12).isActive = true
        backbtn.widthAnchor.constraint(equalToConstant: 30).isActive = true
        backbtn.heightAnchor.constraint(equalToConstant: 30).isActive = true
        backbtn.setImage(UIImage(named: "titleBtnBackN"), for: .normal)
        
    }
    private func makecalltaxibtn(){
        self.view.addSubview(calltaxi)
        calltaxi.translatesAutoresizingMaskIntoConstraints = false
        calltaxi.bottomAnchor.constraint(equalTo: self.view.bottomAnchor, constant: 0).isActive = true
        calltaxi.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        calltaxi.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        calltaxi.heightAnchor.constraint(equalToConstant: 54).isActive = true
        calltaxi.backgroundColor = maincolor
        calltaxi.setTitle("택시 호출", for: .normal)
        calltaxi.setTitleColor(.white, for: .normal)
        calltaxi.titleLabel?.font = UIFont(name: fonts.bold.rawValue, size: 20)
    }
    private func makebox(){
        self.view.addSubview(box)
        box.translatesAutoresizingMaskIntoConstraints = false
        box.bottomAnchor.constraint(equalTo: calltaxi.topAnchor, constant: 0).isActive = true
        box.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        box.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        box.heightAnchor.constraint(equalToConstant: 100).isActive = true
        box.backgroundColor = .white
        box.shadowcolor = .black
        box.shadowoffset = CGSize(width: 0, height: 3)
        box.shadowradius = 6
        box.shadowopacity = 0.16
        self.view.sendSubviewToBack(box)
    }
    private func makeinfolabel(){
        self.view.addSubview(timelabel)
        timelabel.translatesAutoresizingMaskIntoConstraints = false
        timelabel.bottomAnchor.constraint(equalTo: box.bottomAnchor, constant: -18).isActive = true
        timelabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 30).isActive = true
        timelabel.widthAnchor.constraint(equalToConstant: 80).isActive = true
        timelabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        timelabel.text = "예상 시간"
        timelabel.textColor = .rgb(red: 70, green: 70, blue: 70, alpha: 1)
        timelabel.font = UIFont(name: fonts.medium.rawValue, size: 18)
    }
    private func makemoneylabel(){
        self.view.addSubview(timevalue)
        timevalue.translatesAutoresizingMaskIntoConstraints = false
        timevalue.bottomAnchor.constraint(equalTo: box.bottomAnchor, constant: -18).isActive = true
        timevalue.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -32).isActive = true
        timevalue.widthAnchor.constraint(equalToConstant: 200).isActive = true
        timevalue.heightAnchor.constraint(equalToConstant: 30).isActive = true
        timevalue.textAlignment = .right
        let time = presenter.getspendtime()
        let str = "약 \(time)분 예상"
        let attributedString = NSMutableAttributedString(string: str, attributes: [
            .font: UIFont(name: fonts.medium.rawValue, size: 20.0)!,
          .foregroundColor: UIColor(white: 70.0 / 255.0, alpha: 1.0)
        ])
        let range = NSString(string: str).range(of: "\(time)")
        
        attributedString.addAttributes([
            .font: UIFont(name: fonts.bold.rawValue, size: 25.0)!,
          .foregroundColor: UIColor(red: 226.0 / 255.0, green: 83.0 / 255.0, blue: 33.0 / 255.0, alpha: 1.0)
        ], range: range)

        
        timevalue.attributedText = attributedString
        
    }
    private func makedistancelabel(){
        self.view.addSubview(distancelabel)
        distancelabel.translatesAutoresizingMaskIntoConstraints = false
        distancelabel.bottomAnchor.constraint(equalTo: timelabel.topAnchor, constant: -18).isActive = true
        distancelabel.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 30).isActive = true
        distancelabel.widthAnchor.constraint(equalToConstant: 80).isActive = true
        distancelabel.heightAnchor.constraint(equalToConstant: 25).isActive = true
        distancelabel.text = "예상 거리"
        distancelabel.textColor = .rgb(red: 70, green: 70, blue: 70, alpha: 1)
        distancelabel.font = UIFont(name: fonts.medium.rawValue, size: 18)
    }
    private func makedistancevalue(){
        self.view.addSubview(distancevalue)
        distancevalue.translatesAutoresizingMaskIntoConstraints = false
        distancevalue.centerYAnchor.constraint(equalTo: distancelabel.centerYAnchor, constant: 0).isActive = true
        distancevalue.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -32).isActive = true
        distancevalue.widthAnchor.constraint(equalToConstant: 200).isActive = true
        distancevalue.heightAnchor.constraint(equalToConstant: 30).isActive = true
        distancevalue.textAlignment = .right
        let distance = presenter.getdistance()
        let str = "약 \(distance.clean)km"
        let attributedString = NSMutableAttributedString(string: str, attributes: [
            .font: UIFont(name: fonts.medium.rawValue, size: 20.0)!,
          .foregroundColor: UIColor(white: 70.0 / 255.0, alpha: 1.0)
        ])
        let range = NSString(string: str).range(of: "\(distance.clean)")
        
        attributedString.addAttributes([
            .font: UIFont(name: fonts.bold.rawValue, size: 25.0)!,
          .foregroundColor: maincolor
        ], range: range)
        distancevalue.attributedText = attributedString
    }
    private func makecurrentlocationbtn(){
        self.view.addSubview(currentlocationbtn)
        currentlocationbtn.translatesAutoresizingMaskIntoConstraints = false
        currentlocationbtn.bottomAnchor.constraint(equalTo: box.topAnchor, constant: 0).isActive = true
        currentlocationbtn.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: -3).isActive = true
        currentlocationbtn.widthAnchor.constraint(equalToConstant: 54).isActive = true
        currentlocationbtn.heightAnchor.constraint(equalToConstant: 54).isActive = true
        currentlocationbtn.setImage(UIImage(named: "mapBtnMy"), for: .normal)
        
    }
    private func makemapview(){
        self.view.addSubview(mapView)
        mapView.translatesAutoresizingMaskIntoConstraints = false
        mapView.topAnchor.constraint(equalTo: self.view.topAnchor, constant: 0).isActive = true
        mapView.leadingAnchor.constraint(equalTo: self.view.leadingAnchor, constant: 0).isActive = true
        mapView.trailingAnchor.constraint(equalTo: self.view.trailingAnchor, constant: 0).isActive = true
        mapView.bottomAnchor.constraint(equalTo: calltaxi.topAnchor, constant: 0).isActive = true
        mapView.delegate = self
        self.view.sendSubviewToBack(mapView)
        
        
        
    }
    
}
