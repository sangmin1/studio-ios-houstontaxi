//
//  InavieRouteRepo.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/07.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import SwiftyJSON
import iNaviMaps



class InaviRouteRepo {
    static let shared = InaviRouteRepo()
    private init(){}
    
    
    
    // 택시 호출하기 전의 모델
    private var model : InaviRouteModel?
    
    
    
    func setmodel(input : InaviRouteModel){
        self.model = input
    }
    
    func getmodel() -> InaviRouteModel {
        guard let model = self.model else {
            return InaviRouteModel(json: JSON(""))
        }
        return model
    }
    func getcoords() -> [[INVLatLng]] {
        
        
        var invlatlng : [[INVLatLng]] = []
        
        
        guard let model = self.model else {
            return []
        }
        //모델에서 path object array를 먼저 뽑아온다
        for path in model.path {
            //path object에서 뽑아올 coords를 담을 어레이
            var temp : [INVLatLng] = []
            for coord in path.coords {
                //유효성 검사후 배열에 삽입
                if let x = coord.x, let y = coord.y  {
                   
                    temp.append(INVLatLng(lat: y, lng: x))
                }
            }
            //이중 배열에 추가
            invlatlng.append(temp)
        }
        
        return invlatlng
    }
    
    
    
    //탑승 중일때의 모델
    private var onboardingModel : InaviRouteModel?
    
    func setonboardingModel(input : InaviRouteModel){
        self.onboardingModel = input
    }
    
    func getonboardingModel() -> InaviRouteModel {
        guard let model = self.onboardingModel else {
            return InaviRouteModel(json: JSON(""))
        }
        return model
    }
    
    
    func getonboardingcoords() -> [[INVLatLng]] {
        var invlatlng : [[INVLatLng]] = []
        
        
        guard let model = self.onboardingModel else {
            return []
        }
        //모델에서 path object array를 먼저 뽑아온다
        for path in model.path {
            //path object에서 뽑아올 coords를 담을 어레이
            var temp : [INVLatLng] = []
            for coord in path.coords {
                //유효성 검사후 배열에 삽입
                if let x = coord.x, let y = coord.y  {
                   
                    temp.append(INVLatLng(lat: y, lng: x))
                }
            }
            //이중 배열에 추가
            invlatlng.append(temp)
        }
        
        return invlatlng
    }
    
    
    
    
}
