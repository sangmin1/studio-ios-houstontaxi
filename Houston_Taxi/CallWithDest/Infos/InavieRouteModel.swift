//
//  InavieRouteModel.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/07.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import SwiftyJSON


/**
 아이나비 경로 탐색 모델
 */
class InaviRouteModel: NSObject {
    
    
    
    required init(json : JSON){
        
        spend_time = json["route"]["data"]["spend_time"].doubleValue
        distance = json["route"]["data"]["distance"].doubleValue
        
        
        
        header = Header(json : json["header"])
        
        
        for path in json["route"]["data"]["paths"].arrayValue {
            self.path.append(Path(json : path))
        }
    }
    
    
    var spend_time : Double?
    var distance : Double?
    var path : [Path] = []
    var header : Header?
    
    internal class Path : NSObject {
        required init(json : JSON){
            for coords in json["coords"].arrayValue {
                self.coords.append(Coords(json: coords))
            }
        }
        
        
        var coords : [Coords] = []
    }
    
    internal class Coords : NSObject {
        
        required init(json : JSON){
            x = json["x"].doubleValue
            y = json["y"].doubleValue
        }
        
        var x : Double?
        var y : Double?
        
        
        
    }
    
    internal class Header : NSObject {
        
        required init(json : JSON){
            isSuccessful = json["isSuccessful"].boolValue
            resultCode = json["resultCode"].intValue
            resultMessage = json["resultMessage"].stringValue
        }
        
        var isSuccessful : Bool?
        var resultCode : Int?
        var resultMessage : String?
        
    }
    
    
    
}


