//
//  CustomDestMarker.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/07.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit

class CustomDestMarker: UIView  {
    
    
    private var image = UIImageView()
    
    private var label = PaddingLabel()
    
    
    init(frame : CGRect, min : Int){
        super.init(frame: frame)
        makeimage()
        makelabel()
        let attributedString = NSMutableAttributedString(string: "약 \(min)분 예상", attributes: [
          .font: UIFont(name: "InaviRixGoM", size: 14.0)!,
          .foregroundColor: UIColor(white: 18.0 / 255.0, alpha: 1.0)
        ])
        
        let range = NSString(string: "약 \(min)분 예상").range(of: "\(min)")
        
        attributedString.addAttributes([
          .font: UIFont(name: "InaviRixGoB", size: 17.0)!,
          .foregroundColor: UIColor(red: 226.0 / 255.0, green: 83.0 / 255.0, blue: 33.0 / 255.0, alpha: 1.0)
        ], range: range)
        
        label.attributedText = attributedString
    }
    
    required init?(coder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    
    
    
}
extension CustomDestMarker {
    private func makeimage(){
        self.addSubview(image)
        image.translatesAutoresizingMaskIntoConstraints = false
        image.topAnchor.constraint(equalTo: self.topAnchor, constant: 0).isActive = true
        image.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 0).isActive = true
        image.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: 0).isActive = true
        image.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: 0).isActive = true
        image.imagewithinset(leftcap: 0.5, image: UIImage(named: "mapPinArrMin")!)
    }
    private func makelabel(){
        self.addSubview(label)
        label.translatesAutoresizingMaskIntoConstraints = false
        label.topAnchor.constraint(equalTo: self.topAnchor, constant: 4).isActive = true
        label.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: 46).isActive = true
        label.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: -8).isActive = true
        label.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: -12).isActive = true
//        let attributedString = NSMutableAttributedString(string: "약 60분 예상", attributes: [
//          .font: UIFont(name: "InaviRixGoM", size: 14.0)!,
//          .foregroundColor: UIColor(white: 18.0 / 255.0, alpha: 1.0)
//        ])
//        attributedString.addAttributes([
//          .font: UIFont(name: "InaviRixGoB", size: 17.0)!,
//          .foregroundColor: UIColor(red: 226.0 / 255.0, green: 83.0 / 255.0, blue: 33.0 / 255.0, alpha: 1.0)
//        ], range: NSRange(location: 2, length: 2))
//
//        label.attributedText = attributedString
        label.adjustsFontSizeToFitWidth = true
        label.textAlignment = .center
        
        
    }
    
    
    
    
}
