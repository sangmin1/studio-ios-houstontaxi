//
//  CallWithDestPresenter.swift
//  Houston_Taxi
//
//  Created by sangmin han on 2020/08/05.
//  Copyright © 2020 sangmin han. All rights reserved.
//

import Foundation
import UIKit
import iNaviMaps
import RxSwift



class CallWithDestPresenter: NSObject {
    
    weak var delegate : CallWithDestContract?
    weak var viewcontroller : CallWithDestViewController?
    
    private var departname : String = ""
    private var destname : String = ""
    private var startx : Double = 0
    private var starty : Double = 0
    private var endx : Double = 0
    private var endy : Double = 0
    
    
    
    
    
    
    
    init(delegate : CallWithDestContract, viewcontroller : CallWithDestViewController){
        self.delegate = delegate
        self.viewcontroller = viewcontroller
        
    }
    

    
    /**
     레포에서 부터 좌표를 가져와 INVRouteLink 객체 배열을 만들어서 뷰 컨트롤러에 넘겨주는 함수
     경로를 그리는데 필수인 함수
     */
    func setrouteOnMap() -> [INVRouteLink] {
    
        var links : [INVRouteLink] = []
        for coord in InaviRouteRepo.shared.getcoords() {
            let link = INVRouteLink(coords: coord)
            link.lineColor = .rgb(red: 10, green: 185, blue: 180, alpha: 1)
            link.strokeColor = .rgb(red: 10, green: 185, blue: 180, alpha: 1)
            links.append(link)
        }
        return links
    }
    
    
    /**
     레포에서 소요시간을 가져와 뷰컨트롤에 전달해 주는 함수
     */
    func getspendtime() -> Int {
        guard let time = InaviRouteRepo.shared.getmodel().spend_time else {
            return 5
        }
        return Int(time / 60)
    }
    /**
      예상 거리 반환 함수
     */
    func getdistance() -> Double {
        guard let distance = InaviRouteRepo.shared.getmodel().distance else {
            return 0
        }
        return distance / 1000
    }
    
    
    /**
     배차에 관한 정보들을 셋하는 함수
     */
    func setInfos(departname : String , destname : String ,startx: Double, starty: Double, endx: Double, endy: Double){
        self.departname = departname
        self.destname = destname
        self.startx = startx
        self.starty = starty
        self.endx = endx
        self.endy = endy
    }
    
    
    
    func getallocinfos() -> [String : Any]{
        return ["departname" : departname ,"destname" : destname, "startx" : startx, "starty" : starty, "endx" : endx, "endy" : endy ]
    }
    
    
    
    
    
    
    
    
    
    
}

